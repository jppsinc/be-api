# be-api

主に施設管理サイト用のAPIです。

## 概要

ホテル管理画面のレポジトリになります。

## レポジトリ構成

```text
.
├── src
    ├── helper
    │   └── env.go
    └── modules
        ├── booking                 予約管理
        ├── cancellationpolicy      施設設定 > 各種デフォルト設定 > キャンセルポリシー設定
        ├── childsetting            施設設定 > 各種デフォルト設定 > 小人ランク設定
        ├── cognito                 cognito関連
        ├── cpachangeinfo           Google Hotel Ads レポート > CAP変更率
        ├── gallery                 施設設定 > イメージギャラリー
        ├── hotel                   施設設定 > 基本情報設定
        ├── hoteldefaultsetting     施設設定 > 各種デフォルト設定 > デフォルト設定
        ├── inquery                 お問合せ
        ├── inventory               販売設定 > 在庫設定
        ├── lincoln                 TLリンカーン関連
        ├── option                  商品設定 > オプション設定
        ├── passwordresetinquiry    画面右上のユーザー詳細 
        ├── plan                    商品設定 > プラン設定 
        ├── plangroup               商品設定 > プラングループ設定
        ├── planprice               
        ├── portalSiteSpecialCode   商品設定 > プラン設定 > プラン詳細
        ├── portalSiteTag           商品設定 > 客室タイプ設定 > 客室タイプ詳細
        ├── postcancellationpolicy  施設設定 > 各種デフォルト設定 > キャンセルポリシー設定
        ├── price                   販売設定 > 料金設定
        ├── questionnaires          商品設定 > 予約質問設定
        ├── rankprice               販売設定 > 料金ランク設定
        ├── raterank                販売設定 > 料金設定 > 料金ランク
        ├── report                  レポート
        ├── room                    商品設定 > 客室タイプ設定
        ├── roomcategory            商品設定 > 客室カテゴリー設定
        ├── salesdefaultsetting     施設設定 > 各種デフォルト設定 > 販売デフォルト設定
        ├── sitecontrollerauth      サイコン関連
        ├── specialcode             商品設定 > プロモーションコード管理
        ├── tax                     施設設定 > 各種デフォルト設定 > 各種税設定
        └── temairazu               手間いらず関連
```

## ローカルからのbuild & deploy

### build

```bash
make
```

### deploy

```bash
sam deploy --config-env {env}
```

## AWSでのbuild & deploy

使用しているCI/CD: codebuild
該当のプロジェクト: be-admin-{env}-codebuild
