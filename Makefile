
.PHONY: all

# Original make file
all: build

build:
# COGNITO done 
	GOOS=linux GOARCH=amd64 $(MAKE) cog_presignup
	GOOS=linux GOARCH=amd64 $(MAKE) cog_gethotelid
	GOOS=linux GOARCH=amd64 $(MAKE) cog_resetpassword
	GOOS=linux GOARCH=amd64 $(MAKE) cog_signin
	GOOS=linux GOARCH=amd64 $(MAKE) cog_presignin
	GOOS=linux GOARCH=amd64 $(MAKE) cog_refreshtoken
	GOOS=linux GOARCH=amd64 $(MAKE) cog_signincheck  
	GOOS=linux GOARCH=amd64 $(MAKE) cog_show
	GOOS=linux GOARCH=amd64 $(MAKE) cog_forgotpassword
	GOOS=linux GOARCH=amd64 $(MAKE) cog_setforgotpassword


# # # rate rank done
	GOOS=linux GOARCH=amd64 $(MAKE) rr_index
	GOOS=linux GOARCH=amd64 $(MAKE) rr_show
	GOOS=linux GOARCH=amd64 $(MAKE) rr_update
	GOOS=linux GOARCH=amd64 $(MAKE) rr_delete
	GOOS=linux GOARCH=amd64 $(MAKE) rr_create
	GOOS=linux GOARCH=amd64 $(MAKE) rr_order

# # # plan group done
	GOOS=linux GOARCH=amd64 $(MAKE) pg_index
	GOOS=linux GOARCH=amd64 $(MAKE) pg_show
	GOOS=linux GOARCH=amd64 $(MAKE) pg_update
	GOOS=linux GOARCH=amd64 $(MAKE) pg_delete
	GOOS=linux GOARCH=amd64 $(MAKE) pg_create
	GOOS=linux GOARCH=amd64 $(MAKE) pg_order
	GOOS=linux GOARCH=amd64 $(MAKE) pg_planlist

# # # special code done
	GOOS=linux GOARCH=amd64 $(MAKE) sc_index
	GOOS=linux GOARCH=amd64 $(MAKE) sc_show
	GOOS=linux GOARCH=amd64 $(MAKE) sc_update
	GOOS=linux GOARCH=amd64 $(MAKE) sc_delete
	GOOS=linux GOARCH=amd64 $(MAKE) sc_create
	GOOS=linux GOARCH=amd64 $(MAKE) sc_order

# # # room category done
	GOOS=linux GOARCH=amd64 $(MAKE) rc_index
	GOOS=linux GOARCH=amd64 $(MAKE) rc_show
	GOOS=linux GOARCH=amd64 $(MAKE) rc_update
	GOOS=linux GOARCH=amd64 $(MAKE) rc_delete
	GOOS=linux GOARCH=amd64 $(MAKE) rc_create
	GOOS=linux GOARCH=amd64 $(MAKE) rc_order
	GOOS=linux GOARCH=amd64 $(MAKE) rc_roomlist

# # # options done
	GOOS=linux GOARCH=amd64 $(MAKE) op_index
	GOOS=linux GOARCH=amd64 $(MAKE) op_show
	GOOS=linux GOARCH=amd64 $(MAKE) op_update
	GOOS=linux GOARCH=amd64 $(MAKE) op_delete
	GOOS=linux GOARCH=amd64 $(MAKE) op_create
	GOOS=linux GOARCH=amd64 $(MAKE) op_order
	GOOS=linux GOARCH=amd64 $(MAKE) op_planlist

# # # sitecontrollerauth done
	GOOS=linux GOARCH=amd64 $(MAKE) sca_signin
	GOOS=linux GOARCH=amd64 $(MAKE) sca_show

# hotel done
	GOOS=linux GOARCH=amd64 $(MAKE) htl_show
	GOOS=linux GOARCH=amd64 $(MAKE) htl_update

# # # Inquery done
	GOOS=linux GOARCH=amd64 $(MAKE) in_index
	GOOS=linux GOARCH=amd64 $(MAKE) in_show
	GOOS=linux GOARCH=amd64 $(MAKE) in_update
	GOOS=linux GOARCH=amd64 $(MAKE) in_delete
	GOOS=linux GOARCH=amd64 $(MAKE) in_create

# # # PasswordResetInquiry done
	GOOS=linux GOARCH=amd64 $(MAKE) prin_create

# hotel defaultsetting done 
	GOOS=linux GOARCH=amd64 $(MAKE) hds_index
	GOOS=linux GOARCH=amd64 $(MAKE) hds_defaultsearchsetting
	GOOS=linux GOARCH=amd64 $(MAKE) hds_grouphotellist
	GOOS=linux GOARCH=amd64 $(MAKE) hds_update

# # child setting done
	GOOS=linux GOARCH=amd64 $(MAKE) cs_index
	GOOS=linux GOARCH=amd64 $(MAKE) cs_default_cs
	GOOS=linux GOARCH=amd64 $(MAKE) cs_show
	GOOS=linux GOARCH=amd64 $(MAKE) cs_update
	GOOS=linux GOARCH=amd64 $(MAKE) cs_delete
	GOOS=linux GOARCH=amd64 $(MAKE) cs_create

# # salesdefaultsetting done
	GOOS=linux GOARCH=amd64 $(MAKE) sds_index
	GOOS=linux GOARCH=amd64 $(MAKE) sds_update
	GOOS=linux GOARCH=amd64 $(MAKE) sds_create

# # cpa done
	GOOS=linux GOARCH=amd64 $(MAKE) cpa_show
	GOOS=linux GOARCH=amd64 $(MAKE) cpa_modifiedslack
	GOOS=linux GOARCH=amd64 $(MAKE) cpa_update
	GOOS=linux GOARCH=amd64 $(MAKE) cpa_create

# booking not done
	GOOS=linux GOARCH=amd64 $(MAKE) b_index
	GOOS=linux GOARCH=amd64 $(MAKE) b_salescount
	GOOS=linux GOARCH=amd64 $(MAKE) b_csv
	GOOS=linux GOARCH=amd64 $(MAKE) b_cancel
	GOOS=linux GOARCH=amd64 $(MAKE) b_show
	GOOS=linux GOARCH=amd64 $(MAKE) b_update
	GOOS=linux GOARCH=amd64 $(MAKE) b_delete
	GOOS=linux GOARCH=amd64 $(MAKE) b_confemailresend

# cancellationsetting done
	GOOS=linux GOARCH=amd64 $(MAKE) can_index
	GOOS=linux GOARCH=amd64 $(MAKE) can_show
	GOOS=linux GOARCH=amd64 $(MAKE) can_update
	GOOS=linux GOARCH=amd64 $(MAKE) can_delete
	GOOS=linux GOARCH=amd64 $(MAKE) can_create

# gallery
	GOOS=linux GOARCH=amd64 $(MAKE) gal_index
	GOOS=linux GOARCH=amd64 $(MAKE) gal_show
	GOOS=linux GOARCH=amd64 $(MAKE) gal_update
	GOOS=linux GOARCH=amd64 $(MAKE) gal_delete
	GOOS=linux GOARCH=amd64 $(MAKE) gal_deleteall
	GOOS=linux GOARCH=amd64 $(MAKE) gal_create
	GOOS=linux GOARCH=amd64 $(MAKE) gal_share
	GOOS=linux GOARCH=amd64 $(MAKE) gal_limit

# # plan done
	GOOS=linux GOARCH=amd64 $(MAKE) p_index
	GOOS=linux GOARCH=amd64 $(MAKE) p_show
	GOOS=linux GOARCH=amd64 $(MAKE) p_update
	GOOS=linux GOARCH=amd64 $(MAKE) p_order
	GOOS=linux GOARCH=amd64 $(MAKE) p_delete
	GOOS=linux GOARCH=amd64 $(MAKE) p_create

# postcancellationpolicy done 
	GOOS=linux GOARCH=amd64 $(MAKE) pcs_index
	GOOS=linux GOARCH=amd64 $(MAKE) pcs_bankdetail
	GOOS=linux GOARCH=amd64 $(MAKE) pcs_update
	GOOS=linux GOARCH=amd64 $(MAKE) pcs_create

# # questionnaires
	GOOS=linux GOARCH=amd64 $(MAKE) q_index
	GOOS=linux GOARCH=amd64 $(MAKE) q_order
	GOOS=linux GOARCH=amd64 $(MAKE) q_show
	GOOS=linux GOARCH=amd64 $(MAKE) q_update
	GOOS=linux GOARCH=amd64 $(MAKE) q_delete
	GOOS=linux GOARCH=amd64 $(MAKE) q_create

# room done
	GOOS=linux GOARCH=amd64 $(MAKE) rm_index
	GOOS=linux GOARCH=amd64 $(MAKE) rm_show
	GOOS=linux GOARCH=amd64 $(MAKE) rm_update
	GOOS=linux GOARCH=amd64 $(MAKE) rm_delete
	GOOS=linux GOARCH=amd64 $(MAKE) rm_create
	GOOS=linux GOARCH=amd64 $(MAKE) rm_order

#tax
	GOOS=linux GOARCH=amd64 $(MAKE) tx_index
	GOOS=linux GOARCH=amd64 $(MAKE) tx_update
	GOOS=linux GOARCH=amd64 $(MAKE) tx_create

# # reports
	GOOS=linux GOARCH=amd64 $(MAKE) r_index 
	GOOS=linux GOARCH=amd64 $(MAKE) r_indexcsv
	GOOS=linux GOARCH=amd64 $(MAKE) r_gha 
	GOOS=linux GOARCH=amd64 $(MAKE) r_ghacsv
	GOOS=linux GOARCH=amd64 $(MAKE) r_billing

# # temairazu signin
	GOOS=linux GOARCH=amd64 $(MAKE) tm_signin

# # planprice exist
	GOOS=linux GOARCH=amd64 $(MAKE) pp_exist
	GOOS=linux GOARCH=amd64 $(MAKE) pp_exist_show  

# # inventory done
	GOOS=linux GOARCH=amd64 $(MAKE) inv_index
	GOOS=linux GOARCH=amd64 $(MAKE) inv_update
	GOOS=linux GOARCH=amd64 $(MAKE) bulk_inv_update

# price done
	GOOS=linux GOARCH=amd64 $(MAKE) pr_index
	GOOS=linux GOARCH=amd64 $(MAKE) pr_indexfast
	GOOS=linux GOARCH=amd64 $(MAKE) pr_update
	GOOS=linux GOARCH=amd64 $(MAKE) bulk_price_update
	GOOS=linux GOARCH=amd64 $(MAKE) cal_price_update

# tl attributes
	GOOS=linux GOARCH=amd64 $(MAKE) tl_attributes
	GOOS=linux GOARCH=amd64 $(MAKE) tl_updatetime

# # rateprice done
	GOOS=linux GOARCH=amd64 $(MAKE) rp_index
	GOOS=linux GOARCH=amd64 $(MAKE) rp_rankwithprice
	GOOS=linux GOARCH=amd64 $(MAKE) rp_update
	GOOS=linux GOARCH=amd64 $(MAKE) rp_boundary_get
	GOOS=linux GOARCH=amd64 $(MAKE) rp_boundary_set

# ------------- portal_site_info -------------

# portalSiteSpecialCode
	GOOS=linux GOARCH=amd64 $(MAKE) portal_site_special_code_list

# portalSiteTag
	GOOS=linux GOARCH=amd64 $(MAKE) portal_site_tag_list

# rateprice
rp_rankwithprice: ./src/modules/rankprice/rankwithprice/main.go
	go build -o ./src/modules/rankprice/rankwithprice/rankwithprice ./src/modules/rankprice/rankwithprice

rp_index: ./src/modules/rankprice/index/main.go
	go build -o ./src/modules/rankprice/index/index ./src/modules/rankprice/index

rp_update: ./src/modules/rankprice/update/main.go
	go build -o ./src/modules/rankprice/update/update ./src/modules/rankprice/update

rp_boundary_set: ./src/modules/rankprice/setboundary/main.go
	go build -o ./src/modules/rankprice/setboundary/setboundary ./src/modules/rankprice/setboundary

rp_boundary_get: ./src/modules/rankprice/getboundary/main.go
	go build -o ./src/modules/rankprice/getboundary/getboundary ./src/modules/rankprice/getboundary

# tl attributes  
tl_attributes: ./src/modules/lincoln/attributes/main.go
	go build -o ./src/modules/lincoln/attributes/attributes ./src/modules/lincoln/attributes

# tl updatetime  
tl_updatetime: ./src/modules/lincoln/updatetime/main.go
	go build -o ./src/modules/lincoln/updatetime/updatetime ./src/modules/lincoln/updatetime

# price
pr_index: ./src/modules/price/index/main.go
	go build -o ./src/modules/price/index/index ./src/modules/price/index

pr_indexfast: ./src/modules/price/indexfast/main.go
	go build -o ./src/modules/price/indexfast/indexfast ./src/modules/price/indexfast

pr_update: ./src/modules/price/update/main.go
	go build -o ./src/modules/price/update/update ./src/modules/price/update

bulk_price_update: ./src/modules/price/bulkupdate/main.go
	go build -o ./src/modules/price/bulkupdate/bulkupdate ./src/modules/price/bulkupdate

cal_price_update: ./src/modules/price/calupdate/main.go
	go build -o ./src/modules/price/calupdate/calupdate ./src/modules/price/calupdate

# inventory
inv_index: ./src/modules/inventory/index/main.go
	go build -o ./src/modules/inventory/index/index ./src/modules/inventory/index

inv_update: ./src/modules/inventory/update/main.go
	go build -o ./src/modules/inventory/update/update ./src/modules/inventory/update

bulk_inv_update: ./src/modules/inventory/bulkupdate/main.go
	go build -o ./src/modules/inventory/bulkupdate/bulkupdate ./src/modules/inventory/bulkupdate

# reports
r_index: ./src/modules/report/index/main.go
	go build -o ./src/modules/report/index/index ./src/modules/report/index

r_indexcsv: ./src/modules/report/indexcsv/main.go
	go build -o ./src/modules/report/indexcsv/indexcsv ./src/modules/report/indexcsv

r_gha: ./src/modules/report/gha/main.go
	go build -o ./src/modules/report/gha/gha ./src/modules/report/gha

r_ghacsv: ./src/modules/report/ghacsv/main.go
	go build -o ./src/modules/report/ghacsv/ghacsv ./src/modules/report/ghacsv

r_billing: ./src/modules/report/billing/main.go
	go build -o ./src/modules/report/billing/billing ./src/modules/report/billing

# planprice
pp_exist: ./src/modules/planprice/exist/main.go
	go build -o ./src/modules/planprice/exist/exist ./src/modules/planprice/exist

pp_exist_show: ./src/modules/planprice/show/main.go
	go build -o ./src/modules/planprice/show/show ./src/modules/planprice/show


# inventory   
inv_show: ./src/modules/inventory/show/main.go
	go build -o ./src/modules/inventory/show/show ./src/modules/inventory/show

inv_streamplanprice: ./src/modules/inventory/streamplanprice/main.go
	go build -o ./src/modules/inventory/streamplanprice/streamplanprice ./src/modules/inventory/streamplanprice

# temairazu
tm_signin: ./src/modules/temairazu/signin/main.go
	go build -o ./src/modules/temairazu/signin/signin ./src/modules/temairazu/signin

tm_plan: ./src/modules/temairazu/plan/main.go
	go build -o ./src/modules/temairazu/plan/plan ./src/modules/temairazu/plan

tm_inventory: ./src/modules/temairazu/inventory/main.go
	go build -o ./src/modules/temairazu/inventory/inventory ./src/modules/temairazu/inventory

cog_index: ./src/modules/cognito/signincheck/main.go 
	go build -o ./src/modules/cognito/index/index ./src/modules/cognito/index 

cog_gethotelid: ./src/modules/cognito/gethotelid/main.go 
	go build -o ./src/modules/cognito/gethotelid/gethotelid ./src/modules/cognito/gethotelid

cog_presignup: ./src/modules/cognito/presignup/main.go 
	go build -o ./src/modules/cognito/presignup/presignup ./src/modules/cognito/presignup

cog_show: ./src/modules/cognito/show/main.go 
	go build -o ./src/modules/cognito/show/show ./src/modules/cognito/show

cog_signin: ./src/modules/cognito/signin/main.go
	go build -o ./src/modules/cognito/signin/signin ./src/modules/cognito/signin

cog_resetpassword: ./src/modules/cognito/resetpassword/main.go  
	go build -o ./src/modules/cognito/resetpassword/resetpassword ./src/modules/cognito/resetpassword

cog_presignin: ./src/modules/cognito/presignin/main.go   
	go build -o ./src/modules/cognito/presignin/presignin ./src/modules/cognito/presignin

cog_refreshtoken: ./src/modules/cognito/refreshtoken/main.go
	go build -o ./src/modules/cognito/refreshtoken/refreshtoken ./src/modules/cognito/refreshtoken

cog_signincheck: ./src/modules/cognito/signincheck/main.go 
	go build -o ./src/modules/cognito/signincheck/signincheck ./src/modules/cognito/signincheck

cog_disableuser: ./src/modules/cognito/disableuser/main.go 
	go build -o ./src/modules/cognito/disableuser/disableuser ./src/modules/cognito/disableuser

cog_signup: ./src/modules/cognito/signup/main.go
	go build -o ./src/modules/cognito/signup/signup ./src/modules/cognito/signup

cog_forgotpassword: ./src/modules/cognito/forgotpassword/main.go 
	go build -o ./src/modules/cognito/forgotpassword/forgotpassword ./src/modules/cognito/forgotpassword

cog_setforgotpassword: ./src/modules/cognito/setforgotpassword/main.go 
	go build -o ./src/modules/cognito/setforgotpassword/setforgotpassword ./src/modules/cognito/setforgotpassword

cog_confirmsignup: ./src/modules/cognito/confirmsignup/main.go 
	go build -o ./src/modules/cognito/confirmsignup/confirmsignup ./src/modules/cognito/confirmsignup


# sitecontrollerauth
sca_signin: ./src/modules/sitecontrollerauth/signin/main.go
	go build -o ./src/modules/sitecontrollerauth/signin/signin ./src/modules/sitecontrollerauth/signin

sca_show: ./src/modules/sitecontrollerauth/show/main.go
	go build -o ./src/modules/sitecontrollerauth/show/show ./src/modules/sitecontrollerauth/show

# raterank
rr_index: ./src/modules/raterank/index/main.go
	go build -o ./src/modules/raterank/index/index ./src/modules/raterank/index

rr_show: ./src/modules/raterank/show/main.go
	go build -o ./src/modules/raterank/show/show ./src/modules/raterank/show

rr_create: ./src/modules/raterank/create/main.go
	go build -o ./src/modules/raterank/create/create ./src/modules/raterank/create

rr_order: ./src/modules/raterank/order/main.go
	go build -o ./src/modules/raterank/order/order ./src/modules/raterank/order

rr_update: ./src/modules/raterank/update/main.go
	go build -o ./src/modules/raterank/update/update ./src/modules/raterank/update

rr_delete: ./src/modules/raterank/delete/main.go
	go build -o ./src/modules/raterank/delete/delete ./src/modules/raterank/delete


# special code
sc_index: ./src/modules/specialcode/index/main.go
	go build -o ./src/modules/specialcode/index/index ./src/modules/specialcode/index

sc_show: ./src/modules/specialcode/show/main.go
	go build -o ./src/modules/specialcode/show/show ./src/modules/specialcode/show

sc_create: ./src/modules/specialcode/create/main.go
	go build -o ./src/modules/specialcode/create/create ./src/modules/specialcode/create

sc_order: ./src/modules/specialcode/order/main.go
	go build -o ./src/modules/specialcode/order/order ./src/modules/specialcode/order

sc_update: ./src/modules/specialcode/update/main.go
	go build -o ./src/modules/specialcode/update/update ./src/modules/specialcode/update

sc_delete: ./src/modules/specialcode/delete/main.go
	go build -o ./src/modules/specialcode/delete/delete ./src/modules/specialcode/delete

# plan group
pg_index: ./src/modules/plangroup/index/main.go
	go build -o ./src/modules/plangroup/index/index ./src/modules/plangroup/index

pg_show: ./src/modules/plangroup/show/main.go
	go build -o ./src/modules/plangroup/show/show ./src/modules/plangroup/show

pg_create: ./src/modules/plangroup/create/main.go
	go build -o ./src/modules/plangroup/create/create ./src/modules/plangroup/create

pg_update: ./src/modules/plangroup/update/main.go
	go build -o ./src/modules/plangroup/update/update ./src/modules/plangroup/update

pg_delete: ./src/modules/plangroup/delete/main.go
	go build -o ./src/modules/plangroup/delete/delete ./src/modules/plangroup/delete

pg_planlist: ./src/modules/plangroup/planlist/main.go
	go build -o ./src/modules/plangroup/planlist/planlist ./src/modules/plangroup/planlist

pg_order: ./src/modules/plangroup/order/main.go
	go build -o ./src/modules/plangroup/order/order ./src/modules/plangroup/order

# options group
op_index: ./src/modules/option/index/main.go
	go build -o ./src/modules/option/index/index ./src/modules/option/index

op_show: ./src/modules/option/show/main.go
	go build -o ./src/modules/option/show/show ./src/modules/option/show

op_create: ./src/modules/option/create/main.go
	go build -o ./src/modules/option/create/create ./src/modules/option/create

op_update: ./src/modules/option/update/main.go
	go build -o ./src/modules/option/update/update ./src/modules/option/update

op_delete: ./src/modules/option/delete/main.go
	go build -o ./src/modules/option/delete/delete ./src/modules/option/delete

op_order: ./src/modules/option/order/main.go
	go build -o ./src/modules/option/order/order ./src/modules/option/order

op_planlist: ./src/modules/option/planlist/main.go
	go build -o ./src/modules/option/planlist/planlist ./src/modules/option/planlist

# plan group
rc_index: ./src/modules/roomcategory/index/main.go
	go build -o ./src/modules/roomcategory/index/index ./src/modules/roomcategory/index

rc_show: ./src/modules/roomcategory/show/main.go
	go build -o ./src/modules/roomcategory/show/show ./src/modules/roomcategory/show

rc_create: ./src/modules/roomcategory/create/main.go
	go build -o ./src/modules/roomcategory/create/create ./src/modules/roomcategory/create

rc_order: ./src/modules/roomcategory/order/main.go
	go build -o ./src/modules/roomcategory/order/order ./src/modules/roomcategory/order

rc_update: ./src/modules/roomcategory/update/main.go
	go build -o ./src/modules/roomcategory/update/update ./src/modules/roomcategory/update

rc_delete: ./src/modules/roomcategory/delete/main.go
	go build -o ./src/modules/roomcategory/delete/delete ./src/modules/roomcategory/delete

rc_roomlist: ./src/modules/roomcategory/rcroomlist/main.go
	go build -o ./src/modules/roomcategory/rcroomlist/rcroomlist ./src/modules/roomcategory/rcroomlist

# hotel

htl_show: ./src/modules/hotel/show/main.go
	go build -o ./src/modules/hotel/show/show ./src/modules/hotel/show

htl_update: ./src/modules/hotel/update/main.go
	go build -o ./src/modules/hotel/update/update ./src/modules/hotel/update

# site static info
ssi_ssi: ./src/modules/sitestaticinfo/sitestaticinfo/main.go
	go build -o ./src/modules/sitestaticinfo/sitestaticinfo/sitestaticinfo ./src/modules/sitestaticinfo/sitestaticinfo

# inquery
in_index: ./src/modules/inquery/index/main.go
	go build -o ./src/modules/inquery/index/index ./src/modules/inquery/index

in_show: ./src/modules/inquery/show/main.go
	go build -o ./src/modules/inquery/show/show ./src/modules/inquery/show

in_create: ./src/modules/inquery/create/main.go
	go build -o ./src/modules/inquery/create/create ./src/modules/inquery/create

in_update: ./src/modules/inquery/update/main.go
	go build -o ./src/modules/inquery/update/update ./src/modules/inquery/update

in_delete: ./src/modules/inquery/delete/main.go
	go build -o ./src/modules/inquery/delete/delete ./src/modules/inquery/delete


# password reset inquery
prin_create: ./src/modules/passwordresetinquiry/create/main.go
	go build -o ./src/modules/passwordresetinquiry/create/create ./src/modules/passwordresetinquiry/create

# consumption tax
ct_index: ./src/modules/consumptiontax/index/main.go
	go build -o ./src/modules/consumptiontax/index/index ./src/modules/consumptiontax/index

ct_create: ./src/modules/consumptiontax/create/main.go
	go build -o ./src/modules/consumptiontax/create/create ./src/modules/consumptiontax/create

ct_update: ./src/modules/consumptiontax/update/main.go
	go build -o ./src/modules/consumptiontax/update/update ./src/modules/consumptiontax/update

# paymentcompany
pc_index: ./src/modules/paymentcompany/index/main.go
	go build -o ./src/modules/paymentcompany/index/index ./src/modules/paymentcompany/index

pc_show: ./src/modules/paymentcompany/show/main.go
	go build -o ./src/modules/paymentcompany/show/show ./src/modules/paymentcompany/show

pc_create: ./src/modules/paymentcompany/create/main.go
	go build -o ./src/modules/paymentcompany/create/create ./src/modules/paymentcompany/create

pc_update: ./src/modules/paymentcompany/update/main.go
	go build -o ./src/modules/paymentcompany/update/update ./src/modules/paymentcompany/update

pc_delete: ./src/modules/paymentcompany/delete/main.go
	go build -o ./src/modules/paymentcompany/delete/delete ./src/modules/paymentcompany/delete


# hoteldefaultsetting 
hds_index: ./src/modules/hoteldefaultsetting/index/main.go
	go build -o ./src/modules/hoteldefaultsetting/index/index ./src/modules/hoteldefaultsetting/index

hds_defaultsearchsetting: ./src/modules/hoteldefaultsetting/defaultsearchsetting/main.go
	go build -o ./src/modules/hoteldefaultsetting/defaultsearchsetting/defaultsearchsetting ./src/modules/hoteldefaultsetting/defaultsearchsetting

hds_grouphotellist: ./src/modules/hoteldefaultsetting/grouphotellist/main.go
	go build -o ./src/modules/hoteldefaultsetting/grouphotellist/show ./src/modules/hoteldefaultsetting/grouphotellist

hds_update: ./src/modules/hoteldefaultsetting/update/main.go
	go build -o ./src/modules/hoteldefaultsetting/update/update ./src/modules/hoteldefaultsetting/update


# childsetting 
cs_index: ./src/modules/childsetting/index/main.go
	go build -o ./src/modules/childsetting/index/index ./src/modules/childsetting/index

cs_default_cs: ./src/modules/childsetting/defaultcs/main.go
	go build -o ./src/modules/childsetting/defaultcs/defaultcs ./src/modules/childsetting/defaultcs

cs_show: ./src/modules/childsetting/show/main.go
	go build -o ./src/modules/childsetting/show/show ./src/modules/childsetting/show

cs_create: ./src/modules/childsetting/create/main.go
	go build -o ./src/modules/childsetting/create/create ./src/modules/childsetting/create

cs_update: ./src/modules/childsetting/update/main.go
	go build -o ./src/modules/childsetting/update/update ./src/modules/childsetting/update

cs_delete: ./src/modules/childsetting/delete/main.go
	go build -o ./src/modules/childsetting/delete/delete ./src/modules/childsetting/delete


# salesdefaultsetting
sds_index: ./src/modules/salesdefaultsetting/index/main.go
	go build -o ./src/modules/salesdefaultsetting/index/index ./src/modules/salesdefaultsetting/index

sds_create: ./src/modules/salesdefaultsetting/create/main.go
	go build -o ./src/modules/salesdefaultsetting/create/create ./src/modules/salesdefaultsetting/create

sds_update: ./src/modules/salesdefaultsetting/update/main.go
	go build -o ./src/modules/salesdefaultsetting/update/update ./src/modules/salesdefaultsetting/update

# salesdefaultsetting 
cpa_show: ./src/modules/cpachangeinfo/show/main.go
	go build -o ./src/modules/cpachangeinfo/show/show ./src/modules/cpachangeinfo/show

cpa_create: ./src/modules/cpachangeinfo/create/main.go
	go build -o ./src/modules/cpachangeinfo/create/create ./src/modules/cpachangeinfo/create

cpa_update: ./src/modules/cpachangeinfo/update/main.go
	go build -o ./src/modules/cpachangeinfo/update/update ./src/modules/cpachangeinfo/update

cpa_modifiedslack: ./src/modules/cpachangeinfo/cpamodifiedslack/main.go
	go build -o ./src/modules/cpachangeinfo/cpamodifiedslack/cpamodifiedslack ./src/modules/cpachangeinfo/cpamodifiedslack

# booking 
b_index: ./src/modules/booking/index/main.go
	go build -o ./src/modules/booking/index/index ./src/modules/booking/index

b_salescount: ./src/modules/booking/salescount/main.go
	go build -o ./src/modules/booking/salescount/salescount ./src/modules/booking/salescount

b_csv: ./src/modules/booking/csv/main.go
	go build -o ./src/modules/booking/csv/csv ./src/modules/booking/csv

b_show: ./src/modules/booking/show/main.go
	go build -o ./src/modules/booking/show/show ./src/modules/booking/show

b_create: ./src/modules/booking/reserve/main.go
	go build -o ./src/modules/booking/reserve/reserve ./src/modules/booking/reserve

b_cancel: ./src/modules/booking/cancel/main.go
	go build -o ./src/modules/booking/cancel/cancel ./src/modules/booking/cancel

b_update: ./src/modules/booking/update/main.go
	go build -o ./src/modules/booking/update/update ./src/modules/booking/update

b_book: ./src/modules/booking/update/main.go
	go build -o ./src/modules/booking/update/update ./src/modules/booking/update

b_delete: ./src/modules/booking/delete/main.go 
	go build -o ./src/modules/booking/delete/delete ./src/modules/booking/delete

b_confemailresend: ./src/modules/booking/confemailresend/main.go 
	go build -o ./src/modules/booking/confemailresend/confemailresend ./src/modules/booking/confemailresend



# cancellationpolicy
can_index: ./src/modules/cancellationpolicy/index/main.go
	go build -o ./src/modules/cancellationpolicy/index/index ./src/modules/cancellationpolicy/index

can_show: ./src/modules/cancellationpolicy/show/main.go
	go build -o ./src/modules/cancellationpolicy/show/show ./src/modules/cancellationpolicy/show

can_create: ./src/modules/cancellationpolicy/create/main.go
	go build -o ./src/modules/cancellationpolicy/create/create ./src/modules/cancellationpolicy/create

can_update: ./src/modules/cancellationpolicy/update/main.go
	go build -o ./src/modules/cancellationpolicy/update/update ./src/modules/cancellationpolicy/update

can_delete: ./src/modules/cancellationpolicy/delete/main.go
	go build -o ./src/modules/cancellationpolicy/delete/delete ./src/modules/cancellationpolicy/delete


# cognito
# cog_index: ./src/modules/cognito/index/main.go
# 	go build -o ./src/modules/cognito/index/index ./src/modules/cognito/index

# cog_show: ./src/modules/cognito/show/main.go
# 	go build -o ./src/modules/cognito/show/show ./src/modules/cognito/show

# cog_create: ./src/modules/cognito/create/main.go
# 	go build -o ./src/modules/cognito/create/create ./src/modules/cognito/create

# cog_update: ./src/modules/cognito/update/main.go
# 	go build -o ./src/modules/cognito/update/update ./src/modules/cognito/update

# cog_delete: ./src/modules/cognito/delete/main.go
# 	go build -o ./src/modules/cognito/delete/delete ./src/modules/cognito/delete


# gallery
gal_index: ./src/modules/gallery/index/main.go
	go build -o ./src/modules/gallery/index/index ./src/modules/gallery/index

gal_show: ./src/modules/gallery/show/main.go
	go build -o ./src/modules/gallery/show/show ./src/modules/gallery/show

gal_create: ./src/modules/gallery/create/main.go
	go build -o ./src/modules/gallery/create/create ./src/modules/gallery/create

gal_share: ./src/modules/gallery/share/main.go
	go build -o ./src/modules/gallery/share/share ./src/modules/gallery/share

gal_limit: ./src/modules/gallery/imagelimit/main.go
	go build -o ./src/modules/gallery/imagelimit/imagelimit ./src/modules/gallery/imagelimit

gal_update: ./src/modules/gallery/update/main.go
	go build -o ./src/modules/gallery/update/update ./src/modules/gallery/update

gal_delete: ./src/modules/gallery/delete/main.go
	go build -o ./src/modules/gallery/delete/delete ./src/modules/gallery/delete

gal_deleteall: ./src/modules/gallery/deleteall/main.go
	go build -o ./src/modules/gallery/deleteall/deleteall ./src/modules/gallery/deleteall


# grouphotel
gh_index: ./src/modules/grouphotel/index/main.go
	go build -o ./src/modules/grouphotel/index/index ./src/modules/grouphotel/index

gh_show: ./src/modules/grouphotel/show/main.go
	go build -o ./src/modules/grouphotel/show/show ./src/modules/grouphotel/show

gh_create: ./src/modules/grouphotel/create/main.go
	go build -o ./src/modules/grouphotel/create/create ./src/modules/grouphotel/create

gh_update: ./src/modules/grouphotel/update/main.go
	go build -o ./src/modules/grouphotel/update/update ./src/modules/grouphotel/update


# hotelgroup
hg_index: ./src/modules/hotelgroup/index/main.go
	go build -o ./src/modules/hotelgroup/index/index ./src/modules/hotelgroup/index

hg_show: ./src/modules/hotelgroup/show/main.go
	go build -o ./src/modules/hotelgroup/show/show ./src/modules/hotelgroup/show

hg_grouplist: ./src/modules/hotelgroup/grouplist/main.go
	go build -o ./src/modules/hotelgroup/grouplist/grouplist ./src/modules/hotelgroup/grouplist    

hg_grouphotellist: ./src/modules/hotelgroup/grouphotellist/main.go
	go build -o ./src/modules/hotelgroup/grouphotellist/grouphotellist ./src/modules/hotelgroup/grouphotellist   

hg_subgrouphotellist: ./src/modules/hotelgroup/subgrouphotellist/main.go
	go build -o ./src/modules/hotelgroup/subgrouphotellist/subgrouphotellist ./src/modules/hotelgroup/subgrouphotellist  

hg_deactivategroupconfirm: ./src/modules/hotelgroup/deactivategroupconfirm/main.go
	go build -o ./src/modules/hotelgroup/deactivategroupconfirm/deactivategroupconfirm ./src/modules/hotelgroup/deactivategroupconfirm

hg_deactivatesubgroupconfirm: ./src/modules/hotelgroup/deactivatesubgroupconfirm/main.go
	go build -o ./src/modules/hotelgroup/deactivatesubgroupconfirm/deactivatesubgroupconfirm ./src/modules/hotelgroup/deactivatesubgroupconfirm

hg_deactivategroup: ./src/modules/hotelgroup/deactivategroup/main.go
	go build -o ./src/modules/hotelgroup/deactivategroup/deactivategroup ./src/modules/hotelgroup/deactivategroup

hg_deactivatesubgroup: ./src/modules/hotelgroup/deactivatesubgroup/main.go
	go build -o ./src/modules/hotelgroup/deactivatesubgroup/deactivatesubgroup ./src/modules/hotelgroup/deactivatesubgroup

hg_subgrouplist: ./src/modules/hotelgroup/subgrouplist/main.go
	go build -o ./src/modules/hotelgroup/subgrouplist/subgrouplist ./src/modules/hotelgroup/subgrouplist

hg_create: ./src/modules/hotelgroup/create/main.go
	go build -o ./src/modules/hotelgroup/create/create ./src/modules/hotelgroup/create

hg_update: ./src/modules/hotelgroup/update/main.go
	go build -o ./src/modules/hotelgroup/update/update ./src/modules/hotelgroup/update

hg_delete: ./src/modules/hotelgroup/delete/main.go
	go build -o ./src/modules/hotelgroup/delete/delete ./src/modules/hotelgroup/delete


# plan
p_index: ./src/modules/plan/index/main.go
	go build -o ./src/modules/plan/index/index ./src/modules/plan/index

p_show: ./src/modules/plan/show/main.go
	go build -o ./src/modules/plan/show/show ./src/modules/plan/show

p_create: ./src/modules/plan/create/main.go
	go build -o ./src/modules/plan/create/create ./src/modules/plan/create

p_update: ./src/modules/plan/update/main.go    
	go build -o ./src/modules/plan/update/update ./src/modules/plan/update

p_order: ./src/modules/plan/order/main.go    
	go build -o ./src/modules/plan/order/order ./src/modules/plan/order

p_delete: ./src/modules/plan/delete/main.go
	go build -o ./src/modules/plan/delete/delete ./src/modules/plan/delete


# postcancellationpolicy 
pcs_index: ./src/modules/postcancellationpolicy/index/main.go
	go build -o ./src/modules/postcancellationpolicy/index/index ./src/modules/postcancellationpolicy/index

pcs_bankdetail: ./src/modules/postcancellationpolicy/bankdetail/main.go
	go build -o ./src/modules/postcancellationpolicy/bankdetail/bankdetail ./src/modules/postcancellationpolicy/bankdetail

pcs_create: ./src/modules/postcancellationpolicy/create/main.go
	go build -o ./src/modules/postcancellationpolicy/create/create ./src/modules/postcancellationpolicy/create

pcs_update: ./src/modules/postcancellationpolicy/update/main.go
	go build -o ./src/modules/postcancellationpolicy/update/update ./src/modules/postcancellationpolicy/update

pcs_delete: ./src/modules/postcancellationpolicy/delete/main.go
	go build -o ./src/modules/postcancellationpolicy/delete/delete ./src/modules/postcancellationpolicy/delete


# questionnaires
q_index: ./src/modules/questionnaires/index/main.go
	go build -o ./src/modules/questionnaires/index/index ./src/modules/questionnaires/index

q_order: ./src/modules/questionnaires/order/main.go
	go build -o ./src/modules/questionnaires/order/order ./src/modules/questionnaires/order

q_show: ./src/modules/questionnaires/show/main.go
	go build -o ./src/modules/questionnaires/show/show ./src/modules/questionnaires/show

q_create: ./src/modules/questionnaires/create/main.go
	go build -o ./src/modules/questionnaires/create/create ./src/modules/questionnaires/create

q_update: ./src/modules/questionnaires/update/main.go
	go build -o ./src/modules/questionnaires/update/update ./src/modules/questionnaires/update

q_delete: ./src/modules/questionnaires/delete/main.go
	go build -o ./src/modules/questionnaires/delete/delete ./src/modules/questionnaires/delete


# room
rm_index: ./src/modules/room/index/main.go
	go build -o ./src/modules/room/index/index ./src/modules/room/index

rm_show: ./src/modules/room/show/main.go
	go build -o ./src/modules/room/show/show ./src/modules/room/show

rm_create: ./src/modules/room/create/main.go
	go build -o ./src/modules/room/create/create ./src/modules/room/create

rm_order: ./src/modules/room/order/main.go
	go build -o ./src/modules/room/order/order ./src/modules/room/order

rm_update: ./src/modules/room/update/main.go
	go build -o ./src/modules/room/update/update ./src/modules/room/update

rm_delete: ./src/modules/room/delete/main.go
	go build -o ./src/modules/room/delete/delete ./src/modules/room/delete


# sitecontroller
stc_index: ./src/modules/sitecontroller/index/main.go
	go build -o ./src/modules/sitecontroller/index/index ./src/modules/sitecontroller/index

stc_show: ./src/modules/sitecontroller/show/main.go
	go build -o ./src/modules/sitecontroller/show/show ./src/modules/sitecontroller/show

stc_create: ./src/modules/sitecontroller/create/main.go
	go build -o ./src/modules/sitecontroller/create/create ./src/modules/sitecontroller/create

stc_update: ./src/modules/sitecontroller/update/main.go
	go build -o ./src/modules/sitecontroller/update/update ./src/modules/sitecontroller/update

stc_delete: ./src/modules/sitecontroller/delete/main.go
	go build -o ./src/modules/sitecontroller/delete/delete ./src/modules/sitecontroller/delete


# tax
tx_index: ./src/modules/tax/index/main.go
	go build -o ./src/modules/tax/index/index ./src/modules/tax/index

tx_show: ./src/modules/tax/show/main.go
	go build -o ./src/modules/tax/show/show ./src/modules/tax/show

tx_create: ./src/modules/tax/create/main.go
	go build -o ./src/modules/tax/create/create ./src/modules/tax/create

tx_update: ./src/modules/tax/update/main.go
	go build -o ./src/modules/tax/update/update ./src/modules/tax/update

tx_delete: ./src/modules/tax/delete/main.go
	go build -o ./src/modules/tax/delete/delete ./src/modules/tax/delete

portal_site_special_code_list: ./src/modules/portalSiteSpecialCode/list/main.go
	go build -o ./src/modules/portalSiteSpecialCode/list/list ./src/modules/portalSiteSpecialCode/list

portal_site_tag_list: ./src/modules/portalSiteTag/list/main.go
	go build -o ./src/modules/portalSiteTag/list/list ./src/modules/portalSiteTag/list


.PHONY: clean
clean: 
# # raterank
	rm -f ./src/modules/raterank/index/index
	rm -f ./src/modules/raterank/show/show
	rm -f ./src/modules/raterank/create/create
	rm -f ./src/modules/raterank/update/update
	rm -f ./src/modules/raterank/delete/delete

# hotel
	rm -f ./src/modules/hotel/index/index
	rm -f ./src/modules/hotel/show/show
	rm -f ./src/modules/hotel/create/create
	rm -f ./src/modules/hotel/update/update
	rm -f ./src/modules/hotel/delete/delete

# childsetting
	rm -f ./src/modules/childsetting/index/index
	rm -f ./src/modules/childsetting/show/show
	rm -f ./src/modules/childsetting/create/create
	rm -f ./src/modules/childsetting/update/update
	rm -f ./src/modules/childsetting/delete/delete

# salesdefaultsetting
	rm -f ./src/modules/salesdefaultsetting/index/index
	rm -f ./src/modules/salesdefaultsetting/show/show
	rm -f ./src/modules/salesdefaultsetting/create/create
	rm -f ./src/modules/salesdefaultsetting/update/update
	rm -f ./src/modules/salesdefaultsetting/delete/delete

# hoteldefaultsetting
	rm -f ./src/modules/hoteldefaultsetting/index/index
	rm -f ./src/modules/hoteldefaultsetting/grouphotellist/grouphotellist
	rm -f ./src/modules/hoteldefaultsetting/create/create
	rm -f ./src/modules/hoteldefaultsetting/update/update
	rm -f ./src/modules/hoteldefaultsetting/defaultsearchsetting/defaultsearchsetting

# paymentcompany
	rm -f ./src/modules/paymentcompany/index/index
	rm -f ./src/modules/paymentcompany/show/show
	rm -f ./src/modules/paymentcompany/create/create
	rm -f ./src/modules/paymentcompany/update/update
	rm -f ./src/modules/paymentcompany/delete/delete

# inquery
	rm -f ./src/modules/inquery/index/index
	rm -f ./src/modules/inquery/show/show
	rm -f ./src/modules/inquery/create/create
	rm -f ./src/modules/inquery/update/update
	rm -f ./src/modules/inquery/delete/delete

# booking
	rm -f ./src/modules/booking/index/index
	rm -f ./src/modules/booking/show/show
	rm -f ./src/modules/booking/book/book
	rm -f ./src/modules/booking/reserve/reserve
	rm -f ./src/modules/booking/update/update
	rm -f ./src/modules/booking/delete/delete

# cancellationpolicy
	rm -f ./src/modules/cancellationpolicy/index/index
	rm -f ./src/modules/cancellationpolicy/show/show
	rm -f ./src/modules/cancellationpolicy/create/create
	rm -f ./src/modules/cancellationpolicy/update/update
	rm -f ./src/modules/cancellationpolicy/delete/delete

# cognito
	rm -f ./src/modules/cognito/index/index
	rm -f ./src/modules/cognito/show/show 
	rm -f ./src/modules/cognito/refreshtoken/refreshtoken
	rm -f ./src/modules/cognito/signup/signup
	rm -f ./src/modules/cognito/presignup/presignup
	rm -f ./src/modules/cognito/presignin/presignin   
	rm -f ./src/modules/cognito/signin/signin
	rm -f ./src/modules/cognito/resetpassword/resetpassword
	rm -f ./src/modules/cognito/signincheck/signincheck
	rm -f ./src/modules/cognito/confirmsignup/confirmsignup 
	rm -f ./src/modules/cognito/forgotpassword/forgotpassword 
	rm -f ./src/modules/cognito/forgotpassword/setforgotpassword 

# gallery
	rm -f ./src/modules/gallery/index/index
	rm -f ./src/modules/gallery/show/show
	rm -f ./src/modules/gallery/share/share
	rm -f ./src/modules/gallery/create/create
	rm -f ./src/modules/gallery/update/update
	rm -f ./src/modules/gallery/delete/delete

# grouphotel
	rm -f ./src/modules/grouphotel/index/index
	rm -f ./src/modules/grouphotel/show/show
	rm -f ./src/modules/grouphotel/create/create
	rm -f ./src/modules/grouphotel/update/update
	rm -f ./src/modules/grouphotel/delete/delete


# hotelgroup
	rm -f ./src/modules/hotelgroup/index/index 
	rm -f ./src/modules/hotelgroup/show/show
	rm -f ./src/modules/hotelgroup/grouplist/grouplist
	rm -f ./src/modules/hotelgroup/grouphotellist/grouphotellist
	rm -f ./src/modules/hotelgroup/deactivategroup/deactivategroup
	rm -f ./src/modules/hotelgroup/deactivatesubgroupconfirm/deactivatesubgroupconfirm
	rm -f ./src/modules/hotelgroup/deactivategroupconfirm/deactivategroupconfirm
	rm -f ./src/modules/hotelgroup/deactivatesubgroup/deactivatesubgroup  
	rm -f ./src/modules/hotelgroup/subgrouplist/subgrouplist  
	rm -f ./src/modules/hotelgroup/subgrouphotellist/subgrouphotellist  
	rm -f ./src/modules/hotelgroup/create/create
	rm -f ./src/modules/hotelgroup/update/update
	rm -f ./src/modules/hotelgroup/delete/delete

# planprice
	rm -f ./src/modules/planprice/create/create

# plan
	rm -f ./src/modules/plan/index/index
	rm -f ./src/modules/plan/show/show
	rm -f ./src/modules/plan/create/v
	rm -f ./src/modules/plan/update/update
	rm -f ./src/modules/plan/delete/delete

# postcancellationpolicy
	rm -f ./src/modules/postcancellationpolicy/index/index
	rm -f ./src/modules/postcancellationpolicy/bankdetail/bankdetail
	rm -f ./src/modules/postcancellationpolicy/show/show
	rm -f ./src/modules/postcancellationpolicy/create/create
	rm -f ./src/modules/postcancellationpolicy/update/update
	rm -f ./src/modules/postcancellationpolicy/delete/delete


# questionnaires
	rm -f ./src/modules/questionnaires/index/index
	rm -f ./src/modules/questionnaires/show/show
	rm -f ./src/modules/questionnaires/create/create
	rm -f ./src/modules/questionnaires/update/update
	rm -f ./src/modules/questionnaires/delete/delete


# room
	rm -f ./src/modules/room/index/index
	rm -f ./src/modules/room/show/show
	rm -f ./src/modules/room/create/create
	rm -f ./src/modules/room/update/update
	rm -f ./src/modules/room/delete/delete


# sitecontroller
	rm -f ./src/modules/sitecontroller/index/index
	rm -f ./src/modules/sitecontroller/show/show
	rm -f ./src/modules/sitecontroller/create/create
	rm -f ./src/modules/sitecontroller/update/update
	rm -f ./src/modules/sitecontroller/delete/delete

# sitecontrollerauth
	rm -f ./src/modules/sitecontrollerauth/signup/signup
	rm -f ./src/modules/sitecontrollerauth/signin/signin
	rm -f ./src/modules/sitecontrollerauth/resetpassword/resetpassword

# tax
	rm -f ./src/modules/tax/index/index
	rm -f ./src/modules/tax/show/show
	rm -f ./src/modules/tax/create/create
	rm -f ./src/modules/tax/update/update
	rm -f ./src/modules/tax/delete/delete    

# consumption tax
	rm -f ./src/modules/consumptiontax/index/index
	rm -f ./src/modules/consumptiontax/show/show
	rm -f ./src/modules/consumptiontax/create/create
	rm -f ./src/modules/consumptiontax/update/update
	rm -f ./src/modules/consumptiontax/delete/delete

# consumption tax
	rm -f ./src/modules/report/gha/gha
	rm -f ./src/modules/report/index/index