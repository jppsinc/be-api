module be-api

go 1.15

require (
	bitbucket.org/jppsinc/be-modules v1.3.106
	// git-codecommit.us-east-2.amazonaws.com/v1/repos/be-modules.git v1.0.24
	// git-codecommit.us-east-2.amazonaws.com/v1/repos/be-modules.git v1.0.24 bitbucket.org/jppsinc/be-modules.git
	// git-codecommit.us-east-2.amazonaws.com/v1/repos/be-modules.git v0.0.0-20201020080429-7053e99682dd
	// git-codecommit.us-east-2.amazonaws.com/v1/repos/be-modules.git v0.0.0-20201020080429-7053e99682dd
	github.com/asaskevich/govalidator v0.0.0-20210307081110-f21760c49a8d
	github.com/aws/aws-lambda-go v1.24.0
	github.com/aws/aws-sdk-go v1.42.47
	github.com/gocarina/gocsv v0.0.0-20210516172204-ca9e8a8ddea8
	github.com/google/uuid v1.2.0
	github.com/guregu/dynamo v1.17.0
	github.com/joho/godotenv v1.3.0
	golang.org/x/text v0.3.7
)
