package main

import (
	"errors"
	"fmt"
	"net/http"
	"os"

	be "bitbucket.org/jppsinc/be-modules/pkg"
	valid "github.com/asaskevich/govalidator"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/aws/aws-sdk-go/service/dynamodb/expression"
)

func handler(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {

	org := request.Headers["origin"]
	// Table name
	// tableName := "be_options"
	tableName := os.Getenv("TABLE_NAME")
	bookingTable := os.Getenv("BOOKING_TABLE_NAME")

	// Request path parameter
	hotel_id, ok := request.PathParameters["hotel_id"]
	if ok == false || valid.IsUUIDv4(hotel_id) == false {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorInvalidUrlHotelIdNotFound).Error()),
		})
	}

	id, ok := request.PathParameters["id"]
	if ok == false || valid.IsUUIDv4(id) == false {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorInvalidUrlIdNotFound).Error()),
		})
	}

	// DynamoDB
	db := be.Dynamodb()

	// input for GetItem
	input := &dynamodb.GetItemInput{
		Key: map[string]*dynamodb.AttributeValue{
			"hotel_id": {
				S: aws.String(hotel_id),
			},
			"id": {
				S: aws.String(id),
			},
		},
		TableName: aws.String(tableName),
	}

	// GetItem from dynamodb table
	result, err := db.GetItem(input)
	if err != nil {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorFailedToFetchRecord).Error()),
		})
	}

	if len(result.Item) > 0 {
		item := new(be.Option)
		err = dynamodbattribute.UnmarshalMap(result.Item, item)
		if err != nil {
			return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
				ErrorMsg: aws.String(errors.New(be.ErrorCouldNotUnMarshalItem).Error()),
			})
		}

		if len(item.PlanList) == 0 {
			item.PlanList = make([]string, 0)
		}
		if len(item.Images) == 0 {
			item.Images = make([]be.Image, 0)
		}
		if len(item.AvailableDays) == 0 {
			item.AvailableDays = make([]string, 0)
		}
		if len(item.AvailablePairs) == 0 {
			item.AvailablePairs = make([]int64, 0)
		}
		if len(item.AvailablePaymentMethod) == 0 {
			item.AvailablePaymentMethod = make([]string, 0)
		}
		if len(item.QuestionsForCustomer) == 0 {
			item.QuestionsForCustomer = make([]be.OptionQuestion, 0)
		}

		if item.OptionCount == -1 {
			item.RemainingOptionCount = -1
		} else {
			count, err := GetOptionDailyCount(db, hotel_id, bookingTable, item)
			if err != nil {
				fmt.Println("GetOptionDailyCount err : ", err)
			}
			item.RemainingOptionCount = item.OptionCount - count
		}

		return be.ApiResponse(org, http.StatusOK, item)
	} else {
		item := new(be.EmptyStruct)
		return be.ApiResponse(org, http.StatusOK, item)
	}
}

func main() {
	lambda.Start(handler)
}

func GetOptionDailyCount(db *dynamodb.DynamoDB, hotelID string, tableName string, option *be.Option) (int64, error) {

	keyCond := expression.Key("hotel_id").Equal(expression.Value(hotelID))
	filter := expression.Name("created_at").GreaterThanEqual(expression.Value(option.OptionCountUpdateDateTime)).
		And(expression.Name("status").Equal(expression.Value("Confirm")))

	expr, err := expression.NewBuilder().WithKeyCondition(keyCond).WithFilter(filter).Build()
	if err != nil {
		fmt.Println(err)
	}

	item := new([]*BookingSort)

	var lastEvaluatedKey LastKey
	var result *dynamodb.QueryOutput
	projection := aws.String("hotel_id,check_in_date,check_out_date,option_list")

	for {
		pp := new([]*BookingSort)
		if lastEvaluatedKey == (LastKey{}) {
			result, err = db.Query(QueryFirst(expr, tableName, projection))
		} else {
			result, err = db.Query(QueryNext(expr, tableName, projection, lastEvaluatedKey))
		}

		if err != nil {
			fmt.Println(err)
			// return err
		}

		err = dynamodbattribute.UnmarshalListOfMaps(result.Items, &pp)
		if err != nil {
			fmt.Println(err)
			// return err
		}

		*item = append(*item, *pp...)

		if result.LastEvaluatedKey == nil {
			break
		}

		err = dynamodbattribute.UnmarshalMap(result.LastEvaluatedKey, &lastEvaluatedKey)
		if err != nil {
			fmt.Println(err)
			// return err
		}
	}

	fmt.Println("booking count : ", len(*item))

	var count int64
	for _, booking := range *item {

		for _, bookingOptionID := range booking.OptionList {
			if option.ID == bookingOptionID.ID {
				for _, c := range bookingOptionID.CountPerNight {
					count += c
				}
			}
		}
	}

	fmt.Printf("option count : %v : option id : %v\n", count, option.ID)
	return count, nil

}

type BookingSort struct {
	HotelID      string          `json:"hotel_id"`
	BookingDate  string          `json:"booking_date"`
	CheckInDate  string          `json:"check_in_date"`
	CheckOutDate string          `json:"check_out_date"`
	OptionList   []be.OptionList `json:"option_list"`
}

type LastKey struct {
	HotelID string `json:"hotel_id"`
	ID      string `json:"id"`
}

func QueryFirst(expr expression.Expression, tableName string, projection *string) *dynamodb.QueryInput {
	input := &dynamodb.QueryInput{
		KeyConditionExpression:    expr.KeyCondition(),
		TableName:                 aws.String(tableName),
		FilterExpression:          expr.Filter(),
		ExpressionAttributeNames:  expr.Names(),
		ExpressionAttributeValues: expr.Values(),
		ProjectionExpression:      projection,
	}

	return input
}

func QueryNext(expr expression.Expression, tableName string, projection *string, es LastKey) *dynamodb.QueryInput {
	input := &dynamodb.QueryInput{
		KeyConditionExpression:    expr.KeyCondition(),
		TableName:                 aws.String(tableName),
		FilterExpression:          expr.Filter(),
		ExpressionAttributeNames:  expr.Names(),
		ExpressionAttributeValues: expr.Values(),
		ProjectionExpression:      projection,
		ExclusiveStartKey: map[string]*dynamodb.AttributeValue{
			"hotel_id": {
				S: aws.String(es.HotelID),
			},
			"id": {
				S: aws.String(es.ID),
			},
		},
	}

	return input
}
