package main

import (
	"errors"
	"fmt"
	"net/http"
	"os"

	be "bitbucket.org/jppsinc/be-modules/pkg"
	valid "github.com/asaskevich/govalidator"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/aws/aws-sdk-go/service/dynamodb/expression"
)

func handler(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {

	org := request.Headers["origin"]
	// Table name
	// tableName := "be_options"
	tableName := os.Getenv("TABLE_NAME")
	optionTable := os.Getenv("OPTION_MAP_TABLE")

	// Request path parameter
	hotel_id, ok := request.PathParameters["hotel_id"]
	if ok == false || valid.IsUUIDv4(hotel_id) == false {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorInvalidUrlHotelIdNotFound).Error()),
		})
	}

	id, ok := request.PathParameters["id"]
	if ok == false || valid.IsUUIDv4(id) == false {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorInvalidUrlIdNotFound).Error()),
		})
	}

	// DynamoDB
	db := be.Dynamodb()

	// input for GetItem
	ReturnValues := "ALL_OLD"
	input := &dynamodb.DeleteItemInput{
		Key: map[string]*dynamodb.AttributeValue{
			"hotel_id": {
				S: aws.String(hotel_id),
			},
			"id": {
				S: aws.String(id),
			},
		},
		TableName:    aws.String(tableName),
		ReturnValues: &ReturnValues,
	}

	// GetItem from dynamodb table
	result, err := db.DeleteItem(input)
	if err != nil {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorCouldNotDeleteItem).Error()),
		})
	}
	fmt.Println(result)

	err = UpdateMapTable(hotel_id, id, optionTable, db)
	if err != nil {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New("TL Plan map table update failed").Error()),
		})
	}

	item := new(be.Option)
	item.ID = id
	item.HotelID = hotel_id
	return be.ApiResponse(org, http.StatusOK, item)
}

func main() {
	lambda.Start(handler)
}

func UpdateMapTable(hotelId, planId, planMapTable string, db *dynamodb.DynamoDB) error {

	item := new([]*PlanIDMapList)
	keyCond := expression.Key("hotel_id").Equal(expression.Value(hotelId))

	expr, err := expression.NewBuilder().WithKeyCondition(keyCond).Build()
	if err != nil {
		return err
	}

	input := &dynamodb.QueryInput{
		KeyConditionExpression:    expr.KeyCondition(),
		TableName:                 aws.String(planMapTable),
		ExpressionAttributeNames:  expr.Names(),
		ExpressionAttributeValues: expr.Values(),
	}

	// GetItem from dynamodb table
	result, err := db.Query(input)
	if err != nil {
		return err
	}

	err = dynamodbattribute.UnmarshalListOfMaps(result.Items, &item)
	if err != nil {
		return err
	}

	for _, entry := range *item {
		if entry.BEID == planId {
			input := &dynamodb.UpdateItemInput{
				ExpressionAttributeNames: map[string]*string{
					"#K1": aws.String("status"),
				},
				ExpressionAttributeValues: map[string]*dynamodb.AttributeValue{
					":v1": {
						S: aws.String("Delete"),
					},
				},
				Key: map[string]*dynamodb.AttributeValue{
					"hotel_id": {
						S: aws.String(hotelId),
					},
					"be_id": {
						S: aws.String(planId),
					},
				},
				ReturnValues:     aws.String("ALL_NEW"),
				TableName:        aws.String(planMapTable),
				UpdateExpression: aws.String("SET #K1 = :v1"),
			}

			_, err := db.UpdateItem(input)
			if err != nil {
				return err
			}

			fmt.Println("TL option updated for id : ", planId)
			fmt.Println("TL option updated for table : ", planMapTable)
		}
	}
	return nil
}

type PlanIDMapList struct {
	HotelID string `json:"hotel_id"`
	TLID    string `json:"tl_id"`
	BEID    string `json:"be_id"`
}
