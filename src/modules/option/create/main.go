package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"math/rand"
	"net/http"
	"os"
	"time"

	be "bitbucket.org/jppsinc/be-modules/pkg"
	valid "github.com/asaskevich/govalidator"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/google/uuid"
)

// generate random string
var src = rand.NewSource(time.Now().UnixNano())

const letterBytes = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890"
const (
	letterIdxBits = 6                    // 6 bits to represent a letter index
	letterIdxMask = 1<<letterIdxBits - 1 // All 1-bits, as many as letterIdxBits
	letterIdxMax  = 63 / letterIdxBits   // # of letter indices fitting in 63 bits
)

func RandStringBytesMaskImprSrc(n int) string {
	b := make([]byte, n)
	// A src.Int63() generates 63 random bits, enough for letterIdxMax characters!
	for i, cache, remain := n-1, src.Int63(), letterIdxMax; i >= 0; {
		if remain == 0 {
			cache, remain = src.Int63(), letterIdxMax
		}
		if idx := int(cache & letterIdxMask); idx < len(letterBytes) {
			b[i] = letterBytes[idx]
			i--
		}
		cache >>= letterIdxBits
		remain--
	}

	return string(b)
}

func handler(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {

	org := request.Headers["origin"]
	// Table name
	// tableName := "be_options"
	tableName := os.Getenv("TABLE_NAME")

	// Change JSON request data to struct : just to check validity of request data
	var sds be.Option
	var sdsa be.AdminSiteOption

	if err := json.Unmarshal([]byte(request.Body), &sds); err != nil {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorCouldNotUnMarshalItem).Error()),
		})
	}

	// Request path parameter
	hotel_id, ok := request.PathParameters["hotel_id"]
	if !ok || !valid.IsUUIDv4(hotel_id) {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorInvalidUrlHotelIdNotFound).Error()),
		})
	} else {
		sds.HotelID = hotel_id
		sds.ID = uuid.New().String()
		sds.Code = RandStringBytesMaskImprSrc(8)
		sds.CreatedAt = be.DateTimeStamp()
		sds.UpdatedAt = be.DateTimeStamp()
	}

	sdsa.ID = sds.ID
	sdsa.HotelID = sds.HotelID
	sdsa.Name = sds.Name
	sdsa.InternalMemo = sds.InternalMemo
	sdsa.Code = sds.Code
	sdsa.Status = sds.Status
	sdsa.Description = sds.Description
	sdsa.Unit = sds.Unit
	sdsa.PricePerUnit = sds.PricePerUnit
	sdsa.Images = sds.Images
	sdsa.OptionCount = sds.OptionCount
	sdsa.RemainingOptionCount = sds.OptionCount // initialize RemainingOptionCount with OptionCount
	sdsa.OptionCountPerDay = sds.OptionCountPerDay
	sdsa.OptionCountPerBooking = sds.OptionCountPerBooking
	sdsa.AvailablePairs = sds.AvailablePairs
	sdsa.AvailableDays = sds.AvailableDays
	sdsa.Repetition = sds.Repetition
	sdsa.AvailableFor = sds.AvailableFor
	sdsa.AvailablePaymentMethod = sds.AvailablePaymentMethod
	sdsa.CutOffDays = sds.CutOffDays
	sdsa.DisplayFlag = sds.DisplayFlag
	sdsa.DisplayStartDate = sds.DisplayStartDate
	sdsa.DisplayEndDate = sds.DisplayEndDate
	sdsa.SalesFlag = sds.SalesFlag
	sdsa.SalesStartDate = sds.SalesStartDate
	sdsa.SalesEndDate = sds.SalesEndDate
	sdsa.PlanList = sds.PlanList
	sdsa.Cancellationcharge100 = sds.Cancellationcharge100
	sdsa.Order = sds.Order
	sdsa.QuestionsForCustomer = sds.QuestionsForCustomer
	sdsa.OptionCountUpdateDateTime = be.DateTimeStamp()
	sdsa.CreatedAt = sds.CreatedAt
	sdsa.UpdatedAt = sds.UpdatedAt
	sdsa.IsFuturePriceSet = sds.IsFuturePriceSet
	sdsa.FuturePriceStartDate = sds.FuturePriceStartDate
	sdsa.FuturePricePerUnit = sds.FuturePricePerUnit
	sdsa.IsCheckoutDateIncluded = sds.IsCheckoutDateIncluded
	sdsa.ResetOptionCount = sds.ResetOptionCount

	// DynamoDB
	db := be.Dynamodb()

	// change struct data to json data, this data will be stored to database
	av, err := dynamodbattribute.MarshalMap(sdsa)
	if err != nil {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorCouldNotMarshalItem).Error()),
		})
	}
	if len(sdsa.PlanList) == 0 {
		empty := []*dynamodb.AttributeValue{}
		av["remind_email_send_history"] = &dynamodb.AttributeValue{L: empty}
	}
	if len(sdsa.AvailableDays) == 0 {
		empty := []*dynamodb.AttributeValue{}
		av["available_days"] = &dynamodb.AttributeValue{L: empty}
	}
	if len(sdsa.AvailablePaymentMethod) == 0 {
		empty := []*dynamodb.AttributeValue{}
		av["available_payment_method"] = &dynamodb.AttributeValue{L: empty}
	}
	if len(sdsa.AvailablePairs) == 0 {
		empty := []*dynamodb.AttributeValue{}
		av["available_pairs"] = &dynamodb.AttributeValue{L: empty}
	}
	if len(sdsa.Images) == 0 {
		empty := []*dynamodb.AttributeValue{}
		av["images"] = &dynamodb.AttributeValue{L: empty}
	}
	if len(sdsa.PlanList) == 0 {
		empty := []*dynamodb.AttributeValue{}
		av["plan_list"] = &dynamodb.AttributeValue{L: empty}
	}
	if len(sdsa.QuestionsForCustomer) == 0 {
		empty := []*dynamodb.AttributeValue{}
		av["questions_for_customer"] = &dynamodb.AttributeValue{L: empty}
	}

	ReturnItem := "SIZE"
	input := &dynamodb.PutItemInput{
		Item:                        av,
		TableName:                   aws.String(tableName),
		ReturnItemCollectionMetrics: &ReturnItem,
		// ReturnConsumedCapacity:      aws.String("TOTAL"),
		// ReturnValues: aws.String(dynamodb.ReturnValueAllOld),
	}

	_, err = db.PutItem(input)
	if err != nil {
		fmt.Println(err)
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorCouldNotDynamoPutItem).Error()),
		})
	}
	return be.ApiResponse(org, http.StatusCreated, &sds)
}

func main() {
	lambda.Start(handler)
}
