package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"os"

	be "bitbucket.org/jppsinc/be-modules/pkg"

	valid "github.com/asaskevich/govalidator"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
)

func handler(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {

	org := request.Headers["origin"]
	// Table name
	// tableName := "be_options"
	tableName := os.Getenv("TABLE_NAME")

	// Change JSON request data to struct : just to check validity of request data
	var inq be.AdminSiteOption

	if err := json.Unmarshal([]byte(request.Body), &inq); err != nil {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorCouldNotUnMarshalItem).Error()),
		})
	}

	// Request path parameter
	id, ok := request.PathParameters["id"]
	if !ok || !valid.IsUUIDv4(id) {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorInvalidUrlIdNotFound).Error()),
		})
	}

	hotel_id, ok := request.PathParameters["hotel_id"]
	if !ok || !valid.IsUUIDv4(hotel_id) {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorInvalidUrlHotelIdNotFound).Error()),
		})
	} else {
		inq.HotelID = hotel_id
		inq.ID = id
		inq.UpdatedAt = be.DateTimeStamp()
	}

	// DynamoDB
	db := be.Dynamodb()

	old, err := GetOption(hotel_id, id, tableName, db)
	if err != nil {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(err.Error()),
		})
	}

	if inq.ResetOptionCount {
		inq.OptionCountUpdateDateTime = be.DateTimeStamp()
		inq.RemainingOptionCount = inq.OptionCount
	} else {
		// if OptionCountUpdateDateTime is not set in very begining set it while updating
		if old.OptionCountUpdateDateTime == 0 {
			inq.OptionCountUpdateDateTime = be.DateTimeStamp()
		}
	}

	// change struct data to json data, this data will be stored to database
	av, err := dynamodbattribute.MarshalMap(inq)
	if err != nil {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorCouldNotMarshalItem).Error()),
		})
	}

	if len(inq.PlanList) == 0 {
		empty := []*dynamodb.AttributeValue{}
		av["remind_email_send_history"] = &dynamodb.AttributeValue{L: empty}
	}
	if len(inq.AvailableDays) == 0 {
		empty := []*dynamodb.AttributeValue{}
		av["available_days"] = &dynamodb.AttributeValue{L: empty}
	}
	if len(inq.AvailablePaymentMethod) == 0 {
		empty := []*dynamodb.AttributeValue{}
		av["available_payment_method"] = &dynamodb.AttributeValue{L: empty}
	}
	if len(inq.AvailablePairs) == 0 {
		empty := []*dynamodb.AttributeValue{}
		av["available_pairs"] = &dynamodb.AttributeValue{L: empty}
	}
	if len(inq.Images) == 0 {
		empty := []*dynamodb.AttributeValue{}
		av["images"] = &dynamodb.AttributeValue{L: empty}
	}
	if len(inq.PlanList) == 0 {
		empty := []*dynamodb.AttributeValue{}
		av["plan_list"] = &dynamodb.AttributeValue{L: empty}
	}
	if len(inq.QuestionsForCustomer) == 0 {
		empty := []*dynamodb.AttributeValue{}
		av["questions_for_customer"] = &dynamodb.AttributeValue{L: empty}
	}

	ReturnItem := "SIZE"
	input := &dynamodb.PutItemInput{
		Item:                        av,
		TableName:                   aws.String(tableName),
		ReturnItemCollectionMetrics: &ReturnItem,
		// ReturnConsumedCapacity:      aws.String("TOTAL"),
		// ReturnValues: aws.String(dynamodb.ReturnValueAllOld),
	}

	_, err = db.PutItem(input)
	if err != nil {
		fmt.Println(err)
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorCouldNotDynamoPutItem).Error()),
		})
	}
	return be.ApiResponse(org, http.StatusOK, &inq)
}

func main() {
	lambda.Start(handler)
}

func GetOption(hotelID string, id string, tableName string, db *dynamodb.DynamoDB) (*be.AdminSiteOption, error) {

	item := new(be.AdminSiteOption)
	// input for GetItem
	input := &dynamodb.GetItemInput{
		Key: map[string]*dynamodb.AttributeValue{
			"hotel_id": {
				S: aws.String(hotelID),
			},
			"id": {
				S: aws.String(id),
			},
		},
		TableName: aws.String(tableName),
	}

	// GetItem from dynamodb table
	result, err := db.GetItem(input)
	if err != nil {
		return item, err
	}

	if len(result.Item) > 0 {
		err = dynamodbattribute.UnmarshalMap(result.Item, item)
		if err != nil {
			return item, err
		}
		return item, nil
	} else {
		return item, errors.New("record not found")
	}
}
