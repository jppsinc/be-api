package main

import (
	"errors"
	"fmt"
	"net/http"
	"os"

	be "bitbucket.org/jppsinc/be-modules/pkg"
	valid "github.com/asaskevich/govalidator"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/aws/aws-sdk-go/service/dynamodb/expression"
)

func handler(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {

	org := request.Headers["origin"]

	// Table name
	// tableName := "be_plans-dev"
	tableName := os.Getenv("TABLE_NAME")

	fmt.Println("tableName : ", tableName)

	pgMap := make(map[string][]be.MultiLang)

	// Request path parameter
	hotel_id, ok := request.PathParameters["hotel_id"]
	if ok == false || valid.IsUUIDv4(hotel_id) == false {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorInvalidUrlHotelIdNotFound).Error()),
		})
	}

	// DynamoDB
	db := be.Dynamodb()

	keyCond := expression.Key("hotel_id").Equal(expression.Value(hotel_id))

	today := be.DateString("date")
	filter := expression.Name("status").NotEqual(expression.Value("Delete")).And(expression.Name("sales_end_date").GreaterThanEqual(expression.Value(today)))

	item := new([]*be.Plan)

	expr, err := expression.NewBuilder().WithKeyCondition(keyCond).WithFilter(filter).Build()
	if err != nil {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(err.Error()),
		})
	}

	// input for GetItem
	input := &dynamodb.QueryInput{
		KeyConditionExpression:    expr.KeyCondition(),
		TableName:                 aws.String(tableName),
		ExpressionAttributeNames:  expr.Names(),
		ExpressionAttributeValues: expr.Values(),
		FilterExpression:          expr.Filter(),
	}

	// fmt.Println("plan input : ", input)

	// GetItem from dynamodb table
	result, err := db.Query(input)
	if err != nil {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(err.Error()),
		})
	}

	err = dynamodbattribute.UnmarshalListOfMaps(result.Items, &item)
	if err != nil {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(err.Error()),
		})
	}

	fmt.Println("No of plans : ", len(*item))

	for _, plan := range *item {
		for _, pg := range plan.OptionIDList {
			pgMap[pg] = append(pgMap[pg], plan.Title)
		}
	}

	if len(*item) > 0 {
		return be.ApiResponse(org, http.StatusOK, pgMap)
	} else {
		return be.ApiResponse(org, http.StatusOK, []be.EmptyStruct{})
	}

}

func main() {
	lambda.Start(handler)
}
