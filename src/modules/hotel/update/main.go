package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"os"

	be "bitbucket.org/jppsinc/be-modules/pkg"

	valid "github.com/asaskevich/govalidator"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/aws/aws-sdk-go/service/dynamodb/expression"
)

func handler(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {

	org := request.Headers["origin"]
	// Table name
	// tableName := "be_hotels"
	tableName := os.Getenv("TABLE_NAME")

	// Change JSON request data to struct : just to check validity of request data
	var sds be.Hotel

	if err := json.Unmarshal([]byte(request.Body), &sds); err != nil {
		fmt.Println(err)
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorCouldNotUnMarshalItem).Error()),
		})
	}

	// Request path parameter
	hotel_id, ok := request.PathParameters["hotel_id"]
	if ok == false || valid.IsUUIDv4(hotel_id) == false {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorInvalidUrlHotelIdNotFound).Error()),
		})
	}

	sds.HotelID = hotel_id
	sds.UpdatedAt = be.DateTimeStamp()

	// DynamoDB
	db := be.Dynamodb()

	//create expr builder
	filt := expression.Name("facility_code").Equal(expression.Value(sds.FacilityCode))
	expr, err := expression.NewBuilder().WithFilter(filt).Build()
	if err != nil {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorExpressionBuilder).Error()),
		})
	}

	// check if row with this key exist
	// input for GetItem
	input := &dynamodb.GetItemInput{
		Key: map[string]*dynamodb.AttributeValue{
			"hotel_id": {
				S: aws.String(hotel_id),
			},
		},
		TableName: aws.String(tableName),
	}

	// GetItem from dynamodb table
	result, err := db.GetItem(input)
	if err != nil {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorFailedToFetchRecord).Error()),
		})
	}

	// create  slice of pointer of TransactWriteItem
	if len(result.Item) > 0 {
		// change struct data to json data, this data will be stored to database
		av, err := dynamodbattribute.MarshalMap(sds)
		if err != nil {
			return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
				ErrorMsg: aws.String(errors.New(be.ErrorCouldNotMarshalItem).Error()),
			})
		}

		// along with hotel_id we also match facility code while inserting data
		ReturnItem := "SIZE"
		input := &dynamodb.PutItemInput{
			Item:                        av,
			TableName:                   aws.String(tableName),
			ReturnItemCollectionMetrics: &ReturnItem,
			ConditionExpression:         expr.Filter(),
			ExpressionAttributeNames:    expr.Names(),
			ExpressionAttributeValues:   expr.Values(),
		}

		_, err = db.PutItem(input)
		if err != nil {
			return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
				ErrorMsg: aws.String(errors.New(be.ErrorCouldNotDynamoPutItem).Error()),
			})
		}

		// update image gallery for use_count calculation
		if len(sds.Images) > 0{
			UpdateGalleries(hotel_id, sds.Images[0].ID, db)
		}

		return be.ApiResponse(org, http.StatusOK, &sds)
	} else {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorPathParameterInvalid).Error()),
		})
	}
}

func main() {
	lambda.Start(handler)
}

func UpdateGalleries(hotelID string, id string, db *dynamodb.DynamoDB) {
	tableName := os.Getenv("GAL_TABLE_NAME")
	update := expression.Set(expression.Name("updated_at"), expression.Value(be.DateTimeStamp()))
	expr, err := expression.NewBuilder().WithUpdate(update).Build()

	_, err = db.TransactWriteItems(&dynamodb.TransactWriteItemsInput{
		TransactItems: []*dynamodb.TransactWriteItem{
			{
				Update: &dynamodb.Update{
					TableName: aws.String(tableName),
					Key: map[string]*dynamodb.AttributeValue{
						"hotel_id": {
							S: aws.String(hotelID),
						},
						"id": {
							S: aws.String(id),
						},
					},
					ExpressionAttributeNames:  expr.Names(),
					ExpressionAttributeValues: expr.Values(),
					UpdateExpression:          expr.Update(),
				},
			},
		},
	})
	if err != nil {
		fmt.Println("TransactWriteItems error :", err)
	}
}
