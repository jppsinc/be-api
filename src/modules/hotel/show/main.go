package main

import (
	"errors"
	"fmt"
	"net/http"
	"os"

	be "bitbucket.org/jppsinc/be-modules/pkg"
	valid "github.com/asaskevich/govalidator"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
)

type Gallery struct {
	ID                 string   `json:"id"`
	HotelID            string   `json:"hotel_id"`
	Type               string   `json:"type"`
	URL                string   `json:"url"`
	Alt                string   `json:"alt"`
	Desc               string   `json:"desc"`
	IsHide             bool     `json:"is_hide"`
	ShareCount         int64    `json:"share_count"`
	SharedIdList       []string `json:"shared_id_list"`
	SharedWithCategory []string `json:"shared_with_category"`
	UseCount           int64    `json:"use_count"`
	CreatedAt          int64    `json:"created_at"`
	UpdatedAt          int64    `json:"updated_at"`
}

func init() {
	be.SetLocalTime()
}

func main() {
	lambda.Start(handler)
}

func handler(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {

	org := request.Headers["origin"]

	// Table name
	// tableName := "be_hotels"
	tableName := os.Getenv("TABLE_NAME")

	// Request path parameter
	hotel_id, ok := request.PathParameters["hotel_id"]
	if ok == false || valid.IsUUIDv4(hotel_id) == false {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorInvalidUrlHotelIdNotFound).Error()),
		})
	}

	// DynamoDB
	db := be.Dynamodb()

	// input for GetItem
	input := &dynamodb.GetItemInput{
		Key: map[string]*dynamodb.AttributeValue{
			"hotel_id": {
				S: aws.String(hotel_id),
			},
		},
		TableName: aws.String(tableName),
	}

	// GetItem from dynamodb table
	result, err := db.GetItem(input)
	if err != nil {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorFailedToFetchRecord).Error()),
		})
	}

	item := new(be.Hotel)
	err = dynamodbattribute.UnmarshalMap(result.Item, item)
	if err != nil {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorCouldNotUnMarshalItem).Error()),
		})
	}
	if len(result.Item) > 0 {
		if len(item.Images) == 0 {
			item.Images = make([]be.Image, 0)
		}

		headerInfo, err := GetImages(hotel_id, item.HeaderImage.ID)
		if err == nil {
			fmt.Println("header headerInfo.IsHide == false : ", headerInfo.ID)
			item.HeaderImage.ID = headerInfo.ID
			item.HeaderImage.URL = headerInfo.URL
			item.HeaderImage.Alt = headerInfo.Alt
			item.HeaderImage.IsHide = headerInfo.IsHide
		}

		img := make([]be.Image, 0)
		for i := 0; i < len(item.Images); i++ {
			headerInfo, err := GetImages(hotel_id, item.Images[i].ID)
			if err == nil {

				fmt.Println("Image headerInfo.IsHide == false : ", headerInfo.ID)
				e := be.Image{}
				e.ID = headerInfo.ID
				e.URL = headerInfo.URL
				e.Alt = headerInfo.Alt
				e.IsHide = headerInfo.IsHide

				img = append(img, e)
			}
		}

		item.Images = img

		return be.ApiResponse(org, http.StatusOK, item)
	} else {
		item := new(be.EmptyStruct)
		return be.ApiResponse(org, http.StatusOK, item)
	}
}

func GetImages(hotelID string, imageID string) (*Gallery, error) {
	tableName := os.Getenv("GAL_TABLE_NAME")
	// tableName := "be_galleries-prod"

	// DynamoDB
	db := be.Dynamodb()
	item := new(Gallery)

	// input for GetItem
	input := &dynamodb.GetItemInput{
		Key: map[string]*dynamodb.AttributeValue{
			"hotel_id": {
				S: aws.String(hotelID),
			},
			"id": {
				S: aws.String(imageID),
			},
		},
		TableName: aws.String(tableName),
	}

	// GetItem from dynamodb table
	result, err := db.GetItem(input)
	if err != nil {
		fmt.Println("GetItem err : ", err.Error())
		return item, err
	}

	if len(result.Item) > 0 {
		err = dynamodbattribute.UnmarshalMap(result.Item, item)
		if err != nil {
			fmt.Println("UnmarshalMap err : ", err.Error())
			return item, err
		}
		return item, nil
	} else {
		item := new(Gallery)
		return item, nil
	}
}
