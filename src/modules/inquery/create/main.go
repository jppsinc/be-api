package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"os"

	be "bitbucket.org/jppsinc/be-modules/pkg"
	valid "github.com/asaskevich/govalidator"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/google/uuid"
)

func handler(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {

	org := request.Headers["origin"]
	// Table name
	// tableName := "be_inqueries"
	tableName := os.Getenv("TABLE_NAME")
	bucketName := os.Getenv("INQUERY_FILES_BUCKET")

	// Change JSON request data to struct : just to check the validity of request data
	var inq be.Inquery

	if err := json.Unmarshal([]byte(request.Body), &inq); err != nil {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorCouldNotUnMarshalItem).Error()),
		})
	}

	// Request path parameter
	hotel_id, ok := request.PathParameters["hotel_id"]
	if ok == false || valid.IsUUIDv4(hotel_id) == false {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorInvalidUrlHotelIdNotFound).Error()),
		})
	} else {
		inq.HotelID = hotel_id
		inq.ID = fmt.Sprintf("%s", uuid.New())
		inq.CreatedAt = be.DateTimeStamp()
		inq.UpdatedAt = be.DateTimeStamp()
	}

	S3, sess := be.S3()
	_, err := be.CreateBucket(S3, bucketName)
	if err != nil {
		if _, ok := err.(awserr.Error); !ok {
			fmt.Println("Gal err 3 : ", err.Error())
			return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
				ErrorMsg: aws.String(err.Error()),
			})
		}
	}

	path := inq.HotelID + "/" + inq.ID

	for i := 0; i < len(inq.UploadFile); i++ {
		resc, err := be.UploadFileToS3(sess, bucketName, inq.UploadFile[i].FileName, inq.UploadFile[i].FileData, path)
		if err != nil {
			fmt.Println("Gal err 4 : ", err.Error())
			return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
				ErrorMsg: aws.String(err.Error()),
			})
		}

		inq.UploadFile[i].FileURL = resc.Location
		inq.UploadFile[i].FileData = ""
	}

	// DynamoDB
	db := be.Dynamodb()

	// change struct data to json data, this data will be stored to database
	av, err := dynamodbattribute.MarshalMap(inq)
	if err != nil {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorCouldNotMarshalItem).Error()),
		})
	}

	ReturnItem := "SIZE"
	input := &dynamodb.PutItemInput{
		Item:                        av,
		TableName:                   aws.String(tableName),
		ReturnItemCollectionMetrics: &ReturnItem,
		// ReturnConsumedCapacity:      aws.String("TOTAL"),
		// ReturnValues: aws.String(dynamodb.ReturnValueAllOld),
	}

	_, err = db.PutItem(input)
	if err != nil {
		fmt.Println(err)
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorCouldNotDynamoPutItem).Error()),
		})
	}
	return be.ApiResponse(org, http.StatusCreated, &inq)
}

func main() {
	lambda.Start(handler)
}
