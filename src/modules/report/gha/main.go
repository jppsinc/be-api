package main

import (
	"errors"
	"net/http"
	"os"
	"strings"

	be "bitbucket.org/jppsinc/be-modules/pkg"
	valid "github.com/asaskevich/govalidator"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/aws/aws-sdk-go/service/dynamodb/expression"
)

type Gha struct {
	ID                 string    `json:"id"`
	BookingReferenceID string    `json:"booking_reference_id"`
	Status             string    `json:"status"`
	GuestName          string    `json:"guest_name"`
	GuestNameKana      string    `json:"guest_name_kana"`
	BookingDate        string    `json:"booking_date"`
	CheckInDate        string    `json:"check_in_date"`
	CheckOutDate       string    `json:"check_out_date"`
	Nights             int64     `json:"nights"`
	RoomCount          int64     `json:"room_count"`
	Pax                int64     `json:"pax"`
	PlanName           MultiLang `json:"plan_name"`
	RoomType           MultiLang `json:"room_type"`
	TotalCharge        int64     `json:"total_charge"`
	Rate               float64   `json:"rate"`
	CPA                float64   `json:"cpa"`
	Commission         float64   `json:"commission"`
}

type MultiLang struct {
	En string `json:"en"`
	Ja string `json:"ja"`
}

func handler(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {

	org := request.Headers["origin"]
	// Table name
	// tableName := "be_bookings"
	tableName := os.Getenv("TABLE_NAME")
	// cpaTableName := "be_cpa"
	cpaTableName := os.Getenv("GHA_TABLE_NAME")

	// Request path parameter
	hotel_id, ok := request.PathParameters["hotel_id"]
	if ok == false || valid.IsUUIDv4(hotel_id) == false {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorInvalidUrlHotelIdNotFound).Error()),
		})
	}

	keyCond := expression.Key("hotel_id").Equal(expression.Value(hotel_id))

	var finalFilter expression.ConditionBuilder
	var filter []expression.ConditionBuilder

	var startDate string
	var endDate string

	// query string parameter
	if len(request.QueryStringParameters) == 0 {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorQueryStringParameterNotFound).Error()),
		})
	} else {
		startDate, ok = request.QueryStringParameters["startDate"]
		if ok == false || len(strings.TrimSpace(startDate)) == 0 {
			return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
				ErrorMsg: aws.String(errors.New("Error QueryStringParameter startDate").Error()),
			})
		} else {
			filter = append(filter, expression.Name("check_in_date").GreaterThanEqual(expression.Value(startDate)))
		}

		endDate, ok = request.QueryStringParameters["endDate"]
		if ok == false || len(strings.TrimSpace(endDate)) == 0 {
			return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
				ErrorMsg: aws.String(errors.New("Error QueryStringParameter endDate").Error()),
			})
		} else {
			filter = append(filter, expression.Name("check_in_date").LessThanEqual(expression.Value(endDate)))
		}
	}

	filter = append(filter, expression.Name("status").NotEqual(expression.Value("Initiate")))
	filter = append(filter, expression.Name("source").Equal(expression.Value("GoogleHotelAds")))
	filter = append(filter, expression.Name("status").NotEqual(expression.Value("Initiate")))

	for i, temp := range filter {
		if i == 0 {
			finalFilter = temp
		} else {
			finalFilter = finalFilter.And(temp)
		}
	}

	// DynamoDB
	db := be.Dynamodb()
	var input *dynamodb.QueryInput

	expr, err := expression.NewBuilder().WithKeyCondition(keyCond).WithFilter(finalFilter).Build()
	if err != nil {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(err.Error()),
		})
	}

	// input for QueryInput
	input = &dynamodb.QueryInput{
		KeyConditionExpression:    expr.KeyCondition(),
		TableName:                 aws.String(tableName),
		FilterExpression:          expr.Filter(),
		ExpressionAttributeNames:  expr.Names(),
		ExpressionAttributeValues: expr.Values(),
	}

	// GetItem from dynamodb table
	result, err := db.Query(input)
	if err != nil {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(err.Error()),
		})
	}

	// CPA information
	cpaInput := &dynamodb.GetItemInput{
		Key: map[string]*dynamodb.AttributeValue{
			"hotel_id": {
				S: aws.String(hotel_id),
			},
		},
		TableName:            aws.String(cpaTableName),
		ProjectionExpression: aws.String("google_hotel_ads_cpa"),
	}

	// GetItem from dynamodb table
	cpa, err := db.GetItem(cpaInput)
	if err != nil {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorFailedToFetchRecord).Error()),
		})
	}

	cpaItem := new(be.CPAChangeInfo)
	if len(cpa.Item) > 0 {
		err = dynamodbattribute.UnmarshalMap(cpa.Item, cpaItem)
		if err != nil {
			return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
				ErrorMsg: aws.String(errors.New("Could not get cpa info").Error()),
			})
		}
	}

	item := new([]*be.Booking)

	err = dynamodbattribute.UnmarshalListOfMaps(result.Items, &item)
	if err != nil {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorCouldNotUnMarshalItem).Error()),
		})
	}

	gha := make([]Gha, len(*item))
	i := 0
	for _, ent := range *item {

		gha[i].ID = ent.ID
		gha[i].BookingReferenceID = ent.BookingReferenceID
		gha[i].Status = ent.Status
		gha[i].GuestName = ent.GuestName
		gha[i].GuestNameKana = ent.GuestNameKana
		gha[i].BookingDate = ent.BookingDate
		gha[i].CheckInDate = ent.CheckInDate
		gha[i].CheckOutDate = ent.CheckOutDate
		gha[i].Nights = ent.Nights
		gha[i].RoomCount = ent.RoomCount

		for j := 0; j < len(ent.Adult); j++ {
			gha[i].Pax = gha[i].Pax + ent.Adult[j]
		}

		for j := 0; j < len(ent.Child); j++ {
			gha[i].Pax = gha[i].Pax + ent.Child[j]
		}

		gha[i].PlanName.En = ent.PlanName.En
		gha[i].PlanName.Ja = ent.PlanName.Ja
		gha[i].RoomType.En = ent.RoomType.En
		gha[i].RoomType.Ja = ent.RoomType.Ja
		gha[i].TotalCharge = ent.TotalCharge
		gha[i].Rate = cpaItem.GoogleHotelAdsCPA
		gha[i].CPA = (float64(ent.TotalCharge) * cpaItem.GoogleHotelAdsCPA / 100)
		gha[i].Commission = (float64(ent.TotalCharge) / 100)

		i++
	}
	return be.ApiResponse(org, http.StatusOK, gha)

}

func main() {
	lambda.Start(handler)
}
