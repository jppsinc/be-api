package main

import (
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"math"
	"net/http"
	"os"
	"sort"
	"strconv"
	"strings"
	"time"

	be "bitbucket.org/jppsinc/be-modules/pkg"
	valid "github.com/asaskevich/govalidator"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/aws/aws-sdk-go/service/dynamodb/expression"
	"github.com/gocarina/gocsv"
	"golang.org/x/text/encoding/japanese"
	"golang.org/x/text/transform"
)

type LastKey struct {
	HotelID string `json:"hotel_id"`
	ID      string `json:"id"`
}

func QueryFirst(expr expression.Expression, tableName string) *dynamodb.QueryInput {
	input := &dynamodb.QueryInput{
		KeyConditionExpression:    expr.KeyCondition(),
		TableName:                 aws.String(tableName),
		FilterExpression:          expr.Filter(),
		ExpressionAttributeNames:  expr.Names(),
		ExpressionAttributeValues: expr.Values(),
	}

	return input
}

func QueryNext(expr expression.Expression, tableName string, es LastKey) *dynamodb.QueryInput {
	input := &dynamodb.QueryInput{
		KeyConditionExpression:    expr.KeyCondition(),
		TableName:                 aws.String(tableName),
		FilterExpression:          expr.Filter(),
		ExpressionAttributeNames:  expr.Names(),
		ExpressionAttributeValues: expr.Values(),
		ExclusiveStartKey: map[string]*dynamodb.AttributeValue{
			"hotel_id": {
				S: aws.String(es.HotelID),
			},
			"id": {
				S: aws.String(es.ID),
			},
		},
	}

	return input
}

type Total struct {
	Current T `json:"current"`
	Last    T `json:"last"`
}

type T struct {
	Bookings                  int64   `json:"bookings"`            //Number of reservations
	RoomNights                int64   `json:"room_nights"`         //Total nights
	GrossBookingValue         float64 `json:"gross_booking_value"` //Total sales
	AverageDailyRate          float64 `json:"average_daily_rate"`
	AverageTransactionPrice   float64 `json:"average_transaction_price"`
	LengthOfStay              float64 `json:"length_of_stay"`
	AverageRoomsPerBooking    float64 `json:"average_rooms_per_booking"`
	AveragePaxPerBooking      float64 `json:"average_pax_per_booking"`
	AverageAdultsPerBooking   float64 `json:"average_adults_per_booking"`
	AverageChildrenPerBooking float64 `json:"average_children_per_booking"`
	CancellationRate          float64 `json:"cancellation_rate"`
	CancelCount               float64 `json:"cancel_count"`
	NoShowRate                float64 `json:"no_show_rate"`
	LeadTime                  float64 `json:"lead_time"`
}

type BookingDaily struct {
	Date               string `json:"date"`
	Day                string `json:"day"`
	RoomCount          int64  `json:"room_count"`
	Pax                int64  `json:"pax"`
	Adult              int64  `json:"adult"`
	ChildA             int64  `json:"child_A"`
	ChildB             int64  `json:"child_B"`
	ChildC             int64  `json:"child_C"`
	ChildD             int64  `json:"child_D"`
	ChildE             int64  `json:"child_E"`
	ChildF             int64  `json:"child_F"`
	TotalPrice         int64  `json:"total_price"`
	PerRoomPrice       int64  `json:"per_room_price"`
	PerPersonPrice     int64  `json:"per_person_price"`
	CancelledRoomCount int64  `json:"cancelled_room_count"`
	CancelledPax       int64  `json:"cancelled_pax"`
	CancelledPrice     int64  `json:"cancelled_price"`
}

type Booking struct {
	PlanName           string `json:"plan_name"`
	RoomCount          int64  `json:"room_count"`
	Pax                int64  `json:"pax"`
	Adult              int64  `json:"adult"`
	ChildA             int64  `json:"child_A"`
	ChildB             int64  `json:"child_B"`
	ChildC             int64  `json:"child_C"`
	ChildD             int64  `json:"child_D"`
	ChildE             int64  `json:"child_E"`
	ChildF             int64  `json:"child_F"`
	TotalPrice         int64  `json:"total_price"`
	PerRoomPrice       int64  `json:"per_room_price"`
	PerPersonPrice     int64  `json:"per_person_price"`
	CancelledRoomCount int64  `json:"cancelled_room_count"`
	CancelledPax       int64  `json:"cancelled_pax"`
	CancelledPrice     int64  `json:"cancelled_price"`
}

func Days(checkIn string, checkOut string) int64 {
	layout := "2006-01-02"
	cin, _ := time.Parse(layout, checkIn)
	cout, _ := time.Parse(layout, checkOut)

	days := int(cout.Sub(cin).Hours() / 24)
	return int64(days)
}

func GetPlans(hotelID string, db *dynamodb.DynamoDB, tableName string) (*[]*be.Plan, error) {

	keyCond := expression.Key("hotel_id").Equal(expression.Value(hotelID))

	item := new([]*be.Plan)
	expr, err := expression.NewBuilder().WithKeyCondition(keyCond).Build()
	if err != nil {
		return item, err
	}

	// input for GetItem
	input := &dynamodb.QueryInput{
		KeyConditionExpression:    expr.KeyCondition(),
		TableName:                 aws.String(tableName),
		ExpressionAttributeNames:  expr.Names(),
		ExpressionAttributeValues: expr.Values(),
	}

	// GetItem from dynamodb table
	result, err := db.Query(input)
	if err != nil {
		fmt.Println("err :", err)
		return item, err
	}

	err = dynamodbattribute.UnmarshalListOfMaps(result.Items, &item)
	if err != nil {
		return item, err
	}

	// sort by Order
	sort.SliceStable(*item, func(i, j int) bool {
		return *&(*item)[i].Order < *&(*item)[j].Order
	})

	return item, nil
}

func handler(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {

	org := request.Headers["origin"]
	// Table name
	// tableName := "be_bookings-dev"
	tableName := os.Getenv("TABLE_NAME")

	// pTableName := "be_plans-dev"
	pTableName := os.Getenv("PLAN_TABLE_NAME")

	// Request path parameter
	hotel_id, ok := request.PathParameters["hotel_id"]
	if ok == false || valid.IsUUIDv4(hotel_id) == false {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorInvalidUrlHotelIdNotFound).Error()),
		})
	}

	keyCond := expression.Key("hotel_id").Equal(expression.Value(hotel_id))

	var finalFilter expression.ConditionBuilder
	var filter []expression.ConditionBuilder

	var startDate string
	var endDate string
	var condition string

	var finalFilterLast expression.ConditionBuilder
	var filterLast []expression.ConditionBuilder

	var startDateLast string
	var endDateLast string
	loc, _ := time.LoadLocation("Asia/Tokyo")

	// query string parameter
	if len(request.QueryStringParameters) == 0 {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorQueryStringParameterNotFound).Error()),
		})
	} else {
		condition, ok = request.QueryStringParameters["condition"]
		if ok == true {
			period, ok := request.QueryStringParameters["period"]
			if ok == true {
				if period == "1" {
					b := time.Now().In(loc)
					a := time.Now().In(loc).AddDate(0, -1, 0)
					startDate = a.Format("2006-01-02")
					endDate = b.Format("2006-01-02")
					startDateLast = a.AddDate(0, -12, 0).Format("2006-01-02")
					endDateLast = b.AddDate(0, -12, 0).Format("2006-01-02")
				}
				if period == "3" {
					b := time.Now().In(loc)
					a := time.Now().In(loc).AddDate(0, -3, 0)
					startDate = a.Format("2006-01-02")
					endDate = b.Format("2006-01-02")
					startDateLast = a.AddDate(0, -12, 0).Format("2006-01-02")
					endDateLast = b.AddDate(0, -12, 0).Format("2006-01-02")
				}
				if period == "6" {
					b := time.Now().In(loc)
					a := time.Now().In(loc).AddDate(0, -6, 0)
					startDate = a.Format("2006-01-02")
					endDate = b.Format("2006-01-02")
					startDateLast = a.AddDate(0, -12, 0).Format("2006-01-02")
					endDateLast = b.AddDate(0, -12, 0).Format("2006-01-02")
				}
				if period == "12" {
					b := time.Now().In(loc)
					a := time.Now().In(loc).AddDate(0, -12, 0)
					startDate = a.Format("2006-01-02")
					endDate = b.Format("2006-01-02")
					startDateLast = a.AddDate(0, -12, 0).Format("2006-01-02")
					endDateLast = b.AddDate(0, -12, 0).Format("2006-01-02")
				}
				if period == "range" {
					startDate, ok = request.QueryStringParameters["startDate"]
					if ok == false || len(strings.TrimSpace(startDate)) == 0 {
						return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
							ErrorMsg: aws.String(errors.New("Error QueryStringParameter startDate").Error()),
						})
					}

					endDate, ok = request.QueryStringParameters["endDate"]
					if ok == false || len(strings.TrimSpace(endDate)) == 0 {
						return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
							ErrorMsg: aws.String(errors.New("Error QueryStringParameter endDate").Error()),
						})
					}

					a, _ := time.Parse("2006-01-02", startDate)
					b, _ := time.Parse("2006-01-02", endDate)
					startDateLast = a.AddDate(0, -12, 0).Format("2006-01-02")
					endDateLast = b.AddDate(0, -12, 0).Format("2006-01-02")
				}

				switch condition {
				case "bookingDate":
					filter = append(filter, expression.Name("booking_date").GreaterThanEqual(expression.Value(startDate)).And(expression.Name("booking_date").LessThanEqual(expression.Value(endDate))))
					filterLast = append(filterLast, expression.Name("booking_date").GreaterThanEqual(expression.Value(startDateLast)).And(expression.Name("booking_date").LessThanEqual(expression.Value(endDateLast))))

				case "arrivalDate":
					filter = append(filter, expression.Name("check_in_date").GreaterThanEqual(expression.Value(startDate)).And(expression.Name("check_in_date").LessThanEqual(expression.Value(endDate))))
					filterLast = append(filterLast, expression.Name("check_in_date").GreaterThanEqual(expression.Value(startDateLast)).And(expression.Name("check_in_date").LessThanEqual(expression.Value(endDateLast))))

				case "stayDate":
					filter = append(filter, expression.Name("check_in_date").GreaterThanEqual(expression.Value(startDate)).And(expression.Name("check_out_date").LessThanEqual(expression.Value(endDate))))
					filterLast = append(filterLast, expression.Name("check_in_date").GreaterThanEqual(expression.Value(startDateLast)).And(expression.Name("check_out_date").LessThanEqual(expression.Value(endDateLast))))
				}

			} else {
				return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
					ErrorMsg: aws.String(errors.New("Error QueryStringParameter period").Error()),
				})
			}

		}
	}

	rtype, ok := request.QueryStringParameters["type"]
	if ok != true || len(strings.TrimSpace(rtype)) == 0 {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New("Error QueryStringParameter type").Error()),
		})
	}

	status, ok := request.QueryStringParameters["status"]
	if ok == true && len(strings.TrimSpace(status)) != 0 {
		if status != "All" {
			filter = append(filter, expression.Name("status").Equal(expression.Value(status)))
			filterLast = append(filterLast, expression.Name("status").Equal(expression.Value(status)))
		}
	}

	filter = append(filter, expression.Name("status").NotEqual(expression.Value("Initiate")))

	source, ok := request.QueryStringParameters["source"]
	if ok == true && len(strings.TrimSpace(source)) != 0 {
		if source != "All" {
			if source == "Free" {
				filter = append(filter, expression.Name("source").Equal(expression.Value("GoogleHotelAds")).And(expression.Name("is_paid_ad").Equal(expression.Value(false))))
				filterLast = append(filterLast, expression.Name("source").Equal(expression.Value("GoogleHotelAds")).And(expression.Name("is_paid_ad").Equal(expression.Value(false))))
			} else if source == "Paid" {
				filter = append(filter, expression.Name("source").Equal(expression.Value("GoogleHotelAds")).And(expression.Name("is_paid_ad").Equal(expression.Value(true))))
				filterLast = append(filterLast, expression.Name("source").Equal(expression.Value("GoogleHotelAds")).And(expression.Name("is_paid_ad").Equal(expression.Value(true))))
			} else {
				filter = append(filter, expression.Name("source").Equal(expression.Value(source)))
				filterLast = append(filterLast, expression.Name("source").Equal(expression.Value(source)))
			}
		}
	}

	for i, temp := range filter {
		if i == 0 {
			finalFilter = temp
		} else {
			finalFilter = finalFilter.And(temp)
		}
	}

	for i, tmp := range filterLast {
		if i == 0 {
			finalFilterLast = tmp
		} else {
			finalFilterLast = finalFilterLast.And(tmp)
		}
	}

	expr, err := expression.NewBuilder().WithKeyCondition(keyCond).WithFilter(finalFilter).Build()
	if err != nil {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(err.Error()),
		})
	}

	// DynamoDB
	db := be.Dynamodb()

	item := new([]*be.Booking)

	var lastEvaluatedKey LastKey
	var result *dynamodb.QueryOutput

	for {
		pp := new([]*be.Booking)
		if lastEvaluatedKey == (LastKey{}) {
			result, err = db.Query(QueryFirst(expr, tableName))
		} else {
			result, err = db.Query(QueryNext(expr, tableName, lastEvaluatedKey))
		}

		if err != nil {
			fmt.Println(err)
			// return err
		}

		err = dynamodbattribute.UnmarshalListOfMaps(result.Items, &pp)
		if err != nil {
			fmt.Println(err)
			// return err
		}

		for _, i := range *pp {
			*item = append(*item, i)
		}

		if result.LastEvaluatedKey == nil {
			break
		}

		err = dynamodbattribute.UnmarshalMap(result.LastEvaluatedKey, &lastEvaluatedKey)
		if err != nil {
			fmt.Println(err)
			// return err
		}
	}

	// // input for QueryInput
	// input = &dynamodb.QueryInput{
	// 	KeyConditionExpression:    expr.KeyCondition(),
	// 	TableName:                 aws.String(tableName),
	// 	FilterExpression:          expr.Filter(),
	// 	ExpressionAttributeNames:  expr.Names(),
	// 	ExpressionAttributeValues: expr.Values(),
	// }

	// fmt.Println("input: ", input)

	// // GetItem from dynamodb table
	// result, err := db.Query(input)
	// if err != nil {
	// 	return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
	// 		ErrorMsg: aws.String(err.Error()),
	// 	})
	// }

	// fmt.Println("len of result: ", len(result.Items))

	// item := new([]*be.Booking)
	// err = dynamodbattribute.UnmarshalListOfMaps(result.Items, &item)
	// if err != nil {
	// 	return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
	// 		ErrorMsg: aws.String(errors.New(be.ErrorCouldNotUnMarshalItem).Error()),
	// 	})
	// }

	if rtype == "planReport" {

		fmt.Println("type: planReport")

		plans, err := GetPlans(hotel_id, db, pTableName)
		if err != nil {
			return be.ApiResponseGuest(http.StatusBadRequest, be.ErrorBody{
				ErrorMsg: aws.String(err.Error()),
			})
		}

		type OuterplanMap map[string][]be.Booking
		tmp := make(OuterplanMap)

		// loop over the slice and create the map of entries
		for _, ent := range *item {
			tmp[ent.PlanID] = append(tmp[ent.PlanID], *ent)
		}

		var bookingList []Booking

		//  get all keys of map; item: [{key1: [{}, {}, {}]}, {key2: [{}, {}, {}, {}, {}]}]
		for _, plan := range *plans {
			k := plan.ID

			if _, ok := tmp[k]; !ok {
				continue
			}

			// iterate slice of structs: here k is key: tmp[k] means value of key1 = [{}, {}, {}] : {key1: [{}, {}, {}]}
			var booking Booking
			for i := 0; i < len(tmp[k]); i++ {

				booking.PlanName = tmp[k][i].PlanName.Ja

				if tmp[k][i].Status == "Cancel" {
					var temp int64 = 0

					for j := 0; j < len(tmp[k][i].Adult); j++ {
						temp = temp + tmp[k][i].Adult[j]
					}
					for j := 0; j < len(tmp[k][i].ChildInfo); j++ {
						if tmp[k][i].ChildInfo[j].Rank == "Child A" {
							temp = temp + 1
						}
						if tmp[k][i].ChildInfo[j].Rank == "Child B" {
							temp = temp + 1
						}
						if tmp[k][i].ChildInfo[j].Rank == "Child C" {
							temp = temp + 1
						}
						if tmp[k][i].ChildInfo[j].Rank == "Child D" {
							temp = temp + 1
						}
						if tmp[k][i].ChildInfo[j].Rank == "Child E" {
							temp = temp + 1
						}
						if tmp[k][i].ChildInfo[j].Rank == "Child F" {
							temp = temp + 1
						}
					}
					booking.CancelledRoomCount = booking.CancelledRoomCount + tmp[k][i].RoomCount
					booking.CancelledPax = booking.CancelledPax + temp
					booking.CancelledPrice = booking.CancelledPrice + tmp[k][i].TotalCharge
				}

				if tmp[k][i].Status == "Confirm" {
					for j := 0; j < len(tmp[k][i].Adult); j++ {
						booking.Adult = booking.Adult + tmp[k][i].Adult[j]
					}
					for j := 0; j < len(tmp[k][i].ChildInfo); j++ {
						if tmp[k][i].ChildInfo[j].Rank == "Child A" {
							booking.ChildA = booking.ChildA + 1
						}
						if tmp[k][i].ChildInfo[j].Rank == "Child B" {
							booking.ChildB = booking.ChildB + 1
						}
						if tmp[k][i].ChildInfo[j].Rank == "Child C" {
							booking.ChildC = booking.ChildC + 1
						}
						if tmp[k][i].ChildInfo[j].Rank == "Child D" {
							booking.ChildD = booking.ChildD + 1
						}
						if tmp[k][i].ChildInfo[j].Rank == "Child E" {
							booking.ChildE = booking.ChildE + 1
						}
						if tmp[k][i].ChildInfo[j].Rank == "Child F" {
							booking.ChildF = booking.ChildF + 1
						}
					}
					booking.RoomCount = booking.RoomCount + tmp[k][i].RoomCount
					booking.TotalPrice = booking.TotalPrice + tmp[k][i].TotalCharge
				}

				booking.Pax = booking.Adult + booking.ChildA + booking.ChildB + booking.ChildC + booking.ChildD + booking.ChildE

				if booking.RoomCount == 0 {
					booking.PerRoomPrice = booking.TotalPrice
				} else {
					booking.PerRoomPrice = booking.TotalPrice / booking.RoomCount
				}

				if booking.Pax == 0 {
					booking.PerPersonPrice = booking.TotalPrice
				} else {
					booking.PerPersonPrice = booking.TotalPrice / booking.Pax
				}
			}

			bookingList = append(bookingList, booking)
		}

		fmt.Println("type: planReport end")

		// csvContent, _ := gocsv.MarshalString(&bookingList)
		csvContent := GetCSVData("planReport", bookingList, nil, nil)
		fmt.Println("csvContent : ", csvContent)

		a := be.DateString("date")
		a = strings.ReplaceAll(a, "-", "")
		fileName := "report_plan_" + a + ".csv"
		return ApiResponse(org, http.StatusOK, csvContent, fileName)

		// return be.ApiResponse(org, http.StatusOK, bookingList)
	}

	if rtype == "dailyReport" {
		fmt.Println("type: dailyReport")
		DaysMap := map[string]string{"Monday": "月", "Tuesday": "火", "Wednesday": "水", "Thursday": "木", "Friday": "金", "Saturday": "土", "Sunday": "日"}

		type OuterDailyMap map[string][]be.Booking
		tmp := make(OuterDailyMap)

		// loop over the slice and create the map of entries
		if condition == "bookingDate" {
			for _, ent := range *item {
				tmp[ent.BookingDate] = append(tmp[ent.BookingDate], *ent)
			}
		}
		if condition == "arrivalDate" {
			for _, ent := range *item {
				tmp[ent.CheckInDate] = append(tmp[ent.CheckInDate], *ent)
			}
		}
		if condition == "stayDate" {
			for _, ent := range *item {
				tmp[ent.CheckInDate] = append(tmp[ent.CheckInDate], *ent)
			}
		}

		keys := make([]string, 0)
		booking := make([]BookingDaily, 0)

		// get all the keys
		// for k := range tmp {
		// 	keys = append(keys, k)
		// }

		sd, _ := time.Parse("2006-01-02", startDate)
		ed, _ := time.Parse("2006-01-02", endDate)

		ed = ed.AddDate(0, 0, 1)
		for {
			if sd.Before(ed) {
				keys = append(keys, sd.Format("2006-01-02"))
				sd = sd.AddDate(0, 0, 1)
			} else {
				break
			}
		}
		// sort keys
		sort.Strings(keys)

		fmt.Println("=========== : ", keys)
		fmt.Println("=========== len : ", len(keys))

		//  get all keys of map; item: [{key1: [{}, {}, {}]}, {key2: [{}, {}, {}, {}, {}]}]
		for _, k := range keys {
			// iterate slice of structs: here k is key: tmp[k] means value of key1 = [{}, {}, {}] : {key1: [{}, {}, {}]}

			BookingDailyTemp := BookingDaily{}

			BookingDailyTemp.Date = k
			t, _ := time.Parse("2006-01-02", k)
			BookingDailyTemp.Day = DaysMap[t.Weekday().String()]

			for i := 0; i < len(tmp[k]); i++ {

				if tmp[k][i].Status == "Cancel" {
					var temp int64 = 0

					for j := 0; j < len(tmp[k][i].Adult); j++ {
						temp = temp + 1
					}
					for j := 0; j < len(tmp[k][i].ChildInfo); j++ {
						if tmp[k][i].ChildInfo[j].Rank == "Child A" {
							temp = temp + 1
						}
						if tmp[k][i].ChildInfo[j].Rank == "Child B" {
							temp = temp + 1
						}
						if tmp[k][i].ChildInfo[j].Rank == "Child C" {
							temp = temp + 1
						}
						if tmp[k][i].ChildInfo[j].Rank == "Child D" {
							temp = temp + 1
						}
						if tmp[k][i].ChildInfo[j].Rank == "Child E" {
							temp = temp + 1
						}
						if tmp[k][i].ChildInfo[j].Rank == "Child F" {
							temp = temp + 1
						}
					}
					BookingDailyTemp.CancelledRoomCount = BookingDailyTemp.CancelledRoomCount + tmp[k][i].RoomCount
					BookingDailyTemp.CancelledPax = BookingDailyTemp.CancelledPax + temp
					BookingDailyTemp.CancelledPrice = BookingDailyTemp.CancelledPrice + tmp[k][i].TotalCharge
				}

				if tmp[k][i].Status == "Confirm" {
					for j := 0; j < len(tmp[k][i].Adult); j++ {
						BookingDailyTemp.Adult = BookingDailyTemp.Adult + tmp[k][i].Adult[j]
					}
					for j := 0; j < len(tmp[k][i].ChildInfo); j++ {
						if tmp[k][i].ChildInfo[j].Rank == "Child A" {
							BookingDailyTemp.ChildA = BookingDailyTemp.ChildA + 1
						}
						if tmp[k][i].ChildInfo[j].Rank == "Child B" {
							BookingDailyTemp.ChildB = BookingDailyTemp.ChildB + 1
						}
						if tmp[k][i].ChildInfo[j].Rank == "Child C" {
							BookingDailyTemp.ChildC = BookingDailyTemp.ChildC + 1
						}
						if tmp[k][i].ChildInfo[j].Rank == "Child D" {
							BookingDailyTemp.ChildD = BookingDailyTemp.ChildD + 1
						}
						if tmp[k][i].ChildInfo[j].Rank == "Child E" {
							BookingDailyTemp.ChildE = BookingDailyTemp.ChildE + 1
						}
						if tmp[k][i].ChildInfo[j].Rank == "Child F" {
							BookingDailyTemp.ChildF = BookingDailyTemp.ChildF + 1
						}
					}
					BookingDailyTemp.RoomCount = BookingDailyTemp.RoomCount + tmp[k][i].RoomCount
					BookingDailyTemp.TotalPrice = BookingDailyTemp.TotalPrice + tmp[k][i].TotalCharge
				}
			}
			BookingDailyTemp.Pax = BookingDailyTemp.Adult + BookingDailyTemp.ChildA + BookingDailyTemp.ChildB + BookingDailyTemp.ChildC + BookingDailyTemp.ChildD + BookingDailyTemp.ChildE
			if BookingDailyTemp.RoomCount == 0 {
				BookingDailyTemp.PerRoomPrice = BookingDailyTemp.TotalPrice
			} else {
				BookingDailyTemp.PerRoomPrice = BookingDailyTemp.TotalPrice / BookingDailyTemp.RoomCount
			}

			if BookingDailyTemp.Pax == 0 {
				BookingDailyTemp.PerPersonPrice = BookingDailyTemp.TotalPrice
			} else {
				BookingDailyTemp.PerPersonPrice = BookingDailyTemp.TotalPrice / BookingDailyTemp.Pax
			}

			booking = append(booking, BookingDailyTemp)
		}

		fmt.Println("type: dailyReport end")

		// csvContent, _ := gocsv.MarshalString(&booking)
		csvContent := GetCSVData("dailyReport", nil, booking, nil)
		fmt.Println("csvContent : ", csvContent)

		a := be.DateString("date")
		a = strings.ReplaceAll(a, "-", "")
		fileName := "report_daily_" + a + ".csv"
		return ApiResponse(org, http.StatusOK, csvContent, fileName)

		// return be.ApiResponse(org, http.StatusOK, booking)
	}

	if rtype == "totalReport" {
		fmt.Println("type: totalReport")
		tr := new(Total)
		arrayTr := []Total{}

		bookingCount := 0

		for i := 0; i < len(*item); i++ {
			it := *&(*item)[i]

			if it.Status == "Confirm" {
				bookingCount += 1
				tr.Current.RoomNights += it.Nights
				tr.Current.GrossBookingValue += float64(it.TotalCharge)
				tr.Current.AverageRoomsPerBooking += float64(it.RoomCount)
				tr.Current.LengthOfStay += float64(it.Nights)

				for j := 0; j < len(it.Adult); j++ {
					tr.Current.AverageAdultsPerBooking += float64(it.Adult[j])
					tr.Current.AverageChildrenPerBooking += float64(it.Child[j])
				}

				if it.IsNoshow == true {
					tr.Current.NoShowRate += 1.0
				}

				tr.Current.LeadTime += float64(Days(it.BookingDate, it.CheckInDate))
			}
			if it.Status == "Cancel" {
				tr.Current.CancelCount += 1.0
			}
		}

		bookingCountDivisor := 1
		if bookingCount > 0 {
			bookingCountDivisor = bookingCount
		}

		tr.Current.NoShowRate = math.Round((tr.Current.NoShowRate/float64(bookingCountDivisor))*100) / 100
		tr.Current.CancellationRate = math.Round((tr.Current.CancellationRate/float64(bookingCountDivisor))*100) / 100
		tr.Current.AverageChildrenPerBooking = math.Round((tr.Current.AverageChildrenPerBooking/float64(bookingCountDivisor))*100) / 100
		tr.Current.AverageAdultsPerBooking = math.Round((tr.Current.AverageAdultsPerBooking/float64(bookingCountDivisor))*100) / 100
		tr.Current.AveragePaxPerBooking = math.Round((tr.Current.AverageAdultsPerBooking+tr.Current.AverageChildrenPerBooking)*100) / 100
		tr.Current.AverageRoomsPerBooking = math.Round((tr.Current.AverageRoomsPerBooking/float64(bookingCountDivisor))*100) / 100
		tr.Current.LengthOfStay = math.Round((tr.Current.LengthOfStay/float64(bookingCountDivisor))*100) / 100
		tr.Current.AverageTransactionPrice = math.Round((tr.Current.GrossBookingValue/float64(bookingCountDivisor))*100) / 100
		tr.Current.LeadTime = math.Round((tr.Current.LeadTime/float64(bookingCountDivisor))*100) / 100
		tr.Current.CancellationRate = math.Round((tr.Current.CancelCount/(float64(bookingCountDivisor)+tr.Current.CancelCount)*100)*100) / 100

		if status == "Cancel" && tr.Current.CancelCount > 0 {
			tr.Current.CancellationRate = 100
		}
		if status == "Cancel" && tr.Current.CancelCount == 0 {
			tr.Current.CancellationRate = 0
		}

		d := float64(Days(startDate, endDate))
		if d == 0 {
			tr.Current.AverageDailyRate = math.Round((tr.Current.GrossBookingValue/float64(1))*100) / 100
		} else {
			tr.Current.AverageDailyRate = math.Round((tr.Current.GrossBookingValue/float64(Days(startDate, endDate)))*100) / 100
		}
		tr.Current.Bookings = int64(bookingCount)

		// last year data processing
		expr1, err := expression.NewBuilder().WithKeyCondition(keyCond).WithFilter(finalFilterLast).Build()
		if err != nil {
			return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
				ErrorMsg: aws.String(err.Error()),
			})
		}

		itemLast := new([]*be.Booking)

		var lastEvaluatedKey1 LastKey
		var result1 *dynamodb.QueryOutput

		for {
			pp := new([]*be.Booking)
			if lastEvaluatedKey1 == (LastKey{}) {
				result1, err = db.Query(QueryFirst(expr1, tableName))
			} else {
				result1, err = db.Query(QueryNext(expr1, tableName, lastEvaluatedKey1))
			}

			if err != nil {
				fmt.Println(err)
				// return err
			}

			err = dynamodbattribute.UnmarshalListOfMaps(result1.Items, &pp)
			if err != nil {
				fmt.Println(err)
				// return err
			}

			*itemLast = append(*itemLast, *pp...)

			if result1.LastEvaluatedKey == nil {
				break
			}

			err = dynamodbattribute.UnmarshalMap(result1.LastEvaluatedKey, &lastEvaluatedKey1)
			if err != nil {
				fmt.Println(err)
				// return err
			}
		}

		// // input for QueryInput
		// input = &dynamodb.QueryInput{
		// 	KeyConditionExpression:    expr.KeyCondition(),
		// 	TableName:                 aws.String(tableName),
		// 	FilterExpression:          expr.Filter(),
		// 	ExpressionAttributeNames:  expr.Names(),
		// 	ExpressionAttributeValues: expr.Values(),
		// }

		// // GetItem from dynamodb table
		// result, err := db.Query(input)
		// if err != nil {
		// 	return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
		// 		ErrorMsg: aws.String(err.Error()),
		// 	})
		// }

		// itemLast := new([]*be.Booking)
		if len(*itemLast) > 0 {

		} else {
			arrayTr = append(arrayTr, *tr)
			// return be.ApiResponse(org, http.StatusOK, arrayTr)

			// csvContent, _ := gocsv.MarshalString(&arrayTr)
			csvContent := GetCSVData("totalReport", nil, nil, arrayTr)
			fmt.Println("csvContent : ", csvContent)

			a := be.DateString("date")
			a = strings.ReplaceAll(a, "-", "")
			fileName := "report_total_" + a + ".csv"

			return ApiResponse(org, http.StatusOK, csvContent, fileName)
		}
		// processing
		bookingCount = 0

		for i := 0; i < len(*itemLast); i++ {
			it := *&(*itemLast)[i]

			if it.Status == "Confirm" {
				bookingCount += 1
				tr.Last.RoomNights += it.Nights
				tr.Last.GrossBookingValue += float64(it.TotalCharge)
				tr.Last.AverageRoomsPerBooking += float64(it.RoomCount)
				tr.Last.LengthOfStay += float64(it.Nights)

				for j := 0; j < len(it.Adult); j++ {
					tr.Last.AverageAdultsPerBooking += float64(it.Adult[j])
					tr.Last.AverageChildrenPerBooking += float64(it.Child[j])
				}

				if it.IsNoshow == true {
					tr.Last.NoShowRate += 1.0
				}

				tr.Last.LeadTime += float64(Days(it.BookingDate, it.CheckInDate))
			}
			if it.Status == "Cancel" {
				tr.Last.CancelCount += 1.0
			}
		}

		bookingCountDivisor = 1
		if bookingCount > 0 {
			bookingCountDivisor = bookingCount
		}

		tr.Last.NoShowRate = math.Round((tr.Last.NoShowRate/float64(bookingCountDivisor))*100) / 100
		tr.Last.CancellationRate = math.Round((tr.Last.CancellationRate/float64(bookingCountDivisor))*100) / 100
		tr.Last.AverageChildrenPerBooking = math.Round((tr.Last.AverageChildrenPerBooking/float64(bookingCountDivisor))*100) / 100
		tr.Last.AverageAdultsPerBooking = math.Round((tr.Last.AverageAdultsPerBooking/float64(bookingCountDivisor))*100) / 100
		tr.Last.AveragePaxPerBooking = math.Round((tr.Last.AverageAdultsPerBooking+tr.Last.AverageChildrenPerBooking)*100) / 100
		tr.Last.AverageRoomsPerBooking = math.Round((tr.Last.AverageRoomsPerBooking/float64(bookingCountDivisor))*100) / 100
		tr.Last.LengthOfStay = math.Round((tr.Last.LengthOfStay/float64(bookingCountDivisor))*100) / 100
		tr.Last.AverageTransactionPrice = math.Round((tr.Last.GrossBookingValue/float64(bookingCountDivisor))*100) / 100
		tr.Last.LeadTime = math.Round((tr.Last.LeadTime/float64(bookingCountDivisor))*100) / 100
		tr.Last.CancellationRate = math.Round((tr.Last.CancelCount/(float64(bookingCountDivisor)+tr.Last.CancelCount)*100)*100) / 100

		if status == "Cancel" && tr.Last.CancelCount > 0 {
			tr.Last.CancellationRate = 100
		}
		if status == "Cancel" && tr.Last.CancelCount == 0 {
			tr.Last.CancellationRate = 0
		}

		dl := float64(Days(startDate, endDate))
		if dl == 0 {
			tr.Last.AverageDailyRate = math.Round((tr.Last.GrossBookingValue/float64(1))*100) / 100
		} else {
			tr.Last.AverageDailyRate = math.Round((tr.Last.GrossBookingValue/float64(Days(startDate, endDate)))*100) / 100
		}
		tr.Last.Bookings = int64(bookingCount)

		fmt.Println("type: totalReport end")
		arrayTr = append(arrayTr, *tr)

		// csvContent, _ := gocsv.MarshalString(&arrayTr)
		csvContent := GetCSVData("totalReport", nil, nil, arrayTr)
		fmt.Println("csvContent : ", csvContent)

		a := be.DateString("date")
		a = strings.ReplaceAll(a, "-", "")
		fileName := "report_total_" + a + ".csv"

		return ApiResponse(org, http.StatusOK, csvContent, fileName)
	}

	csvContent, _ := gocsv.MarshalString(&item)

	fmt.Println("csvContent : ", csvContent)

	fileName := "report.csv"
	return ApiResponse(org, http.StatusOK, csvContent, fileName)

	// return be.ApiResponse(org, http.StatusOK, item)
}

func ApiResponse(origin string, status int, csvContent string, fileName string) (events.APIGatewayProxyResponse, error) {
	origins := map[string]bool{"https://dev.d7r69723er9j4.amplifyapp.com": true,
		"https://master.d7r69723er9j4.amplifyapp.com": true,
		"https://localhost:3000":                      true,
		"http://localhost:3000":                       true,
		"https://rc-booking.com":                      true,
		"https://yoyaku.rc-booking.com":               true,
		"https://kanri.rc-booking.com":                true,
		"https://test-yoyaku.rc-booking.com":          true,
		"https://dev-yoyaku.rc-booking.com":           true,
		"https://test-kanri.rc-booking.com":           true,
		"https://dev-kanri.rc-booking.com":            true,
		"https://preview-test-kanri.rc-booking.com":   true,
		"https://preview-test-yoyaku.rc-booking.com":  true,
	}

	resp := events.APIGatewayProxyResponse{}
	resp.StatusCode = status
	// fmt.Println("origin :", origin)
	if origins[origin] == true {
		resp.Headers = map[string]string{
			"Access-Control-Allow-Origin":      origin,
			"Access-Control-Allow-Headers":     "origin,Accept,Authorization,Content-Type,X-Amz-Date,X-Api-Key,X-Amz-Security-Token",
			"Access-Control-Allow-Methods":     "DELETE,GET,OPTIONS,POST,PUT",
			"Content-Type":                     "text/csv; charset=Shift-JIS",
			"Access-Control-Allow-Credentials": "true",
			"Content-Disposition":              "attachment; filename=" + fileName,
		}
	} else {
		resp.Headers = map[string]string{
			"Access-Control-Allow-Origin":  "*",
			"Access-Control-Allow-Headers": "origin,Accept,Authorization,Content-Type,X-Amz-Date,X-Api-Key,X-Amz-Security-Token",
			"Access-Control-Allow-Methods": "DELETE,GET,OPTIONS,POST,PUT",
			"Content-Type":                 "text/csv; charset=Shift-JIS",
			"Content-Disposition":          "attachment; filename=" + fileName,
		}
	}

	resp.Body = string(csvContent)
	return resp, nil
}

func main() {
	lambda.Start(handler)
}

// Convert a string encoding from UTF-8 to ShiftJIS
func ToShiftJIS(str string) (string, error) {
	return transformEncoding(strings.NewReader(str), japanese.ShiftJIS.NewEncoder())
}

// Convert a string encoding from ShiftJIS to UTF-8
func FromShiftJIS(str string) (string, error) {
	return transformEncoding(strings.NewReader(str), japanese.ShiftJIS.NewDecoder())
}

func transformEncoding(rawReader io.Reader, trans transform.Transformer) (string, error) {
	ret, err := ioutil.ReadAll(transform.NewReader(rawReader, trans))
	if err == nil {
		return string(ret), nil
	} else {
		return "", err
	}
}

func GetCSVData(rtype string, bookingList []Booking, booking []BookingDaily, arrayTr []Total) string {
	temp := "\xEF\xBB\xBF"

	if rtype == "planReport" {
		temp += ",,,人数,,,,,,,予約金額,,,キャンセル,,\r\n"
		temp += "プラン名,室数,合計,大人,小人A,小人B,小人C,小人D,小人E,小人F,合計,室単価,客単価,室数,人数,金額\r\n"
	} else if rtype == "dailyReport" {
		temp += ",,,,人数,,,,,,,予約金額,,,キャンセル,,\r\n"
		temp += ",,室数,合計,大人,小人A,小人B,小人C,小人D,小人E,小人F,合計,室単価,客単価,室数,人数,金額\r\n"
	} else if rtype == "totalReport" {
		temp += ",予約数,総泊数,販売総額,ADR,平均販売価格,平均泊数,リードタイム,平均予約客室数,平均予約人数,平均大人人数,平均小人人数,キャンセル率,ノーショー率\r\n"
	}

	if rtype == "planReport" {
		for _, b := range bookingList {

			temp += b.PlanName + ","
			temp += strconv.Itoa(int(b.RoomCount)) + ","
			temp += strconv.Itoa(int(b.Pax)) + ","
			temp += strconv.Itoa(int(b.Adult)) + ","
			temp += strconv.Itoa(int(b.ChildA)) + ","
			temp += strconv.Itoa(int(b.ChildB)) + ","
			temp += strconv.Itoa(int(b.ChildC)) + ","
			temp += strconv.Itoa(int(b.ChildD)) + ","
			temp += strconv.Itoa(int(b.ChildE)) + ","
			temp += strconv.Itoa(int(b.ChildF)) + ","
			temp += strconv.Itoa(int(b.TotalPrice)) + ","
			temp += strconv.Itoa(int(b.PerRoomPrice)) + ","
			temp += strconv.Itoa(int(b.PerPersonPrice)) + ","
			temp += strconv.Itoa(int(b.CancelledRoomCount)) + ","
			temp += strconv.Itoa(int(b.CancelledPax)) + ","
			temp += strconv.Itoa(int(b.CancelledPrice)) + "\r\n"
		}
	}

	if rtype == "dailyReport" {
		for _, b := range booking {
			temp += b.Date + ","
			temp += b.Day + ","
			temp += strconv.Itoa(int(b.RoomCount)) + ","
			temp += strconv.Itoa(int(b.Pax)) + ","
			temp += strconv.Itoa(int(b.Adult)) + ","
			temp += strconv.Itoa(int(b.ChildA)) + ","
			temp += strconv.Itoa(int(b.ChildB)) + ","
			temp += strconv.Itoa(int(b.ChildC)) + ","
			temp += strconv.Itoa(int(b.ChildD)) + ","
			temp += strconv.Itoa(int(b.ChildE)) + ","
			temp += strconv.Itoa(int(b.ChildF)) + ","
			temp += strconv.Itoa(int(b.TotalPrice)) + ","
			temp += strconv.Itoa(int(b.PerRoomPrice)) + ","
			temp += strconv.Itoa(int(b.PerPersonPrice)) + ","
			temp += strconv.Itoa(int(b.CancelledRoomCount)) + ","
			temp += strconv.Itoa(int(b.CancelledPax)) + ","
			temp += strconv.Itoa(int(b.CancelledPrice)) + "\r\n"
		}

	}

	if rtype == "totalReport" {
		b := arrayTr[0].Current

		temp += ","
		temp += strconv.Itoa(int(b.Bookings)) + ","
		temp += strconv.Itoa(int(b.RoomNights)) + ","
		temp += fmt.Sprintf("%.2f", b.GrossBookingValue) + ","
		temp += fmt.Sprintf("%.2f", b.AverageDailyRate) + ","
		temp += fmt.Sprintf("%.2f", b.AverageTransactionPrice) + ","
		temp += fmt.Sprintf("%.2f", b.LengthOfStay) + ","
		temp += fmt.Sprintf("%.2f", b.LeadTime) + ","
		temp += fmt.Sprintf("%.2f", b.AverageRoomsPerBooking) + ","
		temp += fmt.Sprintf("%.2f", b.AveragePaxPerBooking) + ","
		temp += fmt.Sprintf("%.2f", b.AverageAdultsPerBooking) + ","
		temp += fmt.Sprintf("%.2f", b.AverageChildrenPerBooking) + ","
		temp += fmt.Sprintf("%.2f", b.CancelCount) + ","
		temp += fmt.Sprintf("%.2f", b.CancellationRate) + ","
		temp += fmt.Sprintf("%.2f", b.NoShowRate) + "\r\n"

		b = arrayTr[0].Last

		temp += "昨年同時期,"
		temp += strconv.Itoa(int(b.Bookings)) + ","
		temp += strconv.Itoa(int(b.RoomNights)) + ","
		temp += fmt.Sprintf("%.2f", b.GrossBookingValue) + ","
		temp += fmt.Sprintf("%.2f", b.AverageDailyRate) + ","
		temp += fmt.Sprintf("%.2f", b.AverageTransactionPrice) + ","
		temp += fmt.Sprintf("%.2f", b.LengthOfStay) + ","
		temp += fmt.Sprintf("%.2f", b.LeadTime) + ","
		temp += fmt.Sprintf("%.2f", b.AverageRoomsPerBooking) + ","
		temp += fmt.Sprintf("%.2f", b.AveragePaxPerBooking) + ","
		temp += fmt.Sprintf("%.2f", b.AverageAdultsPerBooking) + ","
		temp += fmt.Sprintf("%.2f", b.AverageChildrenPerBooking) + ","
		temp += fmt.Sprintf("%.2f", b.CancelCount) + ","
		temp += fmt.Sprintf("%.2f", b.CancellationRate) + ","
		temp += fmt.Sprintf("%.2f", b.NoShowRate) + ","

	}

	return temp
}
