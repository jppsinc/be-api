package main

import (
	"errors"
	"fmt"
	"net/http"
	"os"
	"sort"
	"strconv"

	be "bitbucket.org/jppsinc/be-modules/pkg"
	valid "github.com/asaskevich/govalidator"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/aws/aws-sdk-go/service/dynamodb/expression"
)

type Resp struct {
	TargetMonth              string `json:"target_month"`
	ClosingDate              string `json:"closing_date"`
	ReservationCount         string `json:"reservation_count"`
	CancellationCount        string `json:"cancellation_count"`
	ActualReservationCount   string `json:"actual_reservation_count"`
	CPACharge                int    `json:"cpa_charge"`
	Commission               int    `json:"commission"`
	AccountMaintenanceFee    int    `json:"account_maintenance_fee"`
	PortalSalesCommissionFee int    `json:"portal_sales_commission_fee"`
	HotelPay                 int    `json:"hotel_pay"`
	MonthlyFee               int    `json:"monthly_fee"`
	InitialSettingFee        int    `json:"initial_setting_fee"`
	TotalCharge              int    `json:"total_charge"`
	Status                   string `json:"status"`
	BillingMonth             string `json:"billing_month"`
}

func main() {
	lambda.Start(handler)
}

func handler(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {

	org := request.Headers["origin"]
	// Table name
	// tableName := "be_bookings"
	tableName := os.Getenv("INVOICE_TABLE")

	fmt.Println("INVOICE_TABLE :", tableName)

	// Request path parameter
	hotel_id, ok := request.PathParameters["hotel_id"]
	if ok == false || valid.IsUUIDv4(hotel_id) == false {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorInvalidUrlHotelIdNotFound).Error()),
		})
	}

	keyCond := expression.Key("hotel_id").Equal(expression.Value(hotel_id))

	// DynamoDB
	db := be.Dynamodb()
	var input *dynamodb.QueryInput

	expr, err := expression.NewBuilder().WithKeyCondition(keyCond).Build()
	if err != nil {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(err.Error()),
		})
	}

	// input for QueryInput
	input = &dynamodb.QueryInput{
		KeyConditionExpression:    expr.KeyCondition(),
		TableName:                 aws.String(tableName),
		ExpressionAttributeNames:  expr.Names(),
		ExpressionAttributeValues: expr.Values(),
	}

	// GetItem from dynamodb table
	result, err := db.Query(input)
	if err != nil {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(err.Error()),
		})
	}

	invoices := []be.Invoice{}

	err = dynamodbattribute.UnmarshalListOfMaps(result.Items, &invoices)
	if err != nil {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorCouldNotUnMarshalItem).Error()),
		})
	}

	if len(invoices) == 0 {
		return be.ApiResponse(org, http.StatusOK, nil)
	}

	fmt.Println("Invoice count :", len(invoices))

	// sort by Order
	sort.SliceStable(invoices, func(i, j int) bool {
		return invoices[i].TargetMonth > invoices[j].TargetMonth
	})

	resp := make([]Resp, 0, len(invoices))

	processTargetMonth := invoices[0].TargetMonth
	tmp := Resp{}
	tmp.TargetMonth = invoices[0].TargetMonth
	tmp.BillingMonth = invoices[0].BillingMonth
	tmp.Status = invoices[0].Status
	hotelPay := 0
	initialSettingFee := 0
	closingDateForHotelPay := ""
	billingMonthForHotelPay := ""
	closingDateForInitialSettingFee := ""
	billingMonthForInitialSettingFee := ""

	for _, inv := range invoices {
		if processTargetMonth != inv.TargetMonth {
			tmp.TotalCharge = tmp.Commission + tmp.CPACharge + tmp.PortalSalesCommissionFee + tmp.AccountMaintenanceFee + tmp.MonthlyFee
			if processTargetMonth != "" && tmp.ClosingDate != "" {
				resp = append(resp, tmp)
			}
			// ホテペイは他の請求項目と請求月が違うため、単独のデータを作成
			if hotelPay > 0 {
				tmp1 := Resp{
					TargetMonth:            tmp.TargetMonth,
					ClosingDate:            closingDateForHotelPay,
					ReservationCount:       "-",
					CancellationCount:      "-",
					ActualReservationCount: "-",
					HotelPay:               hotelPay,
					TotalCharge:            hotelPay,
					Status:                 "Invoiced",
					BillingMonth:           billingMonthForHotelPay,
				}
				resp = append(resp, tmp1)
			}

			if initialSettingFee > 0 {
				tmp1 := Resp{
					TargetMonth:            tmp.TargetMonth,
					ClosingDate:            closingDateForInitialSettingFee,
					ReservationCount:       "-",
					CancellationCount:      "-",
					ActualReservationCount: "-",
					InitialSettingFee:      initialSettingFee,
					TotalCharge:            initialSettingFee,
					Status:                 "Invoiced",
					BillingMonth:           billingMonthForInitialSettingFee,
				}
				resp = append(resp, tmp1)
			}
			processTargetMonth = inv.TargetMonth
			tmp = Resp{}
			hotelPay = 0
			initialSettingFee = 0
			tmp.TargetMonth = inv.TargetMonth
			tmp.BillingMonth = inv.BillingMonth
			tmp.Status = inv.Status
		}

		if inv.Category == "HotelGHAFee" {
			tmp.ReservationCount = inv.ReservationCount
			tmp.CancellationCount = inv.CancellationCount
			tmp.ActualReservationCount = inv.ActualReservationCount
			tmp.CPACharge, err = strconv.Atoi(inv.BilledAmountWithoutTax)
			tmp.ClosingDate = inv.ClosingDate
			if err != nil {
				return be.ApiResponse(org, http.StatusInternalServerError, Resp{})
			}
		} else if inv.Category == "PsincGHACommissionFee" {
			tmp.Commission, err = strconv.Atoi(inv.BilledAmountWithoutTax)
			if err != nil {
				return be.ApiResponse(org, http.StatusInternalServerError, Resp{})
			}
		} else if inv.Category == "AccountMaintenanceFee" {
			tmp.AccountMaintenanceFee, err = strconv.Atoi(inv.BilledAmountWithoutTax)
			tmp.ClosingDate = inv.ClosingDate
			if err != nil {
				return be.ApiResponse(org, http.StatusInternalServerError, Resp{})
			}
		} else if inv.Category == "HotelPay" {
			hotelPay, err = strconv.Atoi(inv.BilledAmountWithoutTax)
			if err != nil {
				return be.ApiResponse(org, http.StatusInternalServerError, Resp{})
			}
			closingDateForHotelPay = inv.ClosingDate
			billingMonthForHotelPay = inv.BillingMonth
		} else if inv.Category == "PortalSalesCommissionFee" {
			tmp.PortalSalesCommissionFee, err = strconv.Atoi(inv.BilledAmountWithoutTax)
			if err != nil {
				return be.ApiResponse(org, http.StatusInternalServerError, Resp{})
			}
		} else if inv.Category == "MonthlyFee" {
			tmp.MonthlyFee, err = strconv.Atoi(inv.BilledAmountWithoutTax)
			if err != nil {
				return be.ApiResponse(org, http.StatusInternalServerError, Resp{})
			}
		} else if inv.Category == "InitialSettingFee" {
			initialSettingFee, err = strconv.Atoi(inv.BilledAmountWithoutTax)
			if err != nil {
				return be.ApiResponse(org, http.StatusInternalServerError, Resp{})
			}
			closingDateForInitialSettingFee = inv.ClosingDate
			billingMonthForInitialSettingFee = inv.BillingMonth
		} else {
			fmt.Println(errors.New("存在しない費用項目です。"))
		}
	}

	tmp.TotalCharge = tmp.Commission + tmp.CPACharge + tmp.PortalSalesCommissionFee + tmp.AccountMaintenanceFee + tmp.MonthlyFee
	if processTargetMonth != "" && tmp.ClosingDate != "" {
		resp = append(resp, tmp)
	}
	// ホテペイは他の請求項目と請求月が違うため、単独のデータを作成
	if hotelPay > 0 {
		tmp1 := Resp{
			TargetMonth:            tmp.TargetMonth,
			ClosingDate:            closingDateForHotelPay,
			ReservationCount:       "-",
			CancellationCount:      "-",
			ActualReservationCount: "-",
			HotelPay:               hotelPay,
			TotalCharge:            hotelPay,
			Status:                 "Invoiced",
			BillingMonth:           billingMonthForHotelPay,
		}
		resp = append(resp, tmp1)
	}

	if initialSettingFee > 0 {
		tmp1 := Resp{
			TargetMonth:            tmp.TargetMonth,
			ClosingDate:            closingDateForInitialSettingFee,
			ReservationCount:       "-",
			CancellationCount:      "-",
			ActualReservationCount: "-",
			InitialSettingFee:      initialSettingFee,
			TotalCharge:            initialSettingFee,
			Status:                 "Invoiced",
			BillingMonth:           billingMonthForInitialSettingFee,
		}
		resp = append(resp, tmp1)
	}
	fmt.Println("resp count :", len(resp))

	return be.ApiResponse(org, http.StatusOK, resp)
}
