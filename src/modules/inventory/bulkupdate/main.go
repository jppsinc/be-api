package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"runtime"
	"sync"
	"time"

	be "bitbucket.org/jppsinc/be-modules/pkg"

	valid "github.com/asaskevich/govalidator"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/expression"
)

type BulkUpdateRequest struct {
	RoomIDList       []string `json:"room_id_list"`
	StartDate        string   `json:"start_date"`
	EndDate          string   `json:"end_date"`
	DaysOfWeek       []string `json:"days_of_week"` // All,Sunday,Monday,Tuesday,Wednesday,Thursday,Friday,Holiday,BeforeHoliday
	SalesStatus      string   `json:"sales_status"` // BulkUpdate, OnSale, StopSale
	InventoryCount   int64    `json:"inventory_count"`
	BulkUpdateOption string   `json:"bulk_update_option"` // NoChange, OnSale: if stopped start it
}

type Holiday struct {
	Items []struct {
		Start struct {
			Date string `json:"date"`
		} `json:"start"`
		End struct {
			Date string `json:"date"`
		} `json:"end"`
	} `json:"items"`
}

var wg sync.WaitGroup

func handler(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {

	org := request.Headers["origin"]
	// Table name
	// tableName := "be_inventories-dev"
	tableName := os.Getenv("TABLE_NAME")

	runtime.GOMAXPROCS(runtime.NumCPU())

	// Change JSON request data to struct  : just to check validity of request data
	// var sds be.Inventory
	var sds BulkUpdateRequest

	if err := json.Unmarshal([]byte(request.Body), &sds); err != nil {
		fmt.Println("Unmarshal err :", err)
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorCouldNotUnMarshalItem).Error()),
		})
	}

	hotel_id, ok := request.PathParameters["hotel_id"]
	if ok == false || valid.IsUUIDv4(hotel_id) == false {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorInvalidUrlHotelIdNotFound).Error()),
		})
	}

	// DynamoDB
	db := be.Dynamodb()

	if len(sds.DaysOfWeek) > 0 {
		var updateDates []string
		dayList := make([]string, 0)
		for _, days := range sds.DaysOfWeek {
			switch days {
			case "All":
				updateDates, _ = GetHolidays(sds.StartDate, sds.EndDate, "All")
				fmt.Println("All : ", updateDates)

			case "BeforeHoliday":
				updateDates, _ = GetHolidays(sds.StartDate, sds.EndDate, "BeforeHoliday")
				fmt.Println("BeforeHoliday : ", updateDates)

			case "Holiday":
				updateDates, _ = GetHolidays(sds.StartDate, sds.EndDate, "Holiday")
				fmt.Println("Holiday : ", updateDates)

			default:
				dayList = append(dayList, days)
			}

			if len(dayList) > 0 {
				otherDates, _ := GetDateFromDay(sds.StartDate, sds.EndDate, dayList)
				updateDatesWithDuplicate := append(updateDates, otherDates...)

				// remove duplicate dates
				updateDates = removeDuplicateStr(updateDatesWithDuplicate)
				fmt.Println("otherDates : ", updateDates)
			}
		}

		fmt.Println("final days : ", updateDates)
		InventoryUpdateForAll(hotel_id, sds, db, tableName, updateDates)
	}

	return be.ApiResponse(org, http.StatusOK, sds)
}

func InventoryUpdateRoutine(hotelId string, sds BulkUpdateRequest, db *dynamodb.DynamoDB, tableName string, useDate string, hash string) {

	defer wg.Done()
	start := time.Now()
	var update expression.UpdateBuilder

	switch sds.SalesStatus {
	case "OnSale":
		update = expression.Set(expression.Name("sales_category"), expression.Value(true)).
			Set(expression.Name("updated_at"), expression.Value(be.DateTimeStamp())).
			Set(expression.Name("created_at"), expression.Name("created_at").IfNotExists(expression.Value(be.DateTimeStamp()))).
			Set(expression.Name("use_date"), expression.Value(useDate))

	case "StopSale":
		update = expression.Set(expression.Name("sales_category"), expression.Value(false)).
			Set(expression.Name("updated_at"), expression.Value(be.DateTimeStamp())).
			Set(expression.Name("created_at"), expression.Name("created_at").IfNotExists(expression.Value(be.DateTimeStamp()))).
			Set(expression.Name("use_date"), expression.Value(useDate))

	case "BulkUpdate":
		if sds.BulkUpdateOption == "NoChange" {
			update = expression.Set(expression.Name("vacent_count"), expression.Value(sds.InventoryCount)).
				Set(expression.Name("updated_at"), expression.Value(be.DateTimeStamp())).
				Set(expression.Name("created_at"), expression.Name("created_at").IfNotExists(expression.Value(be.DateTimeStamp()))).
				Set(expression.Name("sales_category"), expression.Name("sales_category").IfNotExists(expression.Value(true))).
				Set(expression.Name("use_date"), expression.Value(useDate))
		} else {
			update = expression.Set(expression.Name("vacent_count"), expression.Value(sds.InventoryCount)).
				Set(expression.Name("sales_category"), expression.Value(true)).
				Set(expression.Name("updated_at"), expression.Value(be.DateTimeStamp())).
				Set(expression.Name("created_at"), expression.Name("created_at").IfNotExists(expression.Value(be.DateTimeStamp()))).
				Set(expression.Name("use_date"), expression.Value(useDate))
		}
	}

	expr1, err := expression.NewBuilder().
		WithUpdate(update).
		Build()

	inputInc := &dynamodb.UpdateItemInput{
		Key: map[string]*dynamodb.AttributeValue{
			"hotel_id": {
				S: aws.String(hotelId),
			},
			"hotel_room_id_use_date": {
				S: aws.String(hash),
			},
		},
		TableName:                 aws.String(tableName),
		ExpressionAttributeNames:  expr1.Names(),
		ExpressionAttributeValues: expr1.Values(),
		UpdateExpression:          expr1.Update(),
	}

	_, err = db.UpdateItem(inputInc)
	if err != nil {
		fmt.Println("UpdateItem err ", err)
	}

	duration := time.Since(start)
	fmt.Printf("\nUpdate successful : CPU Count : %v : Time of execution : %v\n", runtime.NumCPU(), duration)
}

func InventoryUpdateForAll(hotelId string, sds BulkUpdateRequest, db *dynamodb.DynamoDB, tableName string, updateDates []string) {

	for _, useDate := range updateDates {
		for _, roomId := range sds.RoomIDList {
			hash := hotelId + "#" + roomId + "#" + useDate
			fmt.Println("InventoryUpdateForAll hash : ", hash)

			wg.Add(1)
			go InventoryUpdateRoutine(hotelId, sds, db, tableName, useDate, hash)
		}
	}

	wg.Wait()
}

func main() {
	lambda.Start(handler)
}

func GetHolidays(timeMin string, timeMax string, dayType string) ([]string, error) {

	days := make([]string, 0)
	if dayType == "All" {
		startDate, _ := time.Parse("2006-01-02", timeMin)
		endDate, _ := time.Parse("2006-01-02", timeMax)

		endDate = endDate.AddDate(0, 0, 1)
		for {
			if startDate.Before(endDate) {
				days = append(days, startDate.Format("2006-01-02"))
				startDate = startDate.AddDate(0, 0, 1)
			} else {
				break
			}
		}
	} else {
		cinDate, _ := time.Parse("2006-01-02", timeMin)

		timeMin = timeMin + "T00:00:00Z"
		timeMax = timeMax + "T00:00:00Z"

		key := "AIzaSyAs6bBX_abHHXCmfLi3tPVxw6BxZX2sIA8"
		country := "japanese"
		lang := "ja"
		calendarId := country + "__" + lang + "@holiday.calendar.google.com"
		URL := "https://www.googleapis.com/calendar/v3/calendars/" + calendarId + "/events"

		maxResults := 365
		orderBy := "startTime"
		singleEvents := true

		URL = fmt.Sprintf("%v?key=%v&timeMin=%v&timeMax=%v&maxResults=%v&orderBy=%v&singleEvents=%v", URL, key, timeMin, timeMax, maxResults, orderBy, singleEvents)

		var sds Holiday
		resp, err := http.Get(URL)
		if err != nil {
			return days, err
		}

		//We Read the response body on the line below.
		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			log.Fatalln(err)
		}
		if err := json.Unmarshal([]byte(body), &sds); err != nil {
			return days, err
		}

		for _, holiday := range sds.Items {

			// get one back date to apply date.before
			cinDate = cinDate.AddDate(0, 0, -1)

			startDate, _ := time.Parse("2006-01-02", holiday.Start.Date)
			endDate, _ := time.Parse("2006-01-02", holiday.End.Date)

			// endDate = endDate.AddDate(0, 0, 1)
			for {
				if startDate.Before(endDate) {
					if dayType == "Holiday" {
						days = append(days, startDate.Format("2006-01-02"))
					} else {
						beforeHoliday := startDate.AddDate(0, 0, -1)
						if cinDate.Before(beforeHoliday) {
							fmt.Printf("***** cinDate : %v : beforeHoliday : %v", cinDate, beforeHoliday)
							days = append(days, beforeHoliday.Format("2006-01-02"))
						}
					}
					startDate = startDate.AddDate(0, 0, 1)
				} else {
					break
				}
			}
		}
	}

	return days, nil
}

func GetDateFromDay(timeMin string, timeMax string, dayList []string) ([]string, error) {

	days := make([]string, 0)
	startDate, _ := time.Parse("2006-01-02", timeMin)
	endDate, _ := time.Parse("2006-01-02", timeMax)

	endDate = endDate.AddDate(0, 0, 1)
	for {
		if startDate.Before(endDate) {

			dayOfDate := startDate.Weekday().String()
			for _, d := range dayList {
				if d == dayOfDate {
					days = append(days, startDate.Format("2006-01-02"))
				}
			}

			startDate = startDate.AddDate(0, 0, 1)
		} else {
			break
		}
	}

	return days, nil
}

func removeDuplicateStr(strSlice []string) []string {
	allKeys := make(map[string]bool)
	list := []string{}
	for _, item := range strSlice {
		if _, value := allKeys[item]; !value {
			allKeys[item] = true
			list = append(list, item)
		}
	}
	return list
}
