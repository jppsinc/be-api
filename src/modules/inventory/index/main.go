package main

import (
	"errors"
	"fmt"
	"net/http"
	"os"
	"strings"
	"time"

	be "bitbucket.org/jppsinc/be-modules/pkg"
	valid "github.com/asaskevich/govalidator"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/aws/aws-sdk-go/service/dynamodb/expression"
)

type InvLastKey struct {
	HotelID            string `json:"hotel_id"`
	PlanRoomUseDatePax string `json:"hotel_room_id_use_date"`
}

func InvQueryFirst(expr expression.Expression, tableName string) *dynamodb.QueryInput {
	input := &dynamodb.QueryInput{
		KeyConditionExpression:    expr.KeyCondition(),
		TableName:                 aws.String(tableName),
		FilterExpression:          expr.Filter(),
		ExpressionAttributeNames:  expr.Names(),
		ExpressionAttributeValues: expr.Values(),
	}

	return input
}

func InvQueryNext(expr expression.Expression, tableName string, es InvLastKey) *dynamodb.QueryInput {
	input := &dynamodb.QueryInput{
		KeyConditionExpression:    expr.KeyCondition(),
		TableName:                 aws.String(tableName),
		FilterExpression:          expr.Filter(),
		ExpressionAttributeNames:  expr.Names(),
		ExpressionAttributeValues: expr.Values(),
		ExclusiveStartKey: map[string]*dynamodb.AttributeValue{
			"hotel_id": {
				S: aws.String(es.HotelID),
			},
			"hotel_room_id_use_date": {
				S: aws.String(es.PlanRoomUseDatePax),
			},
		},
	}

	return input
}

func handler(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {

	org := request.Headers["origin"]
	// Table name

	// tableName := "be_inventories-prod"
	// rTableName := "be_rooms-prod"

	tableName := os.Getenv("TABLE_NAME")
	rTableName := os.Getenv("ROOM_TABLE_NAME")

	// Request path parameter
	hotel_id, ok := request.PathParameters["hotel_id"]
	if ok == false || valid.IsUUIDv4(hotel_id) == false {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorInvalidUrlHotelIdNotFound).Error()),
		})
	}

	keyCond := expression.Key("hotel_id").Equal(expression.Value(hotel_id))

	var finalFilter expression.ConditionBuilder
	var filter []expression.ConditionBuilder

	var startDate string
	var endDate string

	// query string parameter
	if len(request.QueryStringParameters) == 0 {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorQueryStringParameterNotFound).Error()),
		})
	} else {
		startDate, ok = request.QueryStringParameters["startDate"]
		if ok == false || len(strings.TrimSpace(startDate)) == 0 {
			return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
				ErrorMsg: aws.String(errors.New(be.ErrorQueryStringParameterNotFound).Error()),
			})
		} else {
			filter = append(filter, expression.Name("use_date").GreaterThanEqual(expression.Value(startDate)))
		}

		endDate, ok = request.QueryStringParameters["endDate"]
		if ok == false || len(strings.TrimSpace(endDate)) == 0 {
			return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
				ErrorMsg: aws.String(errors.New(be.ErrorQueryStringParameterNotFound).Error()),
			})
		} else {
			filter = append(filter, expression.Name("use_date").LessThanEqual(expression.Value(endDate)))
		}
	}

	if len(filter) != 0 {
		for i, temp := range filter {
			if i == 0 {
				finalFilter = temp
			} else {
				finalFilter = finalFilter.And(temp)
			}
		}
	}

	// var input *dynamodb.QueryInput

	expr, err := expression.NewBuilder().WithKeyCondition(keyCond).WithFilter(finalFilter).Build()
	if err != nil {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(err.Error()),
		})
	}

	// DynamoDB
	db := be.Dynamodb()

	// ------------------
	final := new([]*be.InventoryIndex)

	var lastEvaluatedKey InvLastKey
	var result *dynamodb.QueryOutput

	for {
		pp := new([]*be.InventoryIndex)
		if lastEvaluatedKey == (InvLastKey{}) {
			result, err = db.Query(InvQueryFirst(expr, tableName))
		} else {
			result, err = db.Query(InvQueryNext(expr, tableName, lastEvaluatedKey))
		}

		if err != nil {
			fmt.Println(err)
		}

		err = dynamodbattribute.UnmarshalListOfMaps(result.Items, &pp)
		if err != nil {
			fmt.Println(err)
		}

		*final = append(*final, *pp...)

		if result.LastEvaluatedKey == nil {
			break
		}

		err = dynamodbattribute.UnmarshalMap(result.LastEvaluatedKey, &lastEvaluatedKey)
		if err != nil {
			fmt.Println(err)
		}
	}
	// ------------------

	rooms, err := GetRoomList(hotel_id, db, rTableName)
	if err != nil {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(err.Error()),
		})
	}

	arrayMap := make([]map[string][]be.InventoryIndex, 0)
	for _, room := range *rooms {
		fmt.Println("Room : ", room.ID)
		temp := make(map[string][]be.InventoryIndex)

		layout := "2006-01-02"
		sDate, _ := time.Parse(layout, startDate)
		eDate, _ := time.Parse(layout, endDate)
		eDate_1 := eDate.AddDate(0, 0, 1)

		for {
			if sDate.Before(eDate_1) {
				flg := true
				for _, el := range *final {
					roomID := strings.Split(el.HotelRoomIDUseDate, "#")
					if roomID[1] == room.ID && roomID[2] == sDate.Format(layout) {
						el.IsInvSet = true
						temp[room.ID] = append(temp[room.ID], *el)
						flg = false
						break
					}
				}
				if flg {
					tempInv := be.InventoryIndex{}
					tempInv.HotelRoomIDUseDate = hotel_id + "#" + room.ID + "#" + sDate.Format(layout)
					tempInv.HotelID = hotel_id
					tempInv.RoomType = room.RoomType
					tempInv.SalesCategory = true
					tempInv.IsInvSet = false
					tempInv.UseDate = sDate.Format(layout)
					temp[room.ID] = append(temp[room.ID], tempInv)
				}
				sDate = sDate.AddDate(0, 0, 1)
			} else {
				break
			}
		}
		arrayMap = append(arrayMap, temp)
	}
	return be.ApiResponse(org, http.StatusOK, arrayMap)
}

func GetRoomList(hotel_id string, db *dynamodb.DynamoDB, rTableName string) (*[]*be.Room, error) {

	keyCond := expression.Key("hotel_id").Equal(expression.Value(hotel_id))
	filter := expression.Name("status").NotEqual(expression.Value("Creating"))

	item := new([]*be.Room)
	expr, err := expression.NewBuilder().WithKeyCondition(keyCond).WithFilter(filter).Build()
	if err != nil {
		return item, err
	}

	// input for GetItem
	input := &dynamodb.QueryInput{
		KeyConditionExpression:    expr.KeyCondition(),
		TableName:                 aws.String(rTableName),
		ExpressionAttributeNames:  expr.Names(),
		ExpressionAttributeValues: expr.Values(),
		FilterExpression:          expr.Filter(),
	}

	// GetItem from dynamodb table
	result, err := db.Query(input)
	if err != nil {
		return item, err
	}

	err = dynamodbattribute.UnmarshalListOfMaps(result.Items, &item)
	if err != nil {
		return item, err
	}

	fmt.Println("eDate_1 : ", len(*item))

	return item, nil
}

func main() {
	lambda.Start(handler)
}
