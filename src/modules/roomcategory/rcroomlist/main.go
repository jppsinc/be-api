package main

import (
	"errors"
	"net/http"
	"os"

	be "bitbucket.org/jppsinc/be-modules/pkg"
	valid "github.com/asaskevich/govalidator"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/aws/aws-sdk-go/service/dynamodb/expression"
)

type RCRoomList struct {
	ID       string  `json:"id"`
	RoomList []RList `json:"room_list"`
}

type RList struct {
	ID       string       `json:"id"`
	RoomType be.MultiLang `json:"room_type"`
}

func handler(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {

	org := request.Headers["origin"]

	// Table name
	// tableName := "be_room_categories-dev"
	// roomTableName := "be_rooms-dev"

	tableName := os.Getenv("TABLE_NAME")
	roomTableName := os.Getenv("ROOM_TABLE_NAME")

	// Request path parameter
	hotel_id, ok := request.PathParameters["hotel_id"]
	if ok == false || valid.IsUUIDv4(hotel_id) == false {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorInvalidUrlHotelIdNotFound).Error()),
		})
	}

	// DynamoDB
	db := be.Dynamodb()

	// input for GetItem
	input := &dynamodb.QueryInput{
		ExpressionAttributeValues: map[string]*dynamodb.AttributeValue{
			":hotel_id": {
				S: aws.String(hotel_id),
			},
		},
		KeyConditionExpression: aws.String("hotel_id = :hotel_id"),
		TableName:              aws.String(tableName),
	}

	// GetItem from dynamodb table
	result, err := db.Query(input)
	if err != nil {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorFailedToFetchRecord).Error()),
		})
	}

	if len(result.Items) > 0 {

		rooms, err := GetRoomList(hotel_id, db, roomTableName)

		item := new([]*RCRoomList)
		err = dynamodbattribute.UnmarshalListOfMaps(result.Items, &item)
		if err != nil {
			return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
				ErrorMsg: aws.String(errors.New(be.ErrorCouldNotUnMarshalItem).Error()),
			})
		}

		for _, room := range *rooms {
			for _, rrg := range room.RoomCategoryList {
				for i := 0; i < len(*item); i++ {
					if (*item)[i].ID == rrg.ID {
						(*item)[i].RoomList = append((*item)[i].RoomList, RList{ID: room.ID, RoomType: room.RoomType})
					}
				}
			}
		}

		for i := 0; i < len(*item); i++ {
			if len(*&(*item)[i].RoomList) == 0 {
				*&(*item)[i].RoomList = make([]RList, 0)
			}
		}

		return be.ApiResponse(org, http.StatusOK, item)
	} else {
		item := []*be.EmptyStruct{}
		return be.ApiResponse(org, http.StatusOK, item)
	}
}

func main() {
	lambda.Start(handler)
}

func GetRoomList(hotel_id string, db *dynamodb.DynamoDB, rTableName string) (*[]*be.Room, error) {

	keyCond := expression.Key("hotel_id").Equal(expression.Value(hotel_id))

	item := new([]*be.Room)
	expr, err := expression.NewBuilder().WithKeyCondition(keyCond).Build()
	if err != nil {
		return item, err
	}

	// input for GetItem
	input := &dynamodb.QueryInput{
		KeyConditionExpression:    expr.KeyCondition(),
		TableName:                 aws.String(rTableName),
		ExpressionAttributeNames:  expr.Names(),
		ExpressionAttributeValues: expr.Values(),
		ProjectionExpression:      aws.String("id,room_type,room_category_list"),
	}

	// GetItem from dynamodb table
	result, err := db.Query(input)
	if err != nil {
		return item, err
	}

	err = dynamodbattribute.UnmarshalListOfMaps(result.Items, &item)
	if err != nil {
		return item, err
	}

	return item, nil
}
