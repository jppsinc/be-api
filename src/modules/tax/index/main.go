package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"os"
	"strings"

	be "bitbucket.org/jppsinc/be-modules/pkg"
	valid "github.com/asaskevich/govalidator"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
)

func handler(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {

	org := request.Headers["origin"]
	// Table name
	// tableName := "be_taxes"

	tableName := os.Getenv("TABLE_NAME")
	tableNameCT := os.Getenv("CT_TABLE_NAME")
	tableNameGH := os.Getenv("GH_TABLE")

	// tableName := "be_taxes-prod"
	// tableNameCT := "be_consumption_taxes-prod"
	// tableNameGH := "be_group_hotel-prod"

	// das := strings.Contains(tableName, "-")
	// if das == false {
	// 	tableNameCT = "be_comsumption_taxes"
	// 	tableNameGH = "be_group_hotel"
	// } else {
	// 	en := strings.Split(tableName, "-")
	// 	if en[1] == "prod" {
	// 		tableNameCT = "be_comsumption_taxes" + "-prod"
	// 		tableNameGH = "be_group_hotel" + "-prod"
	// 	}
	// 	if en[1] == "test" {
	// 		tableNameCT = "be_comsumption_taxes" + "-test"
	// 		tableNameGH = "be_group_hotel" + "-test"
	// 	}
	// 	if en[1] == "dev" {
	// 		tableNameCT = "be_comsumption_taxes" + "-dev"
	// 		tableNameGH = "be_group_hotel" + "-dev"
	// 	}
	// }

	// if len(tableNameCT) == 0 {
	// 	tableNameCT = "be_comsumption_taxes"
	// }

	// tableNameCT := "be_comsumption_taxes"
	// tableNameGH := "be_group_hotel"

	// Request path parameter
	hotel_id, ok := request.PathParameters["hotel_id"]
	if ok == false || valid.IsUUIDv4(hotel_id) == false {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorInvalidUrlHotelIdNotFound).Error()),
		})
	}

	// DynamoDB
	db := be.Dynamodb()

	input := &dynamodb.BatchGetItemInput{
		RequestItems: map[string]*dynamodb.KeysAndAttributes{
			tableName: {
				Keys: []map[string]*dynamodb.AttributeValue{
					{
						"hotel_id": &dynamodb.AttributeValue{
							S: aws.String(hotel_id),
						},
						"tax_type": &dynamodb.AttributeValue{
							S: aws.String("ServiceTax"),
						},
					},
					{
						"hotel_id": &dynamodb.AttributeValue{
							S: aws.String(hotel_id),
						},
						"tax_type": &dynamodb.AttributeValue{
							S: aws.String("AccomodationTax"),
						},
					},
					{
						"hotel_id": &dynamodb.AttributeValue{
							S: aws.String(hotel_id),
						},
						"tax_type": &dynamodb.AttributeValue{
							S: aws.String("HotSpringTax"),
						},
					},
				},
			},
			tableNameCT: {
				Keys: []map[string]*dynamodb.AttributeValue{
					{
						"tax_type": &dynamodb.AttributeValue{
							S: aws.String("ConsumptionTax"),
						},
					},
				},
			},
			tableNameGH: {
				Keys: []map[string]*dynamodb.AttributeValue{
					{
						"hotel_id": &dynamodb.AttributeValue{
							S: aws.String(hotel_id),
						},
					},
				},
			},
		},
	}

	result, err := db.BatchGetItem(input)
	if err != nil {
		fmt.Println("err 1 : ", err)
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorFailedToFetchRecord).Error()),
		})
	}

	// separate all the responses
	itemGH := new(be.GroupHotel)
	err = dynamodbattribute.UnmarshalMap(result.Responses[tableNameGH][0], &itemGH)
	if err != nil {
		fmt.Println("err 2 : ", err)
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorCouldNotUnMarshalItem).Error()),
		})
	}

	item := new([]*be.TaxItem)
	err = dynamodbattribute.UnmarshalListOfMaps(result.Responses[tableName], &item)
	if err != nil {
		fmt.Println("err 3 : ", err)
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorCouldNotUnMarshalItem).Error()),
		})
	}

	for i := 0; i < len(*item); i++ {
		if *&(*item)[i].TaxType == "ServiceTax" {
			*&(*item)[i].ServiceTaxRoundingMethod = itemGH.ServiceTaxRoundingMethod
		}
	}

	if len(*item) == 0 {
		item := make([]*be.TaxItem, 0, 2)
		tmp := be.TaxItem{}
		tmp.TaxType = "ServiceTax"
		p1 := be.Period{}
		// p2 := be.Period{}

		p1.Amount = 0
		p1.EndDate = nil
		p1.StartDate = nil
		p1.Unit = "%"

		tmp.Period1 = &p1

		var s1 string = ""
		tmp.AppliedOn = &s1
		tmp.HotelID = hotel_id

		tmp.ServiceTaxRoundingMethod = itemGH.ServiceTaxRoundingMethod

		item = append(item, &tmp)

		tmpc := be.TaxItem{}
		tmpc.TaxType = "ConsumptionTax"
		tmpc.HotelID = hotel_id
		tmpc.ConsumptionTaxRoundingMethod = itemGH.ConsumptionTaxRoundingMethod

		item = append(item, &tmpc)

		tmps := be.TaxItem{}
		tmps.TaxType = "HotSpringTax"

		p8 := be.MultiLang{}
		p8.En = ""
		p8.Ja = ""
		tmps.Message = &p8

		tmps.Message.En = ""
		tmps.Message.Ja = ""
		tmps.HotelID = hotel_id
		item = append(item, &tmps)

		tmpa := be.TaxItem{}
		tmpa.TaxType = "AccomodationTax"

		p9 := be.MultiLang{}
		p9.En = ""
		p9.Ja = ""
		tmpa.Message = &p9

		tmpa.HotelID = hotel_id
		item = append(item, &tmpa)

		return be.ApiResponse(org, http.StatusOK, item)
	} else {
		item1 := new(be.TaxItem)
		err = dynamodbattribute.UnmarshalMap(result.Responses[tableNameCT][0], &item1)
		if err != nil {
			fmt.Println("err 4 : ", err)
			return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
				ErrorMsg: aws.String(errors.New(be.ErrorCouldNotUnMarshalItem).Error()),
			})
		}

		item1.ConsumptionTaxRoundingMethod = itemGH.ConsumptionTaxRoundingMethod

		// convert structs into byte string
		stringBody, _ := json.Marshal(&item)
		stringBody1, _ := json.Marshal(&item1)
		fmt.Println("item1:", item1.ConsumptionTaxRoundingMethod)
		fmt.Println("item1:", item1.ServiceTaxRoundingMethod)

		// convert byte string to string
		str1 := string(stringBody)
		str2 := string(stringBody1)

		// remove array closing ]
		str1 = strings.TrimSuffix(str1, "]")
		// append , and CTax
		final := str1 + "," + str2 + "]"

		origins := map[string]bool{"https://dev.d7r69723er9j4.amplifyapp.com": true,
			"https://master.d7r69723er9j4.amplifyapp.com": true,
			"https://localhost:3000":                      true,
			"http://localhost:3000":                       true,
			"https://rc-booking.com":                      true,
			"https://yoyaku.rc-booking.com":               true,
			"https://kanri.rc-booking.com":                true,
			"https://test-yoyaku.rc-booking.com":          true,
			"https://dev-yoyaku.rc-booking.com":           true,
			"https://test-kanri.rc-booking.com":           true,
			"https://dev-kanri.rc-booking.com":            true,
			"https://preview-test-kanri.rc-booking.com":   true,
			"https://preview-test-yoyaku.rc-booking.com":  true,
		}

		// prepare response and send
		resp := events.APIGatewayProxyResponse{}
		resp.StatusCode = http.StatusOK

		if origins[org] == true {
			resp.Headers = map[string]string{
				"Access-Control-Allow-Origin":      org,
				"Access-Control-Allow-Headers":     "origin,Accept,Authorization,Content-Type,X-Amz-Date,X-Api-Key,X-Amz-Security-Token",
				"Access-Control-Allow-Methods":     "DELETE,GET,OPTIONS,POST,PUT",
				"Content-Type":                     "application/json",
				"Access-Control-Allow-Credentials": "true",
			}
		} else {
			resp.Headers = map[string]string{
				"Access-Control-Allow-Origin":  "*",
				"Access-Control-Allow-Headers": "origin,Accept,Authorization,Content-Type,X-Amz-Date,X-Api-Key,X-Amz-Security-Token",
				"Access-Control-Allow-Methods": "DELETE,GET,OPTIONS,POST,PUT",
				"Content-Type":                 "application/json",
			}
		}

		// resp.Headers = map[string]string{
		// 	"Access-Control-Allow-Origin":  "*",
		// 	"Access-Control-Allow-Headers": "origin,Accept,Authorization,Content-Type",
		// 	"Content-Type":                 "application/json",
		// }
		resp.Body = string(final)
		return resp, nil
	}
}

func main() {
	lambda.Start(handler)
}
