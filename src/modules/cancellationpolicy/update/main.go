package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"os"

	be "bitbucket.org/jppsinc/be-modules/pkg"

	valid "github.com/asaskevich/govalidator"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/aws/aws-sdk-go/service/dynamodb/expression"
)

type MultiLang struct {
	Ja string `json:"ja"`
	En string `json:"en"`
}

type CC struct {
	DaysBeforeArrival int64   `json:"days_before_arrival"`
	Amount            float64 `json:"amount"`
}

func handler(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {

	org := request.Headers["origin"]
	// Table name
	// tableName := "be_cancellation_policies"
	// pTableName := "be_plans"

	tableName := os.Getenv("TABLE_NAME")
	pTableName := os.Getenv("PLAN_TABLE_NAME")

	// Change JSON request data to struct : just to check validity of request data
	var inq be.CancellationPolicy

	if err := json.Unmarshal([]byte(request.Body), &inq); err != nil {
		fmt.Println(err)
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorCouldNotUnMarshalItem).Error()),
		})
	}

	// Request path parameter
	id, ok := request.PathParameters["id"]
	if ok == false || valid.IsUUIDv4(id) == false {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorInvalidUrlIdNotFound).Error()),
		})
	}

	hotel_id, ok := request.PathParameters["hotel_id"]
	if ok == false || valid.IsUUIDv4(hotel_id) == false {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorInvalidUrlHotelIdNotFound).Error()),
		})
	} else {
		inq.HotelID = hotel_id
		inq.ID = id
		inq.UpdatedAt = be.DateTimeStamp()
	}

	// DynamoDB
	db := be.Dynamodb()

	// check if row with this key exist
	// input for GetItem
	input := &dynamodb.GetItemInput{
		Key: map[string]*dynamodb.AttributeValue{
			"hotel_id": {
				S: aws.String(hotel_id),
			},
			"id": {
				S: aws.String(id),
			},
		},
		TableName: aws.String(tableName),
	}

	// GetItem from dynamodb table
	result, err := db.GetItem(input)
	if err != nil {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorFailedToFetchRecord).Error()),
		})
	}

	if len(result.Item) > 0 {
		// change struct data to json data, this data will be stored to database
		av, err := dynamodbattribute.MarshalMap(inq)
		if err != nil {
			return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
				ErrorMsg: aws.String(errors.New(be.ErrorCouldNotMarshalItem).Error()),
			})
		}

		ReturnItem := "SIZE"
		input := &dynamodb.PutItemInput{
			Item:                        av,
			TableName:                   aws.String(tableName),
			ReturnItemCollectionMetrics: &ReturnItem,
		}

		_, err = db.PutItem(input)
		if err != nil {
			fmt.Println(err)
			return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
				ErrorMsg: aws.String(errors.New(be.ErrorCouldNotDynamoPutItem).Error()),
			})
		} else {
			// update plan
			keyCond := expression.Key("hotel_id").Equal(expression.Value(hotel_id))
			filter := expression.Name("cancellation_policy.id").Equal(expression.Value(inq.ID))
			expr, err := expression.NewBuilder().WithKeyCondition(keyCond).WithFilter(filter).Build()
			if err != nil {
				return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
					ErrorMsg: aws.String(errors.New(be.ErrorExpressionBuilder).Error()),
				})
			}

			// input for GetItem
			input := &dynamodb.QueryInput{
				KeyConditionExpression:    expr.KeyCondition(),
				TableName:                 aws.String(pTableName),
				ExpressionAttributeNames:  expr.Names(),
				ExpressionAttributeValues: expr.Values(),
				FilterExpression:          expr.Filter(),
				ProjectionExpression:      aws.String("id"),
			}

			// GetItem from dynamodb table
			result, err := db.Query(input)
			if err != nil {
				return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
					ErrorMsg: aws.String(errors.New(be.ErrorFailedToFetchRecord).Error()),
				})
			}

			item := new([]*be.Plan)
			err = dynamodbattribute.UnmarshalListOfMaps(result.Items, &item)
			if err != nil {
				return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
					ErrorMsg: aws.String(errors.New(be.ErrorCouldNotUnMarshalItem).Error()),
				})
			}

			// iterate through plan and update
			// create  slice of pointer of TransactWriteItem
			var transactItems []*dynamodb.TransactWriteItem

			update := expression.
				Set(expression.Name("cancellation_policy"), expression.Value(inq))

			expr1, err := expression.NewBuilder().
				WithUpdate(update).
				Build()
			if err != nil {
				return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
					ErrorMsg: aws.String(errors.New(be.ErrorExpressionBuilder).Error()),
				})
			}

			for j := 0; j < len(*item); j++ {

				key := *&(*item)[j].ID
				if key == "None" || len(key) == 0 {
					continue
				}

				fmt.Println("plan id =============================== :", key)
				fmt.Println("hotel id =============================== :", hotel_id)

				// create put item for transactItems
				transactItems = append(transactItems, &dynamodb.TransactWriteItem{
					Update: &dynamodb.Update{
						TableName: aws.String(pTableName),

						Key: map[string]*dynamodb.AttributeValue{
							"hotel_id": {
								S: aws.String(hotel_id),
							},
							"id": {
								S: aws.String(key),
							},
						},
						ExpressionAttributeNames:  expr1.Names(),
						ExpressionAttributeValues: expr1.Values(),
						UpdateExpression:          expr1.Update(),
					},
				})

				// execute TransactWriteItems if there are 25 elements OR iteration number is equal to len(useDate)-1
				if len(transactItems) == 25 || j == len(*item)-1 {
					// execute transact operation

					if _, err := db.TransactWriteItems(&dynamodb.TransactWriteItemsInput{
						TransactItems: transactItems,
					}); err != nil {
						fmt.Println("transactItems ", transactItems)
						fmt.Println("err ", err)
						return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
							ErrorMsg: aws.String(errors.New(be.ErrorTransactWriteItems).Error()),
						})
					}
					fmt.Println("TransactWriteItems successful")
					transactItems = nil
				}
			}
		}
		return be.ApiResponse(org, http.StatusOK, &inq)
	} else {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorPathParameterInvalid).Error()),
		})
	}

}

func main() {
	lambda.Start(handler)
}
