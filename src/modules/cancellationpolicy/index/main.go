package main

import (
	"errors"
	"net/http"
	"os"

	be "bitbucket.org/jppsinc/be-modules/pkg"
	valid "github.com/asaskevich/govalidator"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
)

func handler(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {

	org := request.Headers["origin"]
	// Table name
	// tableName := "be_cancellation_policies"
	// pTableName := "be_post_cancellation_policies"

	tableName := os.Getenv("TABLE_NAME")
	pTableName := os.Getenv("POST_TABLE_NAME")

	// Request path parameter
	hotel_id, ok := request.PathParameters["hotel_id"]
	if ok == false || valid.IsUUIDv4(hotel_id) == false {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorInvalidUrlHotelIdNotFound).Error()),
		})
	}

	// DynamoDB
	db := be.Dynamodb()

	// input for GetItem
	input := &dynamodb.QueryInput{
		ExpressionAttributeValues: map[string]*dynamodb.AttributeValue{
			":hotel_id": {
				S: aws.String(hotel_id),
			},
		},
		KeyConditionExpression: aws.String("hotel_id = :hotel_id"),
		TableName:              aws.String(tableName),
	}

	// GetItem from dynamodb table
	result, err := db.Query(input)
	if err != nil {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorFailedToFetchRecord).Error()),
		})
	}

	item := new([]*be.CancellationPolicy)
	err = dynamodbattribute.UnmarshalListOfMaps(result.Items, &item)
	if err != nil {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorCouldNotUnMarshalItem).Error()),
		})
	}

	// input for GetItem
	inputP := &dynamodb.GetItemInput{
		Key: map[string]*dynamodb.AttributeValue{
			"hotel_id": {
				S: aws.String(hotel_id),
			},
		},
		TableName: aws.String(pTableName),
	}

	// GetItem from dynamodb table
	r, err := db.GetItem(inputP)
	if err != nil {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorFailedToFetchRecord).Error()),
		})
	}

	item1 := new(be.PostCancellationSetting)
	err = dynamodbattribute.UnmarshalMap(r.Item, item1)
	if err != nil {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorCouldNotUnMarshalItem).Error()),
		})
	}

	for i := 0; i < len(*item); i++ {
		if len(r.Item) == 0 {
			*&(*item)[i].CancellationChargeAppliedOnConsumptionTax = false
			*&(*item)[i].CancellationChargeAppliedOnServiceTax = false
		} else {
			*&(*item)[i].CancellationChargeAppliedOnConsumptionTax = item1.CancellationChargeAppliedOnConsumptionTax
			*&(*item)[i].CancellationChargeAppliedOnServiceTax = item1.CancellationChargeAppliedOnServiceTax
		}
		if len(*&(*item)[i].CancellationCharge) == 0 {
			*&(*item)[i].CancellationCharge = make([]be.CC, 0)
		}
	}

	return be.ApiResponse(org, http.StatusOK, item)
}

func main() {
	lambda.Start(handler)
}
