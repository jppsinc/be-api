package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"os"

	be "bitbucket.org/jppsinc/be-modules/pkg"

	valid "github.com/asaskevich/govalidator"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
)

func handler(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {

	org := request.Headers["origin"]
	// Table name
	// tableName := "be_rank_prices-dev"
	tableName := os.Getenv("TABLE_NAME")

	// Change JSON request data to struct  : just to check validity of request data
	var sds []be.RankPrice

	if err := json.Unmarshal([]byte(request.Body), &sds); err != nil {
		fmt.Println("err 1:", err)
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorCouldNotUnMarshalItem).Error()),
		})
	}

	hotel_id, ok := request.PathParameters["hotel_id"]
	if ok == false || valid.IsUUIDv4(hotel_id) == false {
		fmt.Println("err 3 : PathParameters")
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorInvalidUrlHotelIdNotFound).Error()),
		})
	}

	// DynamoDB
	db := be.Dynamodb()

	for i, price := range sds {
		fmt.Println("price:", i)
		fmt.Println("price:", price)
		// change struct data to json data, this data will be stored to database
		av, err := dynamodbattribute.MarshalMap(price)
		if err != nil {
			fmt.Println("err 6:", err)
			return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
				ErrorMsg: aws.String(errors.New(be.ErrorCouldNotMarshalItem).Error()),
			})
		}

		input := &dynamodb.PutItemInput{
			Item:      av,
			TableName: aws.String(tableName),
		}

		_, err = db.PutItem(input)
		if err != nil {
			fmt.Println("err 7:", err)
			return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
				ErrorMsg: aws.String(errors.New(be.ErrorCouldNotDynamoPutItem).Error()),
			})
		}
	}

	return be.ApiResponse(org, http.StatusOK, sds)
}

func main() {
	lambda.Start(handler)
}
