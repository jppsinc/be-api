package main

import (
	"errors"
	"fmt"
	"net/http"
	"os"
	"strings"

	be "bitbucket.org/jppsinc/be-modules/pkg"
	valid "github.com/asaskevich/govalidator"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/aws/aws-sdk-go/service/dynamodb/expression"
)

func handler(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {

	org := request.Headers["origin"]
	// Table name

	// tableName := "be_rank_prices-dev"

	tableName := os.Getenv("TABLE_NAME")

	// Request path parameter
	hotel_id, ok := request.PathParameters["hotel_id"]
	if ok == false || valid.IsUUIDv4(hotel_id) == false {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorInvalidUrlHotelIdNotFound).Error()),
		})
	}

	// DynamoDB
	db := be.Dynamodb()

	rankPrice, err := GetRankPriceData(hotel_id, db, tableName)
	if err != nil {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(err.Error()),
		})
	}

	rankPriceMap := make(map[string]string, 0)

	for _, rp := range *rankPrice {
		s := strings.Split(rp.PlanRoomRankPax, "#")
		rankPriceMap[s[0]+"#"+s[1]] = s[0] + "#" + s[1]
	}

	final := make([]string, 0)
	for k := range rankPriceMap {
		final = append(final, k)
	}

	if len(final) == 0 {
		return be.ApiResponse(org, http.StatusOK, []be.EmptyStruct{})
	}

	return be.ApiResponse(org, http.StatusOK, final)
}

func GetRankPriceData(hotel_id string, db *dynamodb.DynamoDB, rrTableName string) (*[]*be.RankPriceIndex, error) {

	keyCond := expression.Key("hotel_id").Equal(expression.Value(hotel_id))

	item := new([]*be.RankPriceIndex)
	expr, err := expression.NewBuilder().WithKeyCondition(keyCond).Build()
	if err != nil {
		return item, err
	}

	// input for GetItem
	input := &dynamodb.QueryInput{
		KeyConditionExpression:    expr.KeyCondition(),
		TableName:                 aws.String(rrTableName),
		ExpressionAttributeNames:  expr.Names(),
		ExpressionAttributeValues: expr.Values(),
	}

	fmt.Println("room input : ", input)

	// GetItem from dynamodb table
	result, err := db.Query(input)
	if err != nil {
		return item, err
	}

	err = dynamodbattribute.UnmarshalListOfMaps(result.Items, &item)
	if err != nil {
		return item, err
	}

	fmt.Println("No of RankPrice entry : ", len(*item))

	return item, nil
}

func main() {
	lambda.Start(handler)
}
