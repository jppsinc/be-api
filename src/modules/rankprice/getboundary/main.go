package main

import (
	"errors"
	"net/http"
	"os"

	be "bitbucket.org/jppsinc/be-modules/pkg"
	valid "github.com/asaskevich/govalidator"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
)

type RankPriceBoundary struct {
	HotelID   string  `json:"hotel_id"`
	PlanRoom  string  `json:"plan_room"`
	PlanID    string  `json:"plan_id"`
	RoomID    string  `json:"room_id"`
	MaxPrice  float64 `json:"max_price"`
	MinPrice  float64 `json:"min_price"`
	IsSet     bool    `json:"is_set"`
	CreatedAt int64   `json:"created_at"`
	UpdatedAt int64   `json:"updated_at"`
}

func handler(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {

	org := request.Headers["origin"]
	// Table name

	// tableName := "be_rank_prices-dev"
	// rTableName := "be_rooms-dev"
	// rrTableName := "be_rate_ranks-dev"

	tableName := os.Getenv("TABLE_NAME")

	// Request path parameter
	hotel_id, ok := request.PathParameters["hotel_id"]
	if ok == false || valid.IsUUIDv4(hotel_id) == false {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorInvalidUrlHotelIdNotFound).Error()),
		})
	}

	room_id, ok := request.QueryStringParameters["roomId"]
	if ok == false || valid.IsUUIDv4(room_id) == false {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New("Room id not found").Error()),
		})
	}

	plan_id, ok := request.QueryStringParameters["planId"]
	if ok == false || valid.IsUUIDv4(plan_id) == false {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New("Plan id not found").Error()),
		})
	}

	// DynamoDB
	db := be.Dynamodb()

	rankPriceBoundary, err := GetRankPriceData(hotel_id, db, tableName, room_id, plan_id)
	if err != nil {
		if err.Error() == "No Data" {
			return be.ApiResponse(org, http.StatusNotFound, be.EmptyStruct{})
		}
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(err.Error()),
		})
	}

	return be.ApiResponse(org, http.StatusOK, rankPriceBoundary)
}

func GetRankPriceData(hotel_id string, db *dynamodb.DynamoDB, rrTableName string, room_id string, plan_id string) (RankPriceBoundary, error) {

	input := &dynamodb.GetItemInput{
		Key: map[string]*dynamodb.AttributeValue{
			"hotel_id": {
				S: aws.String(hotel_id),
			},
			"plan_room": {
				S: aws.String(plan_id + "#" + room_id),
			},
		},
		TableName: aws.String(rrTableName),
	}
	item := RankPriceBoundary{}
	// GetItem from dynamodb table
	result, err := db.GetItem(input)
	if err != nil {
		return item, err
	}

	if len(result.Item) > 0 {
		err = dynamodbattribute.UnmarshalMap(result.Item, &item)
		if err != nil {
			return item, err
		}
	} else {
		return item, errors.New("No Data")
	}
	return item, nil
}

func main() {
	lambda.Start(handler)
}
