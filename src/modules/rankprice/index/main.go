package main

import (
	"errors"
	"fmt"
	"net/http"
	"os"
	"sort"

	be "bitbucket.org/jppsinc/be-modules/pkg"
	valid "github.com/asaskevich/govalidator"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/aws/aws-sdk-go/service/dynamodb/expression"
)

func handler(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {

	org := request.Headers["origin"]
	// Table name

	// tableName := "be_rank_prices-dev"
	// rTableName := "be_rooms-dev"
	// rrTableName := "be_rate_ranks-dev"

	tableName := os.Getenv("TABLE_NAME")
	rTableName := os.Getenv("ROOM_TABLE_NAME")
	rrTableName := os.Getenv("RR_TABLE_NAME")

	// Request path parameter
	hotel_id, ok := request.PathParameters["hotel_id"]
	if ok == false || valid.IsUUIDv4(hotel_id) == false {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorInvalidUrlHotelIdNotFound).Error()),
		})
	}

	room_id, ok := request.QueryStringParameters["roomId"]
	if ok == false || valid.IsUUIDv4(room_id) == false {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New("Room id not found").Error()),
		})
	}

	plan_id, ok := request.QueryStringParameters["planId"]
	if ok == false || valid.IsUUIDv4(plan_id) == false {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New("Plan id not found").Error()),
		})
	}

	// DynamoDB
	db := be.Dynamodb()

	rankPrice, err := GetRankPriceData(hotel_id, db, tableName, room_id, plan_id)
	if err != nil {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(err.Error()),
		})
	}

	rooms, err := GetRoomData(hotel_id, db, rTableName, room_id)
	if err != nil {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(err.Error()),
		})
	}

	minPax := rooms.MinOccupancy
	maxPax := rooms.MaxPax

	rateRank, err := GetRateRankData(hotel_id, db, rrTableName)
	if err != nil {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(err.Error()),
		})
	}

	rankPriceFinal := make([]map[string][]be.RankPriceIndex, 0)

	rankPriceMap := make(map[string]be.RankPriceIndex, 0)

	for _, rp := range *rankPrice {
		rankPriceMap[rp.PlanRoomRankPax] = *rp
	}

	for _, rr := range *rateRank {
		rankPriceResult := make(map[string][]be.RankPriceIndex, 0)
		for i := minPax; i <= maxPax; i++ {

			planRoomRankPax := plan_id + "#" + room_id + "#" + rr.Name + "#" + fmt.Sprintf("%v", i)

			if val, ok := rankPriceMap[planRoomRankPax]; ok {
				val.RateRankName = rr.Name
				val.IsPriceSet = true
				rankPriceResult[rr.Name] = append(rankPriceResult[rr.Name], val)
			} else {
				temp := be.RankPriceIndex{}
				temp.HotelID = hotel_id
				temp.PlanRoomRankPax = planRoomRankPax
				temp.Pax = i
				temp.PlanID = plan_id
				temp.Price = 0
				temp.IsPriceSet = false
				temp.RateRankID = rr.ID
				temp.RateRankName = rr.Name
				temp.CreatedAt = be.DateTimeStamp()
				temp.UpdatedAt = be.DateTimeStamp()
				temp.RoomID = room_id

				rankPriceResult[rr.Name] = append(rankPriceResult[rr.Name], temp)
			}
		}
		rankPriceFinal = append(rankPriceFinal, rankPriceResult)
	}

	return be.ApiResponse(org, http.StatusOK, rankPriceFinal)
}

func GetRoomData(hotel_id string, db *dynamodb.DynamoDB, rTableName string, room_id string) (*be.Room, error) {

	input := &dynamodb.GetItemInput{
		Key: map[string]*dynamodb.AttributeValue{
			"hotel_id": {
				S: aws.String(hotel_id),
			},
			"id": {
				S: aws.String(room_id),
			},
		},
		TableName: aws.String(rTableName),
	}
	item := new(be.Room)
	// GetItem from dynamodb table
	result, err := db.GetItem(input)
	if err != nil {
		return item, err
	}

	err = dynamodbattribute.UnmarshalMap(result.Item, item)
	if err != nil {
		return item, err
	}

	return item, nil
}

func GetRateRankData(hotel_id string, db *dynamodb.DynamoDB, rrTableName string) (*[]*be.RateRank, error) {

	// input for GetItem
	input := &dynamodb.QueryInput{
		ExpressionAttributeValues: map[string]*dynamodb.AttributeValue{
			":hotel_id": {
				S: aws.String(hotel_id),
			},
		},
		KeyConditionExpression: aws.String("hotel_id = :hotel_id"),
		TableName:              aws.String(rrTableName),
	}

	item := new([]*be.RateRank)
	// GetItem from dynamodb table
	result, err := db.Query(input)
	if err != nil {
		return item, err
	}

	err = dynamodbattribute.UnmarshalListOfMaps(result.Items, &item)
	if err != nil {
		return item, err
	}

	// sort by Order
	sort.SliceStable(*item, func(i, j int) bool {
		return *&(*item)[i].Order < *&(*item)[j].Order
	})

	return item, nil
}

func GetRankPriceData(hotel_id string, db *dynamodb.DynamoDB, rrTableName string, room_id string, plan_id string) (*[]*be.RankPriceIndex, error) {

	keyCond := expression.KeyAnd(expression.Key("hotel_id").Equal(expression.Value(hotel_id)), expression.Key("plan_room_rank_pax").BeginsWith(plan_id+"#"+room_id))

	item := new([]*be.RankPriceIndex)
	expr, err := expression.NewBuilder().WithKeyCondition(keyCond).Build()
	if err != nil {
		return item, err
	}

	// input for GetItem
	input := &dynamodb.QueryInput{
		KeyConditionExpression:    expr.KeyCondition(),
		TableName:                 aws.String(rrTableName),
		ExpressionAttributeNames:  expr.Names(),
		ExpressionAttributeValues: expr.Values(),
	}

	fmt.Println("room input : ", input)

	// GetItem from dynamodb table
	result, err := db.Query(input)
	if err != nil {
		return item, err
	}

	err = dynamodbattribute.UnmarshalListOfMaps(result.Items, &item)
	if err != nil {
		return item, err
	}

	fmt.Println("No of rooms : ", len(*item))

	return item, nil
}

func main() {
	lambda.Start(handler)
}
