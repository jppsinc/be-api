package main

import (
	"errors"
	"fmt"
	"net/http"
	"os"
	"runtime"
	"strconv"
	"strings"
	"sync"
	"time"

	be "bitbucket.org/jppsinc/be-modules/pkg"
	valid "github.com/asaskevich/govalidator"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/aws/aws-sdk-go/service/dynamodb/expression"
)

type LastKey struct {
	HotelID            string `json:"hotel_id"`
	PlanRoomUseDatePax string `json:"plan_room_use_date_pax"`
}

func QueryFirst(expr expression.Expression, tableName string) *dynamodb.QueryInput {
	input := &dynamodb.QueryInput{
		KeyConditionExpression:    expr.KeyCondition(),
		TableName:                 aws.String(tableName),
		FilterExpression:          expr.Filter(),
		ExpressionAttributeNames:  expr.Names(),
		ExpressionAttributeValues: expr.Values(),
	}

	return input
}

func QueryNext(expr expression.Expression, tableName string, es LastKey) *dynamodb.QueryInput {
	input := &dynamodb.QueryInput{
		KeyConditionExpression:    expr.KeyCondition(),
		TableName:                 aws.String(tableName),
		FilterExpression:          expr.Filter(),
		ExpressionAttributeNames:  expr.Names(),
		ExpressionAttributeValues: expr.Values(),
		ExclusiveStartKey: map[string]*dynamodb.AttributeValue{
			"hotel_id": {
				S: aws.String(es.HotelID),
			},
			"plan_room_use_date_pax": {
				S: aws.String(es.PlanRoomUseDatePax),
			},
		},
	}

	return input
}

var wg sync.WaitGroup

func handler(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	org := request.Headers["origin"]
	// Table name

	mu := &sync.Mutex{}

	// tableName := "be_prices-dev"
	// rTableName := "be_rooms-dev"
	// pTableName := "be_plans-dev"

	start := time.Now()
	tableName := os.Getenv("TABLE_NAME")
	rTableName := os.Getenv("ROOM_TABLE_NAME")
	pTableName := os.Getenv("PLAN_TABLE_NAME")

	runtime.GOMAXPROCS(runtime.NumCPU())

	// Request path parameter
	hotel_id, ok := request.PathParameters["hotel_id"]
	if ok == false || valid.IsUUIDv4(hotel_id) == false {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorInvalidUrlHotelIdNotFound).Error()),
		})
	}

	var keyCond expression.KeyConditionBuilder

	var finalFilter expression.ConditionBuilder
	var filter []expression.ConditionBuilder

	var startDate string
	var endDate string
	var roomId string
	var planId string

	// query string parameter
	if len(request.QueryStringParameters) == 0 {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorQueryStringParameterNotFound).Error()),
		})
	} else {
		startDate, ok = request.QueryStringParameters["startDate"]
		if ok == false || len(strings.TrimSpace(startDate)) == 0 {
			return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
				ErrorMsg: aws.String(errors.New(be.ErrorQueryStringParameterNotFound).Error()),
			})
		} else {
			filter = append(filter, expression.Name("use_date").GreaterThanEqual(expression.Value(startDate)))
		}

		endDate, ok = request.QueryStringParameters["endDate"]
		if ok == false || len(strings.TrimSpace(endDate)) == 0 {
			return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
				ErrorMsg: aws.String(errors.New(be.ErrorQueryStringParameterNotFound).Error()),
			})
		} else {
			filter = append(filter, expression.Name("use_date").LessThanEqual(expression.Value(endDate)))
		}

		roomId, ok = request.QueryStringParameters["roomId"]
		if ok == true && valid.IsUUIDv4(roomId) == true {
			filter = append(filter, expression.Name("room_id").Equal(expression.Value(roomId)))
		}

		planId, ok = request.QueryStringParameters["planId"]
		if ok == true && valid.IsUUIDv4(planId) == true {
			// filter = append(filter, expression.Name("plan_id").Equal(expression.Value(planId)))
			keyCond = expression.KeyAnd(expression.Key("hotel_id").Equal(expression.Value(hotel_id)), expression.Key("plan_room_use_date_pax").BeginsWith(planId))
		} else {
			keyCond = expression.Key("hotel_id").Equal(expression.Value(hotel_id))
		}
	}

	if len(filter) != 0 {
		for i, temp := range filter {
			if i == 0 {
				finalFilter = temp
			} else {
				finalFilter = finalFilter.And(temp)
			}
		}
	}

	// var input *dynamodb.QueryInput

	expr, err := expression.NewBuilder().WithKeyCondition(keyCond).WithFilter(finalFilter).Build()
	if err != nil {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(err.Error()),
		})
	}

	// // DynamoDB
	db := be.Dynamodb()
	item := new([]*be.PriceIndex)

	var lastEvaluatedKey LastKey
	var result *dynamodb.QueryOutput

	for {
		pp := new([]*be.PriceIndex)
		if lastEvaluatedKey == (LastKey{}) {
			result, err = db.Query(QueryFirst(expr, tableName))
		} else {
			result, err = db.Query(QueryNext(expr, tableName, lastEvaluatedKey))
		}

		if err != nil {
			fmt.Println(err)
			// return err
		}

		err = dynamodbattribute.UnmarshalListOfMaps(result.Items, &pp)
		if err != nil {
			fmt.Println(err)
			// return err
		}

		for _, i := range *pp {
			*item = append(*item, i)
		}

		if result.LastEvaluatedKey == nil {
			break
		}

		err = dynamodbattribute.UnmarshalMap(result.LastEvaluatedKey, &lastEvaluatedKey)
		if err != nil {
			fmt.Println(err)
			// return err
		}
	}

	rooms, err := GetRoomList(hotel_id, db, rTableName, roomId)
	if err != nil {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(err.Error()),
		})
	}

	// if room not found return empty array.
	if len(*rooms) == 0 {
		return be.ApiResponse(org, http.StatusOK, []be.EmptyStruct{})
	}

	// planMap := make([]map[string][]be.PriceIndex, 0)

	roomMap1 := make([]map[string][]map[string][]be.PriceIndex, 0)

	// temp := make(map[string][]be.PriceIndex)
	for _, room := range *rooms {
		wg.Add(1)
		fmt.Println("\nRoom : ", room.ID)
		go ProcessRoutine(startDate, endDate, hotel_id, room, db, pTableName, planId, item, &roomMap1, &wg, mu)
	}
	wg.Wait()
	// roomMap1 = append(roomMap1, roomMap)
	duration := time.Since(start)
	fmt.Printf("\nUpdate successful : CPU Count : %v : Time of execution : %v\n", runtime.NumCPU(), duration)
	return be.ApiResponse(org, http.StatusOK, roomMap1)
}

func ProcessRoutine(startDate string, endDate string, hotel_id string, room *be.Room, db *dynamodb.DynamoDB, pTableName string, planId string, item *[]*be.PriceIndex, roomMap1 *[]map[string][]map[string][]be.PriceIndex, wg *sync.WaitGroup, mu *sync.Mutex) {
	defer wg.Done()
	roomMap := make(map[string][]map[string][]be.PriceIndex, 0)

	layout := "2006-01-02"
	sDate, _ := time.Parse(layout, startDate)
	eDate, _ := time.Parse(layout, endDate)
	eDate_1 := eDate.AddDate(0, 0, 1)

	planList, err := GetPlanList(hotel_id, room.ID, db, pTableName, planId)
	if err != nil {
		// return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
		// 	ErrorMsg: aws.String(err.Error()),
		// })
	}

	planMap := make([]map[string][]be.PriceIndex, 0)

	for planID, planInfo := range planList {
		sDate, _ = time.Parse(layout, startDate)
		temp := make(map[string][]be.PriceIndex)

		cnt := 0
		for {
			if sDate.Before(eDate_1) {
				for pax := room.MinOccupancy; pax <= room.MaxPax; pax++ {
					cnt++

					flg := true
					// check if price is already set
					for _, el := range *item {
						planRoomUseDatePax := strings.Split(el.PlanRoomUseDatePax, "#")
						itemPax, _ := strconv.Atoi(planRoomUseDatePax[2])
						if planRoomUseDatePax[1] == room.ID && planRoomUseDatePax[3] == sDate.Format(layout) && planID == planRoomUseDatePax[0] && int64(itemPax) == pax {
							el.IsPriceSet = true
							temp[planID] = append(temp[planID], *el)
							flg = false
							fmt.Printf(" [old cnt : %v : usedate : %v : pax : %v : price : %v : ] ", cnt, sDate.Format(layout), pax, el.Price)
						}
					}
					// if price is not set
					if flg {
						tempPrice := be.PriceIndex{}
						px := strconv.Itoa(int(pax))
						tempPrice.PlanRoomUseDatePax = planID + "#" + room.ID + "#" + px + "#" + sDate.Format(layout)
						tempPrice.HotelID = hotel_id
						tempPrice.RoomType = room.RoomType
						tempPrice.SalesCategory = true
						tempPrice.PlanTitle = planInfo.Title
						tempPrice.UseDate = sDate.Format(layout)
						if planInfo.Pricing == "PerRoom" {
							tempPrice.ChargeSystem = "RC"
						} else {
							tempPrice.ChargeSystem = "PPC"
						}
						tempPrice.Pax = pax
						tempPrice.Price = 0
						tempPrice.IsPriceSet = false
						tempPrice.RoomID = room.ID

						temp[planID] = append(temp[planID], tempPrice)
						fmt.Printf(" [new cnt : %v : usedate : %v : pax : %v : price : %v : ] ", cnt, sDate.Format(layout), pax, 0)
					}
				}
			} else {
				break
			}
			sDate = sDate.AddDate(0, 0, 1)

		}
		if len(temp) > 0 {
			mu.Lock()
			planMap = append(planMap, temp)
			mu.Unlock()

		}
		fmt.Println("")
	}
	if len(planMap) > 0 {
		roomMap[room.ID] = planMap
		*roomMap1 = append(*roomMap1, roomMap)
	}
}

func GetRoomList(hotel_id string, db *dynamodb.DynamoDB, rTableName string, roomId string) (*[]*be.Room, error) {

	keyCond := expression.Key("hotel_id").Equal(expression.Value(hotel_id))
	filter := expression.Name("status").NotEqual(expression.Value("Creating"))

	item := new([]*be.Room)
	item1 := new([]*be.Room)
	expr, err := expression.NewBuilder().WithKeyCondition(keyCond).WithFilter(filter).Build()
	if err != nil {
		return item, err
	}

	// input for GetItem
	input := &dynamodb.QueryInput{
		KeyConditionExpression:    expr.KeyCondition(),
		TableName:                 aws.String(rTableName),
		ExpressionAttributeNames:  expr.Names(),
		ExpressionAttributeValues: expr.Values(),
		FilterExpression:          expr.Filter(),
	}

	// fmt.Println("room input : ", input)

	// GetItem from dynamodb table
	result, err := db.Query(input)
	if err != nil {
		return item, err
	}

	err = dynamodbattribute.UnmarshalListOfMaps(result.Items, &item)
	if err != nil {
		return item, err
	}

	if len(*item) > 0 && len(roomId) > 0 {
		for _, it := range *item {
			if it.ID == roomId {
				*item1 = append(*item1, it)
			}
		}

		return item1, nil
	}

	fmt.Println("No of rooms : ", len(*item))

	return item, nil
}

func GetPlanList(hotel_id string, room_id string, db *dynamodb.DynamoDB, pTableName string, planId string) (map[string]*be.Plan, error) {

	keyCond := expression.Key("hotel_id").Equal(expression.Value(hotel_id))
	// filter := expression.Name("status").Equal(expression.Value("Sales")).And(expression.Name("room_id_list").Contains(room_id)).And(expression.Name("sales_category").Equal(expression.Value(true)))
	filter := expression.Name("status").NotEqual(expression.Value("Delete")).And(expression.Name("room_id_list").Contains(room_id))

	item := new([]*be.Plan)
	pMap := make(map[string]*be.Plan, 0)

	expr, err := expression.NewBuilder().WithKeyCondition(keyCond).WithFilter(filter).Build()
	if err != nil {
		return pMap, err
	}

	// input for GetItem
	input := &dynamodb.QueryInput{
		KeyConditionExpression:    expr.KeyCondition(),
		TableName:                 aws.String(pTableName),
		ExpressionAttributeNames:  expr.Names(),
		ExpressionAttributeValues: expr.Values(),
		FilterExpression:          expr.Filter(),
	}

	// fmt.Println("plan input : ", input)

	// GetItem from dynamodb table
	result, err := db.Query(input)
	if err != nil {
		return pMap, err
	}

	err = dynamodbattribute.UnmarshalListOfMaps(result.Items, &item)
	if err != nil {
		return pMap, err
	}

	for _, e := range *item {
		if len(planId) > 0 {
			if e.ID == planId {
				pMap[e.ID] = e
			}
		} else {
			pMap[e.ID] = e
		}
	}

	fmt.Println("No of plans : ", len(*item))

	return pMap, nil
}

func main() {
	lambda.Start(handler)
}