package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"

	be "bitbucket.org/jppsinc/be-modules/pkg"

	valid "github.com/asaskevich/govalidator"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/aws/aws-sdk-go/service/dynamodb/expression"
)

type BulkUpdateRequest struct {
	StartDate                     string   `json:"start_date"`
	EndDate                       string   `json:"end_date"`
	DaysOfWeek                    []string `json:"days_of_week"` //All,Sunday,Monday,Tuesday,Wednesday,Thursday,Friday,Holiday,BeforeHoliday
	RankID                        string   `json:"rank"`         //NoChange, uuid of rate rank
	UnrankedChargeRenewNotRequire bool     `json:"unranked_charge_renew_not_require"`
	PriceChangeNotRequire         bool     `json:"price_change_not_require"`
	PerPersonPriceChangeOrder     string   `json:"per_person_price_change_order"` // Up,Down
	PerPersonPriceChangeAmount    float64  `json:"per_person_price_change_amount"`
	PerRoomPriceChangeOrder       string   `json:"per_room_price_change_order"` // Up,Down
	PerRoomPriceChangeAmount      float64  `json:"per_room_price_change_amount"`
	SalesStatus                   string   `json:"sales_status"`          // NoChange, OnSale, StopSale
	IsSpecialRankPrice            bool     `json:"is_special_rank_price"` //either price is changed (+/-) or it is stopped. BG color is changed.
	Pax                           []int64  `json:"pax"`                   // pax for SalesCategory
	Target                        []Target `json:"target"`
}

type Target struct {
	PlanID string `json:"plan_id"`
	RoomID string `json:"room_id"`
}

type Price struct {
	UseDate            string `json:"use_date"`
	HotelID            string `json:"hotel_id"`
	PlanRoomUseDatePax string `json:"plan_room_use_date_pax"`
	// PlanTitle          MultiLang `json:"plan_title"`
	RoomID string `json:"room_id"`
	// RoomType           MultiLang `json:"room_type"`
	RateRankID         string  `json:"rate_rank_id"`
	RateRankName       string  `json:"rate_rank_name"`
	SalesCategory      bool    `json:"sales_category"`
	Pax                int64   `json:"pax"`
	Price              float64 `json:"price"`
	IsBooked           bool    `json:"is_booked"`
	ChargeSystem       string  `json:"charge_system"`
	IsSpecialRankPrice bool    `json:"is_special_rank_price"` //either price is changed (+/-) or it is stopped. BG color is changed.
	CreatedAt          int64   `json:"created_at"`
	UpdatedAt          int64   `json:"updated_at"`
}

type Holiday struct {
	Items []struct {
		Start struct {
			Date string `json:"date"`
		} `json:"start"`
		End struct {
			Date string `json:"date"`
		} `json:"end"`
	} `json:"items"`
}

// var wg sync.WaitGroup

func removeDuplicateStr(strSlice []string) []string {
	allKeys := make(map[string]bool)
	list := []string{}
	for _, item := range strSlice {
		if _, value := allKeys[item]; !value {
			allKeys[item] = true
			list = append(list, item)
		}
	}
	return list
}

func handler(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {

	org := request.Headers["origin"]
	// Table name
	// priceTableName := "be_prices-dev"
	// roomTableName := "be_rooms-dev"
	// rrpTableName := "be_rank_prices-dev"
	priceTableName := os.Getenv("PRICE_TABLE_NAME")
	roomTableName := os.Getenv("ROOM_TABLE_NAME")
	rrpTableName := os.Getenv("RRP_TABLE_NAME")

	// Change JSON request data to struct  : just to check validity of request data
	var sds BulkUpdateRequest

	if err := json.Unmarshal([]byte(request.Body), &sds); err != nil {
		fmt.Println("err 1:", err)
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorCouldNotUnMarshalItem).Error()),
		})
	}

	hotelId, ok := request.PathParameters["hotel_id"]
	if ok == false || valid.IsUUIDv4(hotelId) == false {
		fmt.Println("err 3 : PathParameters")
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorInvalidUrlHotelIdNotFound).Error()),
		})
	}

	// DynamoDB
	db := be.Dynamodb()

	roomMap, err := GetRoomPax(hotelId, roomTableName, db)
	if err != nil {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(err.Error()),
		})
	}

	if len(sds.DaysOfWeek) > 0 {
		var updateDates []string
		dayList := make([]string, 0)
		for _, days := range sds.DaysOfWeek {
			switch days {
			case "All":
				updateDates, _ = GetHolidays(sds.StartDate, sds.EndDate, "All")
				fmt.Println("All : ", updateDates)

			case "BeforeHoliday":
				updateDates, _ = GetHolidays(sds.StartDate, sds.EndDate, "BeforeHoliday")
				fmt.Println("BeforeHoliday : ", updateDates)

			case "Holiday":
				updateDates, _ = GetHolidays(sds.StartDate, sds.EndDate, "Holiday")
				fmt.Println("Holiday : ", updateDates)

			default:
				dayList = append(dayList, days)
			}

			if len(dayList) > 0 {
				otherDates, _ := GetDateFromDay(sds.StartDate, sds.EndDate, dayList)
				updateDatesWithDuplicate := append(updateDates, otherDates...)

				// remove duplicate dates
				updateDates = removeDuplicateStr(updateDatesWithDuplicate)
				fmt.Println("otherDates : ", updateDates)
			}
		}

		fmt.Println("final days : ", updateDates)
		PriceUpdateForAll(hotelId, sds, db, priceTableName, roomMap, rrpTableName, updateDates)
	}

	return be.ApiResponse(org, http.StatusOK, sds)
}

func PriceUpdateRoutine(useDate string, rankPrice map[string][2]string, minMax map[string]int, i int, isStopSale bool, target Target, sds BulkUpdateRequest, hotelId string, db *dynamodb.DynamoDB, tableName string, roomMap map[string]map[string]int, rrpTableName string, unrankedChargeRenewNotRequire bool) {

	// hash key for price table
	hash := target.PlanID + "#" + target.RoomID + "#" + fmt.Sprintf("%v", i) + "#" + useDate

	fmt.Println("PriceUpdateForAll hash : ", hash)

	var update expression.UpdateBuilder

	// if RankID not selected
	if sds.RankID == "NoChange" {
		fmt.Println("---------------------------------------------------------------------------------------")
		// if rate rank price need to be modified
		if !sds.PriceChangeNotRequire {

			var modifiedPrice float64
			if sds.PerPersonPriceChangeOrder == "Up" {
				modifiedPrice = sds.PerPersonPriceChangeAmount
			}
			if sds.PerPersonPriceChangeOrder == "Down" {
				modifiedPrice = sds.PerPersonPriceChangeAmount
			}

			if sds.PerRoomPriceChangeOrder == "Up" {
				modifiedPrice = sds.PerRoomPriceChangeAmount
			}
			if sds.PerRoomPriceChangeOrder == "Down" {
				modifiedPrice = sds.PerRoomPriceChangeAmount
			}

			if sds.PerPersonPriceChangeOrder == "Up" || sds.PerRoomPriceChangeOrder == "Up" {
				// logic to modify content
				switch sds.SalesStatus {
				case "OnSale":
					if isStopSale {
						fmt.Println("OnSale 1 : isStopSale")
						update = expression.Set(expression.Name("sales_category"), expression.Value(false)).
							Set(expression.Name("price"), expression.Name("price").Plus(expression.Value(modifiedPrice))).
							Set(expression.Name("is_special_rank_price"), expression.Value(true)).
							Set(expression.Name("use_date"), expression.Value(useDate)).
							Set(expression.Name("updated_at"), expression.Value(be.DateTimeStamp())).
							Set(expression.Name("room_id"), expression.Value(target.RoomID)).
							Set(expression.Name("created_at"), expression.Name("created_at").IfNotExists(expression.Value(be.DateTimeStamp()))).
							Set(expression.Name("pax"), expression.Value(i))
					} else {
						fmt.Println("OnSale 1 : else")
						update = expression.Set(expression.Name("sales_category"), expression.Value(true)).
							Set(expression.Name("price"), expression.Name("price").Plus(expression.Value(modifiedPrice))).
							Set(expression.Name("is_special_rank_price"), expression.Value(true)).
							Set(expression.Name("use_date"), expression.Value(useDate)).
							Set(expression.Name("updated_at"), expression.Value(be.DateTimeStamp())).
							Set(expression.Name("room_id"), expression.Value(target.RoomID)).
							Set(expression.Name("created_at"), expression.Name("created_at").IfNotExists(expression.Value(be.DateTimeStamp()))).
							Set(expression.Name("pax"), expression.Value(i))
					}

				case "StopSale":
					fmt.Println("StopSale 1 : StopSale")
					update = expression.Set(expression.Name("sales_category"), expression.Value(false)).
						Set(expression.Name("price"), expression.Name("price").Plus(expression.Value(modifiedPrice))).
						Set(expression.Name("is_special_rank_price"), expression.Value(true)).
						Set(expression.Name("use_date"), expression.Value(useDate)).
						Set(expression.Name("updated_at"), expression.Value(be.DateTimeStamp())).
						Set(expression.Name("room_id"), expression.Value(target.RoomID)).
						Set(expression.Name("created_at"), expression.Name("created_at").IfNotExists(expression.Value(be.DateTimeStamp()))).
						Set(expression.Name("pax"), expression.Value(i))

				case "NoChange":
					if isStopSale {
						fmt.Println("NoChange 1 : NoChange")
						update = expression.Set(expression.Name("price"), expression.Name("price").Plus(expression.Value(modifiedPrice))).
							Set(expression.Name("sales_category"), expression.Value(false)).
							Set(expression.Name("use_date"), expression.Value(useDate)).
							Set(expression.Name("is_special_rank_price"), expression.Value(true)).
							Set(expression.Name("updated_at"), expression.Value(be.DateTimeStamp())).
							Set(expression.Name("room_id"), expression.Value(target.RoomID)).
							Set(expression.Name("created_at"), expression.Name("created_at").IfNotExists(expression.Value(be.DateTimeStamp()))).
							Set(expression.Name("pax"), expression.Value(i))
					} else {
						fmt.Println("NoChange 1 : NoChange else")
						update = expression.Set(expression.Name("price"), expression.Name("price").Plus(expression.Value(modifiedPrice))).
							Set(expression.Name("use_date"), expression.Value(useDate)).
							Set(expression.Name("is_special_rank_price"), expression.Value(true)).
							Set(expression.Name("updated_at"), expression.Value(be.DateTimeStamp())).
							Set(expression.Name("room_id"), expression.Value(target.RoomID)).
							Set(expression.Name("created_at"), expression.Name("created_at").IfNotExists(expression.Value(be.DateTimeStamp()))).
							Set(expression.Name("sales_category"), expression.Name("sales_category").IfNotExists(expression.Value(true))).
							Set(expression.Name("pax"), expression.Value(i))
					}

				}
			}

			if sds.PerPersonPriceChangeOrder == "Down" || sds.PerRoomPriceChangeOrder == "Down" {
				// logic to modify content
				switch sds.SalesStatus {
				case "OnSale":
					if isStopSale {
						fmt.Println("OnSale 1 : isStopSale")
						update = expression.Set(expression.Name("sales_category"), expression.Value(false)).
							Set(expression.Name("price"), expression.Name("price").Minus(expression.Value(modifiedPrice))).
							Set(expression.Name("is_special_rank_price"), expression.Value(true)).
							Set(expression.Name("use_date"), expression.Value(useDate)).
							Set(expression.Name("updated_at"), expression.Value(be.DateTimeStamp())).
							Set(expression.Name("room_id"), expression.Value(target.RoomID)).
							Set(expression.Name("created_at"), expression.Name("created_at").IfNotExists(expression.Value(be.DateTimeStamp()))).
							Set(expression.Name("pax"), expression.Value(i))

					} else {
						fmt.Println("OnSale 1 : isStopSale else")
						update = expression.Set(expression.Name("sales_category"), expression.Value(true)).
							Set(expression.Name("price"), expression.Name("price").Minus(expression.Value(modifiedPrice))).
							Set(expression.Name("is_special_rank_price"), expression.Value(true)).
							Set(expression.Name("use_date"), expression.Value(useDate)).
							Set(expression.Name("updated_at"), expression.Value(be.DateTimeStamp())).
							Set(expression.Name("room_id"), expression.Value(target.RoomID)).
							Set(expression.Name("created_at"), expression.Name("created_at").IfNotExists(expression.Value(be.DateTimeStamp()))).
							Set(expression.Name("pax"), expression.Value(i))
					}

				case "StopSale":
					fmt.Println("StopSale 1 : StopSale")
					update = expression.Set(expression.Name("sales_category"), expression.Value(false)).
						Set(expression.Name("price"), expression.Name("price").Minus(expression.Value(modifiedPrice))).
						Set(expression.Name("is_special_rank_price"), expression.Value(true)).
						Set(expression.Name("use_date"), expression.Value(useDate)).
						Set(expression.Name("updated_at"), expression.Value(be.DateTimeStamp())).
						Set(expression.Name("room_id"), expression.Value(target.RoomID)).
						Set(expression.Name("created_at"), expression.Name("created_at").IfNotExists(expression.Value(be.DateTimeStamp()))).
						Set(expression.Name("pax"), expression.Value(i))
				case "NoChange":
					if isStopSale {
						fmt.Println("NoChange 1 : NoChange")
						update = expression.Set(expression.Name("price"), expression.Name("price").Minus(expression.Value(modifiedPrice))).
							Set(expression.Name("sales_category"), expression.Value(false)).
							Set(expression.Name("use_date"), expression.Value(useDate)).
							Set(expression.Name("is_special_rank_price"), expression.Value(true)).
							Set(expression.Name("updated_at"), expression.Value(be.DateTimeStamp())).
							Set(expression.Name("room_id"), expression.Value(target.RoomID)).
							Set(expression.Name("created_at"), expression.Name("created_at").IfNotExists(expression.Value(be.DateTimeStamp()))).
							Set(expression.Name("pax"), expression.Value(i))
					} else {
						fmt.Println("NoChange 1 : NoChange else")
						update = expression.Set(expression.Name("price"), expression.Name("price").Minus(expression.Value(modifiedPrice))).
							Set(expression.Name("use_date"), expression.Value(useDate)).
							Set(expression.Name("is_special_rank_price"), expression.Value(true)).
							Set(expression.Name("updated_at"), expression.Value(be.DateTimeStamp())).
							Set(expression.Name("room_id"), expression.Value(target.RoomID)).
							Set(expression.Name("created_at"), expression.Name("created_at").IfNotExists(expression.Value(be.DateTimeStamp()))).
							Set(expression.Name("sales_category"), expression.Name("sales_category").IfNotExists(expression.Value(true))).
							Set(expression.Name("pax"), expression.Value(i))
					}

				}
			}

			var expr1 expression.Expression
			if unrankedChargeRenewNotRequire {
				// cond := expression.Name("is_special_rank_price").Equal(expression.Value(false)).Or(expression.AttributeNotExists(expression.Name("is_special_rank_price")))
				// cond := expression.And(expression.Name("plan_room_use_date_pax").Equal(expression.Value(hash)), expression.Size(expression.Name("is_special_rank_price")).Equal(expression.Value(false)))
				cond := (expression.Name("plan_room_use_date_pax").Equal(expression.Value(hash)).And(expression.Name("is_special_rank_price").Equal(expression.Value(false)))).Or(expression.AttributeNotExists(expression.Name("is_special_rank_price")))
				expr1, _ = expression.NewBuilder().
					WithUpdate(update).
					WithCondition(cond).
					Build()
			} else {
				cond := expression.Name("plan_room_use_date_pax").Equal(expression.Value(hash))
				expr1, _ = expression.NewBuilder().
					WithUpdate(update).
					WithCondition(cond).
					Build()
			}

			inputInc := &dynamodb.UpdateItemInput{
				Key: map[string]*dynamodb.AttributeValue{
					"hotel_id": {
						S: aws.String(hotelId),
					},
					"plan_room_use_date_pax": {
						S: aws.String(hash),
					},
				},
				TableName:                 aws.String(tableName),
				ExpressionAttributeNames:  expr1.Names(),
				ExpressionAttributeValues: expr1.Values(),
				UpdateExpression:          expr1.Update(),
				ConditionExpression:       expr1.Condition(),
			}

			_, err := db.UpdateItem(inputInc)
			if err != nil {
				fmt.Println("UpdateItem err inputInc", inputInc)
				fmt.Println("UpdateItem err 1", err)
			} else {
				fmt.Println("UpdateItem inputInc 1 : ", inputInc)
				fmt.Println("Successful Price Update for hash : ", hash)

			}
			fmt.Println("=====================================================================")
		}
	} else {

		// key for rate rank price map
		rankPriceKey := target.PlanID + "#" + target.RoomID + "#" + fmt.Sprintf("%v", i) + "#" + sds.RankID
		price := rankPrice[rankPriceKey]

		// logic to adjust the rank price
		a, _ := strconv.Atoi(price[0])
		modifiedPrice := float64(a)

		flg := false

		if !sds.PriceChangeNotRequire {
			if sds.PerPersonPriceChangeOrder == "Up" {
				fmt.Println("modifiedPrice 1: ", modifiedPrice)
				modifiedPrice = modifiedPrice + sds.PerPersonPriceChangeAmount
				flg = true
				fmt.Println("PerPersonPriceChangeAmount 1: ", modifiedPrice)
				fmt.Println("modifiedPrice 1: ", sds.PerPersonPriceChangeAmount)
			}
			if sds.PerPersonPriceChangeOrder == "Down" {
				modifiedPrice = modifiedPrice - sds.PerPersonPriceChangeAmount
				flg = true
				fmt.Println("modifiedPrice 2: ", modifiedPrice)
			}

			if sds.PerRoomPriceChangeOrder == "Up" {
				modifiedPrice = modifiedPrice + sds.PerRoomPriceChangeAmount
				flg = true
				fmt.Println("modifiedPrice 3: ", modifiedPrice)
			}
			if sds.PerRoomPriceChangeOrder == "Down" {
				modifiedPrice = modifiedPrice - sds.PerRoomPriceChangeAmount
				flg = true
				fmt.Println("modifiedPrice 4: ", modifiedPrice)
			}
		}

		// if price is modified set is_special_rank_price = true
		if flg {
			// logic to modify content
			switch sds.SalesStatus {
			case "OnSale":
				if isStopSale {
					fmt.Println("OnSale 2 : isStopSale")
					update = expression.Set(expression.Name("sales_category"), expression.Value(false)).
						Set(expression.Name("rate_rank_id"), expression.Value(sds.RankID)).
						Set(expression.Name("price"), expression.Value(modifiedPrice)).
						Set(expression.Name("rate_rank_name"), expression.Value(price[1])).
						Set(expression.Name("is_special_rank_price"), expression.Value(true)).
						Set(expression.Name("use_date"), expression.Value(useDate)).
						Set(expression.Name("updated_at"), expression.Value(be.DateTimeStamp())).
						Set(expression.Name("room_id"), expression.Value(target.RoomID)).
						Set(expression.Name("created_at"), expression.Name("created_at").IfNotExists(expression.Value(be.DateTimeStamp()))).
						Set(expression.Name("pax"), expression.Value(i))
				} else {
					fmt.Println("OnSale 2 : isStopSale else")
					update = expression.Set(expression.Name("sales_category"), expression.Value(true)).
						Set(expression.Name("rate_rank_id"), expression.Value(sds.RankID)).
						Set(expression.Name("price"), expression.Value(modifiedPrice)).
						Set(expression.Name("rate_rank_name"), expression.Value(price[1])).
						Set(expression.Name("is_special_rank_price"), expression.Value(true)).
						Set(expression.Name("use_date"), expression.Value(useDate)).
						Set(expression.Name("updated_at"), expression.Value(be.DateTimeStamp())).
						Set(expression.Name("room_id"), expression.Value(target.RoomID)).
						Set(expression.Name("created_at"), expression.Name("created_at").IfNotExists(expression.Value(be.DateTimeStamp()))).
						Set(expression.Name("pax"), expression.Value(i))
				}

			case "StopSale":
				fmt.Println("StopSale 2 : StopSale")
				update = expression.Set(expression.Name("sales_category"), expression.Value(false)).
					Set(expression.Name("rate_rank_id"), expression.Value(sds.RankID)).
					Set(expression.Name("price"), expression.Value(modifiedPrice)).
					Set(expression.Name("rate_rank_name"), expression.Value(price[1])).
					Set(expression.Name("is_special_rank_price"), expression.Value(true)).
					Set(expression.Name("use_date"), expression.Value(useDate)).
					Set(expression.Name("updated_at"), expression.Value(be.DateTimeStamp())).
					Set(expression.Name("room_id"), expression.Value(target.RoomID)).
					Set(expression.Name("created_at"), expression.Name("created_at").IfNotExists(expression.Value(be.DateTimeStamp()))).
					Set(expression.Name("pax"), expression.Value(i))

			case "NoChange":
				if isStopSale {
					fmt.Println("NoChange 2 : isStopSale")
					update = expression.Set(expression.Name("rate_rank_id"), expression.Value(sds.RankID)).
						Set(expression.Name("price"), expression.Value(modifiedPrice)).
						Set(expression.Name("rate_rank_name"), expression.Value(price[1])).
						Set(expression.Name("is_special_rank_price"), expression.Value(true)).
						Set(expression.Name("sales_category"), expression.Value(false)).
						Set(expression.Name("use_date"), expression.Value(useDate)).
						Set(expression.Name("updated_at"), expression.Value(be.DateTimeStamp())).
						Set(expression.Name("room_id"), expression.Value(target.RoomID)).
						Set(expression.Name("created_at"), expression.Name("created_at").IfNotExists(expression.Value(be.DateTimeStamp()))).
						Set(expression.Name("pax"), expression.Value(i))
				} else {
					fmt.Println("NoChange 2 : isStopSale else")
					update = expression.Set(expression.Name("rate_rank_id"), expression.Value(sds.RankID)).
						Set(expression.Name("price"), expression.Value(modifiedPrice)).
						Set(expression.Name("rate_rank_name"), expression.Value(price[1])).
						Set(expression.Name("is_special_rank_price"), expression.Value(true)).
						Set(expression.Name("use_date"), expression.Value(useDate)).
						Set(expression.Name("updated_at"), expression.Value(be.DateTimeStamp())).
						Set(expression.Name("room_id"), expression.Value(target.RoomID)).
						Set(expression.Name("created_at"), expression.Name("created_at").IfNotExists(expression.Value(be.DateTimeStamp()))).
						Set(expression.Name("sales_category"), expression.Name("sales_category").IfNotExists(expression.Value(true))).
						Set(expression.Name("pax"), expression.Value(i))
				}

			}
		} else {
			// if price is not modified set is_special_rank_price = false
			// logic to modify content
			switch sds.SalesStatus {
			case "OnSale":
				if isStopSale {
					fmt.Println("StopSale 3 : isStopSale")
					update = expression.Set(expression.Name("sales_category"), expression.Value(false)).
						Set(expression.Name("rate_rank_id"), expression.Value(sds.RankID)).
						Set(expression.Name("price"), expression.Value(modifiedPrice)).
						Set(expression.Name("rate_rank_name"), expression.Value(price[1])).
						Set(expression.Name("is_special_rank_price"), expression.Value(false)).
						Set(expression.Name("use_date"), expression.Value(useDate)).
						Set(expression.Name("updated_at"), expression.Value(be.DateTimeStamp())).
						Set(expression.Name("room_id"), expression.Value(target.RoomID)).
						Set(expression.Name("created_at"), expression.Name("created_at").IfNotExists(expression.Value(be.DateTimeStamp()))).
						Set(expression.Name("pax"), expression.Value(i))
				} else {
					fmt.Println("StopSale 3 : isStopSale")
					update = expression.Set(expression.Name("sales_category"), expression.Value(true)).
						Set(expression.Name("rate_rank_id"), expression.Value(sds.RankID)).
						Set(expression.Name("price"), expression.Value(modifiedPrice)).
						Set(expression.Name("rate_rank_name"), expression.Value(price[1])).
						Set(expression.Name("is_special_rank_price"), expression.Value(false)).
						Set(expression.Name("use_date"), expression.Value(useDate)).
						Set(expression.Name("updated_at"), expression.Value(be.DateTimeStamp())).
						Set(expression.Name("room_id"), expression.Value(target.RoomID)).
						Set(expression.Name("created_at"), expression.Name("created_at").IfNotExists(expression.Value(be.DateTimeStamp()))).
						Set(expression.Name("pax"), expression.Value(i))
				}

			case "StopSale":
				fmt.Println("StopSale 3 : isStopSale")
				update = expression.Set(expression.Name("sales_category"), expression.Value(false)).
					Set(expression.Name("rate_rank_id"), expression.Value(sds.RankID)).
					Set(expression.Name("price"), expression.Value(modifiedPrice)).
					Set(expression.Name("rate_rank_name"), expression.Value(price[1])).
					Set(expression.Name("is_special_rank_price"), expression.Value(false)).
					Set(expression.Name("use_date"), expression.Value(useDate)).
					Set(expression.Name("updated_at"), expression.Value(be.DateTimeStamp())).
					Set(expression.Name("room_id"), expression.Value(target.RoomID)).
					Set(expression.Name("created_at"), expression.Name("created_at").IfNotExists(expression.Value(be.DateTimeStamp()))).
					Set(expression.Name("pax"), expression.Value(i))

			case "NoChange":
				if isStopSale {
					fmt.Println("StopSale 3 : isStopSale")
					update = expression.Set(expression.Name("rate_rank_id"), expression.Value(sds.RankID)).
						Set(expression.Name("price"), expression.Value(modifiedPrice)).
						Set(expression.Name("rate_rank_name"), expression.Value(price[1])).
						Set(expression.Name("is_special_rank_price"), expression.Value(false)).
						Set(expression.Name("sales_category"), expression.Value(false)).
						Set(expression.Name("use_date"), expression.Value(useDate)).
						Set(expression.Name("updated_at"), expression.Value(be.DateTimeStamp())).
						Set(expression.Name("room_id"), expression.Value(target.RoomID)).
						Set(expression.Name("created_at"), expression.Name("created_at").IfNotExists(expression.Value(be.DateTimeStamp()))).
						Set(expression.Name("pax"), expression.Value(i))
				} else {
					fmt.Println("StopSale 3 : isStopSale")
					update = expression.Set(expression.Name("rate_rank_id"), expression.Value(sds.RankID)).
						Set(expression.Name("price"), expression.Value(modifiedPrice)).
						Set(expression.Name("rate_rank_name"), expression.Value(price[1])).
						Set(expression.Name("is_special_rank_price"), expression.Value(false)).
						Set(expression.Name("use_date"), expression.Value(useDate)).
						Set(expression.Name("updated_at"), expression.Value(be.DateTimeStamp())).
						Set(expression.Name("room_id"), expression.Value(target.RoomID)).
						Set(expression.Name("created_at"), expression.Name("created_at").IfNotExists(expression.Value(be.DateTimeStamp()))).
						Set(expression.Name("sales_category"), expression.Name("sales_category").IfNotExists(expression.Value(true))).
						Set(expression.Name("pax"), expression.Value(i))
				}

			}
		}

		var expr1 expression.Expression
		if unrankedChargeRenewNotRequire {
			// cond := expression.Size(expression.Name("is_special_rank_price")).Equal(expression.Value(false))
			// cond := expression.Or(expression.Name("is_special_rank_price").Equal(expression.Value(false)), expression.Name("is_special_rank_price").Equal(expression.Value(false)))
			cond := expression.Name("is_special_rank_price").Equal(expression.Value(false)).Or(expression.AttributeNotExists(expression.Name("is_special_rank_price")))
			expr1, _ = expression.NewBuilder().
				WithCondition(cond).
				WithUpdate(update).
				Build()

		} else {
			expr1, _ = expression.NewBuilder().
				WithUpdate(update).
				Build()
		}

		inputInc := &dynamodb.UpdateItemInput{
			Key: map[string]*dynamodb.AttributeValue{
				"hotel_id": {
					S: aws.String(hotelId),
				},
				"plan_room_use_date_pax": {
					S: aws.String(hash),
				},
			},
			TableName:                 aws.String(tableName),
			ExpressionAttributeNames:  expr1.Names(),
			ExpressionAttributeValues: expr1.Values(),
			UpdateExpression:          expr1.Update(),
			ConditionExpression:       expr1.Condition(),
		}

		_, err := db.UpdateItem(inputInc)
		if err != nil {
			fmt.Println("UpdateItem inputInc : ", inputInc)
			fmt.Println("UpdateItem err ", err)
		} else {
			fmt.Println("UpdateItem inputInc 2 : ", inputInc)
			fmt.Println("Successful Price Update for hash : ", hash)
		}
		fmt.Println("=====================================================================")
	}
}

func PriceUpdateForAll(hotelId string, sds BulkUpdateRequest, db *dynamodb.DynamoDB, tableName string, roomMap map[string]map[string]int, rrpTableName string, updateDates []string) {

	for _, useDate := range updateDates {
		for _, target := range sds.Target {
			// rankPrice = map[PlanRoomPaxRankID] = [RankPrice, RankName]
			rankPrice, err := GetRankPriceData(hotelId, db, rrpTableName, target.RoomID, target.PlanID)
			if err != nil {
				fmt.Println("GetRankPriceData err : ", err)
			}

			minMax := roomMap[target.RoomID]

			for i := minMax["min"]; i <= minMax["max"]; i++ {

				isStopSale := false
				for _, n := range sds.Pax {
					if n == int64(i) {
						isStopSale = true
					}
				}
				// wg.Add(1)
				PriceUpdateRoutine(useDate, rankPrice, minMax, i, isStopSale, target, sds, hotelId, db, tableName, roomMap, rrpTableName, sds.UnrankedChargeRenewNotRequire)

			}

			// 	// hash key for price table
			// 	hash := target.PlanID + "#" + target.RoomID + "#" + fmt.Sprintf("%v", i) + "#" + useDate

			// 	fmt.Println("PriceUpdateForAll hash : ", hash)

			// 	var update expression.UpdateBuilder

			// 	// if RankID not selected
			// 	if sds.RankID == "NoChange" {
			// 		fmt.Println("---------------------------------------------------------------------------------------")
			// 		// if rate rank price need to be modified
			// 		if !sds.PriceChangeNotRequire {
			// 			if sds.PerPersonPriceChangeOrder == "Up" || sds.PerRoomPriceChangeOrder == "Up" {
			// 				// logic to modify content
			// 				switch sds.SalesStatus {
			// 				case "OnSale":
			// 					if isStopSale {
			// 						fmt.Println("OnSale 1 : isStopSale")
			// 						update = expression.Set(expression.Name("sales_category"), expression.Value(false)).
			// 							Set(expression.Name("price"), expression.Name("price").Plus(expression.Value(sds.PerPersonPriceChangeAmount))).
			// 							Set(expression.Name("is_special_rank_price"), expression.Value(true))
			// 					} else {
			// 						fmt.Println("OnSale 1 : else")
			// 						update = expression.Set(expression.Name("sales_category"), expression.Value(true)).
			// 							Set(expression.Name("price"), expression.Name("price").Plus(expression.Value(sds.PerPersonPriceChangeAmount))).
			// 							Set(expression.Name("is_special_rank_price"), expression.Value(true))
			// 					}

			// 				case "StopSale":
			// 					fmt.Println("StopSale 1 : StopSale")
			// 					update = expression.Set(expression.Name("sales_category"), expression.Value(false)).
			// 						Set(expression.Name("price"), expression.Name("price").Plus(expression.Value(sds.PerPersonPriceChangeAmount))).
			// 						Set(expression.Name("is_special_rank_price"), expression.Value(true))

			// 				case "NoChange":
			// 					if isStopSale {
			// 						fmt.Println("NoChange 1 : NoChange")
			// 						update = expression.Set(expression.Name("price"), expression.Name("price").Plus(expression.Value(sds.PerPersonPriceChangeAmount))).
			// 							Set(expression.Name("sales_category"), expression.Value(false))
			// 					} else {
			// 						fmt.Println("NoChange 1 : NoChange else")
			// 						update = expression.Set(expression.Name("price"), expression.Name("price").Plus(expression.Value(sds.PerPersonPriceChangeAmount)))
			// 					}

			// 				}
			// 			}

			// 			if sds.PerPersonPriceChangeOrder == "Down" || sds.PerRoomPriceChangeOrder == "Down" {
			// 				// logic to modify content
			// 				switch sds.SalesStatus {
			// 				case "OnSale":
			// 					if isStopSale {
			// 						fmt.Println("OnSale 1 : isStopSale")
			// 						update = expression.Set(expression.Name("sales_category"), expression.Value(false)).
			// 							Set(expression.Name("price"), expression.Name("price").Minus(expression.Value(sds.PerPersonPriceChangeAmount))).
			// 							Set(expression.Name("is_special_rank_price"), expression.Value(true))
			// 					} else {
			// 						fmt.Println("OnSale 1 : isStopSale else")
			// 						update = expression.Set(expression.Name("sales_category"), expression.Value(true)).
			// 							Set(expression.Name("price"), expression.Name("price").Minus(expression.Value(sds.PerPersonPriceChangeAmount))).
			// 							Set(expression.Name("is_special_rank_price"), expression.Value(true))
			// 					}

			// 				case "StopSale":
			// 					fmt.Println("StopSale 1 : StopSale")
			// 					update = expression.Set(expression.Name("sales_category"), expression.Value(false)).
			// 						Set(expression.Name("price"), expression.Name("price").Minus(expression.Value(sds.PerPersonPriceChangeAmount))).
			// 						Set(expression.Name("is_special_rank_price"), expression.Value(true))
			// 				case "NoChange":
			// 					if isStopSale {
			// 						fmt.Println("NoChange 1 : NoChange")
			// 						update = expression.Set(expression.Name("price"), expression.Name("price").Minus(expression.Value(sds.PerPersonPriceChangeAmount))).
			// 							Set(expression.Name("sales_category"), expression.Value(false))
			// 					} else {
			// 						fmt.Println("NoChange 1 : NoChange else")
			// 						update = expression.Set(expression.Name("price"), expression.Name("price").Minus(expression.Value(sds.PerPersonPriceChangeAmount)))
			// 					}

			// 				}
			// 			}

			// 			cond := expression.Name("plan_room_use_date_pax").Equal(expression.Value(hash))

			// 			expr1, err := expression.NewBuilder().
			// 				WithUpdate(update).
			// 				WithCondition(cond).
			// 				Build()

			// 			inputInc := &dynamodb.UpdateItemInput{
			// 				Key: map[string]*dynamodb.AttributeValue{
			// 					"hotel_id": {
			// 						S: aws.String(hotelId),
			// 					},
			// 					"plan_room_use_date_pax": {
			// 						S: aws.String(hash),
			// 					},
			// 				},
			// 				TableName:                 aws.String(tableName),
			// 				ExpressionAttributeNames:  expr1.Names(),
			// 				ExpressionAttributeValues: expr1.Values(),
			// 				UpdateExpression:          expr1.Update(),
			// 				ConditionExpression:       expr1.Condition(),
			// 			}

			// 			_, err = db.UpdateItem(inputInc)
			// 			if err != nil {
			// 				fmt.Println("UpdateItem err inputInc", inputInc)
			// 				fmt.Println("UpdateItem err 1", err)
			// 			} else {
			// 				fmt.Println("UpdateItem inputInc 1 : ", inputInc)
			// 				fmt.Println("Successful Price Update for hash : ", hash)

			// 			}
			// 			fmt.Println("=====================================================================")
			// 		}
			// 	} else {

			// 		// key for rate rank price map
			// 		rankPriceKey := target.PlanID + "#" + target.RoomID + "#" + fmt.Sprintf("%v", i) + "#" + sds.RankID
			// 		price := rankPrice[rankPriceKey]

			// 		// logic to adjust the rank price
			// 		a, _ := strconv.Atoi(price[0])
			// 		modifiedPrice := float64(a)

			// 		flg := false

			// 		if !sds.PriceChangeNotRequire {
			// 			if sds.PerPersonPriceChangeOrder == "Up" {
			// 				fmt.Println("modifiedPrice 1: ", modifiedPrice)
			// 				modifiedPrice = modifiedPrice + sds.PerPersonPriceChangeAmount
			// 				flg = true
			// 				fmt.Println("PerPersonPriceChangeAmount 1: ", modifiedPrice)
			// 				fmt.Println("modifiedPrice 1: ", sds.PerPersonPriceChangeAmount)
			// 			}
			// 			if sds.PerPersonPriceChangeOrder == "Down" {
			// 				modifiedPrice = modifiedPrice - sds.PerPersonPriceChangeAmount
			// 				flg = true
			// 				fmt.Println("modifiedPrice 2: ", modifiedPrice)
			// 			}

			// 			if sds.PerRoomPriceChangeOrder == "Up" {
			// 				modifiedPrice = modifiedPrice + sds.PerRoomPriceChangeAmount
			// 				flg = true
			// 				fmt.Println("modifiedPrice 3: ", modifiedPrice)
			// 			}
			// 			if sds.PerRoomPriceChangeOrder == "Down" {
			// 				modifiedPrice = modifiedPrice - sds.PerRoomPriceChangeAmount
			// 				flg = true
			// 				fmt.Println("modifiedPrice 4: ", modifiedPrice)
			// 			}
			// 		}

			// 		// if price is modified set is_special_rank_price = true
			// 		if flg {
			// 			// logic to modify content
			// 			switch sds.SalesStatus {
			// 			case "OnSale":
			// 				if isStopSale {
			// 					fmt.Println("OnSale 2 : isStopSale")
			// 					update = expression.Set(expression.Name("sales_category"), expression.Value(false)).
			// 						Set(expression.Name("rate_rank_id"), expression.Value(sds.RankID)).
			// 						Set(expression.Name("price"), expression.Value(modifiedPrice)).
			// 						Set(expression.Name("rate_rank_name"), expression.Value(price[1])).
			// 						Set(expression.Name("is_special_rank_price"), expression.Value(true))
			// 				} else {
			// 					fmt.Println("OnSale 2 : isStopSale else")
			// 					update = expression.Set(expression.Name("sales_category"), expression.Value(true)).
			// 						Set(expression.Name("rate_rank_id"), expression.Value(sds.RankID)).
			// 						Set(expression.Name("price"), expression.Value(modifiedPrice)).
			// 						Set(expression.Name("rate_rank_name"), expression.Value(price[1])).
			// 						Set(expression.Name("is_special_rank_price"), expression.Value(true))
			// 				}

			// 			case "StopSale":
			// 				fmt.Println("StopSale 2 : StopSale")
			// 				update = expression.Set(expression.Name("sales_category"), expression.Value(false)).
			// 					Set(expression.Name("rate_rank_id"), expression.Value(sds.RankID)).
			// 					Set(expression.Name("price"), expression.Value(modifiedPrice)).
			// 					Set(expression.Name("rate_rank_name"), expression.Value(price[1])).
			// 					Set(expression.Name("is_special_rank_price"), expression.Value(true))

			// 			case "NoChange":
			// 				if isStopSale {
			// 					fmt.Println("NoChange 2 : isStopSale")
			// 					update = expression.Set(expression.Name("rate_rank_id"), expression.Value(sds.RankID)).
			// 						Set(expression.Name("price"), expression.Value(modifiedPrice)).
			// 						Set(expression.Name("rate_rank_name"), expression.Value(price[1])).
			// 						Set(expression.Name("is_special_rank_price"), expression.Value(true)).
			// 						Set(expression.Name("sales_category"), expression.Value(false))
			// 				} else {
			// 					fmt.Println("NoChange 2 : isStopSale else")
			// 					update = expression.Set(expression.Name("rate_rank_id"), expression.Value(sds.RankID)).
			// 						Set(expression.Name("price"), expression.Value(modifiedPrice)).
			// 						Set(expression.Name("rate_rank_name"), expression.Value(price[1])).
			// 						Set(expression.Name("is_special_rank_price"), expression.Value(true))
			// 				}

			// 			}
			// 		} else {
			// 			// if price is not modified set is_special_rank_price = false
			// 			// logic to modify content
			// 			switch sds.SalesStatus {
			// 			case "OnSale":
			// 				if isStopSale {
			// 					fmt.Println("StopSale 3 : isStopSale")
			// 					update = expression.Set(expression.Name("sales_category"), expression.Value(false)).
			// 						Set(expression.Name("rate_rank_id"), expression.Value(sds.RankID)).
			// 						Set(expression.Name("price"), expression.Value(modifiedPrice)).
			// 						Set(expression.Name("rate_rank_name"), expression.Value(price[1])).
			// 						Set(expression.Name("is_special_rank_price"), expression.Value(false))
			// 				} else {
			// 					fmt.Println("StopSale 3 : isStopSale")
			// 					update = expression.Set(expression.Name("sales_category"), expression.Value(true)).
			// 						Set(expression.Name("rate_rank_id"), expression.Value(sds.RankID)).
			// 						Set(expression.Name("price"), expression.Value(modifiedPrice)).
			// 						Set(expression.Name("rate_rank_name"), expression.Value(price[1])).
			// 						Set(expression.Name("is_special_rank_price"), expression.Value(false))
			// 				}

			// 			case "StopSale":
			// 				fmt.Println("StopSale 3 : isStopSale")
			// 				update = expression.Set(expression.Name("sales_category"), expression.Value(false)).
			// 					Set(expression.Name("rate_rank_id"), expression.Value(sds.RankID)).
			// 					Set(expression.Name("price"), expression.Value(modifiedPrice)).
			// 					Set(expression.Name("rate_rank_name"), expression.Value(price[1])).
			// 					Set(expression.Name("is_special_rank_price"), expression.Value(false))

			// 			case "NoChange":
			// 				if isStopSale {
			// 					fmt.Println("StopSale 3 : isStopSale")
			// 					update = expression.Set(expression.Name("rate_rank_id"), expression.Value(sds.RankID)).
			// 						Set(expression.Name("price"), expression.Value(modifiedPrice)).
			// 						Set(expression.Name("rate_rank_name"), expression.Value(price[1])).
			// 						Set(expression.Name("is_special_rank_price"), expression.Value(false)).
			// 						Set(expression.Name("sales_category"), expression.Value(false))
			// 				} else {
			// 					fmt.Println("StopSale 3 : isStopSale")
			// 					update = expression.Set(expression.Name("rate_rank_id"), expression.Value(sds.RankID)).
			// 						Set(expression.Name("price"), expression.Value(modifiedPrice)).
			// 						Set(expression.Name("rate_rank_name"), expression.Value(price[1])).
			// 						Set(expression.Name("is_special_rank_price"), expression.Value(false))
			// 				}

			// 			}
			// 		}

			// 		expr1, err := expression.NewBuilder().
			// 			WithUpdate(update).
			// 			Build()

			// 		inputInc := &dynamodb.UpdateItemInput{
			// 			Key: map[string]*dynamodb.AttributeValue{
			// 				"hotel_id": {
			// 					S: aws.String(hotelId),
			// 				},
			// 				"plan_room_use_date_pax": {
			// 					S: aws.String(hash),
			// 				},
			// 			},
			// 			TableName:                 aws.String(tableName),
			// 			ExpressionAttributeNames:  expr1.Names(),
			// 			ExpressionAttributeValues: expr1.Values(),
			// 			UpdateExpression:          expr1.Update(),
			// 		}

			// 		_, err = db.UpdateItem(inputInc)
			// 		if err != nil {
			// 			fmt.Println("UpdateItem inputInc : ", inputInc)
			// 			fmt.Println("UpdateItem err ", err)
			// 		} else {
			// 			fmt.Println("UpdateItem inputInc 2 : ", inputInc)
			// 			fmt.Println("Successful Price Update for hash : ", hash)
			// 		}
			// 		fmt.Println("=====================================================================")
			// 	}
			// }

		}
	}
	// wg.Wait()
}

func main() {
	lambda.Start(handler)
}

func GetRoomPax(hotelID string, tableName string, db *dynamodb.DynamoDB) (map[string]map[string]int, error) {
	roomMap := make(map[string]map[string]int)
	// input for GetItem
	input := &dynamodb.QueryInput{
		ExpressionAttributeValues: map[string]*dynamodb.AttributeValue{
			":hotel_id": {
				S: aws.String(hotelID),
			},
		},
		KeyConditionExpression: aws.String("hotel_id = :hotel_id"),
		TableName:              aws.String(tableName),
	}

	// GetItem from dynamodb table
	result, err := db.Query(input)
	if err != nil {
		return roomMap, err
	}
	if len(result.Items) > 0 {

		items := new([]be.Room)
		err = dynamodbattribute.UnmarshalListOfMaps(result.Items, items)
		if err != nil {
			return roomMap, err
		}

		for _, it := range *items {
			minMaxPax := make(map[string]int)
			minMaxPax["min"] = int(it.MinOccupancy)
			minMaxPax["max"] = int(it.MaxPax)

			roomMap[it.ID] = minMaxPax
		}

		return roomMap, nil
	} else {
		return roomMap, errors.New("Room not found")
	}
}

func GetRankPriceData(hotel_id string, db *dynamodb.DynamoDB, rrTableName string, room_id string, plan_id string) (map[string][2]string, error) {

	rrpData := make(map[string][2]string, 0)
	keyCond := expression.KeyAnd(expression.Key("hotel_id").Equal(expression.Value(hotel_id)), expression.Key("plan_room_rank_pax").BeginsWith(plan_id+"#"+room_id))

	item := new([]*be.RankPrice)
	expr, err := expression.NewBuilder().WithKeyCondition(keyCond).Build()
	if err != nil {
		return rrpData, err
	}

	// input for GetItem
	input := &dynamodb.QueryInput{
		KeyConditionExpression:    expr.KeyCondition(),
		TableName:                 aws.String(rrTableName),
		ExpressionAttributeNames:  expr.Names(),
		ExpressionAttributeValues: expr.Values(),
	}

	// GetItem from dynamodb table
	result, err := db.Query(input)
	if err != nil {
		return rrpData, err
	}

	err = dynamodbattribute.UnmarshalListOfMaps(result.Items, &item)
	if err != nil {
		return rrpData, err
	}

	for _, rrp := range *item {
		var temp [2]string

		p := strings.Split(rrp.PlanRoomRankPax, "#")
		key := p[0] + "#" + p[1] + "#" + p[3] + "#" + rrp.RateRankID

		temp[0] = fmt.Sprintf("%v", rrp.Price)
		temp[1] = p[2]

		rrpData[key] = temp
	}

	return rrpData, nil
}

func GetHolidays(timeMin string, timeMax string, dayType string) ([]string, error) {

	days := make([]string, 0)
	if dayType == "All" {
		startDate, _ := time.Parse("2006-01-02", timeMin)
		endDate, _ := time.Parse("2006-01-02", timeMax)

		endDate = endDate.AddDate(0, 0, 1)
		for {
			if startDate.Before(endDate) {
				days = append(days, startDate.Format("2006-01-02"))
				startDate = startDate.AddDate(0, 0, 1)
			} else {
				break
			}
		}
	} else {
		cinDate, _ := time.Parse("2006-01-02", timeMin)

		timeMin = timeMin + "T00:00:00Z"
		timeMax = timeMax + "T00:00:00Z"

		key := "AIzaSyAs6bBX_abHHXCmfLi3tPVxw6BxZX2sIA8"
		country := "japanese"
		lang := "ja"
		calendarId := country + "__" + lang + "@holiday.calendar.google.com"
		URL := "https://www.googleapis.com/calendar/v3/calendars/" + calendarId + "/events"

		maxResults := 365
		orderBy := "startTime"
		singleEvents := true

		URL = fmt.Sprintf("%v?key=%v&timeMin=%v&timeMax=%v&maxResults=%v&orderBy=%v&singleEvents=%v", URL, key, timeMin, timeMax, maxResults, orderBy, singleEvents)

		var sds Holiday
		resp, err := http.Get(URL)
		if err != nil {
			return days, err
		}

		//We Read the response body on the line below.
		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			log.Fatalln(err)
		}
		if err := json.Unmarshal([]byte(body), &sds); err != nil {
			return days, err
		}

		for _, holiday := range sds.Items {

			// get one back date to apply date.before
			cinDate = cinDate.AddDate(0, 0, -1)

			startDate, _ := time.Parse("2006-01-02", holiday.Start.Date)
			endDate, _ := time.Parse("2006-01-02", holiday.End.Date)

			// endDate = endDate.AddDate(0, 0, 1)
			for {
				if startDate.Before(endDate) {
					if dayType == "Holiday" {
						days = append(days, startDate.Format("2006-01-02"))
					} else {
						beforeHoliday := startDate.AddDate(0, 0, -1)
						if cinDate.Before(beforeHoliday) {
							fmt.Printf("***** cinDate : %v : beforeHoliday : %v", cinDate, beforeHoliday)
							days = append(days, beforeHoliday.Format("2006-01-02"))
						}
					}
					startDate = startDate.AddDate(0, 0, 1)
				} else {
					break
				}
			}
		}
	}

	return days, nil
}

func GetDateFromDay(timeMin string, timeMax string, dayList []string) ([]string, error) {

	days := make([]string, 0)
	startDate, _ := time.Parse("2006-01-02", timeMin)
	endDate, _ := time.Parse("2006-01-02", timeMax)

	endDate = endDate.AddDate(0, 0, 1)
	for {
		if startDate.Before(endDate) {

			dayOfDate := startDate.Weekday().String()
			for _, d := range dayList {
				if d == dayOfDate {
					days = append(days, startDate.Format("2006-01-02"))
				}
			}

			startDate = startDate.AddDate(0, 0, 1)
		} else {
			break
		}
	}

	return days, nil
}
