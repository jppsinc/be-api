package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"os"
	"strconv"
	"strings"

	be "bitbucket.org/jppsinc/be-modules/pkg"

	valid "github.com/asaskevich/govalidator"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/aws/aws-sdk-go/service/dynamodb/expression"
)

type PriceUpdateRequest struct {
	Target []struct {
		PlanID string `json:"plan_id"`
		RoomID string `json:"room_id"`
	} `json:"target"`
	Dates []struct {
		Date          string `json:"date"`
		RankID        string `json:"rank_id"`
		StopSalePaxes []int  `json:"stop_sale_paxes"`
	} `json:"dates"`
}

func handler(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {

	org := request.Headers["origin"]
	// Table name
	// priceTableName := "be_prices-dev"
	// roomTableName := "be_rooms-dev"
	// rrpTableName := "be_rank_prices-dev"
	// planTableName := "be_plans-dev"

	priceTableName := os.Getenv("PRICE_TABLE_NAME")
	roomTableName := os.Getenv("ROOM_TABLE_NAME")
	planTableName := os.Getenv("PLAN_TABLE_NAME")
	rrpTableName := os.Getenv("RRP_TABLE_NAME")

	// Change JSON request data to struct  : just to check validity of request data
	// var sds be.Price
	var sds PriceUpdateRequest

	if err := json.Unmarshal([]byte(request.Body), &sds); err != nil {
		fmt.Println("err 1:", err)
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorCouldNotUnMarshalItem).Error()),
		})
	}

	hotelId, ok := request.PathParameters["hotel_id"]
	if ok == false || valid.IsUUIDv4(hotelId) == false {
		fmt.Println("err 3 : PathParameters")
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorInvalidUrlHotelIdNotFound).Error()),
		})
	}

	// DynamoDB
	db := be.Dynamodb()

	target := sds.Target
	dates := sds.Dates

	for _, t := range target {

		planID := t.PlanID
		roomID := t.RoomID

		fmt.Printf("plan id : %v : room id : %v\n", planID, roomID)

		minPax, maxPax, roomType, err := GetRoomPax(hotelId, roomID, roomTableName, db)
		if err != nil {
			return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
				ErrorMsg: aws.String(err.Error()),
			})
		}
		_ = roomType
		fmt.Printf("minPax : %v : maxPax : %v\n", minPax, maxPax)

		planData, err := GetPlan(hotelId, planID, planTableName, db)
		if err != nil {
			return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
				ErrorMsg: aws.String(err.Error()),
			})
		}

		rankPrice, err := GetRankPriceData(hotelId, db, rrpTableName, roomID, planID)
		if err != nil {
			return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
				ErrorMsg: aws.String(err.Error()),
			})
		}

		// fmt.Printf("rankPrice : %v \n", rankPrice)

		for _, d := range dates {

			// rank price update logic
			// rank id is uuid it should be longer
			if len(d.RankID) > 6 {

				fmt.Printf("inside rank update : d.RankID : %v\n", d.RankID)
				for i := minPax; i <= maxPax; i++ {
					hash := planID + "#" + roomID + "#" + strconv.Itoa((i)) + "#" + d.Date

					rankPriceKey := planID + "#" + roomID + "#" + strconv.Itoa((i)) + "#" + d.RankID

					price, _ := strconv.ParseFloat(rankPrice[rankPriceKey][0], 64)
					// if price == 0 {
					// 	return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
					// 		ErrorMsg: aws.String(errors.New(be.ErrorInvalidUrlHotelIdNotFound).Error()),
					// 	})
					// }

					fmt.Printf("hash : %v : rankPriceKey : %v : price : %v\n", hash, rankPriceKey, price)

					cs := ""
					if planData.Pricing == "PerRoom" {
						cs = "RC"
					} else {
						cs = "PPC"
					}

					var update expression.UpdateBuilder

					update = expression.Set(expression.Name("price"), expression.Value(price)).
						Set(expression.Name("use_date"), expression.Value(d.Date)).
						Set(expression.Name("room_id"), expression.Value(roomID)).
						Set(expression.Name("rate_rank_id"), expression.Value(d.RankID)).
						Set(expression.Name("rate_rank_name"), expression.Value(rankPrice[rankPriceKey][1])).
						Set(expression.Name("sales_category"), expression.Value(true)).
						Set(expression.Name("room_type"), expression.Value(roomType)).
						Set(expression.Name("pax"), expression.Value(i)).
						Set(expression.Name("charge_system"), expression.Value(cs)).
						Set(expression.Name("plan_title"), expression.Value(planData.Title)).
						Set(expression.Name("created_at"), expression.Name("created_at").IfNotExists(expression.Value(be.DateTimeStamp()))).
						Set(expression.Name("updated_at"), expression.Value(be.DateTimeStamp()))

					expr1, err := expression.NewBuilder().
						WithUpdate(update).
						Build()

					inputInc := &dynamodb.UpdateItemInput{
						Key: map[string]*dynamodb.AttributeValue{
							"hotel_id": {
								S: aws.String(hotelId),
							},
							"plan_room_use_date_pax": {
								S: aws.String(hash),
							},
						},
						TableName:                 aws.String(priceTableName),
						ExpressionAttributeNames:  expr1.Names(),
						ExpressionAttributeValues: expr1.Values(),
						UpdateExpression:          expr1.Update(),
					}

					fmt.Println("inputInc ", inputInc)

					_, err = db.UpdateItem(inputInc)
					if err != nil {
						fmt.Println("UpdateItem err ", err)
					}

					fmt.Println("Rank update finished")
				}
			}

			// stop sale logic
			if len(d.StopSalePaxes) > 0 {
				fmt.Printf("inside stop sale update\n")
				for j := 0; j < len(d.StopSalePaxes); j++ {

					// hash := planID + "#" + roomID + "#" + strconv.Itoa((d.StopSalePaxes[j])) + d.RankID
					hash := planID + "#" + roomID + "#" + strconv.Itoa((d.StopSalePaxes[j])) + "#" + d.Date

					fmt.Printf("hash : %v\n", hash)

					update := expression.Set(expression.Name("sales_category"), expression.Value(false))

					expr1, err := expression.NewBuilder().
						WithUpdate(update).
						Build()

					inputInc := &dynamodb.UpdateItemInput{
						Key: map[string]*dynamodb.AttributeValue{
							"hotel_id": {
								S: aws.String(hotelId),
							},
							"plan_room_use_date_pax": {
								S: aws.String(hash),
							},
						},
						TableName:                 aws.String(priceTableName),
						ExpressionAttributeNames:  expr1.Names(),
						ExpressionAttributeValues: expr1.Values(),
						UpdateExpression:          expr1.Update(),
					}

					_, err = db.UpdateItem(inputInc)
					if err != nil {
						fmt.Println("UpdateItem err ", err)
					}

					fmt.Println("stop sale update finished")
				}
			}

			fmt.Println("Total Update finished")
		}
	}

	return be.ApiResponse(org, http.StatusOK, sds)
}

func main() {
	lambda.Start(handler)
}

func GetPlan(hotelId string, planID string, planTableName string, db *dynamodb.DynamoDB) (be.Plan, error) {

	item := be.Plan{}
	// input for GetItem
	input := &dynamodb.GetItemInput{
		Key: map[string]*dynamodb.AttributeValue{
			"hotel_id": {
				S: aws.String(hotelId),
			},
			"id": {
				S: aws.String(planID),
			},
		},
		TableName: aws.String(planTableName),
	}
	// fmt.Println("err 3:", err)
	// fmt.Println("input :", input)
	// GetItem from dynamodb table
	result, err := db.GetItem(input)
	if err != nil {
		return item, err
	}
	if len(result.Item) > 0 {

		err = dynamodbattribute.UnmarshalMap(result.Item, &item)
		if err != nil {
			return item, err
		}

		return item, nil
	} else {
		return item, errors.New("Plan not found")
	}
}

func GetRoomPax(hotelID string, roomID string, tableName string, db *dynamodb.DynamoDB) (int, int, be.MultiLang, error) {

	// input for GetItem
	input := &dynamodb.GetItemInput{
		Key: map[string]*dynamodb.AttributeValue{
			"hotel_id": {
				S: aws.String(hotelID),
			},
			"id": {
				S: aws.String(roomID),
			},
		},
		TableName: aws.String(tableName),
	}
	// fmt.Println("err 3:", err)
	// fmt.Println("input :", input)
	// GetItem from dynamodb table
	result, err := db.GetItem(input)
	if err != nil {
		return 0, 0, be.MultiLang{}, err
	}
	if len(result.Item) > 0 {
		item := new(be.Room)
		err = dynamodbattribute.UnmarshalMap(result.Item, item)
		if err != nil {
			return 0, 0, be.MultiLang{}, err
		}

		return int(item.MinOccupancy), int(item.MaxPax), item.RoomType, nil
	} else {
		return 0, 0, be.MultiLang{}, errors.New("Room not found")
	}

}

func GetRankPriceData(hotel_id string, db *dynamodb.DynamoDB, rrTableName string, room_id string, plan_id string) (map[string][2]string, error) {

	rrpData := make(map[string][2]string, 0)
	keyCond := expression.KeyAnd(expression.Key("hotel_id").Equal(expression.Value(hotel_id)), expression.Key("plan_room_rank_pax").BeginsWith(plan_id+"#"+room_id))

	item := new([]*be.RankPrice)
	expr, err := expression.NewBuilder().WithKeyCondition(keyCond).Build()
	if err != nil {
		return rrpData, err
	}

	// input for GetItem
	input := &dynamodb.QueryInput{
		KeyConditionExpression:    expr.KeyCondition(),
		TableName:                 aws.String(rrTableName),
		ExpressionAttributeNames:  expr.Names(),
		ExpressionAttributeValues: expr.Values(),
	}

	// GetItem from dynamodb table
	result, err := db.Query(input)
	if err != nil {
		return rrpData, err
	}

	if len(result.Items) == 0 {
		return rrpData, errors.New("Rate rank price not set for this plan and room combination")
	}

	err = dynamodbattribute.UnmarshalListOfMaps(result.Items, &item)
	if err != nil {
		return rrpData, err
	}

	for _, rrp := range *item {
		var temp [2]string

		p := strings.Split(rrp.PlanRoomRankPax, "#")
		key := p[0] + "#" + p[1] + "#" + p[3] + "#" + rrp.RateRankID

		temp[0] = fmt.Sprintf("%v", rrp.Price)
		temp[1] = p[2]

		rrpData[key] = temp
	}

	return rrpData, nil
}
