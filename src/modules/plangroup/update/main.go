package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"os"

	be "bitbucket.org/jppsinc/be-modules/pkg"

	valid "github.com/asaskevich/govalidator"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/aws/aws-sdk-go/service/dynamodb/expression"
)

func handler(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {

	org := request.Headers["origin"]
	// Table name
	// tableName := "be_rate_ranks"
	tableName := os.Getenv("TABLE_NAME")

	// Change JSON request data to struct : just to check validity of request data
	var inq be.PlanGroup

	if err := json.Unmarshal([]byte(request.Body), &inq); err != nil {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorCouldNotUnMarshalItem).Error()),
		})
	}

	// Request path parameter
	id, ok := request.PathParameters["id"]
	if ok == false || valid.IsUUIDv4(id) == false {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorInvalidUrlIdNotFound).Error()),
		})
	}

	hotel_id, ok := request.PathParameters["hotel_id"]
	if ok == false || valid.IsUUIDv4(hotel_id) == false {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorInvalidUrlHotelIdNotFound).Error()),
		})
	} else {
		inq.HotelID = hotel_id
		inq.ID = id
		inq.UpdatedAt = be.DateTimeStamp()
	}

	// DynamoDB
	db := be.Dynamodb()

	// change struct data to json data, this data will be stored to database
	av, err := dynamodbattribute.MarshalMap(inq)
	if err != nil {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorCouldNotMarshalItem).Error()),
		})
	}

	ReturnItem := "SIZE"
	input := &dynamodb.PutItemInput{
		Item:                        av,
		TableName:                   aws.String(tableName),
		ReturnItemCollectionMetrics: &ReturnItem,
	}

	_, err = db.PutItem(input)
	if err != nil {
		fmt.Println(err)
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorCouldNotDynamoPutItem).Error()),
		})
	}
	err = UpdatePlan(hotel_id, inq)
	if err != nil{
		fmt.Println("UpdatePlan err : ", err)
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorCouldNotDynamoPutItem).Error()),
		})
	}
	return be.ApiResponse(org, http.StatusOK, &inq)
}

func main() {
	lambda.Start(handler)
}

func UpdatePlan(hotel_id string, pgData be.PlanGroup) error {

	// tableName := "be_plans"
	// rTableName := "be_rooms"
	planTableName := os.Getenv("PLAN_TABLE_NAME")

	keyCond := expression.Key("hotel_id").Equal(expression.Value(hotel_id))
	filter := expression.Name("status").NotEqual(expression.Value("Delete"))

	expr, err := expression.NewBuilder().WithKeyCondition(keyCond).WithFilter(filter).Build()
	if err != nil {
		return err
	}

	// input for GetItem
	input := &dynamodb.QueryInput{
		KeyConditionExpression:    expr.KeyCondition(),
		TableName:                 aws.String(planTableName),
		ExpressionAttributeNames:  expr.Names(),
		ExpressionAttributeValues: expr.Values(),
		FilterExpression:          expr.Filter(),
	}

	// DynamoDB
	db := be.Dynamodb()

	// GetItem from dynamodb table
	result, err := db.Query(input)
	if err != nil {
		return err
	}

	item := new([]*be.Plan)
	err = dynamodbattribute.UnmarshalListOfMaps(result.Items, &item)
	if err != nil {
		return err
	}
	
	for _, plan := range *item {

		pgDataList := make([]be.PlanGroupList, 0)
		for _, pg := range plan.PlanGroupList {

			fmt.Printf("plan id : %v : pgid : %v \n", plan.ID, pg.ID)
			if pg.ID == pgData.ID {
				temp := be.PlanGroupList{}
				temp.ID = pgData.ID
				temp.Name = pgData.Name

				pgDataList = append(pgDataList, temp)
			} else {
				pgDataList = append(pgDataList, pg)
			}
		}

		if len(pgDataList) > 0 {
			update := expression.Set(expression.Name("plan_group_list"), expression.Value(pgDataList))
			expr1, _ := expression.NewBuilder().
				WithUpdate(update).
				Build()
	
			input := &dynamodb.UpdateItemInput{
				Key: map[string]*dynamodb.AttributeValue{
					"hotel_id": {
						S: aws.String(hotel_id),
					},
					"id": {
						S: aws.String(plan.ID),
					},
				},
				TableName:                 aws.String(planTableName),
				ExpressionAttributeNames:  expr1.Names(),
				ExpressionAttributeValues: expr1.Values(),
				UpdateExpression:          expr1.Update(),
			}
	
			fmt.Println("==========================")
			fmt.Printf("hotel_id : %v : plan.ID : %v : pgDataList : %v\n", hotel_id, plan.ID, pgDataList)
			_, err = db.UpdateItem(input)
			if err != nil {
				return err
			}
		}
	}

	return nil
}