package main

import (
	"fmt"
	"net/http"
	"os"

	"github.com/guregu/dynamo"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"

	be "bitbucket.org/jppsinc/be-modules/pkg"
)

type Response struct {
	PortalSiteID   string             `json:"portal_site_id"`
	PortalSiteName string             `json:"portal_site_name"`
	PortalSiteTags []be.PortalSiteTag `json:"portal_site_tags"`
}

var portalSiteTable dynamo.Table
var portalSiteTagTable dynamo.Table
var groupHotelTable dynamo.Table

func init() {
	portalSite := os.Getenv("PORTAL_SITE")
	portalSiteTag := os.Getenv("PORTAL_SITE_TAG")
	groupHotel := os.Getenv("GROUP_HOTEL")

	sess := session.Must(session.NewSession())
	db := dynamo.New(sess, &aws.Config{Region: aws.String(os.Getenv("AWS_REGION"))})

	portalSiteTable = db.Table(portalSite)
	portalSiteTagTable = db.Table(portalSiteTag)
	groupHotelTable = db.Table(groupHotel)
}

func main() {
	lambda.Start(handler)
}

func handler(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	org := request.Headers["origin"]

	hotelID, ok := request.PathParameters["hotel_id"]
	if !ok {
		fmt.Println("パラメータが不正です")
		return be.ApiResponse(org, http.StatusBadRequest, "パラメータが不正です")
	}

	groupHotel, err := getGroupHotel(hotelID)
	if err != nil {
		fmt.Println(err.Error())
		return be.ApiResponse(org, http.StatusInternalServerError, err.Error())
	}

	response, err := getPortalSiteTags(groupHotel)
	if err != nil {
		fmt.Println(err.Error())
		return be.ApiResponse(org, http.StatusInternalServerError, err.Error())
	}

	return be.ApiResponse(org, http.StatusOK, response)
}

func getGroupHotel(hotelID string) (be.GroupHotel, error) {
	var groupHotel be.GroupHotel
	err := groupHotelTable.Get("hotel_id", hotelID).One(&groupHotel)
	if err != nil {
		return be.GroupHotel{}, err
	}

	return groupHotel, nil
}

func getPortalSiteTags(groupHotel be.GroupHotel) ([]Response, error) {
	var response []Response

	for _, portalSiteInfo := range groupHotel.PortalSiteInfoList {
		var tagsData Response
		var portalSite be.PortalSite
		var portalSiteTags []be.PortalSiteTag
		//portal-site-id必要か？

		err := portalSiteTable.Get("id", portalSiteInfo.PortalSiteID).One(&portalSite)
		if err != nil {
			return nil, err
		}

		err = portalSiteTagTable.Get("portal_site_id", portalSiteInfo.PortalSiteID).Filter("category = ?", "room").All(&portalSiteTags)
		if err != nil {
			return nil, err
		}
		tagsData.PortalSiteID = portalSite.ID
		tagsData.PortalSiteName = portalSite.Name.Ja
		tagsData.PortalSiteTags = portalSiteTags

		response = append(response, tagsData)

	}

	return response, nil
}
