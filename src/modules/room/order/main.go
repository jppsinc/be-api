package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"os"

	be "bitbucket.org/jppsinc/be-modules/pkg"

	valid "github.com/asaskevich/govalidator"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/expression"
)

func handler(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {

	org := request.Headers["origin"]
	// Table name
	// tableName := "be_rooms"
	tableName := os.Getenv("TABLE_NAME")

	// Change JSON request data to struct  : just to check validity of request data
	// plan and room have same put body schema for reordering
	var sds be.PlanOrder

	if err := json.Unmarshal([]byte(request.Body), &sds); err != nil {
		fmt.Println("err 1:", err)
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorCouldNotUnMarshalItem).Error()),
		})
	}

	hotel_id, ok := request.PathParameters["hotel_id"]
	if ok == false || valid.IsUUIDv4(hotel_id) == false {
		fmt.Println("err 3:")
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorInvalidUrlHotelIdNotFound).Error()),
		})
	}

	// DynamoDB
	db := be.Dynamodb()

	var transactItems []*dynamodb.TransactWriteItem
	for j := 0; j < len(sds); j++ {

		fmt.Println("j : ", j)

		update := expression.Set(expression.Name("order"), expression.Value(sds[j].Order))
		expr, _ := expression.NewBuilder().
			WithUpdate(update).
			Build()

		// create put item for transactItems
		transactItems = append(transactItems, &dynamodb.TransactWriteItem{
			Update: &dynamodb.Update{
				TableName: aws.String(tableName),
				Key: map[string]*dynamodb.AttributeValue{
					"hotel_id": {
						S: aws.String(hotel_id),
					},
					"id": {
						S: aws.String(sds[j].ID),
					},
				},
				ExpressionAttributeNames:  expr.Names(),
				ExpressionAttributeValues: expr.Values(),
				UpdateExpression:          expr.Update(),
			},
		})

		// execute TransactWriteItems if there are 25 elements OR iteration number is equal to len(useDate)-1
		if len(transactItems) == 25 || j == len(sds)-1 {
			// execute transact operation

			if _, err := db.TransactWriteItems(&dynamodb.TransactWriteItemsInput{
				TransactItems: transactItems,
			}); err != nil {
				fmt.Println("transactItems 1", transactItems)
				fmt.Println("err 1", err)
				return be.ApiResponseGuest(http.StatusBadRequest, be.ErrorBody{
					ErrorMsg: aws.String(errors.New("Unable to update :TransactWriteItems failed").Error()),
				})
			}
			transactItems = nil
		}
	}
	return be.ApiResponse(org, http.StatusOK, &sds)
}

func main() {
	lambda.Start(handler)
}
