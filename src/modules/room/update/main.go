package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"os"
	"sort"

	be "bitbucket.org/jppsinc/be-modules/pkg"

	valid "github.com/asaskevich/govalidator"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/aws/aws-sdk-go/service/dynamodb/expression"
)

func sortRoomCategory(tableName string, hotel_id string, rcl []be.RoomCategoryList) ([]be.RoomCategoryList, error) {
	// DynamoDB
	db := be.Dynamodb()

	l := make([]be.RoomCategoryList, 0)

	// input for GetItem
	input := &dynamodb.QueryInput{
		ExpressionAttributeValues: map[string]*dynamodb.AttributeValue{
			":hotel_id": {
				S: aws.String(hotel_id),
			},
		},
		KeyConditionExpression: aws.String("hotel_id = :hotel_id"),
		TableName:              aws.String(tableName),
	}

	// GetItem from dynamodb table
	result, err := db.Query(input)
	if err != nil {
		return rcl, err
	}

	if len(result.Items) > 0 {
		item := new([]*be.RoomCategory)
		err = dynamodbattribute.UnmarshalListOfMaps(result.Items, &item)
		if err != nil {
			return rcl, err
		}

		// sort by Order
		sort.SliceStable(*item, func(i, j int) bool {
			return (*item)[i].Order < (*item)[j].Order
		})

		for _, RCTotal := range *item {
			for _, RCRoom := range rcl {
				if RCTotal.ID == RCRoom.ID {
					l = append(l, RCRoom)
				}
			}
		}
	}

	return l, err
}

func handler(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {

	org := request.Headers["origin"]
	// Table name
	// tableName := "be_rooms"
	tableName := os.Getenv("TABLE_NAME")
	rcTableName := os.Getenv("RC_TABLE_NAME")

	// Change JSON request data to struct : just to check validity of request data
	var inq be.Room

	if err := json.Unmarshal([]byte(request.Body), &inq); err != nil {
		fmt.Println("Unmarshal update:", err)
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorCouldNotUnMarshalItem).Error()),
		})
	}

	// Request path parameter
	id, ok := request.PathParameters["id"]
	if ok == false || valid.IsUUIDv4(id) == false {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorInvalidUrlIdNotFound).Error()),
		})
	}

	hotel_id, ok := request.PathParameters["hotel_id"]
	if ok == false || valid.IsUUIDv4(hotel_id) == false {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorInvalidUrlHotelIdNotFound).Error()),
		})
	} else {
		inq.HotelID = hotel_id
		inq.ID = id
		inq.UpdatedAt = be.DateTimeStamp()
	}

	if len(inq.RoomCategoryList) > 0 {
		inq.RoomCategoryList, _ = sortRoomCategory(rcTableName, hotel_id, inq.RoomCategoryList)
	}

	// DynamoDB
	db := be.Dynamodb()

	//create expr builder
	filt := expression.Name("code").Equal(expression.Value(inq.Code))
	expr, err := expression.NewBuilder().WithFilter(filt).Build()
	if err != nil {
		fmt.Println("err 5 :", err)
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorExpressionBuilder).Error()),
		})
	}

	// check if row with this key exist
	// input for GetItem
	input := &dynamodb.GetItemInput{
		Key: map[string]*dynamodb.AttributeValue{
			"hotel_id": {
				S: aws.String(hotel_id),
			},
			"id": {
				S: aws.String(id),
			},
		},
		TableName: aws.String(tableName),
	}

	// GetItem from dynamodb table
	result, err := db.GetItem(input)
	if err != nil {
		fmt.Println("err 3 :", err)
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorFailedToFetchRecord).Error()),
		})
	}

	if len(result.Item) > 0 {
		// change struct data to json data, this data will be stored to database
		av, err := dynamodbattribute.MarshalMap(inq)
		if err != nil {
			fmt.Println("err 1 :", err)
			return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
				ErrorMsg: aws.String(errors.New(be.ErrorCouldNotMarshalItem).Error()),
			})
		}

		ReturnItem := "SIZE"
		input := &dynamodb.PutItemInput{
			Item:                        av,
			TableName:                   aws.String(tableName),
			ReturnItemCollectionMetrics: &ReturnItem,
			ConditionExpression:         expr.Filter(),
			ExpressionAttributeNames:    expr.Names(),
			ExpressionAttributeValues:   expr.Values(),
		}

		_, err = db.PutItem(input)
		if err != nil {
			fmt.Println("err 2 :", err)
			return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
				ErrorMsg: aws.String(errors.New(be.ErrorCouldNotDynamoPutItem).Error()),
			})
		}

		// update image gallery for use_count calculation
		if len(inq.Image) > 0 {
			UpdateGalleries(hotel_id, inq.Image[0].ID, db)
		}

		return be.ApiResponse(org, http.StatusOK, &inq)
	} else {
		fmt.Println("err 4 :", err)
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorRecordNotFound).Error()),
		})
	}

}

func main() {
	lambda.Start(handler)
}

func UpdateGalleries(hotelID string, id string, db *dynamodb.DynamoDB) {
	tableName := os.Getenv("GAL_TABLE_NAME")
	update := expression.Set(expression.Name("updated_at"), expression.Value(be.DateTimeStamp()))
	expr, err := expression.NewBuilder().WithUpdate(update).Build()

	_, err = db.TransactWriteItems(&dynamodb.TransactWriteItemsInput{
		TransactItems: []*dynamodb.TransactWriteItem{
			{
				Update: &dynamodb.Update{
					TableName: aws.String(tableName),
					Key: map[string]*dynamodb.AttributeValue{
						"hotel_id": {
							S: aws.String(hotelID),
						},
						"id": {
							S: aws.String(id),
						},
					},
					ExpressionAttributeNames:  expr.Names(),
					ExpressionAttributeValues: expr.Values(),
					UpdateExpression:          expr.Update(),
				},
			},
		},
	})
	if err != nil {
		fmt.Println("TransactWriteItems error :", err)
	}
}
