package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"math/rand"
	"net/http"
	"os"
	"sort"
	"time"

	be "bitbucket.org/jppsinc/be-modules/pkg"
	valid "github.com/asaskevich/govalidator"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/google/uuid"
)

// generate random string
var src = rand.NewSource(time.Now().UnixNano())

const letterBytes = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890"
const (
	letterIdxBits = 6                    // 6 bits to represent a letter index
	letterIdxMask = 1<<letterIdxBits - 1 // All 1-bits, as many as letterIdxBits
	letterIdxMax  = 63 / letterIdxBits   // # of letter indices fitting in 63 bits
)

func RandStringBytesMaskImprSrc(n int) string {
	b := make([]byte, n)
	// A src.Int63() generates 63 random bits, enough for letterIdxMax characters!
	for i, cache, remain := n-1, src.Int63(), letterIdxMax; i >= 0; {
		if remain == 0 {
			cache, remain = src.Int63(), letterIdxMax
		}
		if idx := int(cache & letterIdxMask); idx < len(letterBytes) {
			b[i] = letterBytes[idx]
			i--
		}
		cache >>= letterIdxBits
		remain--
	}

	return string(b)
}

func sortRoomCategory(tableName string, hotel_id string, rcl []be.RoomCategoryList) ([]be.RoomCategoryList, error) {
	// DynamoDB
	db := be.Dynamodb()

	l := make([]be.RoomCategoryList, 0)

	// input for GetItem
	input := &dynamodb.QueryInput{
		ExpressionAttributeValues: map[string]*dynamodb.AttributeValue{
			":hotel_id": {
				S: aws.String(hotel_id),
			},
		},
		KeyConditionExpression: aws.String("hotel_id = :hotel_id"),
		TableName:              aws.String(tableName),
	}

	// GetItem from dynamodb table
	result, err := db.Query(input)
	if err != nil {
		return rcl, err
	}

	if len(result.Items) > 0 {
		item := new([]*be.RoomCategory)
		err = dynamodbattribute.UnmarshalListOfMaps(result.Items, &item)
		if err != nil {
			return rcl, err
		}

		// sort by Order
		sort.SliceStable(*item, func(i, j int) bool {
			return (*item)[i].Order < (*item)[j].Order
		})

		for _, RCTotal := range *item {
			for _, RCRoom := range rcl {
				if RCTotal.ID == RCRoom.ID {
					l = append(l, RCRoom)
				}
			}
		}
	}

	return l, err
}

func handler(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {

	org := request.Headers["origin"]
	// Table name
	// tableName := "be_rooms"
	tableName := os.Getenv("TABLE_NAME")
	rcTableName := os.Getenv("RC_TABLE_NAME")

	// Change JSON request data to struct : just to check validity of request data
	var sds be.Room

	if err := json.Unmarshal([]byte(request.Body), &sds); err != nil {
		fmt.Println("err 1 :", err)
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorCouldNotUnMarshalItem).Error()),
		})
	}

	// Request path parameter
	hotel_id, ok := request.PathParameters["hotel_id"]
	if ok == false || valid.IsUUIDv4(hotel_id) == false {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorInvalidUrlHotelIdNotFound).Error()),
		})
	} else {
		sds.ID = fmt.Sprintf("%s", uuid.New())
		sds.HotelID = hotel_id
		sds.Code = RandStringBytesMaskImprSrc(8)
		sds.CreatedAt = be.DateTimeStamp()
		sds.UpdatedAt = be.DateTimeStamp()
	}

	if len(sds.RoomCategoryList) > 0 {
		sds.RoomCategoryList, _ = sortRoomCategory(rcTableName, hotel_id, sds.RoomCategoryList)
	}

	// DynamoDB
	db := be.Dynamodb()

	// change struct data to json data, this data will be stored to database
	av, err := dynamodbattribute.MarshalMap(sds)
	if err != nil {
		fmt.Println("err 2 :", err)
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorCouldNotMarshalItem).Error()),
		})
	}

	ReturnItem := "SIZE"
	input := &dynamodb.PutItemInput{
		Item:                        av,
		TableName:                   aws.String(tableName),
		ReturnItemCollectionMetrics: &ReturnItem,
	}

	_, err = db.PutItem(input)
	if err != nil {
		fmt.Println("err 3 :", err)
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorCouldNotDynamoPutItem).Error()),
		})
	}
	return be.ApiResponse(org, http.StatusCreated, &sds)
}

func main() {
	lambda.Start(handler)
}
