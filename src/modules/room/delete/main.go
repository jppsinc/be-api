package main

import (
	"errors"
	"fmt"
	"net/http"
	"os"

	be "bitbucket.org/jppsinc/be-modules/pkg"
	valid "github.com/asaskevich/govalidator"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/aws/aws-sdk-go/service/dynamodb/expression"
)

func handler(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {

	org := request.Headers["origin"]
	// Table name
	// tableName := "be_rooms"
	tableName := os.Getenv("TABLE_NAME")
	// tableNamePlan := "be_plans"
	tableNamePlan := os.Getenv("PLAN_TABLE_NAME")
	roomMapTable := os.Getenv("ROOM_MAP_TABLE")

	// Request path parameter
	hotel_id, ok := request.PathParameters["hotel_id"]
	if ok == false || valid.IsUUIDv4(hotel_id) == false {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorInvalidUrlHotelIdNotFound).Error()),
		})
	}

	id, ok := request.PathParameters["id"]
	if ok == false || valid.IsUUIDv4(id) == false {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorInvalidUrlIdNotFound).Error()),
		})
	}

	// DynamoDB
	db := be.Dynamodb()

	// check if row with this key exist
	// input for GetItem
	input := &dynamodb.GetItemInput{
		Key: map[string]*dynamodb.AttributeValue{
			"hotel_id": {
				S: aws.String(hotel_id),
			},
			"id": {
				S: aws.String(id),
			},
		},
		TableName: aws.String(tableName),
	}

	// GetItem from dynamodb table
	result, err := db.GetItem(input)
	if err != nil {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorFailedToFetchRecord).Error()),
		})
	}

	if len(result.Item) > 0 {
		// input for GetItem
		ReturnValues := "ALL_OLD"
		input := &dynamodb.DeleteItemInput{
			Key: map[string]*dynamodb.AttributeValue{
				"hotel_id": {
					S: aws.String(hotel_id),
				},
				"id": {
					S: aws.String(id),
				},
			},
			TableName:    aws.String(tableName),
			ReturnValues: &ReturnValues,
		}
		// GetItem from dynamodb table
		result, err := db.DeleteItem(input)
		if err != nil {
			return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
				ErrorMsg: aws.String(errors.New(be.ErrorCouldNotDeleteItem).Error()),
			})
		}
		fmt.Println(result)

		err = UpdateMapTable(hotel_id, id, roomMapTable, db)
		if err != nil {
			return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
				ErrorMsg: aws.String(errors.New("TL Room map table update failed").Error()),
			})
		}

		item := new(be.Room)
		item.HotelID = hotel_id
		item.ID = id

		// delete room from plans
		inputPlan := &dynamodb.QueryInput{
			ExpressionAttributeValues: map[string]*dynamodb.AttributeValue{
				":hotel_id": {
					S: aws.String(hotel_id),
				},
			},
			KeyConditionExpression: aws.String("hotel_id = :hotel_id"),
			TableName:              aws.String(tableNamePlan),
		}

		// GetItem from dynamodb table
		resultPlan, err := db.Query(inputPlan)
		if err != nil {
			return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
				ErrorMsg: aws.String(errors.New(be.ErrorFailedToFetchRecord).Error()),
			})
		}

		plans := new([]*be.Plan)
		err = dynamodbattribute.UnmarshalListOfMaps(resultPlan.Items, &plans)
		if err != nil {
			return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
				ErrorMsg: aws.String(errors.New(be.ErrorCouldNotUnMarshalItem).Error()),
			})
		}

		for l := 0; l < len(*plans); l++ {
			plan := *&(*plans)[l]

			// check if room is attached with this plan
			found := false
			for _, p := range plan.RoomInfo {
				if p.RoomID == id {
					found = true
				}
			}

			// if room is attached remove it
			if found == true {
				sds := new(be.Plan)
				for _, r := range plan.RoomInfo {
					if r.RoomID == id {
						// skip this room info
					} else {
						sds.RoomInfo = append(sds.RoomInfo, r)
						sds.RoomIDList = append(sds.RoomIDList, r.RoomID)
						sds.RoomNameList = append(sds.RoomNameList, r.RoomType.Ja)
					}
				}

				update := expression.
					Set(expression.Name("room_info"), expression.Value(sds.RoomInfo)).
					Set(expression.Name("room_id_list"), expression.Value(sds.RoomIDList)).
					Set(expression.Name("room_iname_list"), expression.Value(sds.RoomNameList))

				expr, err := expression.NewBuilder().
					WithUpdate(update).
					Build()

				input := &dynamodb.UpdateItemInput{
					Key: map[string]*dynamodb.AttributeValue{
						"hotel_id": {
							S: aws.String(hotel_id),
						},
						"id": {
							S: aws.String(plan.ID),
						},
					},
					TableName:                 aws.String(tableNamePlan),
					ExpressionAttributeNames:  expr.Names(),
					ExpressionAttributeValues: expr.Values(),
					UpdateExpression:          expr.Update(),
				}

				_, err = db.UpdateItem(input)
				if err != nil {
					fmt.Println("err :", err)
					return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
						ErrorMsg: aws.String(errors.New(be.ErrorCouldNotDynamoPutItem).Error()),
					})
				}

				fmt.Println("Plan updated successfully")
			}

		}

		return be.ApiResponse(org, http.StatusOK, item)

	} else {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorRecordNotFound).Error()),
		})
	}
}

func main() {
	lambda.Start(handler)
}

func UpdateMapTable(hotelId, roomId, planMapTable string, db *dynamodb.DynamoDB) error {

	item := new([]*PlanIDMapList)
	keyCond := expression.Key("hotel_id").Equal(expression.Value(hotelId))

	expr, err := expression.NewBuilder().WithKeyCondition(keyCond).Build()
	if err != nil {
		return err
	}

	input := &dynamodb.QueryInput{
		KeyConditionExpression:    expr.KeyCondition(),
		TableName:                 aws.String(planMapTable),
		ExpressionAttributeNames:  expr.Names(),
		ExpressionAttributeValues: expr.Values(),
	}

	// GetItem from dynamodb table
	result, err := db.Query(input)
	if err != nil {
		return err
	}

	err = dynamodbattribute.UnmarshalListOfMaps(result.Items, &item)
	if err != nil {
		return err
	}
	for _, entry := range *item {
		if entry.BEID == roomId {

			input := &dynamodb.UpdateItemInput{
				ExpressionAttributeNames: map[string]*string{
					"#K1": aws.String("status"),
				},
				ExpressionAttributeValues: map[string]*dynamodb.AttributeValue{
					":v1": {
						S: aws.String("Delete"),
					},
				},
				Key: map[string]*dynamodb.AttributeValue{
					"hotel_id": {
						S: aws.String(hotelId),
					},
					"be_id": {
						S: aws.String(roomId),
					},
				},
				ReturnValues:     aws.String("ALL_NEW"),
				TableName:        aws.String(planMapTable),
				UpdateExpression: aws.String("SET #K1 = :v1"),
			}

			_, err := db.UpdateItem(input)
			if err != nil {
				return err
			}

			fmt.Println("TL room updated for id : ", roomId)
			fmt.Println("TL room updated for table : ", planMapTable)
		}
	}
	return nil
}

type PlanIDMapList struct {
	HotelID  string `json:"hotel_id"`
	TLID     string `json:"tl_id"`
	BEID     string `json:"be_id"`
}
