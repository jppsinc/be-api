package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"os"

	be "bitbucket.org/jppsinc/be-modules/pkg"

	valid "github.com/asaskevich/govalidator"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
)

func handler(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {

	org := request.Headers["origin"]
	// Table name
	// tableName := "be_child_settings"
	tableName := os.Getenv("TABLE_NAME")

	// Change JSON request data to struct : just to check validity of request data
	var cs be.ChildSetting

	if err := json.Unmarshal([]byte(request.Body), &cs); err != nil {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorCouldNotUnMarshalItem).Error()),
		})
	}

	// Request path parameter
	hotel_id, ok := request.PathParameters["hotel_id"]
	if ok == false || valid.IsUUIDv4(hotel_id) == false {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorInvalidUrlHotelIdNotFound).Error()),
		})
	} else {
		cs.HotelID = hotel_id
		cs.CreatedAt = be.DateTimeStamp()
		cs.UpdatedAt = be.DateTimeStamp()
	}

	sc, err := GetSiteControllerUsed(hotel_id)
	if err != nil {
		fmt.Println("GetSiteControllerUsed error: ", err)
	}

	fmt.Println("site controller used : ", sc)

	if sc == "TL-Lincoln" {
		for i, c := range cs.ChildInfo {
			if c.Rank == "Child A" || c.Rank == "ChildA" {
				cs.ChildInfo[i].Name.Ja = "小学生高学年"
				cs.ChildInfo[i].Name.En = "Elementary school upper grades"
				cs.ChildInfo[i].Name.Ko = "초등학생 고학년"
				cs.ChildInfo[i].Name.ZhCN = "小学高年级"
				cs.ChildInfo[i].Name.ZhTW = "小學高年級"
			}

			if c.Rank == "Child B" || c.Rank == "ChildB" {
				cs.ChildInfo[i].Name.Ja = "小学生低学年"
				cs.ChildInfo[i].Name.En = "Elementary school lower grades"
				cs.ChildInfo[i].Name.Ko = "초등학생 저학년"
				cs.ChildInfo[i].Name.ZhCN = "小学低年级"
				cs.ChildInfo[i].Name.ZhTW = "小學低年級"
			}

			if c.Rank == "Child C" || c.Rank == "ChildC" {
				cs.ChildInfo[i].Name.Ja = "幼児 食事・布団あり"
				cs.ChildInfo[i].Name.En = "Infant with meals and futons"
				cs.ChildInfo[i].Name.Ko = "유아 식사・이불 있음"
				cs.ChildInfo[i].Name.ZhCN = "提供婴儿餐和蒲团"
				cs.ChildInfo[i].Name.ZhTW = "提供嬰兒餐和蒲團"
			}

			if c.Rank == "Child D" || c.Rank == "ChildD" {
				cs.ChildInfo[i].Name.Ja = "幼児 食事あり"
				cs.ChildInfo[i].Name.En = "Infant with meals"
				cs.ChildInfo[i].Name.Ko = "유아 식사 있음"
				cs.ChildInfo[i].Name.ZhCN = "婴儿吃饭"
				cs.ChildInfo[i].Name.ZhTW = "嬰兒吃飯"
			}

			if c.Rank == "Child E" || c.Rank == "ChildE" {
				cs.ChildInfo[i].Name.Ja = "幼児 布団あり"
				cs.ChildInfo[i].Name.En = "Infant with futons"
				cs.ChildInfo[i].Name.Ko = "유아 이불 있음"
				cs.ChildInfo[i].Name.ZhCN = "有婴儿被褥"
				cs.ChildInfo[i].Name.ZhTW = "有嬰兒被褥"
			}

			if c.Rank == "Child F" || c.Rank == "ChildF" {
				cs.ChildInfo[i].Name.Ja = "幼児 布団なし"
				cs.ChildInfo[i].Name.En = "Infant without futons"
				cs.ChildInfo[i].Name.Ko = "유아 이불 없음"
				cs.ChildInfo[i].Name.ZhCN = "没有婴儿被褥"
				cs.ChildInfo[i].Name.ZhTW = "沒有嬰兒被褥"
			}
		}
	}

	// DynamoDB
	db := be.Dynamodb()

	// change struct data to json data, this data will be stored to database
	av, err := dynamodbattribute.MarshalMap(cs)
	if err != nil {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorCouldNotMarshalItem).Error()),
		})
	}

	ReturnItem := "SIZE"
	input := &dynamodb.PutItemInput{
		Item:                        av,
		TableName:                   aws.String(tableName),
		ReturnItemCollectionMetrics: &ReturnItem,
	}
	_, err = db.PutItem(input)
	if err != nil {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorCouldNotDynamoPutItem).Error()),
		})
	}
	return be.ApiResponse(org, http.StatusCreated, &cs)
}

func GetSiteControllerUsed(hotelID string) (string, error) {

	// tableName := "be_hotel_default_settings-dev"
	tableName := os.Getenv("HDS_TABLE")
	sc := ""
	// DynamoDB
	db := be.Dynamodb()

	// input for GetItem
	input := &dynamodb.GetItemInput{
		Key: map[string]*dynamodb.AttributeValue{
			"hotel_id": {
				S: aws.String(hotelID),
			},
		},
		TableName: aws.String(tableName),
	}

	// GetItem from dynamodb table
	result, err := db.GetItem(input)
	if err != nil {
		return sc, err
	}

	item := new(be.HotelDefaultSettings)
	err = dynamodbattribute.UnmarshalMap(result.Item, item)
	if err != nil {
		return sc, err
	}

	return item.SiteController.Name.En, nil

}

func main() {
	lambda.Start(handler)
}
