package main

import (
	"errors"
	"fmt"
	"net/http"
	"os"

	be "bitbucket.org/jppsinc/be-modules/pkg"
	valid "github.com/asaskevich/govalidator"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
)

func handler(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {

	org := request.Headers["origin"]
	// Table name
	// tableName := "be_child_settings"
	tableName := os.Getenv("TABLE_NAME")

	// Request path parameter
	hotel_id, ok := request.PathParameters["hotel_id"]
	if ok == false || valid.IsUUIDv4(hotel_id) == false {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorInvalidUrlHotelIdNotFound).Error()),
		})
	}

	// DynamoDB
	db := be.Dynamodb()

	// input for GetItem
	ReturnValues := "ALL_OLD"
	input := &dynamodb.DeleteItemInput{
		Key: map[string]*dynamodb.AttributeValue{
			"hotel_id": {
				S: aws.String(hotel_id),
			},
		},
		TableName:    aws.String(tableName),
		ReturnValues: &ReturnValues,
	}

	// GetItem from dynamodb table
	result, err := db.DeleteItem(input)
	if err != nil {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorCouldNotDeleteItem).Error()),
		})
	}
	fmt.Println(result)

	item := new(be.ChildSetting)
	// err = dynamodbattribute.UnmarshalMap(result, item)
	// if err != nil {
	// 	return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
	// 		aws.String(errors.New(be.ErrorCouldNotUnMarshalItem).Error()),
	// 	})
	// }
	return be.ApiResponse(org, http.StatusOK, item)
}

func main() {
	lambda.Start(handler)
}
