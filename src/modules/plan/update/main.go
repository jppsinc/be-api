package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"os"

	be "bitbucket.org/jppsinc/be-modules/pkg"

	valid "github.com/asaskevich/govalidator"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/aws/aws-sdk-go/service/dynamodb/expression"
)

func SetRoomPlanList(hotel_id string, RoomIDList []string) error {

	// tableName := "be_plans"
	// rTableName := "be_rooms"
	tableName := os.Getenv("TABLE_NAME")
	rTableName := os.Getenv("ROOM_TABLE_NAME")

	keyCond := expression.Key("hotel_id").Equal(expression.Value(hotel_id))
	filter := expression.Name("status").NotEqual(expression.Value("Delete"))
	// filt := expression.Name("sales_category").In(expression.Value(true))

	expr, err := expression.NewBuilder().WithKeyCondition(keyCond).WithFilter(filter).Build()
	if err != nil {
		return err
	}

	// input for GetItem
	input := &dynamodb.QueryInput{
		KeyConditionExpression:    expr.KeyCondition(),
		TableName:                 aws.String(tableName),
		ExpressionAttributeNames:  expr.Names(),
		ExpressionAttributeValues: expr.Values(),
		FilterExpression:          expr.Filter(),
	}

	// DynamoDB
	db := be.Dynamodb()

	// GetItem from dynamodb table
	result, err := db.Query(input)
	if err != nil {
		return err
	}

	item := new([]*be.Plan)
	err = dynamodbattribute.UnmarshalListOfMaps(result.Items, &item)
	if err != nil {
		return err
	}

	for i := 0; i < len(RoomIDList); i++ {
		var planList []string

		for j := 0; j < len(*item); j++ {
			pl := *&(*item)[j].RoomIDList
			for k := 0; k < len(pl); k++ {
				if RoomIDList[i] == pl[k] {
					planList = append(planList, *&(*item)[j].Title.Ja)
				}
			}
		}

		if len(planList) > 0 {
			update := expression.Set(expression.Name("plan_list"), expression.Value(planList))
			expr1, err := expression.NewBuilder().
				WithUpdate(update).
				Build()

			input := &dynamodb.UpdateItemInput{
				Key: map[string]*dynamodb.AttributeValue{
					"hotel_id": {
						S: aws.String(hotel_id),
					},
					"id": {
						S: aws.String(RoomIDList[i]),
					},
				},
				TableName:                 aws.String(rTableName),
				ExpressionAttributeNames:  expr1.Names(),
				ExpressionAttributeValues: expr1.Values(),
				UpdateExpression:          expr1.Update(),
			}

			_, err = db.UpdateItem(input)
			if err != nil {
				return err
			}
		}

	}
	return nil
}

func handler(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {

	org := request.Headers["origin"]
	// Table name
	// tableName := "be_plans"
	tableName := os.Getenv("TABLE_NAME")
	oTableName := os.Getenv("OPTION_TABLE_NAME")

	// Change JSON request data to struct  : just to check validity of request data
	var sds be.Plan

	// hotelId := "60da9011-9591-47f3-b6fc-f7ac21b0ffa8"
	// planId := "669c5169-ac68-4b6b-b17c-231be9bc863e"
	// var optionList []string = []string{"28ddd24c-e736-47eb-bfd3-ee4895dbe393", "6bb15461-4cf8-409e-a177-ee088edd28d4", "cd7e5349-975e-4f7a-9a8f-b4401d98922a"}
	// // db := be.Dynamodb()
	// UpdateOptionPlanList(hotelId, planId, optionList, "be_plans-dev", be.Dynamodb())
	// return be.ApiResponse(org, http.StatusOK, &sds)

	if err := json.Unmarshal([]byte(request.Body), &sds); err != nil {
		fmt.Println("err 1:", err)
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorCouldNotUnMarshalItem).Error()),
		})
	}

	// Request path parameter
	id, ok := request.PathParameters["id"]
	if ok == false || valid.IsUUIDv4(id) == false {
		fmt.Println("err 2:")
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorInvalidUrlIdNotFound).Error()),
		})
	}

	hotel_id, ok := request.PathParameters["hotel_id"]
	if ok == false || valid.IsUUIDv4(hotel_id) == false {
		fmt.Println("err 3:")
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorInvalidUrlHotelIdNotFound).Error()),
		})
	} else {
		sds.HotelID = hotel_id
		sds.ID = id
		sds.UpdatedAt = be.DateTimeStamp()
	}

	// create a separate field for list of ids of all the attached hotel.
	// it is used in temairazu plan api
	sds.RoomIDList = nil
	sds.RoomNameList = nil
	for _, info := range sds.RoomInfo {
		sds.RoomIDList = append(sds.RoomIDList, info.RoomID)
		sds.RoomNameList = append(sds.RoomNameList, info.RoomType.Ja)
	}

	// for _, info := range sds.Meal {
	// 	if info.Breakfast != nil {
	// 		sds.Breakfast = true
	// 		sds.BreakfastPlace = info.Place
	// 	}else{
	// 		sds.Breakfast = false
	// 		sds.BreakfastPlace = ""
	// 	}
	// 	if info.Lunch != nil {
	// 		sds.Lunch = true
	// 		sds.LunchPlace = info.Place
	// 	}else{
	// 		sds.Lunch = false
	// 		sds.LunchPlace = ""
	// 	}
	// 	if info.Dinner != nil {
	// 		sds.Dinner = true
	// 		sds.DinnerPlace = info.Place
	// 	}else{
	// 		sds.Dinner = false
	// 		sds.DinnerPlace = ""
	// 	}
	// }

	// DynamoDB
	db := be.Dynamodb()

	//create expr builder
	filt := expression.Name("code").Equal(expression.Value(sds.Code))
	expr, err := expression.NewBuilder().WithFilter(filt).Build()
	if err != nil {
		fmt.Println("err 4:", err)
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorExpressionBuilder).Error()),
		})
	}

	// check if row with this key exist
	// input for GetItem
	input := &dynamodb.GetItemInput{
		Key: map[string]*dynamodb.AttributeValue{
			"hotel_id": {
				S: aws.String(hotel_id),
			},
			"id": {
				S: aws.String(id),
			},
		},
		TableName: aws.String(tableName),
	}

	// GetItem from dynamodb table
	result, err := db.GetItem(input)
	if err != nil {
		fmt.Println("err 5:", err)
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorFailedToFetchRecord).Error()),
		})
	}

	if len(result.Item) > 0 {
		// change struct data to json data, this data will be stored to database
		av, err := dynamodbattribute.MarshalMap(sds)
		if err != nil {
			fmt.Println("err 6:", err)
			return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
				ErrorMsg: aws.String(errors.New(be.ErrorCouldNotMarshalItem).Error()),
			})
		}

		if len(sds.OptionIDList) == 0 {
			empty := []*dynamodb.AttributeValue{}
			av["option_id_list"] = &dynamodb.AttributeValue{L: empty}
		}
		if len(sds.PlanGroupList) == 0 {
			empty := []*dynamodb.AttributeValue{}
			av["plan_group_list"] = &dynamodb.AttributeValue{L: empty}
		}
		if len(sds.QuestionsForCustomer) == 0 {
			empty := []*dynamodb.AttributeValue{}
			av["questions_for_customer"] = &dynamodb.AttributeValue{L: empty}
		}
		if len(sds.RoomIDList) == 0 {
			empty := []*dynamodb.AttributeValue{}
			av["room_id_list"] = &dynamodb.AttributeValue{L: empty}
		}

		ReturnItem := "SIZE"
		input := &dynamodb.PutItemInput{
			Item:                        av,
			TableName:                   aws.String(tableName),
			ReturnItemCollectionMetrics: &ReturnItem,
			ConditionExpression:         expr.Filter(),
			ExpressionAttributeNames:    expr.Names(),
			ExpressionAttributeValues:   expr.Values(),
		}

		_, err = db.PutItem(input)
		if err != nil {
			fmt.Println("err 7:", err)
			return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
				ErrorMsg: aws.String(errors.New(be.ErrorCouldNotDynamoPutItem).Error()),
			})
		}

		fmt.Println("SetRoomPlanList start")
		err = SetRoomPlanList(hotel_id, sds.RoomIDList)
		if err != nil {
			fmt.Println("err 4.1:", err)
			return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
				ErrorMsg: aws.String(errors.New("Room update failed").Error()),
			})
		}
		fmt.Println("SetRoomPlanList end")

		fmt.Println("UpdateOptionPlanList start")

		err = UpdateOptionPlanList(hotel_id, sds.ID, sds.OptionIDList, oTableName, db)
		if err != nil {
			return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
				ErrorMsg: aws.String(errors.New("option update failed").Error()),
			})
		}
		fmt.Println("UpdateOptionPlanList end")

		// update image gallery for use_count calculation
		if len(sds.PlanImages) > 0 {
			UpdateGalleries(hotel_id, sds.PlanImages[0].ID, db)
		}

		return be.ApiResponse(org, http.StatusOK, &sds)
	} else {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorRecordNotFound).Error()),
		})
	}

}

func main() {
	lambda.Start(handler)
}

func UpdateGalleries(hotelID string, id string, db *dynamodb.DynamoDB) {
	tableName := os.Getenv("GAL_TABLE_NAME")
	update := expression.Set(expression.Name("updated_at"), expression.Value(be.DateTimeStamp()))
	expr, err := expression.NewBuilder().WithUpdate(update).Build()

	_, err = db.TransactWriteItems(&dynamodb.TransactWriteItemsInput{
		TransactItems: []*dynamodb.TransactWriteItem{
			{
				Update: &dynamodb.Update{
					TableName: aws.String(tableName),
					Key: map[string]*dynamodb.AttributeValue{
						"hotel_id": {
							S: aws.String(hotelID),
						},
						"id": {
							S: aws.String(id),
						},
					},
					ExpressionAttributeNames:  expr.Names(),
					ExpressionAttributeValues: expr.Values(),
					UpdateExpression:          expr.Update(),
				},
			},
		},
	})
	if err != nil {
		fmt.Println("TransactWriteItems error :", err)
	}
}

func UpdateOptionPlanList(hotelId string, planId string, optionList []string, tableName string, db *dynamodb.DynamoDB) error {

	optionPlanMap := make(map[string]string)
	for _, updatedPlanOption := range optionList {
		optionPlanMap[updatedPlanOption] = planId
	}

	fmt.Println("optionPlanMap :", optionPlanMap)

	keyCond := expression.Key("hotel_id").Equal(expression.Value(hotelId))

	expr, err := expression.NewBuilder().WithKeyCondition(keyCond).Build()
	if err != nil {
		fmt.Println("expression error : ", err)
		return err
	}

	// input for GetItem
	input := &dynamodb.QueryInput{
		KeyConditionExpression:    expr.KeyCondition(),
		TableName:                 aws.String(tableName),
		ExpressionAttributeNames:  expr.Names(),
		ExpressionAttributeValues: expr.Values(),
		ProjectionExpression:      aws.String("plan_list,id"),
	}

	// GetItem from dynamodb table
	result, err := db.Query(input)
	if err != nil {
		fmt.Println("expression error : ", err)
		return err
	}

	options := new([]*be.Option)
	err = dynamodbattribute.UnmarshalListOfMaps(result.Items, &options)
	if err != nil {
		fmt.Println("expression error : ", err)
		return err
	}

	updatedOptionList := make(map[string]bool)

	for _, option := range optionList {
		updatedOptionList[option] = true
	}

	var update expression.UpdateBuilder

	for i := 0; i < len(*options); i++ {

		// check if options[i] is present in updatedOptionList
		// if it is there check if updated planid is present in this options plan list
		if _, ok := updatedOptionList[(*options)[i].ID]; ok {
			flg := false
			for _, pid := range (*options)[i].PlanList {
				if pid == planId {
					flg = true
				}
			}
			if !flg {
				update = expression.Set(expression.Name("plan_list"), expression.Name("plan_list").ListAppend(expression.Value([]string{planId})))
				err := UpdateOption(hotelId, (*options)[i].ID, update, tableName, db)
				if err != nil {
					fmt.Println("UpdateOption1 error : ", err)
					return err
				}
			}
		} else {
			flg := false
			plan := make([]string, 0)
			for _, pid := range (*options)[i].PlanList {
				if pid == planId {
					flg = true
				} else {
					plan = append(plan, pid)
				}
			}
			if flg {
				if len(plan) > 0 {
					update = expression.Set(expression.Name("plan_list"), expression.Value(plan))
				} else {
					update = expression.Set(expression.Name("plan_list"), expression.Value([]string{}))
				}

				err := UpdateOption(hotelId, (*options)[i].ID, update, tableName, db)
				if err != nil {
					fmt.Println("UpdateOption error : ", err)
					return err
				}
			}
		}

	}

	return nil
}

func UpdateOption(hotelId string, optionId string, update expression.UpdateBuilder, tableName string, db *dynamodb.DynamoDB) error {
	expr1, err := expression.NewBuilder().WithUpdate(update).Build()
	if err != nil {
		fmt.Println("expression error : ", err)
		return err
	}

	input := &dynamodb.UpdateItemInput{
		Key: map[string]*dynamodb.AttributeValue{
			"hotel_id": {
				S: aws.String(hotelId),
			},
			"id": {
				S: aws.String(optionId),
			},
		},
		TableName:                 aws.String(tableName),
		ExpressionAttributeNames:  expr1.Names(),
		ExpressionAttributeValues: expr1.Values(),
		UpdateExpression:          expr1.Update(),
	}

	fmt.Println("plan list update input:", input)

	_, err = db.UpdateItem(input)
	if err != nil {
		fmt.Println("expression error : ", err)
		return err
	}

	return nil
}
