package main

import (
	"errors"
	"net/http"
	"os"

	be "bitbucket.org/jppsinc/be-modules/pkg"
	valid "github.com/asaskevich/govalidator"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
)

func handler(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {

	org := request.Headers["origin"]
	// Table name
	// tableName := "be_plans"
	tableName := os.Getenv("TABLE_NAME")

	// Request path parameter
	hotel_id, ok := request.PathParameters["hotel_id"]
	if ok == false || valid.IsUUIDv4(hotel_id) == false {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorInvalidUrlHotelIdNotFound).Error()),
		})
	}

	id, ok := request.PathParameters["id"]
	if ok == false || valid.IsUUIDv4(id) == false {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorInvalidUrlIdNotFound).Error()),
		})
	}

	// DynamoDB
	db := be.Dynamodb()

	// input for GetItem
	input := &dynamodb.GetItemInput{
		Key: map[string]*dynamodb.AttributeValue{
			"hotel_id": {
				S: aws.String(hotel_id),
			},
			"id": {
				S: aws.String(id),
			},
		},
		TableName: aws.String(tableName),
	}

	// GetItem from dynamodb table
	result, err := db.GetItem(input)
	if err != nil {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorFailedToFetchRecord).Error()),
		})
	}

	item := new(be.Plan)
	err = dynamodbattribute.UnmarshalMap(result.Item, item)
	if err != nil {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorCouldNotUnMarshalItem).Error()),
		})
	}
	if len(result.Item) > 0 {
		if len(item.Payment) == 0 {
			item.Payment = make([]string, 0)
		}
		if len(item.RoomIDList) == 0 {
			item.RoomIDList = make([]string, 0)
		}
		if len(item.RoomNameList) == 0 {
			item.RoomNameList = make([]string, 0)
		}
		if len(item.PlanImages) == 0 {
			item.PlanImages = make([]be.Image, 0)
		}
		if len(item.Tax) == 0 {
			item.Tax = make([]be.PlanTax, 0)
		}
		if len(item.Meal) == 0 {
			item.Meal = make([]be.Meal, 0)
		}
		if len(item.QuestionsForCustomer) == 0 {
			item.QuestionsForCustomer = make([]be.QuestionsForCustomer, 0)
		}
		if len(item.RoomInfo) == 0 {
			item.RoomInfo = make([]be.RoomInfo, 0)
		}
		if len(item.ChildSetting.ChildInfo) == 0 {
			item.ChildSetting.ChildInfo = make([]be.ChildInfo, 0)
		}
		if len(item.OptionIDList) == 0 {
			item.OptionIDList = make([]string, 0)
		}
		if len(item.PlanGroupList) == 0 {
			item.PlanGroupList = make([]be.PlanGroupList, 0)
		}
		if len(item.SpecialCodes) == 0 {
			item.SpecialCodes = make([]string, 0)
		}
		return be.ApiResponse(org, http.StatusOK, item)
	} else {
		item := new(be.EmptyStruct)
		return be.ApiResponse(org, http.StatusOK, item)
	}

}

func main() {
	lambda.Start(handler)
}
