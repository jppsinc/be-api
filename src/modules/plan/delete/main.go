package main

import (
	"errors"
	"fmt"
	"net/http"
	"os"
	"strconv"

	be "bitbucket.org/jppsinc/be-modules/pkg"
	valid "github.com/asaskevich/govalidator"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/aws/aws-sdk-go/service/dynamodb/expression"
)

type Plan struct {
	ID        string `json:"id"`
	HotelID   string `json:"hotel_id"`
	Status    string `json:"status"`
	UpdatedAt int64  `json:"updated_at"`
}

type RoomSort struct {
	RoomIDList []string  `json:"room_id_list"`
	Title      MultiLang `json:"title"`
}

type LastKey struct {
	HotelID string `json:"hotel_id"`
	ID      string `json:"id"`
}

type MultiLang struct {
	Ja string `json:"ja"`
	En string `json:"en"`
}

func QueryFirst(expr expression.Expression, tableName string, projection *string) *dynamodb.QueryInput {
	input := &dynamodb.QueryInput{
		KeyConditionExpression:    expr.KeyCondition(),
		TableName:                 aws.String(tableName),
		ExpressionAttributeNames:  expr.Names(),
		ExpressionAttributeValues: expr.Values(),
		FilterExpression:          expr.Filter(),
		ProjectionExpression:      projection,
	}

	return input
}

func QueryNext(expr expression.Expression, tableName string, projection *string, es LastKey) *dynamodb.QueryInput {
	input := &dynamodb.QueryInput{
		KeyConditionExpression:    expr.KeyCondition(),
		TableName:                 aws.String(tableName),
		ExpressionAttributeNames:  expr.Names(),
		ExpressionAttributeValues: expr.Values(),
		FilterExpression:          expr.Filter(),
		ProjectionExpression:      projection,
		ExclusiveStartKey: map[string]*dynamodb.AttributeValue{
			"hotel_id": {
				S: aws.String(es.HotelID),
			},
			"id": {
				S: aws.String(es.ID),
			},
		},
	}

	return input
}

func UpdateRoomPlanList(hotel_id string, RoomIDList []string) error {

	// tableName := "be_plans-dev"
	// rTableName := "be_rooms-dev"
	tableName := os.Getenv("TABLE_NAME")
	rTableName := os.Getenv("ROOM_TABLE_NAME")

	// DynamoDB
	db := be.Dynamodb()

	keyCond := expression.Key("hotel_id").Equal(expression.Value(hotel_id))
	filter := expression.Name("status").NotEqual(expression.Value("Delete"))
	expr, err := expression.NewBuilder().WithFilter(filter).WithKeyCondition(keyCond).Build()

	final := new([]*RoomSort)

	var lastEvaluatedKey LastKey
	var result *dynamodb.QueryOutput
	projection := aws.String("room_id_list,title")
	for {
		pp := new([]*RoomSort)
		if lastEvaluatedKey == (LastKey{}) {
			result, err = db.Query(QueryFirst(expr, tableName, projection))
		} else {
			result, err = db.Query(QueryNext(expr, tableName, projection, lastEvaluatedKey))
		}

		if err != nil {
			fmt.Println(err)
			return err
		}

		err = dynamodbattribute.UnmarshalListOfMaps(result.Items, &pp)
		if err != nil {
			fmt.Println(err)
			return err
		}

		for _, i := range *pp {
			*final = append(*final, i)
		}

		if result.LastEvaluatedKey == nil {
			break
		}

		err = dynamodbattribute.UnmarshalMap(result.LastEvaluatedKey, &lastEvaluatedKey)
		if err != nil {
			fmt.Println(err)
			return err
		}
	}

	fmt.Println("len(*final) :", len(*final))
	for i := 0; i < len(RoomIDList); i++ {
		var planList []string
		for j := 0; j < len(*final); j++ {
			pl := *&(*final)[j].RoomIDList
			for k := 0; k < len(pl); k++ {
				if RoomIDList[i] == pl[k] {
					planList = append(planList, *&(*final)[j].Title.Ja)
				}
			}
		}

		update := expression.Set(expression.Name("plan_list"), expression.Value(planList))
		expr1, err := expression.NewBuilder().
			WithUpdate(update).
			Build()

		input := &dynamodb.UpdateItemInput{
			Key: map[string]*dynamodb.AttributeValue{
				"hotel_id": {
					S: aws.String(hotel_id),
				},
				"id": {
					S: aws.String(RoomIDList[i]),
				},
			},
			TableName:                 aws.String(rTableName),
			ExpressionAttributeNames:  expr1.Names(),
			ExpressionAttributeValues: expr1.Values(),
			UpdateExpression:          expr1.Update(),
		}

		fmt.Println("input:", input)

		_, err = db.UpdateItem(input)
		if err != nil {
			return err
		}
	}
	return nil
}

func handler(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {

	org := request.Headers["origin"]
	// Table name

	tableName := os.Getenv("TABLE_NAME")
	planMapTable := os.Getenv("PLAN_MAP_TABLE")
	// tableName := "be_plans-dev"
	// planMapTable := "be_tl_plan_id_maps-dev"

	// Request path parameter
	hotel_id, ok := request.PathParameters["hotel_id"]
	if ok == false || valid.IsUUIDv4(hotel_id) == false {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorInvalidUrlHotelIdNotFound).Error()),
		})
	}

	id, ok := request.PathParameters["id"]
	if ok == false || valid.IsUUIDv4(id) == false {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorInvalidUrlIdNotFound).Error()),
		})
	}

	fmt.Printf("plan delete started hotel id : %v : plan id : %v\n", hotel_id, id)

	var sds be.Plan
	sds.HotelID = hotel_id
	sds.ID = id
	sds.Status = "Delete"
	sds.UpdatedAt = be.DateTimeStamp()

	// DynamoDB
	db := be.Dynamodb()

	// check if row with this key exist
	// input for GetItem
	input := &dynamodb.GetItemInput{
		Key: map[string]*dynamodb.AttributeValue{
			"hotel_id": {
				S: aws.String(hotel_id),
			},
			"id": {
				S: aws.String(id),
			},
		},
		TableName: aws.String(tableName),
	}

	// GetItem from dynamodb table
	result, err := db.GetItem(input)
	if err != nil {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorFailedToFetchRecord).Error()),
		})
	}

	if len(result.Item) > 0 {
		item := new(be.Plan)
		err = dynamodbattribute.UnmarshalMap(result.Item, item)
		if err != nil {
			return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
				ErrorMsg: aws.String(errors.New(be.ErrorCouldNotUnMarshalItem).Error()),
			})
		}

		input := &dynamodb.UpdateItemInput{
			ExpressionAttributeNames: map[string]*string{
				"#K1": aws.String("status"),
				"#K2": aws.String("updated_at"),
			},
			ExpressionAttributeValues: map[string]*dynamodb.AttributeValue{
				":v1": {
					S: aws.String(sds.Status),
				},
				":v2": {
					N: aws.String(strconv.FormatInt(sds.UpdatedAt, 10)),
				},
			},
			Key: map[string]*dynamodb.AttributeValue{
				"hotel_id": {
					S: aws.String(sds.HotelID),
				},
				"id": {
					S: aws.String(sds.ID),
				},
			},
			ReturnValues:     aws.String("ALL_NEW"),
			TableName:        aws.String(tableName),
			UpdateExpression: aws.String("SET #K1 = :v1, #K2 = :v2"),
		}

		fmt.Printf("plan delete started hotel id : %v : plan id : %v : input : %v\n", hotel_id, id, input)

		_, err := db.UpdateItem(input)
		if err != nil {
			return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
				ErrorMsg: aws.String(errors.New(be.ErrorCouldNotUpdateItem).Error()),
			})
		}

		fmt.Printf("plan status changed to delete hotel id : %v : plan id : %v\n", hotel_id, id)

		err = UpdateMapTable(hotel_id, sds.ID, planMapTable, db)
		if err != nil {
			fmt.Println("UpdateMapTable err : ", err)
			return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
				ErrorMsg: aws.String(errors.New("TL Plan map table update failed").Error()),
			})
		}

		err = UpdateRoomPlanList(hotel_id, sds.RoomIDList)
		if err != nil {
			return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
				ErrorMsg: aws.String(errors.New("Room update failed").Error()),
			})
		}

		return be.ApiResponse(org, http.StatusOK, &sds)
	} else {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorRecordNotFound).Error()),
		})
	}
}

func UpdateMapTable(hotelId, planId, planMapTable string, db *dynamodb.DynamoDB) error {

	item := new([]*PlanIDMapList)
	keyCond := expression.Key("hotel_id").Equal(expression.Value(hotelId))

	expr, err := expression.NewBuilder().WithKeyCondition(keyCond).Build()
	if err != nil {
		return err
	}

	input := &dynamodb.QueryInput{
		KeyConditionExpression:    expr.KeyCondition(),
		TableName:                 aws.String(planMapTable),
		ExpressionAttributeNames:  expr.Names(),
		ExpressionAttributeValues: expr.Values(),
	}

	// GetItem from dynamodb table
	result, err := db.Query(input)
	if err != nil {
		return err
	}

	err = dynamodbattribute.UnmarshalListOfMaps(result.Items, &item)
	if err != nil {
		return err
	}

	for _, entry := range *item {
		if entry.BEID == planId {
			fmt.Printf("plan update map table started hotel id : %v : plan id : %v\n", hotelId, planId)
			input := &dynamodb.UpdateItemInput{
				ExpressionAttributeNames: map[string]*string{
					"#K1": aws.String("status"),
				},
				ExpressionAttributeValues: map[string]*dynamodb.AttributeValue{
					":v1": {
						S: aws.String("Delete"),
					},
				},
				Key: map[string]*dynamodb.AttributeValue{
					"hotel_id": {
						S: aws.String(hotelId),
					},
					"tl_id": {
						S: aws.String(entry.TLID),
					},
				},
				ReturnValues:     aws.String("ALL_NEW"),
				TableName:        aws.String(planMapTable),
				UpdateExpression: aws.String("SET #K1 = :v1"),
			}

			fmt.Printf("plan update map table started hotel id : %v : plan id : %v : input : %v \n", hotelId, planId, input)

			_, err := db.UpdateItem(input)
			if err != nil {
				return err
			}
			fmt.Println("TL plan updated for id : ", planId)
		}

	}

	return nil
}

type PlanIDMapList struct {
	HotelID  string `json:"hotel_id"`
	TLID     string `json:"tl_id"`
	BEID     string `json:"be_id"`
}

func main() {
	lambda.Start(handler)
}
