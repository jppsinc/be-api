package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"math/rand"
	"net/http"
	"os"
	"time"

	be "bitbucket.org/jppsinc/be-modules/pkg"
	valid "github.com/asaskevich/govalidator"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/aws/aws-sdk-go/service/dynamodb/expression"
	"github.com/google/uuid"
)

// generate random string
var src = rand.NewSource(time.Now().UnixNano())

const letterBytes = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890"
const (
	letterIdxBits = 6                    // 6 bits to represent a letter index
	letterIdxMask = 1<<letterIdxBits - 1 // All 1-bits, as many as letterIdxBits
	letterIdxMax  = 63 / letterIdxBits   // # of letter indices fitting in 63 bits
)

func RandStringBytesMaskImprSrc(n int) string {
	b := make([]byte, n)
	// A src.Int63() generates 63 random bits, enough for letterIdxMax characters!
	for i, cache, remain := n-1, src.Int63(), letterIdxMax; i >= 0; {
		if remain == 0 {
			cache, remain = src.Int63(), letterIdxMax
		}
		if idx := int(cache & letterIdxMask); idx < len(letterBytes) {
			b[i] = letterBytes[idx]
			i--
		}
		cache >>= letterIdxBits
		remain--
	}

	return string(b)
}

type RoomSort struct {
	RoomIDList []string  `json:"room_id_list"`
	Title      MultiLang `json:"title"`
}

type LastKey struct {
	HotelID string `json:"hotel_id"`
	ID      string `json:"id"`
}

type MultiLang struct {
	Ja string `json:"ja"`
	En string `json:"en"`
}

func QueryFirst(expr expression.Expression, tableName string, projection *string) *dynamodb.QueryInput {
	input := &dynamodb.QueryInput{
		KeyConditionExpression:    expr.KeyCondition(),
		TableName:                 aws.String(tableName),
		ExpressionAttributeNames:  expr.Names(),
		ExpressionAttributeValues: expr.Values(),
		FilterExpression:          expr.Filter(),
		ProjectionExpression:      projection,
	}

	return input
}

func QueryNext(expr expression.Expression, tableName string, projection *string, es LastKey) *dynamodb.QueryInput {
	input := &dynamodb.QueryInput{
		KeyConditionExpression:    expr.KeyCondition(),
		TableName:                 aws.String(tableName),
		ExpressionAttributeNames:  expr.Names(),
		ExpressionAttributeValues: expr.Values(),
		FilterExpression:          expr.Filter(),
		ProjectionExpression:      projection,
		ExclusiveStartKey: map[string]*dynamodb.AttributeValue{
			"hotel_id": {
				S: aws.String(es.HotelID),
			},
			"id": {
				S: aws.String(es.ID),
			},
		},
	}

	return input
}

func UpdateOptionPlanList(hotelId string, planId string, optionList []string, tableName string, db *dynamodb.DynamoDB) error {

	for _, optionId := range optionList {
		// input for GetItem
		input := &dynamodb.GetItemInput{
			Key: map[string]*dynamodb.AttributeValue{
				"hotel_id": {
					S: aws.String(hotelId),
				},
				"id": {
					S: aws.String(optionId),
				},
			},
			TableName:            aws.String(tableName),
			ProjectionExpression: aws.String("plan_list"),
		}

		// GetItem from dynamodb table
		result, err := db.GetItem(input)
		if err != nil {
			return err
		}

		if len(result.Item) > 0 {
			item := new(be.Option)
			err = dynamodbattribute.UnmarshalMap(result.Item, item)
			if err != nil {
				return err
			}

			flag := false
			for _, pid := range item.PlanList {
				if pid == planId {
					flag = true
				}
			}

			if !flag {
				item.PlanList = append(item.PlanList, planId)
				update := expression.Set(expression.Name("plan_list"), expression.Value(item.PlanList))
				expr1, err := expression.NewBuilder().WithUpdate(update).Build()
				if err != nil {
					return err
				}

				input := &dynamodb.UpdateItemInput{
					Key: map[string]*dynamodb.AttributeValue{
						"hotel_id": {
							S: aws.String(hotelId),
						},
						"id": {
							S: aws.String(optionId),
						},
					},
					TableName:                 aws.String(tableName),
					ExpressionAttributeNames:  expr1.Names(),
					ExpressionAttributeValues: expr1.Values(),
					UpdateExpression:          expr1.Update(),
				}

				fmt.Println("input:", input)

				_, err = db.UpdateItem(input)
				if err != nil {
					return err
				}
			}
		}
	}

	return nil
}

func SetRoomPlanList(hotel_id string, RoomIDList []string) error {

	// tableName := "be_plans"
	// rTableName := "be_rooms"
	tableName := os.Getenv("TABLE_NAME")
	rTableName := os.Getenv("ROOM_TABLE_NAME")

	// DynamoDB
	db := be.Dynamodb()

	keyCond := expression.Key("hotel_id").Equal(expression.Value(hotel_id))
	filter := expression.Name("status").NotEqual(expression.Value("Delete"))
	expr, err := expression.NewBuilder().WithFilter(filter).WithKeyCondition(keyCond).Build()

	final := new([]*RoomSort)

	var lastEvaluatedKey LastKey
	var result *dynamodb.QueryOutput
	projection := aws.String("room_id_list,title")
	for {
		pp := new([]*RoomSort)
		if lastEvaluatedKey == (LastKey{}) {
			result, err = db.Query(QueryFirst(expr, tableName, projection))
		} else {
			result, err = db.Query(QueryNext(expr, tableName, projection, lastEvaluatedKey))
		}

		if err != nil {
			fmt.Println(err)
			return err
		}

		err = dynamodbattribute.UnmarshalListOfMaps(result.Items, &pp)
		if err != nil {
			fmt.Println(err)
			return err
		}

		for _, i := range *pp {
			*final = append(*final, i)
		}

		if result.LastEvaluatedKey == nil {
			break
		}

		err = dynamodbattribute.UnmarshalMap(result.LastEvaluatedKey, &lastEvaluatedKey)
		if err != nil {
			fmt.Println(err)
			return err
		}
	}

	fmt.Println("len(*final) :", len(*final))
	for i := 0; i < len(RoomIDList); i++ {
		var planList []string
		for j := 0; j < len(*final); j++ {
			pl := *&(*final)[j].RoomIDList
			for k := 0; k < len(pl); k++ {
				if RoomIDList[i] == pl[k] {
					planList = append(planList, *&(*final)[j].Title.Ja)
				}
			}
		}

		fmt.Println("planList:", planList)

		update := expression.Set(expression.Name("plan_list"), expression.Value(planList))
		expr1, err := expression.NewBuilder().
			WithUpdate(update).
			Build()

		input := &dynamodb.UpdateItemInput{
			Key: map[string]*dynamodb.AttributeValue{
				"hotel_id": {
					S: aws.String(hotel_id),
				},
				"id": {
					S: aws.String(RoomIDList[i]),
				},
			},
			TableName:                 aws.String(rTableName),
			ExpressionAttributeNames:  expr1.Names(),
			ExpressionAttributeValues: expr1.Values(),
			UpdateExpression:          expr1.Update(),
		}

		fmt.Println("input:", input)

		_, err = db.UpdateItem(input)
		if err != nil {
			return err
		}
	}
	return nil
}

func handler(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {

	org := request.Headers["origin"]
	// Table name
	// tableName := "be_plans"
	tableName := os.Getenv("TABLE_NAME")
	oTableName := os.Getenv("OPTION_TABLE_NAME")

	// Change JSON request data to struct : just to check validity of request data
	var sds be.Plan

	if err := json.Unmarshal([]byte(request.Body), &sds); err != nil {
		fmt.Println(err)
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorCouldNotUnMarshalItem).Error()),
		})
	}

	// Request path parameter
	hotel_id, ok := request.PathParameters["hotel_id"]
	if ok == false || valid.IsUUIDv4(hotel_id) == false {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorInvalidUrlHotelIdNotFound).Error()),
		})
	} else {
		sds.ID = fmt.Sprintf("%s", uuid.New())
		sds.HotelID = hotel_id
		sds.Code = RandStringBytesMaskImprSrc(8)
		sds.CreatedAt = be.DateTimeStamp()
		sds.UpdatedAt = be.DateTimeStamp()
	}

	// create a separate field for list of ids of all the attached hotel.
	// it is used in temairazu plan api
	sds.RoomIDList = nil
	sds.RoomNameList = nil
	for _, info := range sds.RoomInfo {
		sds.RoomIDList = append(sds.RoomIDList, info.RoomID)
		sds.RoomNameList = append(sds.RoomNameList, info.RoomType.Ja)
	}
	fmt.Println("sds.RoomIDList :", sds.RoomIDList)
	fmt.Println("sds.RoomNameList :", sds.RoomNameList)

	// for _, info := range sds.Meal {
	// 	if info.Breakfast != nil {
	// 		sds.Breakfast = true
	// 		sds.BreakfastPlace = info.Place
	// 	}
	// 	if info.Lunch != nil {
	// 		sds.Lunch = true
	// 		sds.LunchPlace = info.Place
	// 	}
	// 	if info.Dinner != nil {
	// 		sds.Dinner = true
	// 		sds.DinnerPlace = info.Place
	// 	}
	// }

	// DynamoDB
	db := be.Dynamodb()

	// change struct data to json data, this data will be stored to database
	av, err := dynamodbattribute.MarshalMap(sds)
	if err != nil {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorCouldNotMarshalItem).Error()),
		})
	}

	if len(sds.OptionIDList) == 0 {
		empty := []*dynamodb.AttributeValue{}
		av["option_id_list"] = &dynamodb.AttributeValue{L: empty}
	}
	if len(sds.PlanGroupList) == 0 {
		empty := []*dynamodb.AttributeValue{}
		av["plan_group_list"] = &dynamodb.AttributeValue{L: empty}
	}
	if len(sds.QuestionsForCustomer) == 0 {
		empty := []*dynamodb.AttributeValue{}
		av["questions_for_customer"] = &dynamodb.AttributeValue{L: empty}
	}
	if len(sds.RoomIDList) == 0 {
		empty := []*dynamodb.AttributeValue{}
		av["room_id_list"] = &dynamodb.AttributeValue{L: empty}
	}

	ReturnItem := "SIZE"
	input := &dynamodb.PutItemInput{
		Item:                        av,
		TableName:                   aws.String(tableName),
		ReturnItemCollectionMetrics: &ReturnItem,
	}

	_, err = db.PutItem(input)
	if err != nil {
		fmt.Println(err)
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorCouldNotDynamoPutItem).Error()),
		})
	}

	err = SetRoomPlanList(hotel_id, sds.RoomIDList)
	if err != nil {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New("Room update failed").Error()),
		})
	}

	err = UpdateOptionPlanList(hotel_id, sds.ID, sds.OptionIDList, oTableName, db)
	if err != nil {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New("option update failed").Error()),
		})
	}

	return be.ApiResponse(org, http.StatusCreated, &sds)
}

func main() {
	lambda.Start(handler)
}
