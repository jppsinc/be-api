package main

import (
	"errors"
	"fmt"
	"net/http"
	"os"
	"sort"
	"strings"

	be "bitbucket.org/jppsinc/be-modules/pkg"
	valid "github.com/asaskevich/govalidator"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/aws/aws-sdk-go/service/dynamodb/expression"
)

func handler(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {

	org := request.Headers["origin"]
	// handle option
	if request.HTTPMethod == http.MethodOptions {
		item := new(be.EmptyStruct)
		return be.ApiResponse(org, http.StatusOK, item)
	}
	// Table name
	// tableName := "be_plans"
	tableName := os.Getenv("TABLE_NAME")

	// Request path parameter
	hotel_id, ok := request.PathParameters["hotel_id"]
	if ok == false || valid.IsUUIDv4(hotel_id) == false {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorInvalidUrlHotelIdNotFound).Error()),
		})
	}

	keyCond := expression.Key("hotel_id").Equal(expression.Value(hotel_id))

	var finalFilter expression.ConditionBuilder
	var finalFilter1 expression.ConditionBuilder
	var finalFilter2 expression.ConditionBuilder
	var filter []expression.ConditionBuilder
	var filter1 []expression.ConditionBuilder
	var filter2 []expression.ConditionBuilder

	roomFlag := false
	optionFlag := false

	// query string parameter
	if len(request.QueryStringParameters) == 0 {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorQueryStringParameterNotFound).Error()),
		})
	} else {
		filt, ok := request.QueryStringParameters["filter"]
		if ok == true || len(strings.TrimSpace(filt)) != 0 {
			switch filt {
			case "PlanForSale":
				filter = append(filter, expression.Name("sales_category").Equal(expression.Value(true)))
			case "PlanOnStop":
				filter = append(filter, expression.Name("sales_category").Equal(expression.Value(false)).And(expression.Name("status").Equal(expression.Value("Sales"))))
			case "ExpiredSalesPlan":
				// cd := fmt.Sprintf("%v", time.Now().Format("2006-01-02"))
				cd := be.DateString("date")
				fmt.Println("sales_end_date :", cd)
				filter = append(filter, expression.Name("sales_end_date").LessThanEqual(expression.Value(cd)))
			case "PreparingPlan":
				filter = append(filter, expression.Name("status").Equal(expression.Value("Creating")))
			case "DisplayPlan":
				filter = append(filter, expression.Name("show_in_plan_list").Equal(expression.Value(true)))
			case "HiddenPlan":
				filter = append(filter, expression.Name("show_in_plan_list").Equal(expression.Value(false)))
			}
		}

		specialCode, ok := request.QueryStringParameters["specialCode"]
		if ok == true || len(strings.TrimSpace(specialCode)) != 0 {
			filter = append(filter, expression.Name("special_code").Equal(expression.Value(specialCode)))
		}

		salesStart, ok := request.QueryStringParameters["salesStart"]
		if ok == true || len(strings.TrimSpace(salesStart)) != 0 {
			filter = append(filter, expression.Name("sales_start_date").LessThanEqual(expression.Value(salesStart)))
		}

		salesEnd, ok := request.QueryStringParameters["salesEnd"]
		if ok == true || len(strings.TrimSpace(salesEnd)) != 0 {
			filter = append(filter, expression.Name("sales_end_date").GreaterThanEqual(expression.Value(salesEnd)))
		}

		planName, ok := request.QueryStringParameters["planName"]
		if ok == true || len(strings.TrimSpace(planName)) != 0 {
			filter = append(filter, expression.Name("title.ja").Contains(planName).Or(expression.Name("code").Equal(expression.Value(planName))))
		}

		roomType, ok := request.QueryStringParameters["roomType"]
		if ok == true && len(strings.TrimSpace(roomType)) != 0 {

			ids := strings.Split(roomType, ",")
			for _, id := range ids {
				filter1 = append(filter1, expression.Name("room_id_list").Contains(id))
			}

			// We need: otherfilters AND (f1 OR f2 OR f3) where f1 OR f2 OR f3 is room id
			// this loop creates : (f1 OR f2 OR f3)
			// later we add it with AND
			if len(filter1) != 0 {
				roomFlag = true
				for i, temp := range filter1 {
					if i == 0 {
						finalFilter1 = temp
					} else {
						finalFilter1 = finalFilter1.Or(temp)
					}
				}
			}
		}

		optionList, ok := request.QueryStringParameters["option"]
		if ok == true && len(optionList) != 0 {

			ids := strings.Split(optionList, ",")
			for _, id := range ids {
				filter2 = append(filter2, expression.Name("option_list").Contains(id))
			}

			// We need: otherfilters AND (f1 OR f2 OR f3) where f1 OR f2 OR f3 is room id
			// this loop creates : (f1 OR f2 OR f3)
			// later we add it with AND
			if len(filter2) != 0 {
				optionFlag = true
				for i, temp := range filter2 {
					if i == 0 {
						finalFilter2 = temp
					} else {
						finalFilter2 = finalFilter2.Or(temp)
					}
				}
			}
		}

		meal, ok := request.QueryStringParameters["meal"]
		if ok == true || len(strings.TrimSpace(meal)) != 0 {
			s := strings.Split(meal, ",")
			for _, v := range s {
				filter = append(filter, expression.Name(strings.ToLower(v)).Equal(expression.Value(true)))
			}
		}

		filter = append(filter, expression.Name("status").NotEqual(expression.Value("Delete")))
	}

	if len(filter) != 0 {
		for i, temp := range filter {
			if i == 0 {
				finalFilter = temp
			} else {
				finalFilter = finalFilter.And(temp)
			}
		}
	}

	if roomFlag == true {
		finalFilter = finalFilter.And(finalFilter1)
	}

	if optionFlag == true {
		finalFilter = finalFilter.And(finalFilter1)
	}

	var input *dynamodb.QueryInput
	if len(filter) == 0 {
		expr, err := expression.NewBuilder().WithKeyCondition(keyCond).Build()
		if err != nil {
			return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
				ErrorMsg: aws.String(err.Error()),
			})
		}

		// input for GetItem
		input = &dynamodb.QueryInput{
			KeyConditionExpression:    expr.KeyCondition(),
			TableName:                 aws.String(tableName),
			ExpressionAttributeNames:  expr.Names(),
			ExpressionAttributeValues: expr.Values(),
		}
	} else {
		expr, err := expression.NewBuilder().WithKeyCondition(keyCond).WithFilter(finalFilter).Build()
		if err != nil {
			return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
				ErrorMsg: aws.String(err.Error()),
			})
		}

		// input for GetItem
		input = &dynamodb.QueryInput{
			KeyConditionExpression:    expr.KeyCondition(),
			TableName:                 aws.String(tableName),
			FilterExpression:          expr.Filter(),
			ExpressionAttributeNames:  expr.Names(),
			ExpressionAttributeValues: expr.Values(),
		}
	}

	fmt.Println("input :", input)

	// DynamoDB
	db := be.Dynamodb()

	// GetItem from dynamodb table
	result, err := db.Query(input)
	if err != nil {
		fmt.Println("err :", err)
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorFailedToFetchRecord).Error()),
		})
	}

	// item1 := new([]*be.Plan)
	item := new([]*be.Plan)
	err = dynamodbattribute.UnmarshalListOfMaps(result.Items, &item)
	if err != nil {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorCouldNotUnMarshalItem).Error()),
		})
	}

	// sort by Order
	sort.SliceStable(*item, func(i, j int) bool {
		return *&(*item)[i].Order < *&(*item)[j].Order
	})

	if len(*item) > 0 {
		for i := 0; i < len(*item); i++ {
			if len(*&(*item)[i].Payment) == 0 {
				*&(*item)[i].Payment = make([]string, 0)
			}
			if len(*&(*item)[i].RoomIDList) == 0 {
				*&(*item)[i].RoomIDList = make([]string, 0)
			}
			if len(*&(*item)[i].RoomNameList) == 0 {
				*&(*item)[i].RoomNameList = make([]string, 0)
			}
			if len(*&(*item)[i].PlanImages) == 0 {
				*&(*item)[i].PlanImages = make([]be.Image, 0)
			}
			if len(*&(*item)[i].Tax) == 0 {
				*&(*item)[i].Tax = make([]be.PlanTax, 0)
			}
			if len(*&(*item)[i].Meal) == 0 {
				*&(*item)[i].Meal = make([]be.Meal, 0)
			}
			if len(*&(*item)[i].QuestionsForCustomer) == 0 {
				*&(*item)[i].QuestionsForCustomer = make([]be.QuestionsForCustomer, 0)
			}
			if len(*&(*item)[i].RoomInfo) == 0 {
				*&(*item)[i].RoomInfo = make([]be.RoomInfo, 0)
			}
			if len(*&(*item)[i].ChildSetting.ChildInfo) == 0 {
				*&(*item)[i].ChildSetting.ChildInfo = make([]be.ChildInfo, 0)
			}
			if len(*&(*item)[i].OptionIDList) == 0 {
				*&(*item)[i].OptionIDList = make([]string, 0)
			}
			if len(*&(*item)[i].PlanGroupList) == 0 {
				*&(*item)[i].PlanGroupList = make([]be.PlanGroupList, 0)
			}
			if len(*&(*item)[i].SpecialCodes) == 0 {
				*&(*item)[i].SpecialCodes = make([]string, 0)
			}
		}
		return be.ApiResponse(org, http.StatusOK, item)
	} else {
		return be.ApiResponse(org, http.StatusOK, []be.Plan{})
	}
}

func main() {
	lambda.Start(handler)
}
