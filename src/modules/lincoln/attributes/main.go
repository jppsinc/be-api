package main

import (
	"net/http"

	be "bitbucket.org/jppsinc/be-modules/pkg"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
)

type AttributeList struct {
	Hotel []string `json:"hotel"`
	Room  []string `json:"room"`
	Plan  []string `json:"plan"`
}

func handler(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {

	org := request.Headers["origin"]

	h := []string{"a", "b"}
	r := []string{"a", "b"}
	p := []string{"a", "b"}

	item :=AttributeList{h, r, p}

	return be.ApiResponse(org, http.StatusOK, item)
}

func main() {
	lambda.Start(handler)
}
