package main

import (
	"errors"
	"net/http"
	"os"
	"strings"

	be "bitbucket.org/jppsinc/be-modules/pkg"
	valid "github.com/asaskevich/govalidator"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
)

func handler(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {

	org := request.Headers["origin"]
	// Table name
	// tableName := "be_rooms"
	tableName := ""

	// Request path parameter
	hotel_id, ok := request.PathParameters["hotel_id"]
	if !ok || !valid.IsUUIDv4(hotel_id) {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorInvalidUrlHotelIdNotFound).Error()),
		})
	}

	// query string parameter
	if len(request.QueryStringParameters) == 0 {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorQueryStringParameterNotFound).Error()),
		})
	} else {
		of, ok := request.QueryStringParameters["of"]
		if ok || len(strings.TrimSpace(of)) != 0 {

			switch of {
			case "price":
				tableName = os.Getenv("PRICE_TABLE")
			case "room":
				tableName = os.Getenv("ROOM_TABLE")
			case "inventory":
				tableName = os.Getenv("INVENTORY_TABLE")
			case "plan":
				tableName = os.Getenv("PLAN_TABLE")
			case "option":
				tableName = os.Getenv("OPTION_TABLE")
			default:
				return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
					ErrorMsg: aws.String(errors.New("Wrong value supplied for QP : of").Error()),
				})
			}
		}
	}

	// DynamoDB
	db := be.Dynamodb()

	// input for GetItem
	input := &dynamodb.GetItemInput{
		Key: map[string]*dynamodb.AttributeValue{
			"hotel_id": {
				S: aws.String(hotel_id),
			},
		},
		TableName: aws.String(tableName),
	}
	// GetItem from dynamodb table
	result, err := db.GetItem(input)
	if err != nil {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorFailedToFetchRecord).Error()),
		})
	}

	item := new(be.UpdateTime)
	err = dynamodbattribute.UnmarshalMap(result.Item, item)
	if err != nil {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorCouldNotUnMarshalItem).Error()),
		})
	}

	if len(result.Item) > 0 {
		return be.ApiResponse(org, http.StatusOK, item)
	} else {
		return be.ApiResponse(org, http.StatusOK, be.UpdateTime{UpdatedAt: "0000-00-00 00-00-00"})
	}
}

func main() {
	lambda.Start(handler)
}
