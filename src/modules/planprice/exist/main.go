package main

import (
	"errors"
	"fmt"
	"net/http"
	"os"
	"sync"
	"time"

	be "bitbucket.org/jppsinc/be-modules/pkg"
	valid "github.com/asaskevich/govalidator"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/aws/aws-sdk-go/service/dynamodb/expression"
)

type Resp struct {
	ID    string `json:"id"`
	Exist bool   `json:"exist"`
}

func SalesDefaultSetting(hotel_id string) (int, int, error) {
	// Table name
	// tableName := "be_sales_default_settings-stg"
	tableName := os.Getenv("SDS_TABLE_NAME")

	// DynamoDB
	db := be.Dynamodb()

	// input for GetItem
	input := &dynamodb.GetItemInput{
		Key: map[string]*dynamodb.AttributeValue{
			"hotel_id": {
				S: aws.String(hotel_id),
			},
		},
		TableName: aws.String(tableName),
	}

	// GetItem from dynamodb table
	result, err := db.GetItem(input)
	if err != nil {
		return 0, 0, err
	}

	item := new(be.SalesDefaultSetting)
	err = dynamodbattribute.UnmarshalMap(result.Item, item)
	if err != nil {
		return 0, 0, err
	}
	if len(result.Item) > 0 {
		return item.MinimumReservationCharge, item.ReservablePeriod, nil
	} else {
		return 0, 0, err
	}
}

func CutOffDays(checkIn string) int {
	layout := "2006-01-02"
	cin, _ := time.Parse(layout, checkIn)
	booking, _ := time.Parse(layout, be.DateString("date"))
	days := int(cin.Sub(booking).Hours() / 24)
	return days
}

func GetPlanPricesWithCutoff(hotelID string, planId string, checkIN string, cod int, minResCharge int, db *dynamodb.DynamoDB, tableName string) (*[]*be.PlanPrice, error) {
	var keyCond expression.KeyConditionBuilder
	var finalFilter expression.ConditionBuilder
	var filter []expression.ConditionBuilder

	item := new([]*be.PlanPrice)

	filter = append(filter, expression.Name("cut_off_days").NotEqual(expression.Value(0)).
		And(expression.Name("cut_off_days").LessThan(expression.Value(cod))).
		Or(expression.Name("cut_off_days").Equal(expression.Value(0))))

	filter = append(filter, expression.Name("sales_category").Equal(expression.Value(true)))
	filter = append(filter, expression.Name("show_in_plan_list").Equal(expression.Value(true)))
	filter = append(filter, expression.Name("status").Equal(expression.Value("Sales")).Or(expression.Name("status").Equal(expression.Value("sales"))))
	filter = append(filter, expression.Name("plan_id").Equal(expression.Value(planId)))

	filter = append(filter, expression.Name("count_vacant").GreaterThanEqual(expression.Value(1)))
	filter = append(filter, expression.Name("price").GreaterThan(expression.Value(0)))
	filter = append(filter, expression.Name("price").GreaterThanEqual(expression.Value(minResCharge)))

	keyCond = expression.KeyAnd(expression.Key("hotel_id").Equal(expression.Value(hotelID)), expression.Key("use_date").Equal(expression.Value(checkIN)))

	var input *dynamodb.QueryInput
	for i, temp := range filter {
		if i == 0 {
			finalFilter = temp
		} else {
			finalFilter = finalFilter.And(temp)
		}
	}

	expr, err := expression.NewBuilder().WithKeyCondition(keyCond).WithFilter(finalFilter).Build()
	if err != nil {
		return item, err
	}

	// input for GetItem
	input = &dynamodb.QueryInput{
		KeyConditionExpression:    expr.KeyCondition(),
		TableName:                 aws.String(tableName),
		FilterExpression:          expr.Filter(),
		ExpressionAttributeNames:  expr.Names(),
		ExpressionAttributeValues: expr.Values(),
		IndexName:                 aws.String("hotel-id-use-date-index"),
	}

	// GetItem from dynamodb table
	result, err := db.Query(input)
	if err != nil {
		return item, err
	}

	err = dynamodbattribute.UnmarshalListOfMaps(result.Items, &item)
	if err != nil {
		return item, err
	}

	return item, nil
}

func GetPlanPricesNoCutoff(hotelID string, planId string, minResCharge int, db *dynamodb.DynamoDB, tableName string, mm int, resp *[]Resp, wg *sync.WaitGroup) (*[]*be.PlanPrice, error) {

	defer wg.Done()
	var keyCond expression.KeyConditionBuilder
	var finalFilter expression.ConditionBuilder
	var filter []expression.ConditionBuilder

	item := new([]*be.PlanPrice)

	filter = append(filter, expression.Name("sales_category").Equal(expression.Value(true)))
	filter = append(filter, expression.Name("show_in_plan_list").Equal(expression.Value(true)))
	filter = append(filter, expression.Name("status").Equal(expression.Value("Sales")).Or(expression.Name("status").Equal(expression.Value("sales"))))

	// dt := be.DateString("date")
	filter = append(filter, expression.Name("count_vacant").GreaterThanEqual(expression.Value(1)))
	filter = append(filter, expression.Name("sc_price").GreaterThan(expression.Value(0)))

	// no need to check for date. If we include date query returns 0 rows.
	// if we do not use use_date in filter it will return past date data.
	// to overcome that problem we will create lambda function that will remove past use_date entries every day.
	// filter = append(filter, expression.Name("use_date").GreaterThan(expression.Value(dt)))
	filter = append(filter, expression.Name("sc_price").GreaterThanEqual(expression.Value(minResCharge)))

	keyCond = expression.KeyAnd(expression.Key("hotel_id").Equal(expression.Value(hotelID)), expression.Key("plan_room_use_date_pax").BeginsWith(planId))

	var input *dynamodb.QueryInput
	for i, temp := range filter {
		if i == 0 {
			finalFilter = temp
		} else {
			finalFilter = finalFilter.And(temp)
		}
	}

	expr, err := expression.NewBuilder().WithKeyCondition(keyCond).WithFilter(finalFilter).Build()
	if err != nil {
		return item, err
	}

	// input for GetItem
	input = &dynamodb.QueryInput{
		KeyConditionExpression:    expr.KeyCondition(),
		TableName:                 aws.String(tableName),
		FilterExpression:          expr.Filter(),
		ExpressionAttributeNames:  expr.Names(),
		ExpressionAttributeValues: expr.Values(),
	}

	// fmt.Println("input : ", input)

	// GetItem from dynamodb table
	result, err := db.Query(input)
	if err != nil {
		return item, err
	}

	err = dynamodbattribute.UnmarshalListOfMaps(result.Items, &item)
	if err != nil {
		return item, err
	}
	fmt.Printf("len : %v : %v\n", len(result.Items), planId)
	if len(*item) > 0 {
		(*resp)[mm].Exist = true
		(*resp)[mm].ID = planId
	}

	return item, nil
}

func GetPlans(hotelID string, db *dynamodb.DynamoDB, tableName string) (*[]*be.Plan, error) {

	keyCond := expression.Key("hotel_id").Equal(expression.Value(hotelID))
	filter := expression.Name("status").NotEqual(expression.Value("Delete"))

	item := new([]*be.Plan)
	expr, err := expression.NewBuilder().WithKeyCondition(keyCond).WithFilter(filter).Build()
	if err != nil {
		return item, err
	}

	// input for GetItem
	input := &dynamodb.QueryInput{
		KeyConditionExpression:    expr.KeyCondition(),
		TableName:                 aws.String(tableName),
		ExpressionAttributeNames:  expr.Names(),
		ExpressionAttributeValues: expr.Values(),
		FilterExpression:          expr.Filter(),
	}

	// GetItem from dynamodb table
	result, err := db.Query(input)
	if err != nil {
		fmt.Println("err :", err)
		return item, err
	}

	err = dynamodbattribute.UnmarshalListOfMaps(result.Items, &item)
	if err != nil {
		return item, err
	}

	return item, nil
}

func handler(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	org := request.Headers["origin"]
	// Table name
	// tableName := "be_planprices-stg"
	// pTableName := "be_plans-stg"

	tableName := os.Getenv("TABLE_NAME")
	pTableName := os.Getenv("PLAN_TABLE_NAME")

	// Request path parameter
	hotel_id, ok := request.PathParameters["hotel_id"]
	if !ok || !valid.IsUUIDv4(hotel_id) {
		return be.ApiResponseGuest(http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorInvalidUrlHotelIdNotFound).Error()),
		})
	}

	db := be.Dynamodb()
	plans, err := GetPlans(hotel_id, db, pTableName)
	if err != nil {
		return be.ApiResponseGuest(http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(err.Error()),
		})
	}

	fmt.Printf("================len(*plans) :%v : ", len(*plans))

	minResCharge, reservablePeriod, _ := SalesDefaultSetting(hotel_id)

	resp := make([]Resp, len(*plans))

	var wg sync.WaitGroup
	for mm, plan := range *plans {
		resp[mm].ID = plan.ID
		fmt.Printf("number :%v : ", mm)

		wg.Add(1)

		if (plan.Status != "Sales" && plan.Status != "sales") || plan.SalesCategory == false || plan.ShowInPlanList == false {

			fmt.Println("Plan status is not sales OR salesCategory is false for plan :", plan.Title.En)
			go GoRoutineForNotExist(mm, &resp, &wg)

		} else if plan.CutOffDays > 0 {

			fmt.Println("cut off day > 0 for Plan id :", plan.Title.En)
			go GoRoutineWithCuttOff(hotel_id, resp[mm].ID, minResCharge, db, tableName, reservablePeriod, mm, &resp, &wg)

		} else {

			fmt.Printf("cut off day 0 for Plan id : %v : %v\n", plan.Title.En, plan.ID)
			go GetPlanPricesNoCutoff(hotel_id, resp[mm].ID, minResCharge, db, tableName, mm, &resp, &wg)

		}
	}

	wg.Wait()

	return be.ApiResponse(org, http.StatusOK, resp)

}

func main() {
	lambda.Start(handler)
}

func GoRoutineForNotExist(mm int, resp *[]Resp, wg *sync.WaitGroup) {
	defer wg.Done()
	(*resp)[mm].Exist = false
}

// func GoRoutineWithCuttOff(hotel_id, resp[mm].ID, checkIn, cod, minResCharge, db, tableName) {
func GoRoutineWithCuttOff(hotel_id string, planID string, minResCharge int, db *dynamodb.DynamoDB, tableName string, reservablePeriod int, mm int, resp *[]Resp, wg *sync.WaitGroup) {

	defer wg.Done()
	loc, _ := time.LoadLocation("Asia/Tokyo")
	today := be.DateString("date")
	t2day, _ := time.Parse("2006-01-02", today)

	for b := 0; b < reservablePeriod; b++ {

		var checkIn string
		checkIn = t2day.AddDate(0, 0, b+1).In(loc).Format("2006-01-02")

		cod := CutOffDays(checkIn)
		// find all the planprices for this plan in this date
		item, _ := GetPlanPricesWithCutoff(hotel_id, planID, checkIn, cod, minResCharge, db, tableName)

		if len(*item) > 0 {
			(*resp)[mm].Exist = true
			(*resp)[mm].ID = planID
			break
		}
	}
}
