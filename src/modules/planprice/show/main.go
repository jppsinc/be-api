package main

import (
	"fmt"
	"net/http"
	"os"

	"github.com/guregu/dynamo"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"

	be "bitbucket.org/jppsinc/be-modules/pkg"
)

type Resp struct {
	Exist bool `json:"exist"`
}

var planPriceTable dynamo.Table

func init() {
	sess := session.Must(session.NewSession())
	db := dynamo.New(sess, &aws.Config{Region: aws.String(os.Getenv("AWS_REGION"))})

	planPrice := os.Getenv("PLAN_PRICE")

	planPriceTable = db.Table(planPrice)
}

func main() {
	lambda.Start(handler)
}

func handler(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	org := request.Headers["origin"]

	hotelID, ok := request.PathParameters["hotel_id"]
	if !ok {
		return be.ApiResponse(org, http.StatusBadRequest, "パラメータが不正です")
	}

	id, ok := request.PathParameters["id"]
	if !ok {
		return be.ApiResponse(org, http.StatusBadRequest, "パラメータが不正です")
	}

	response := Resp{}
	exist, err := checkPlanPriceExist(hotelID, id)
	if err != nil {
		fmt.Println(err.Error())
		return be.ApiResponse(org, http.StatusInternalServerError, err.Error())
	}

	response.Exist = exist

	return be.ApiResponse(org, http.StatusOK, response)
}

func checkPlanPriceExist(hotelID, id string) (bool, error) {
	var planPrice be.PlanPrice
	err := planPriceTable.Get("hotel_id", hotelID).
		Range("plan_room_use_date_pax", dynamo.BeginsWith, id).
		Order(false).
		Limit(1).
		One(&planPrice)
	if err != nil && err.Error() != "dynamo: no item found" {
		return false, err
	}

	if err != nil && err.Error() == "dynamo: no item found" {
		return false, nil
	}

	if planPrice.UseDate < be.DateString("date") {
		return false, nil
	}

	return true, nil
}
