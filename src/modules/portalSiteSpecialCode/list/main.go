package main

import (
	"fmt"
	"net/http"
	"os"

	"github.com/guregu/dynamo"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"

	be "bitbucket.org/jppsinc/be-modules/pkg"
)

type Response struct {
	PortalSiteID           string                     `json:"portal_site_id"`
	PortalSiteName         string                     `json:"portal_site_name"`
	PortalSiteSpecialCodes []be.PortalSiteSpecialCode `json:"portal_site_special_codes"`
}

var portalSiteTable dynamo.Table
var portalSiteSpecialCodeTable dynamo.Table
var groupHotelTable dynamo.Table

func init() {
	portalSite := os.Getenv("PORTAL_SITE")
	portalSiteSpecialCode := os.Getenv("PORTAL_SITE_SPECIAL_CODE")
	groupHotel := os.Getenv("GROUP_HOTEL")

	sess := session.Must(session.NewSession())
	db := dynamo.New(sess, &aws.Config{Region: aws.String(os.Getenv("AWS_REGION"))})

	portalSiteTable = db.Table(portalSite)
	portalSiteSpecialCodeTable = db.Table(portalSiteSpecialCode)
	groupHotelTable = db.Table(groupHotel)
}

func main() {
	lambda.Start(handler)
}

func handler(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	org := request.Headers["origin"]

	hotelID, ok := request.PathParameters["hotel_id"]
	if !ok {
		fmt.Println("パラメータが不正です。")
		return be.ApiResponse(org, http.StatusBadRequest, "パラメータが不正です")
	}

	groupHotel, err := getGroupHotel(hotelID)
	if err != nil {
		fmt.Println(err.Error())
		return be.ApiResponse(org, http.StatusInternalServerError, err.Error())
	}

	response, err := getPortalSiteSpecilaCodes(groupHotel)
	if err != nil {
		fmt.Println(err.Error())
		return be.ApiResponse(org, http.StatusInternalServerError, err.Error())
	}

	return be.ApiResponse(org, http.StatusOK, response)
}

func getGroupHotel(hotelID string) (be.GroupHotel, error) {
	var groupHotel be.GroupHotel
	err := groupHotelTable.Get("hotel_id", hotelID).One(&groupHotel)
	if err != nil {
		return be.GroupHotel{}, err
	}

	return groupHotel, nil
}

func getPortalSiteSpecilaCodes(groupHotel be.GroupHotel) ([]Response, error) {
	var response []Response
	for _, portalSiteInfo := range groupHotel.PortalSiteInfoList {
		var specialCodeData Response
		var portalSite be.PortalSite
		var portalSiteSpecialCodes []be.PortalSiteSpecialCode
		err := portalSiteTable.Get("id", portalSiteInfo.PortalSiteID).One(&portalSite)
		if err != nil {
			return nil, err
		}

		//portal-site-id必要か？

		err = portalSiteSpecialCodeTable.Get("portal_site_id", portalSiteInfo.PortalSiteID).All(&portalSiteSpecialCodes)
		if err != nil {
			return nil, err
		}

		specialCodeData.PortalSiteID = portalSite.ID
		specialCodeData.PortalSiteName = portalSite.Name.Ja
		specialCodeData.PortalSiteSpecialCodes = portalSiteSpecialCodes

		response = append(response, specialCodeData)
	}

	return response, nil
}
