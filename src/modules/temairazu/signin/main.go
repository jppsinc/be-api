package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"os"
	"strings"

	be "bitbucket.org/jppsinc/be-modules/pkg"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
)

type SCUser struct {
	LoginID   string `json:"LoginID"`
	LoginPass string `json:"LoginPass"`
	HotelID   string `json:"hotel_id"`
}

type NG struct {
	Response string `json:"response"`
	Error    string `json:"error"`
}

type OK struct {
	Response string `json:"response"`
}

func handler(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {

	fmt.Println("------------------------")
	org := request.Headers["origin"]
	// Table name
	// tableName := "be_sitecontroller_users"
	tableName := os.Getenv("TABLE_NAME")

	// Change JSON request data to struct : just to check validity of request data
	var inq SCUser
	var internal string
	var ok bool

	if err := json.Unmarshal([]byte(request.Body), &inq); err != nil {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorCouldNotUnMarshalItem).Error()),
		})
	}

	if len(request.QueryStringParameters) == 0 {
		internal = "no"
	} else {
		internal, ok = request.QueryStringParameters["internal"]
		if ok == false || len(strings.TrimSpace(internal)) == 0 {
			return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
				ErrorMsg: aws.String(errors.New(be.ErrorQueryStringParameterInvalid).Error()),
			})
		}
	}

	fmt.Printf("LoginID : %v : LoginPass : %v : HotelID : %v : internal : %v \n", inq.LoginID, inq.LoginPass, inq.HotelID, internal)

	// DynamoDB
	db := be.Dynamodb()

	// input for GetItem
	input := &dynamodb.GetItemInput{
		Key: map[string]*dynamodb.AttributeValue{
			"LoginID": {
				S: aws.String(inq.LoginID),
			},
			"LoginPass": {
				S: aws.String(inq.LoginPass),
			},
		},
		TableName: aws.String(tableName),
	}

	// GetItem from dynamodb table
	result, err := db.GetItem(input)
	if err != nil {
		fmt.Println("GetItem err : ", err)
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorFailedToFetchRecord).Error()),
		})
	}

	if len(result.Item) > 0 {
		if internal == "no" {
			Ok := new(OK)
			Ok.Response = "OK"
			fmt.Println("Ok.Response : ", Ok.Response)
			return be.ApiResponseCSV(http.StatusOK, Ok)
		} else {
			item := new(SCUser)
			err = dynamodbattribute.UnmarshalMap(result.Item, item)
			if err != nil {
				return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
					ErrorMsg: aws.String(errors.New(be.ErrorCouldNotUnMarshalItem).Error()),
				})
			}
			fmt.Println("SCUser : ", item)
			return be.ApiResponse(org, http.StatusOK, item)
		}
	} else {
		Ng := be.GetTMError("login ID or password is incorrect.")
		fmt.Println("login ID or password is incorrect.")
		if internal == "no" {
			return be.ApiResponseCSV(http.StatusUnauthorized, Ng)
		} else {
			return be.ApiResponse(org, http.StatusUnauthorized, Ng)
		}
	}
}

func main() {
	lambda.Start(handler)
}
