package main

import (
	"errors"
	"net/http"
	"os"

	be "bitbucket.org/jppsinc/be-modules/pkg"
	valid "github.com/asaskevich/govalidator"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
)

func GetRoundingMethods(db *dynamodb.DynamoDB, hotel_id string) (string, error) {
	tableNameGH := os.Getenv("GH_TABLE")
	// input for GetItem
	input := &dynamodb.GetItemInput{
		Key: map[string]*dynamodb.AttributeValue{
			"hotel_id": {
				S: aws.String(hotel_id),
			},
		},
		TableName: aws.String(tableNameGH),
	}

	// GetItem from dynamodb table
	result, err := db.GetItem(input)
	if err != nil {
		return "", errors.New(be.ErrorFailedToFetchRecord)
	}

	item := new(be.GroupHotel)
	if len(result.Item) > 0 {
		err = dynamodbattribute.UnmarshalMap(result.Item, item)
		if err != nil {
			return "", errors.New(be.ErrorCouldNotUnMarshalItem)
		}
	}

	cnc := "ROUNDDOWN"
	if item.CancellationFeeRoundingMethod != "" {
		cnc = item.CancellationFeeRoundingMethod
	}

	return cnc, nil
}

func handler(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {

	org := request.Headers["origin"]
	// Table name
	// tableName := "be_post_cancellation_policies"
	tableName := os.Getenv("TABLE_NAME")

	// Request path parameter
	hotel_id, ok := request.PathParameters["hotel_id"]
	if ok == false || valid.IsUUIDv4(hotel_id) == false {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorInvalidUrlHotelIdNotFound).Error()),
		})
	}

	// DynamoDB
	db := be.Dynamodb()

	// input for GetItem
	input := &dynamodb.GetItemInput{
		Key: map[string]*dynamodb.AttributeValue{
			"hotel_id": {
				S: aws.String(hotel_id),
			},
		},
		TableName: aws.String(tableName),
	}

	// GetItem from dynamodb table
	result, err := db.GetItem(input)
	if err != nil {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorFailedToFetchRecord).Error()),
		})
	}

	item := new(be.PostCancellationSetting)
	err = dynamodbattribute.UnmarshalMap(result.Item, item)
	if err != nil {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorCouldNotUnMarshalItem).Error()),
		})
	}
	// if len(result.Item) > 0 {
	cnc, err := GetRoundingMethods(db, hotel_id)
	if err != nil {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorFailedToFetchRecord).Error()),
		})
	}

	item.CancellationFeeRoundingMethod = cnc
	return be.ApiResponse(org, http.StatusOK, item)
	// } else {
	// 	item.CancellationFeeRoundingMethod = cnc
	// 	return be.ApiResponse(org, http.StatusOK, item)
	// }
}

func main() {
	lambda.Start(handler)
}
