package main

import (
	"errors"
	"fmt"
	"net/http"
	"os"

	be "bitbucket.org/jppsinc/be-modules/pkg"
	valid "github.com/asaskevich/govalidator"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/aws/aws-sdk-go/service/dynamodb/expression"
)

func handler(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {

	org := request.Headers["origin"]
	// Table name
	// tableName := "be_bookings"
	tableName := os.Getenv("TABLE_NAME")

	// Request path parameter
	hotel_id, ok := request.PathParameters["hotel_id"]
	if ok == false || valid.IsUUIDv4(hotel_id) == false {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorInvalidUrlHotelIdNotFound).Error()),
		})
	}

	id, ok := request.PathParameters["id"]
	if ok == false || valid.IsUUIDv4(id) == false {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorInvalidUrlIdNotFound).Error()),
		})
	}

	// DynamoDB
	db := be.Dynamodb()

	// input for GetItem
	input := &dynamodb.GetItemInput{
		Key: map[string]*dynamodb.AttributeValue{
			"hotel_id": {
				S: aws.String(hotel_id),
			},
			"id": {
				S: aws.String(id),
			},
		},
		// ProjectionExpression: aws.String("confirmation_email_send_history"),
		TableName: aws.String(tableName),
	}

	// GetItem from dynamodb table
	result, err := db.GetItem(input)
	if err != nil {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorFailedToFetchRecord).Error()),
		})
	}

	item := new(be.Booking)
	err = dynamodbattribute.UnmarshalMap(result.Item, item)
	if err != nil {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorCouldNotUnMarshalItem).Error()),
		})
	}
	if len(result.Item) > 0 {
		if len(item.ConfirmationEmailSendHistory) < 5 {
			dt := be.DateString("datetime")
			item.ConfirmationEmailSendHistory = append(item.ConfirmationEmailSendHistory, dt)

			update := expression.
				Set(expression.Name("confirmation_email_send_history"), expression.Value(item.ConfirmationEmailSendHistory))

			expr, _ := expression.NewBuilder().
				WithUpdate(update).
				Build()

			inputInc := &dynamodb.UpdateItemInput{
				Key: map[string]*dynamodb.AttributeValue{
					"hotel_id": {
						S: aws.String(hotel_id),
					},
					"id": {
						S: aws.String(id),
					},
				},
				TableName:                 aws.String(tableName),
				ExpressionAttributeNames:  expr.Names(),
				ExpressionAttributeValues: expr.Values(),
				UpdateExpression:          expr.Update(),
			}

			_, err = db.UpdateItem(inputInc)
			if err != nil {
				return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
					ErrorMsg: aws.String(errors.New(be.ErrorCouldNotDynamoPutItem).Error()),
				})
			}

			return be.ApiResponse(org, http.StatusOK, item)
		} else {
			e := new(be.MultiLang)
			e.En = `Can't send email more than 5 times.`
			e.Ja = `5回以上メールを送信することはできません。`

			return be.ApiResponse(org, http.StatusForbidden, e)
		}
		// return be.ApiResponse(org, http.StatusOK, item)
	} else {
		fmt.Println("Updated empty")
		item := new(be.EmptyStruct)
		return be.ApiResponse(org, http.StatusOK, item)
	}
}

func main() {
	lambda.Start(handler)
}
