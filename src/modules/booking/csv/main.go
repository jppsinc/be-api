package main

import (
	"errors"
	"fmt"
	"os"
	"strconv"

	// "io"
	"net/http"
	"strings"

	be "bitbucket.org/jppsinc/be-modules/pkg"
	valid "github.com/asaskevich/govalidator"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/aws/aws-sdk-go/service/dynamodb/expression"
	// "golang.org/x/text/encoding/japanese"
	// "golang.org/x/text/transform"
)

type LastKey struct {
	HotelID string `json:"hotel_id"`
	ID      string `json:"id"`
}

func QueryFirstWithOutFilter(expr expression.Expression, tableName string) *dynamodb.QueryInput {
	input := &dynamodb.QueryInput{
		KeyConditionExpression:    expr.KeyCondition(),
		TableName:                 aws.String(tableName),
		ExpressionAttributeNames:  expr.Names(),
		ExpressionAttributeValues: expr.Values(),
	}

	// fmt.Println("input 1: ", input)

	return input
}

func QueryNextWithOutFilter(expr expression.Expression, tableName string, es LastKey) *dynamodb.QueryInput {
	input := &dynamodb.QueryInput{
		KeyConditionExpression:    expr.KeyCondition(),
		TableName:                 aws.String(tableName),
		ExpressionAttributeNames:  expr.Names(),
		ExpressionAttributeValues: expr.Values(),
		ExclusiveStartKey: map[string]*dynamodb.AttributeValue{
			"hotel_id": {
				S: aws.String(es.HotelID),
			},
			"id": {
				S: aws.String(es.ID),
			},
		},
	}
	// fmt.Println("input 2: ", input)

	return input
}

func QueryFirstWithFilter(expr expression.Expression, tableName string) *dynamodb.QueryInput {
	input := &dynamodb.QueryInput{
		KeyConditionExpression:    expr.KeyCondition(),
		TableName:                 aws.String(tableName),
		FilterExpression:          expr.Filter(),
		ExpressionAttributeNames:  expr.Names(),
		ExpressionAttributeValues: expr.Values(),
	}
	// fmt.Println("input 3: ", input)

	return input
}

func QueryNextWithFilter(expr expression.Expression, tableName string, es LastKey) *dynamodb.QueryInput {
	input := &dynamodb.QueryInput{
		KeyConditionExpression:    expr.KeyCondition(),
		TableName:                 aws.String(tableName),
		FilterExpression:          expr.Filter(),
		ExpressionAttributeNames:  expr.Names(),
		ExpressionAttributeValues: expr.Values(),
		ExclusiveStartKey: map[string]*dynamodb.AttributeValue{
			"hotel_id": {
				S: aws.String(es.HotelID),
			},
			"id": {
				S: aws.String(es.ID),
			},
		},
	}
	// fmt.Println("input 4: ", input)

	return input
}

type BookingCSV struct {
	BookingReferenceID string `csv:"booking_reference_id"`
	Status             string `csv:"status"`
	GuestName          string `csv:"guest_name_kana"`
	Device             string `csv:"device"`
	BookingDate        string `csv:"booking_date"`
	CheckInDate        string `csv:"check_in_date"`
	CheckOutDate       string `csv:"check_out_date"`
	Nights             int64  `csv:"nights"`
	RoomCount          int64  `csv:"room_count"`
	Pax                int64  `csv:"pax"`
	Child              int64  `csv:"child"`
	PlanName           string `csv:"plan_name"`
	RoomType           string `csv:"room_type"`
	TotalCharge        int64  `csv:"total_charge"`
	PaymentMethod      string `csv:"payment_method"`
	Source             string `csv:"source"`
}

func handler(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {

	org := request.Headers["origin"]
	// Table name
	// tableName := "be_bookings"
	tableName := os.Getenv("TABLE_NAME")

	// Request path parameter
	hotel_id, ok := request.PathParameters["hotel_id"]
	if ok == false || valid.IsUUIDv4(hotel_id) == false {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorInvalidUrlHotelIdNotFound).Error()),
		})
	}

	keyCond := expression.Key("hotel_id").Equal(expression.Value(hotel_id))

	var finalFilter expression.ConditionBuilder
	var filter []expression.ConditionBuilder

	// query string parameter
	if len(request.QueryStringParameters) == 0 {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorQueryStringParameterNotFound).Error()),
		})
	} else {
		condition, ok := request.QueryStringParameters["condition"]
		if ok == true || len(strings.TrimSpace(condition)) != 0 {
			switch condition {
			case "bookingDate":
				startDate, ok := request.QueryStringParameters["startDate"]
				if ok == true || len(strings.TrimSpace(startDate)) != 0 {
					filter = append(filter, expression.Name("booking_date").GreaterThanEqual(expression.Value(startDate)))
				}

				endDate, ok := request.QueryStringParameters["endDate"]
				if ok == true || len(strings.TrimSpace(endDate)) != 0 {
					filter = append(filter, expression.Name("booking_date").LessThanEqual(expression.Value(endDate)))
				}

			case "arrivalDate":
				startDate, ok := request.QueryStringParameters["startDate"]
				if ok == true || len(strings.TrimSpace(startDate)) != 0 {
					filter = append(filter, expression.Name("check_in_date").GreaterThanEqual(expression.Value(startDate)))
				}

				endDate, ok := request.QueryStringParameters["endDate"]
				if ok == true || len(strings.TrimSpace(endDate)) != 0 {
					filter = append(filter, expression.Name("check_in_date").LessThanEqual(expression.Value(endDate)))
				}

			case "departureDate":
				startDate, ok := request.QueryStringParameters["startDate"]
				if ok == true || len(strings.TrimSpace(startDate)) != 0 {
					filter = append(filter, expression.Name("check_out_date").GreaterThanEqual(expression.Value(startDate)))
				}

				endDate, ok := request.QueryStringParameters["endDate"]
				if ok == true || len(strings.TrimSpace(endDate)) != 0 {
					filter = append(filter, expression.Name("check_out_date").LessThanEqual(expression.Value(endDate)))
				}

			case "stayDate":
				startDate, ok := request.QueryStringParameters["startDate"]
				if ok == true || len(strings.TrimSpace(startDate)) != 0 {
					filter = append(filter, expression.Name("check_in_date").GreaterThanEqual(expression.Value(startDate)))
				}

				endDate, ok := request.QueryStringParameters["endDate"]
				if ok == true || len(strings.TrimSpace(endDate)) != 0 {
					filter = append(filter, expression.Name("check_out_date").LessThanEqual(expression.Value(endDate)))
				}

			case "lastModifiedDate":
				updatedAt, ok := request.QueryStringParameters["updated_at"]
				if ok == true || len(strings.TrimSpace(updatedAt)) != 0 {
					filter = append(filter, expression.Name("updated_at").GreaterThanEqual(expression.Value(updatedAt)))
				}
			}
		}
	}

	filter = append(filter, expression.Name("status").NotEqual(expression.Value("Initiate")))

	guestName, ok := request.QueryStringParameters["guestName"]
	if ok == true || len(strings.TrimSpace(guestName)) != 0 {
		filter = append(filter, expression.Name("guest_name").Contains(guestName).Or(expression.Name("guest_name_kana").Contains(guestName)))
	}

	guestEmail, ok := request.QueryStringParameters["guestEmail"]
	if ok == true || len(strings.TrimSpace(guestEmail)) != 0 {
		filter = append(filter, expression.Name("guest_email").Contains(guestEmail))
	}

	bookerName, ok := request.QueryStringParameters["bookerName"]
	if ok == true || len(strings.TrimSpace(bookerName)) != 0 {
		filter = append(filter, expression.Name("booker_name").Contains(bookerName).Or(expression.Name("booker_name_kana").Contains(bookerName)))
	}

	bookerEmail, ok := request.QueryStringParameters["bookerEmail"]
	if ok == true || len(strings.TrimSpace(bookerEmail)) != 0 {
		filter = append(filter, expression.Name("booker_email").Equal(expression.Value(bookerEmail)))
	}

	guestContact, ok := request.QueryStringParameters["guestContact"]
	if ok == true || len(strings.TrimSpace(guestContact)) != 0 {
		filter = append(filter, expression.Name("guest_contact").Contains(guestContact))
	}

	bookerContact, ok := request.QueryStringParameters["bookerContact"]
	if ok == true || len(strings.TrimSpace(bookerContact)) != 0 {
		filter = append(filter, expression.Name("booker_contact").Contains(bookerContact))
	}

	planId, ok := request.QueryStringParameters["planId"]
	if ok == true || len(strings.TrimSpace(planId)) != 0 {
		if planId != "All" {
			filter = append(filter, expression.Name("plan_id").Equal(expression.Value(planId)))
		}
	}

	planType, ok := request.QueryStringParameters["planType"]
	if ok == true || len(strings.TrimSpace(planType)) != 0 {
		if planType != "All" {
			filter = append(filter, expression.Name("plan_type").Equal(expression.Value(planType)))
		}
	}

	roomTypeId, ok := request.QueryStringParameters["roomTypeId"]
	if ok == true || len(strings.TrimSpace(roomTypeId)) != 0 {
		if roomTypeId != "All" {
			filter = append(filter, expression.Name("room_id").Equal(expression.Value(roomTypeId)))
		}
	}

	paymentMethod, ok := request.QueryStringParameters["paymentMethod"]
	if ok == true || len(strings.TrimSpace(paymentMethod)) != 0 {
		if paymentMethod != "All" {
			filter = append(filter, expression.Name("payment_method").Equal(expression.Value(paymentMethod)))
		}
	}

	device, ok := request.QueryStringParameters["device"]
	if ok == true || len(strings.TrimSpace(device)) != 0 {
		if device != "All" {
			filter = append(filter, expression.Name("device_used").Equal(expression.Value(device)))
		}
	}

	status, ok := request.QueryStringParameters["status"]
	if ok == true || len(strings.TrimSpace(status)) != 0 {
		if status != "All" {
			filter = append(filter, expression.Name("status").Equal(expression.Value(status)))
		}
	}

	source, ok := request.QueryStringParameters["source"]
	if ok == true || len(strings.TrimSpace(source)) != 0 {
		if source != "All" {
			filter = append(filter, expression.Name("source").Equal(expression.Value(source)))
		}
	}

	referenceId, ok := request.QueryStringParameters["referenceId"]
	if ok == true || len(strings.TrimSpace(referenceId)) != 0 {
		filter = append(filter, expression.Name("booking_reference_id").Equal(expression.Value(referenceId)))
	}

	email, ok := request.QueryStringParameters["email"]
	if ok == true || len(strings.TrimSpace(email)) != 0 {
		filter = append(filter, expression.Name("guest_email").Contains(email).Or(expression.Name("booker_email").Contains(email)))
	}

	for i, temp := range filter {
		if i == 0 {
			finalFilter = temp
		} else {
			finalFilter = finalFilter.And(temp)
		}
	}

	// DynamoDB
	db := be.Dynamodb()
	// var input *dynamodb.QueryInput

	final := new([]*be.Booking)

	var lastEvaluatedKey LastKey
	var result *dynamodb.QueryOutput

	if len(filter) == 0 {
		expr, err := expression.NewBuilder().WithKeyCondition(keyCond).Build()
		if err != nil {
			return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
				ErrorMsg: aws.String(err.Error()),
			})
		}

		for {
			pp := new([]*be.Booking)
			if lastEvaluatedKey == (LastKey{}) {
				result, err = db.Query(QueryFirstWithOutFilter(expr, tableName))
			} else {
				result, err = db.Query(QueryNextWithOutFilter(expr, tableName, lastEvaluatedKey))
			}

			if err != nil {
				fmt.Println(err)
			}

			err = dynamodbattribute.UnmarshalListOfMaps(result.Items, &pp)
			if err != nil {
				return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
					ErrorMsg: aws.String(errors.New(be.ErrorCouldNotUnMarshalItem).Error()),
				})
			}

			for _, i := range *pp {
				*final = append(*final, i)
			}

			if result.LastEvaluatedKey == nil {
				break
			}

			err = dynamodbattribute.UnmarshalMap(result.LastEvaluatedKey, &lastEvaluatedKey)
			if err != nil {
				return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
					ErrorMsg: aws.String(errors.New(be.ErrorCouldNotUnMarshalItem).Error()),
				})
			}
		}
	} else {
		expr, err := expression.NewBuilder().WithKeyCondition(keyCond).WithFilter(finalFilter).Build()
		if err != nil {
			return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
				ErrorMsg: aws.String(err.Error()),
			})
		}

		for {
			pp := new([]*be.Booking)
			if lastEvaluatedKey == (LastKey{}) {
				result, err = db.Query(QueryFirstWithFilter(expr, tableName))
			} else {
				result, err = db.Query(QueryNextWithFilter(expr, tableName, lastEvaluatedKey))
			}

			if err != nil {
				fmt.Println(err)
			}

			err = dynamodbattribute.UnmarshalListOfMaps(result.Items, &pp)
			if err != nil {
				fmt.Println("err :", err)
				return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
					ErrorMsg: aws.String(errors.New(be.ErrorCouldNotUnMarshalItem).Error()),
				})
			}

			for _, i := range *pp {
				*final = append(*final, i)
			}

			if result.LastEvaluatedKey == nil {
				break
			}

			err = dynamodbattribute.UnmarshalMap(result.LastEvaluatedKey, &lastEvaluatedKey)
			if err != nil {
				return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
					ErrorMsg: aws.String(errors.New(be.ErrorCouldNotUnMarshalItem).Error()),
				})
			}
		}
	}

	if len(*final) > 0 {

		gha := []*BookingCSV{}
		for _, ent := range *final {

			var tmp BookingCSV

			tmp.BookingReferenceID = ent.BookingReferenceID
			tmp.Status = ent.Status

			if ent.PageLanguage == "en" {
				tmp.GuestName = ent.GuestName
			} else {
				tmp.GuestName = ent.GuestNameKana
			}

			tmp.Device = ent.DeviceUsed
			tmp.BookingDate = ent.BookingDate
			tmp.CheckInDate = ent.CheckInDate
			tmp.CheckOutDate = ent.CheckOutDate
			tmp.Nights = ent.Nights
			tmp.RoomCount = ent.RoomCount

			for j := 0; j < len(ent.Adult); j++ {
				tmp.Pax = tmp.Pax + ent.Adult[j]
			}

			for j := 0; j < len(ent.Child); j++ {
				tmp.Child = tmp.Child + ent.Child[j]
			}

			tmp.PlanName = ent.PlanName.Ja
			tmp.RoomType = ent.RoomType.Ja
			tmp.TotalCharge = ent.TotalCharge
			tmp.PaymentMethod = ent.PaymentMethod
			tmp.Source = ent.Source

			gha = append(gha, &tmp)
		}

		csvContent := GetCSVData(gha)
		fmt.Println("csvContent : ", csvContent)

		a := be.DateString("datetime")

		a = strings.ReplaceAll(a, "-", "_")
		a = strings.ReplaceAll(a, ":", "_")
		a = strings.ReplaceAll(a, " ", "_")
		fileName := "bookings_" + a

		return ApiResponse(org, http.StatusOK, gha, csvContent, fileName)

	} else {
		item := new(be.EmptyStruct)
		return be.ApiResponse(org, http.StatusOK, item)
	}
}

func ApiResponse(origin string, status int, body interface{}, csvContent string, fileName string) (events.APIGatewayProxyResponse, error) {
	origins := map[string]bool{"https://dev.d7r69723er9j4.amplifyapp.com": true,
		"https://master.d7r69723er9j4.amplifyapp.com": true,
		"https://localhost:3000":                      true,
		"http://localhost:3000":                       true,
		"https://rc-booking.com":                      true,
		"https://yoyaku.rc-booking.com":               true,
		"https://kanri.rc-booking.com":                true,
		"https://test-yoyaku.rc-booking.com":          true,
		"https://dev-yoyaku.rc-booking.com":           true,
		"https://test-kanri.rc-booking.com":           true,
		"https://dev-kanri.rc-booking.com":            true,
		"https://preview-test-kanri.rc-booking.com":   true,
		"https://preview-test-yoyaku.rc-booking.com":  true,
	}

	resp := events.APIGatewayProxyResponse{}
	resp.StatusCode = status
	fmt.Println("origin :", origin)
	if origins[origin] == true {
		resp.Headers = map[string]string{
			"Access-Control-Allow-Origin":      origin,
			"Access-Control-Allow-Headers":     "origin,Accept,Authorization,Content-Type,X-Amz-Date,X-Api-Key,X-Amz-Security-Token",
			"Access-Control-Allow-Methods":     "DELETE,GET,OPTIONS,POST,PUT",
			"Content-Type":                     "text/csv; charset=Shift-JIS",
			"Access-Control-Allow-Credentials": "true",
			"Content-Disposition":              "attachment; filename=" + fileName,
		}
	} else {
		resp.Headers = map[string]string{
			"Access-Control-Allow-Origin":  "*",
			"Access-Control-Allow-Headers": "origin,Accept,Authorization,Content-Type,X-Amz-Date,X-Api-Key,X-Amz-Security-Token",
			"Access-Control-Allow-Methods": "DELETE,GET,OPTIONS,POST,PUT",
			"Content-Type":                 "text/csv; charset=Shift-JIS",
			"Content-Disposition":          "attachment; filename=" + fileName,
		}
	}

	fmt.Println("fileName :", fileName)
	fmt.Println("resp.Headers :", resp.Headers)

	resp.Body = csvContent
	return resp, nil
}

func main() {
	lambda.Start(handler)
}

func GetCSVData(gha []*BookingCSV) string {

	temp := "\xEF\xBB\xBF"
	temp += "予約番号,ステータス,ゲスト名,デバイス,到着日,出発日,泊数,室数,人数(小人),プラン名,客室タイプ,総額,支払,予約日,経由\r\n"

	for _, b := range gha {
		temp += b.BookingReferenceID + ","

		if b.Status == "Initiate" {
			temp += "仮予約" + ","
		} else if b.Status == "Confirm" {
			temp += "確定" + ","
		} else if b.Status == "Cancel" {
			temp += "キャンセル" + ","
		}

		temp += b.GuestName + ","
		temp += b.Device + ","
		temp += b.CheckInDate + ","
		temp += b.CheckOutDate + ","
		temp += strconv.Itoa(int(b.Nights)) + ","
		temp += strconv.Itoa(int(b.RoomCount)) + ","
		temp += strconv.Itoa(int(b.Pax)) + "(" + strconv.Itoa(int(b.Child)) + "),"
		temp += b.PlanName + ","
		temp += b.RoomType + ","
		temp += strconv.Itoa(int(b.TotalCharge)) + ","

		if b.PaymentMethod == "AtHotel" {
			temp += "現地払い" + ","
		} else {
			temp += "クレジットカード" + ","
		}

		temp += b.BookingDate + ","
		temp += b.Source + "\r\n"
	}

	return temp
}
