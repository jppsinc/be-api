package main

import (
	"errors"
	"fmt"
	"net/http"
	"os"
	"sort"
	"strings"
	"time"

	be "bitbucket.org/jppsinc/be-modules/pkg"
	valid "github.com/asaskevich/govalidator"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/aws/aws-sdk-go/service/dynamodb/expression"
)

type LastKey struct {
	HotelID string `json:"hotel_id"`
	ID      string `json:"id"`
}

type BookingSalesCount struct {
	ID     string `json:"id"`
	Date   string `json:"date"`
	Key    string `json:"key"`
	RoomId string `json:"room_id"`
	PlanId string `json:"plan_id"`
	Count  int    `json:"count"`
}

type BookingSalesCountResp struct {
	Date   string `json:"date"`
	RoomId string `json:"room_id"`
	PlanId string `json:"plan_id"`
	Count  int    `json:"count"`
}

type BookingData struct {
	HotelID      string `json:"hotel_id"`
	ID           string `json:"id"`
	CheckInDate  string `json:"check_in_date"`
	CheckOutDate string `json:"check_out_date"`
	RoomID       string `json:"room_id"`
	PlanID       string `json:"plan_id"`
	RoomCount    int    `json:"room_count"`
}

func QueryFirstWithFilter(expr expression.Expression, tableName string) *dynamodb.QueryInput {
	input := &dynamodb.QueryInput{
		KeyConditionExpression:    expr.KeyCondition(),
		TableName:                 aws.String(tableName),
		FilterExpression:          expr.Filter(),
		ExpressionAttributeNames:  expr.Names(),
		ExpressionAttributeValues: expr.Values(),
	}
	fmt.Println("input 3: ", input)

	return input
}

func QueryNextWithFilter(expr expression.Expression, tableName string, es LastKey) *dynamodb.QueryInput {
	input := &dynamodb.QueryInput{
		KeyConditionExpression:    expr.KeyCondition(),
		TableName:                 aws.String(tableName),
		FilterExpression:          expr.Filter(),
		ExpressionAttributeNames:  expr.Names(),
		ExpressionAttributeValues: expr.Values(),
		ExclusiveStartKey: map[string]*dynamodb.AttributeValue{
			"hotel_id": {
				S: aws.String(es.HotelID),
			},
			"id": {
				S: aws.String(es.ID),
			},
		},
	}
	fmt.Println("input 4: ", input)

	return input
}

func handler(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {

	org := request.Headers["origin"]
	// Table name
	// tableName := "be_bookings-dev"
	tableName := os.Getenv("TABLE_NAME")

	// Request path parameter
	hotel_id, ok := request.PathParameters["hotel_id"]
	if ok == false || valid.IsUUIDv4(hotel_id) == false {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorInvalidUrlHotelIdNotFound).Error()),
		})
	}

	keyCond := expression.Key("hotel_id").Equal(expression.Value(hotel_id))

	var finalFilter expression.ConditionBuilder
	var filter []expression.ConditionBuilder
	var startDate string
	var endDate string

	// query string parameter
	if len(request.QueryStringParameters) == 0 {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorQueryStringParameterNotFound).Error()),
		})
	} else {
		startDate, ok = request.QueryStringParameters["startDate"]
		if ok == true || len(strings.TrimSpace(startDate)) != 0 {
			filter = append(filter, expression.Name("check_in_date").GreaterThanEqual(expression.Value(startDate)))
		}

		endDate, ok = request.QueryStringParameters["endDate"]
		if ok == true || len(strings.TrimSpace(endDate)) != 0 {
			filter = append(filter, expression.Name("check_in_date").LessThanEqual(expression.Value(endDate)))
		}
	}

	for i, temp := range filter {
		if i == 0 {
			finalFilter = temp
		} else {
			finalFilter = finalFilter.And(temp)
		}
	}

	// DynamoDB
	db := be.Dynamodb()
	// var input *dynamodb.QueryInput

	final := new([]*BookingData)

	var lastEvaluatedKey LastKey
	var result *dynamodb.QueryOutput

	expr, err := expression.NewBuilder().WithKeyCondition(keyCond).WithFilter(finalFilter).Build()
	if err != nil {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(err.Error()),
		})
	}

	for {
		pp := new([]*BookingData)
		if lastEvaluatedKey == (LastKey{}) {
			result, err = db.Query(QueryFirstWithFilter(expr, tableName))
		} else {
			result, err = db.Query(QueryNextWithFilter(expr, tableName, lastEvaluatedKey))
		}

		if err != nil {
			fmt.Println(err)
		}

		err = dynamodbattribute.UnmarshalListOfMaps(result.Items, &pp)
		if err != nil {
			fmt.Println("err :", err)
			return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
				ErrorMsg: aws.String(errors.New(be.ErrorCouldNotUnMarshalItem).Error()),
			})
		}

		for _, i := range *pp {
			*final = append(*final, i)
		}

		if result.LastEvaluatedKey == nil {
			break
		}

		err = dynamodbattribute.UnmarshalMap(result.LastEvaluatedKey, &lastEvaluatedKey)
		if err != nil {
			return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
				ErrorMsg: aws.String(errors.New(be.ErrorCouldNotUnMarshalItem).Error()),
			})
		}
	}

	fmt.Println("len(*final) : ", len(*final))

	if len(*final) == 0 {
		return be.ApiResponse(org, http.StatusOK, []be.EmptyStruct{})
	}

	resp := make(map[string]BookingSalesCount, 0)

	if len(*final) > 0 {

		for i := 0; i < len(*final); i++ {

			layout := "2006-01-02"
			sDate, _ := time.Parse(layout, (*final)[i].CheckInDate)
			eDate, _ := time.Parse(layout, (*final)[i].CheckOutDate)
			eDate_1 := eDate.AddDate(0, 0, 1)

			for {
				if sDate.Before(eDate_1) {
					t := BookingSalesCount{}

					date := sDate.Format(layout)
					key := (*final)[i].PlanID + "#" + (*final)[i].RoomID + "#" + date

					count := resp[key].Count + int((*final)[i].RoomCount)

					t.Key = key
					t.Date = date
					t.PlanId = (*final)[i].PlanID
					t.RoomId = (*final)[i].RoomID
					t.Count = count

					resp[key] = t

					sDate = sDate.AddDate(0, 0, 1)
				} else {
					break
				}
			}
		}
	}

	results := make([]BookingSalesCountResp, 0)

	if len(resp) == 0 {
		return be.ApiResponse(org, http.StatusOK, []be.EmptyStruct{})
	}

	for _, v := range resp {
		tmp := BookingSalesCountResp{}
		tmp.Count = v.Count
		tmp.Date = v.Date
		tmp.PlanId = v.PlanId
		tmp.RoomId = v.RoomId

		results = append(results, tmp)
	}

	// sort by Order
	sort.SliceStable(results, func(i, j int) bool {
		return *&(results)[i].Date < *&(results)[j].Date
	})

	return be.ApiResponse(org, http.StatusOK, results)

}

func main() {
	lambda.Start(handler)
}
