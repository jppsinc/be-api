package main

import (
	"errors"
	"net/http"
	"os"

	be "bitbucket.org/jppsinc/be-modules/pkg"
	valid "github.com/asaskevich/govalidator"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
)

func handler(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {

	org := request.Headers["origin"]
	// Table name
	// tableName := "be_bookings"
	tableName := os.Getenv("TABLE_NAME")

	// Request path parameter
	hotel_id, ok := request.PathParameters["hotel_id"]
	if ok == false || valid.IsUUIDv4(hotel_id) == false {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorInvalidUrlHotelIdNotFound).Error()),
		})
	}

	id, ok := request.PathParameters["id"]
	if ok == false || valid.IsUUIDv4(id) == false {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorInvalidUrlIdNotFound).Error()),
		})
	}

	// DynamoDB
	db := be.Dynamodb()

	// input for GetItem
	input := &dynamodb.GetItemInput{
		Key: map[string]*dynamodb.AttributeValue{
			"hotel_id": {
				S: aws.String(hotel_id),
			},
			"id": {
				S: aws.String(id),
			},
		},
		TableName: aws.String(tableName),
	}

	// GetItem from dynamodb table
	result, err := db.GetItem(input)
	if err != nil {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorFailedToFetchRecord).Error()),
		})
	}

	item := new(be.Booking)
	err = dynamodbattribute.UnmarshalMap(result.Item, item)
	if err != nil {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorCouldNotUnMarshalItem).Error()),
		})
	}
	if len(result.Item) > 0 {
		// item.CancellationPolicy.NoshowPanelty = item.CancellationPolicy.NoshowPenalty
		item.CancellationPolicy.NoshowPenalty = item.CancellationPolicy.NoshowPanelty
		if len(item.Adult) == 0 {
			item.Adult = make([]int64, 0)
		}
		if len(item.Child) == 0 {
			item.Child = make([]int64, 0)
		}
		if len(item.ChildAge) == 0 {
			item.ChildAge = make([]int64, 0)
		}
		if len(item.ChildInfo) == 0 {
			item.ChildInfo = make([]be.ChildInfo, 0)
		}
		if len(item.QuestionAnswers) == 0 {
			item.QuestionAnswers = make([]be.QuestionAnswer, 0)
		}
		if len(item.Meal) == 0 {
			item.Meal = make([]be.Meal, 0)
		}
		if len(item.ConfirmationEmailSendHistory) == 0 {
			item.ConfirmationEmailSendHistory = make([]string, 0)
		}
		if len(item.PlanGroupDiscountList) == 0 {
			item.PlanGroupDiscountList = make([]be.PlanGroupDiscount, 0)
		}
		if len(item.OptionList) == 0 {
			item.OptionList = make([]be.OptionList, 0)
		}
		// if item.PageLanguage == "en" {
		// 	item.GuestNameKana = item.GuestName
		// }

		return be.ApiResponse(org, http.StatusOK, item)
	} else {
		item := be.EmptyStruct{}
		return be.ApiResponse(org, http.StatusOK, item)
	}
}

func main() {
	lambda.Start(handler)
}
