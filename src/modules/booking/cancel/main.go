package main

import (
	"encoding/json"
	"errors"
	"net/http"
	"os"
	"strconv"

	be "bitbucket.org/jppsinc/be-modules/pkg"

	valid "github.com/asaskevich/govalidator"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
)

type BookingCancel struct {
	ID                        string  `json:"id"`
	HotelID                   string  `json:"hotel_id"`
	BookingReferenceID        string  `json:"booking_reference_id"`
	Status                    string  `json:"status"`
	IsNoshow                  bool    `json:"is_noshow"`
	CancellationCharge        float64 `json:"cancellation_charge"`
	CancelledAt               int64   `json:"cancelled_at"`
	CancelledBy               string  `json:"cancelled_by"`
	CancellationChargeMethod  string  `json:"cancellation_charge_method"`
	CancellationContactMethod string  `json:"cancellation_contact_method"`
	AvoidCancellationEmail    bool    `json:"avoid_cancellation_email"`
}

func handler(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {

	org := request.Headers["origin"]
	// Table name
	// tableName := "be_bookings"
	tableName := os.Getenv("TABLE_NAME")

	// Change JSON request data to struct : just to check validity of request data
	var sds BookingCancel

	if err := json.Unmarshal([]byte(request.Body), &sds); err != nil {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorCouldNotUnMarshalItem).Error()),
		})
	}

	// Request path parameter
	booking_id, ok := request.PathParameters["id"]
	if ok == false || valid.IsUUIDv4(booking_id) == false {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New("Invalid booking id").Error()),
		})
	}

	// Request path parameter
	hotel_id, ok := request.PathParameters["hotel_id"]
	if ok == false || valid.IsUUIDv4(hotel_id) == false {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorInvalidUrlHotelIdNotFound).Error()),
		})
	} else {
		sds.HotelID = hotel_id
		sds.ID = booking_id
		sds.CancelledAt = be.DateTimeStamp()
		sds.Status = "Cancel"
		sds.CancelledBy = "Hotel"
	}

	// DynamoDB
	db := be.Dynamodb()

	// check if row with this key exist
	// input for GetItem
	input := &dynamodb.GetItemInput{
		Key: map[string]*dynamodb.AttributeValue{
			"hotel_id": {
				S: aws.String(hotel_id),
			},
			"id": {
				S: aws.String(sds.ID),
			},
		},
		TableName: aws.String(tableName),
	}

	// GetItem from dynamodb table
	result, err := db.GetItem(input)
	if err != nil {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorFailedToFetchRecord).Error()),
		})
	}

	if len(result.Item) > 0 {
		item := new(BookingCancel)
		err = dynamodbattribute.UnmarshalMap(result.Item, item)
		if err != nil {
			return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
				ErrorMsg: aws.String(errors.New(be.ErrorCouldNotUnMarshalItem).Error()),
			})
		}

		input := &dynamodb.UpdateItemInput{
			ExpressionAttributeNames: map[string]*string{
				"#K1": aws.String("cancellation_charge_method"),
				"#K2": aws.String("cancellation_contact_method"),
				"#K3": aws.String("cancelled_at"),
				"#K4": aws.String("status"),
				"#K5": aws.String("avoid_cancellation_email"),
				"#K6": aws.String("cancellation_charge"),
				"#K7": aws.String("is_noshow"),
				"#K8": aws.String("cancelled_by"),
			},
			ExpressionAttributeValues: map[string]*dynamodb.AttributeValue{
				":v1": {
					// N: aws.String(strconv.FormatFloat(sds.CancellationCharge, 'f', 2, 64)),
					S: aws.String(sds.CancellationChargeMethod),
				},
				":v2": {
					S: aws.String(sds.CancellationContactMethod),
				},
				":v3": {
					N: aws.String(strconv.FormatInt(sds.CancelledAt, 10)),
				},
				":v4": {
					S: aws.String(sds.Status),
				},
				":v5": {
					BOOL: aws.Bool(sds.AvoidCancellationEmail),
				},
				":v6": {
					N: aws.String(strconv.FormatFloat(sds.CancellationCharge, 'f', 2, 64)),
				},
				":v7": {
					BOOL: aws.Bool(sds.IsNoshow),
				},
				":v8": {
					S: aws.String(sds.CancelledBy),
				},
			},
			Key: map[string]*dynamodb.AttributeValue{
				"hotel_id": {
					S: aws.String(sds.HotelID),
				},
				"id": {
					S: aws.String(sds.ID),
				},
			},
			ReturnValues:     aws.String("ALL_NEW"),
			TableName:        aws.String(tableName),
			UpdateExpression: aws.String("SET #K1 = :v1, #K2 = :v2, #K3 = :v3, #K4 = :v4, #K5 = :v5, #K6 = :v6, #K7 = :v7, #K8 = :v8"),
		}

		_, err := db.UpdateItem(input)
		if err != nil {
			return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
				ErrorMsg: aws.String(errors.New(be.ErrorCouldNotUpdateItem).Error()),
			})
		}
		return be.ApiResponse(org, http.StatusOK, &sds)
	} else {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorRecordNotFound).Error()),
		})
	}
}

func main() {
	lambda.Start(handler)
}
