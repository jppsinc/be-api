package main

import (
	"errors"
	"fmt"
	"net/http"
	"os"
	"strings"

	be "bitbucket.org/jppsinc/be-modules/pkg"
	valid "github.com/asaskevich/govalidator"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/aws/aws-sdk-go/service/dynamodb/expression"
)

type LastKey struct {
	HotelID string `json:"hotel_id"`
	ID      string `json:"id"`
}

func QueryFirstWithOutFilter(expr expression.Expression, tableName string) *dynamodb.QueryInput {
	input := &dynamodb.QueryInput{
		KeyConditionExpression:    expr.KeyCondition(),
		TableName:                 aws.String(tableName),
		ExpressionAttributeNames:  expr.Names(),
		ExpressionAttributeValues: expr.Values(),
	}

	fmt.Println("input 1: ", input)

	return input
}

func QueryNextWithOutFilter(expr expression.Expression, tableName string, es LastKey) *dynamodb.QueryInput {
	input := &dynamodb.QueryInput{
		KeyConditionExpression:    expr.KeyCondition(),
		TableName:                 aws.String(tableName),
		ExpressionAttributeNames:  expr.Names(),
		ExpressionAttributeValues: expr.Values(),
		ExclusiveStartKey: map[string]*dynamodb.AttributeValue{
			"hotel_id": {
				S: aws.String(es.HotelID),
			},
			"id": {
				S: aws.String(es.ID),
			},
		},
	}
	fmt.Println("input 2: ", input)

	return input
}

func QueryFirstWithFilter(expr expression.Expression, tableName string) *dynamodb.QueryInput {
	input := &dynamodb.QueryInput{
		KeyConditionExpression:    expr.KeyCondition(),
		TableName:                 aws.String(tableName),
		FilterExpression:          expr.Filter(),
		ExpressionAttributeNames:  expr.Names(),
		ExpressionAttributeValues: expr.Values(),
	}
	fmt.Println("input 3: ", input)

	return input
}

func QueryNextWithFilter(expr expression.Expression, tableName string, es LastKey) *dynamodb.QueryInput {
	input := &dynamodb.QueryInput{
		KeyConditionExpression:    expr.KeyCondition(),
		TableName:                 aws.String(tableName),
		FilterExpression:          expr.Filter(),
		ExpressionAttributeNames:  expr.Names(),
		ExpressionAttributeValues: expr.Values(),
		ExclusiveStartKey: map[string]*dynamodb.AttributeValue{
			"hotel_id": {
				S: aws.String(es.HotelID),
			},
			"id": {
				S: aws.String(es.ID),
			},
		},
	}
	fmt.Println("input 4: ", input)

	return input
}

type BookingIDs struct {
	HotelID      string `json:"hotel_id"`
	TLID         string `json:"tl_id"`
	BEID         string `json:"booking_reference_id"`
	Confirmed    bool   `json:"confirmed"`
	Modified     bool   `json:"modified"`
	Cancelled    bool   `json:"cancelled"`
	CreatedAt    string `json:"created_at"`
	ErrorMessage string `json:"error_message"`
	UpdatedAt    string `json:"updated_at"`
	CancelledAt  string `json:"cancelled_at"`
}

func handler(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {

	org := request.Headers["origin"]
	// Table name
	// tableName := "be_bookings-stg"
	tableName := os.Getenv("TABLE_NAME")

	// Request path parameter
	hotel_id, ok := request.PathParameters["hotel_id"]
	if ok == false || valid.IsUUIDv4(hotel_id) == false {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorInvalidUrlHotelIdNotFound).Error()),
		})
	}

	keyCond := expression.Key("hotel_id").Equal(expression.Value(hotel_id))

	var finalFilter expression.ConditionBuilder
	var filter []expression.ConditionBuilder

	// query string parameter
	if len(request.QueryStringParameters) == 0 {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorQueryStringParameterNotFound).Error()),
		})
	} else {
		condition, ok := request.QueryStringParameters["condition"]
		if ok == true || len(strings.TrimSpace(condition)) != 0 {
			switch condition {
			case "bookingDate":
				startDate, ok := request.QueryStringParameters["startDate"]
				if ok == true || len(strings.TrimSpace(startDate)) != 0 {
					filter = append(filter, expression.Name("booking_date").GreaterThanEqual(expression.Value(startDate)))
				}

				endDate, ok := request.QueryStringParameters["endDate"]
				if ok == true || len(strings.TrimSpace(endDate)) != 0 {
					filter = append(filter, expression.Name("booking_date").LessThanEqual(expression.Value(endDate)))
				}

			case "arrivalDate":
				startDate, ok := request.QueryStringParameters["startDate"]
				if ok == true || len(strings.TrimSpace(startDate)) != 0 {
					filter = append(filter, expression.Name("check_in_date").GreaterThanEqual(expression.Value(startDate)))
				}

				endDate, ok := request.QueryStringParameters["endDate"]
				if ok == true || len(strings.TrimSpace(endDate)) != 0 {
					filter = append(filter, expression.Name("check_in_date").LessThanEqual(expression.Value(endDate)))
				}

			case "departureDate":
				startDate, ok := request.QueryStringParameters["startDate"]
				if ok == true || len(strings.TrimSpace(startDate)) != 0 {
					filter = append(filter, expression.Name("check_out_date").GreaterThanEqual(expression.Value(startDate)))
				}

				endDate, ok := request.QueryStringParameters["endDate"]
				if ok == true || len(strings.TrimSpace(endDate)) != 0 {
					filter = append(filter, expression.Name("check_out_date").LessThanEqual(expression.Value(endDate)))
				}

			case "stayDate":
				startDate, ok := request.QueryStringParameters["startDate"]
				if ok == true || len(strings.TrimSpace(startDate)) != 0 {
					filter = append(filter, expression.Name("check_in_date").GreaterThanEqual(expression.Value(startDate)))
				}

				endDate, ok := request.QueryStringParameters["endDate"]
				if ok == true || len(strings.TrimSpace(endDate)) != 0 {
					filter = append(filter, expression.Name("check_out_date").LessThanEqual(expression.Value(endDate)))
				}

			case "lastModifiedDate":
				updatedAt, ok := request.QueryStringParameters["updated_at"]
				if ok == true || len(strings.TrimSpace(updatedAt)) != 0 {
					filter = append(filter, expression.Name("updated_at").GreaterThanEqual(expression.Value(updatedAt)))
				}
			}
		}
	}

	guestName, ok := request.QueryStringParameters["guestName"]
	if ok == true || len(strings.TrimSpace(guestName)) != 0 {
		filter = append(filter, expression.Name("guest_name").Contains(guestName).Or(expression.Name("guest_name_kana").Contains(guestName)))
	}

	guestEmail, ok := request.QueryStringParameters["guestEmail"]
	if ok == true || len(strings.TrimSpace(guestEmail)) != 0 {
		filter = append(filter, expression.Name("guest_email").Contains(guestEmail))
	}

	bookerName, ok := request.QueryStringParameters["bookerName"]
	if ok == true || len(strings.TrimSpace(bookerName)) != 0 {
		filter = append(filter, expression.Name("booker_name").Contains(bookerName).Or(expression.Name("booker_name_kana").Contains(bookerName)))
	}

	bookerEmail, ok := request.QueryStringParameters["bookerEmail"]
	if ok == true || len(strings.TrimSpace(bookerEmail)) != 0 {
		filter = append(filter, expression.Name("booker_email").Equal(expression.Value(bookerEmail)))
	}

	guestContact, ok := request.QueryStringParameters["guestContact"]
	if ok == true || len(strings.TrimSpace(guestContact)) != 0 {
		filter = append(filter, expression.Name("guest_contact").Contains(guestContact))
	}

	bookerContact, ok := request.QueryStringParameters["bookerContact"]
	if ok == true || len(strings.TrimSpace(bookerContact)) != 0 {
		filter = append(filter, expression.Name("booker_contact").Contains(bookerContact))
	}

	planId, ok := request.QueryStringParameters["planId"]
	if ok == true || len(strings.TrimSpace(planId)) != 0 {
		if planId != "All" {
			filter = append(filter, expression.Name("plan_id").Equal(expression.Value(planId)))
		}
	}

	planType, ok := request.QueryStringParameters["planType"]
	if ok == true || len(strings.TrimSpace(planType)) != 0 {
		if planType != "All" {
			filter = append(filter, expression.Name("plan_type").Equal(expression.Value(planType)))
		}
	}

	roomTypeId, ok := request.QueryStringParameters["roomTypeId"]
	if ok == true || len(strings.TrimSpace(roomTypeId)) != 0 {
		if roomTypeId != "All" {
			filter = append(filter, expression.Name("room_id").Equal(expression.Value(roomTypeId)))
		}
	}

	paymentMethod, ok := request.QueryStringParameters["paymentMethod"]
	if ok == true || len(strings.TrimSpace(paymentMethod)) != 0 {
		if paymentMethod != "All" {
			filter = append(filter, expression.Name("payment_method").Equal(expression.Value(paymentMethod)))
		}
	}

	device, ok := request.QueryStringParameters["device"]
	if ok == true || len(strings.TrimSpace(device)) != 0 {
		if device != "All" {
			filter = append(filter, expression.Name("device_used").Equal(expression.Value(device)))
		}
	}

	status, ok := request.QueryStringParameters["status"]
	if ok == true || len(strings.TrimSpace(status)) != 0 {
		if status != "All" {
			filter = append(filter, expression.Name("status").Equal(expression.Value(status)))
		}
	}

	filter = append(filter, expression.Name("status").NotEqual(expression.Value("Initiate")))

	source, ok := request.QueryStringParameters["source"]
	if ok == true && len(strings.TrimSpace(source)) != 0 {
		if source != "All" {
			if source == "Free" {
				filter = append(filter, expression.Name("source").Equal(expression.Value("GoogleHotelAds")).And(expression.Name("is_paid_ad").Equal(expression.Value(false))))
			} else if source == "Paid" {
				filter = append(filter, expression.Name("source").Equal(expression.Value("GoogleHotelAds")).And(expression.Name("is_paid_ad").Equal(expression.Value(true))))
			} else {
				filter = append(filter, expression.Name("source").Equal(expression.Value(source)))
			}
		}
	}

	referenceId, ok := request.QueryStringParameters["referenceId"]
	if ok == true || len(strings.TrimSpace(referenceId)) != 0 {
		filter = append(filter, expression.Name("booking_reference_id").Equal(expression.Value(referenceId)))
	}

	email, ok := request.QueryStringParameters["email"]
	if ok == true || len(strings.TrimSpace(email)) != 0 {
		filter = append(filter, expression.Name("guest_email").Contains(email).Or(expression.Name("booker_email").Contains(email)))
	}

	for i, temp := range filter {
		if i == 0 {
			finalFilter = temp
		} else {
			finalFilter = finalFilter.And(temp)
		}
	}

	// DynamoDB
	db := be.Dynamodb()
	// var input *dynamodb.QueryInput

	final := new([]*be.BookingIndex)

	var lastEvaluatedKey LastKey
	var result *dynamodb.QueryOutput

	if len(filter) == 0 {
		expr, err := expression.NewBuilder().WithKeyCondition(keyCond).Build()
		if err != nil {
			return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
				ErrorMsg: aws.String(err.Error()),
			})
		}

		for {
			pp := new([]*be.BookingIndex)
			if lastEvaluatedKey == (LastKey{}) {
				result, err = db.Query(QueryFirstWithOutFilter(expr, tableName))
			} else {
				result, err = db.Query(QueryNextWithOutFilter(expr, tableName, lastEvaluatedKey))
			}

			if err != nil {
				fmt.Println(err)
			}

			err = dynamodbattribute.UnmarshalListOfMaps(result.Items, &pp)
			if err != nil {
				return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
					ErrorMsg: aws.String(errors.New(be.ErrorCouldNotUnMarshalItem).Error()),
				})
			}

			for _, i := range *pp {
				*final = append(*final, i)
			}

			if result.LastEvaluatedKey == nil {
				break
			}

			err = dynamodbattribute.UnmarshalMap(result.LastEvaluatedKey, &lastEvaluatedKey)
			if err != nil {
				return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
					ErrorMsg: aws.String(errors.New(be.ErrorCouldNotUnMarshalItem).Error()),
				})
			}
		}
	} else {
		expr, err := expression.NewBuilder().WithKeyCondition(keyCond).WithFilter(finalFilter).Build()
		if err != nil {
			return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
				ErrorMsg: aws.String(err.Error()),
			})
		}

		for {
			pp := new([]*be.BookingIndex)
			if lastEvaluatedKey == (LastKey{}) {
				result, err = db.Query(QueryFirstWithFilter(expr, tableName))
			} else {
				result, err = db.Query(QueryNextWithFilter(expr, tableName, lastEvaluatedKey))
			}

			if err != nil {
				fmt.Println(err)
			}

			err = dynamodbattribute.UnmarshalListOfMaps(result.Items, &pp)
			if err != nil {
				fmt.Println("err :", err)
				return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
					ErrorMsg: aws.String(errors.New(be.ErrorCouldNotUnMarshalItem).Error()),
				})
			}

			for _, i := range *pp {
				*final = append(*final, i)
			}

			if result.LastEvaluatedKey == nil {
				break
			}

			err = dynamodbattribute.UnmarshalMap(result.LastEvaluatedKey, &lastEvaluatedKey)
			if err != nil {
				return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
					ErrorMsg: aws.String(errors.New(be.ErrorCouldNotUnMarshalItem).Error()),
				})
			}
		}
	}

	// // GetItem from dynamodb table
	// result, err := db.Query(input)
	// if err != nil {
	// 	return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
	// 		ErrorMsg: aws.String(err.Error()),
	// 	})
	// }

	// item := new([]*be.Booking)
	// err = dynamodbattribute.UnmarshalListOfMaps(result.Items, &item)
	// if err != nil {
	// 	fmt.Println(err)
	// 	return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
	// 		ErrorMsg: aws.String(errors.New(be.ErrorCouldNotUnMarshalItem).Error()),
	// 	})
	// }

	sc, _ := GetSiteControllerUsed(hotel_id)
	fmt.Println("Site controller used : ", sc)

	if len(*final) > 0 {

		for i := 0; i < len(*final); i++ {

			tlStatus, err := GetTLBookingStatus(hotel_id, *&(*final)[i].BookingReferenceID)
			if err != nil {
				fmt.Println("GetTLBookingStatus err : ", err)
			}

			if sc == "TL-Lincoln" {
				if len(tlStatus.ErrorMessage) > 0 {
					*&(*final)[i].TLBookingSyncErrorMessage = tlStatus.ErrorMessage
					*&(*final)[i].TLBookingSyncStatus = "Error"
				} else {
					*&(*final)[i].TLBookingSyncStatus = "Success"
				}
			}

			// *&(*final)[i].CancellationPolicy.NoshowPanelty = *&(*final)[i].CancellationPolicy.NoshowPenalty
			*&(*final)[i].CancellationPolicy.NoshowPenalty = *&(*final)[i].CancellationPolicy.NoshowPanelty
			if len(*&(*final)[i].Adult) == 0 {
				*&(*final)[i].Adult = make([]int64, 0)
			}
			if len(*&(*final)[i].Child) == 0 {
				*&(*final)[i].Child = make([]int64, 0)
			}
			if len(*&(*final)[i].ChildAge) == 0 {
				*&(*final)[i].ChildAge = make([]int64, 0)
			}
			if len(*&(*final)[i].ChildInfo) == 0 {
				*&(*final)[i].ChildInfo = make([]be.ChildInfo, 0)
			}
			if len(*&(*final)[i].QuestionAnswers) == 0 {
				*&(*final)[i].QuestionAnswers = make([]be.QuestionAnswer, 0)
			}
			if len(*&(*final)[i].Meal) == 0 {
				*&(*final)[i].Meal = make([]be.Meal, 0)
			}
			if len(*&(*final)[i].ConfirmationEmailSendHistory) == 0 {
				*&(*final)[i].ConfirmationEmailSendHistory = make([]string, 0)
			}
			// if *&(*final)[i].PageLanguage == "en" {
			// 	*&(*final)[i].GuestNameKana = *&(*final)[i].GuestName
			// }
			if len(*&(*final)[i].PlanGroupDiscountList) == 0 {
				*&(*final)[i].PlanGroupDiscountList = make([]be.PlanGroupDiscount, 0)
			}
			if len(*&(*final)[i].OptionList) == 0 {
				*&(*final)[i].OptionList = make([]be.OptionList, 0)
			}
		}

		return be.ApiResponse(org, http.StatusOK, final)
	} else {
		item := new([]*be.BookingIndex)
		err := dynamodbattribute.UnmarshalListOfMaps(result.Items, &item)
		if err != nil {
			return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
				ErrorMsg: aws.String(errors.New(be.ErrorCouldNotUnMarshalItem).Error()),
			})
		}
		return be.ApiResponse(org, http.StatusOK, item)
	}
}

func main() {
	lambda.Start(handler)
}

func GetSiteControllerUsed(hotelID string) (string, error) {

	// tableName := "be_hotel_default_settings-stg"
	tableName := os.Getenv("HDS_TABLE")
	sc := ""
	// DynamoDB
	db := be.Dynamodb()

	// input for GetItem
	input := &dynamodb.GetItemInput{
		Key: map[string]*dynamodb.AttributeValue{
			"hotel_id": {
				S: aws.String(hotelID),
			},
		},
		TableName: aws.String(tableName),
	}

	// GetItem from dynamodb table
	result, err := db.GetItem(input)
	if err != nil {
		return sc, err
	}

	item := new(be.HotelDefaultSettings)
	err = dynamodbattribute.UnmarshalMap(result.Item, item)
	if err != nil {
		return sc, err
	}

	return item.SiteController.Name.En, nil

}

func GetTLBookingStatus(hotelID string, bookingID string) (BookingIDs, error) {

	// tableName := "be_tl_booking_id_maps-stg"
	tableName := os.Getenv("BOOKING_MAP_TABLE")
	item := BookingIDs{}
	// DynamoDB
	db := be.Dynamodb()

	// input for GetItem
	input := &dynamodb.GetItemInput{
		Key: map[string]*dynamodb.AttributeValue{
			"hotel_id": {
				S: aws.String(hotelID),
			},
			"booking_reference_id": {
				S: aws.String(bookingID),
			},
		},
		TableName: aws.String(tableName),
	}

	// GetItem from dynamodb table
	result, err := db.GetItem(input)
	if err != nil {
		return item, err
	}

	err = dynamodbattribute.UnmarshalMap(result.Item, item)
	if err != nil {
		return item, err
	}

	return item, nil

}
