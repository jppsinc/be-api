package main

import (
	"encoding/json"
	"errors"
	"net/http"
	"os"

	be "bitbucket.org/jppsinc/be-modules/pkg"

	valid "github.com/asaskevich/govalidator"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/aws/aws-sdk-go/service/dynamodb/expression"
)

func handler(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {

	org := request.Headers["origin"]
	// Table name
	// tableName := "be_bookings"
	tableName := os.Getenv("TABLE_NAME")

	// Change JSON request data to struct : just to check validity of request data
	var sds be.Booking

	if err := json.Unmarshal([]byte(request.Body), &sds); err != nil {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorCouldNotUnMarshalItem).Error()),
		})
	}

	// Request path parameter
	id, ok := request.PathParameters["id"]
	if ok == false || valid.IsUUIDv4(id) == false {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorInvalidUrlIdNotFound).Error()),
		})
	}

	hotel_id, ok := request.PathParameters["hotel_id"]
	if ok == false || valid.IsUUIDv4(hotel_id) == false {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorInvalidUrlHotelIdNotFound).Error()),
		})
	} else {
		sds.HotelID = hotel_id
		sds.ID = id
		sds.UpdatedAt = be.DateTimeStamp()
	}

	// DynamoDB
	db := be.Dynamodb()

	//create expr builder
	filt := expression.Name("booking_reference_id").Equal(expression.Value(sds.BookingReferenceID))
	expr, err := expression.NewBuilder().WithFilter(filt).Build()
	if err != nil {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorExpressionBuilder).Error()),
		})
	}

	// check if row with this key exist
	// input for GetItem
	input := &dynamodb.GetItemInput{
		Key: map[string]*dynamodb.AttributeValue{
			"hotel_id": {
				S: aws.String(hotel_id),
			},
			"id": {
				S: aws.String(id),
			},
		},
		TableName: aws.String(tableName),
	}

	// GetItem from dynamodb table
	result, err := db.GetItem(input)
	if err != nil {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorFailedToFetchRecord).Error()),
		})
	}

	if len(result.Item) > 0 {
		// change struct data to json data, this data will be stored to database
		av, err := dynamodbattribute.MarshalMap(sds)
		if err != nil {
			return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
				ErrorMsg: aws.String(errors.New(be.ErrorCouldNotMarshalItem).Error()),
			})
		}

		if len(sds.PlanPriceIDList) == 0 {
			empty := []*dynamodb.AttributeValue{}
			av["plan_price_id_list"] = &dynamodb.AttributeValue{L: empty}
		}
		if len(sds.QuestionAnswers) == 0 {
			empty := []*dynamodb.AttributeValue{}
			av["question_answers"] = &dynamodb.AttributeValue{L: empty}
		}
		if len(sds.QuestionsForCustomer) == 0 {
			empty := []*dynamodb.AttributeValue{}
			av["questions_for_customer"] = &dynamodb.AttributeValue{L: empty}
		}
		if len(sds.ConfirmationEmailSendHistory) == 0 {
			empty := []*dynamodb.AttributeValue{}
			av["confirmation_email_send_history"] = &dynamodb.AttributeValue{L: empty}
		}
		if len(sds.Meal) == 0 {
			empty := []*dynamodb.AttributeValue{}
			av["meal"] = &dynamodb.AttributeValue{L: empty}
		}
		if len(sds.ChildInfo) == 0 {
			empty := []*dynamodb.AttributeValue{}
			av["child_info"] = &dynamodb.AttributeValue{L: empty}
		}
		if len(sds.ChildAServiceTaxList) == 0 {
			empty := []*dynamodb.AttributeValue{}
			av["child_A_service_tax_list"] = &dynamodb.AttributeValue{L: empty}
		}
		if len(sds.ChildBServiceTaxList) == 0 {
			empty := []*dynamodb.AttributeValue{}
			av["child_B_service_tax_list"] = &dynamodb.AttributeValue{L: empty}
		}
		if len(sds.ChildCServiceTaxList) == 0 {
			empty := []*dynamodb.AttributeValue{}
			av["child_C_service_tax_list"] = &dynamodb.AttributeValue{L: empty}
		}
		if len(sds.ChildDServiceTaxList) == 0 {
			empty := []*dynamodb.AttributeValue{}
			av["child_D_service_tax_list"] = &dynamodb.AttributeValue{L: empty}
		}
		if len(sds.ChildEServiceTaxList) == 0 {
			empty := []*dynamodb.AttributeValue{}
			av["child_E_service_tax_list"] = &dynamodb.AttributeValue{L: empty}
		}
		if len(sds.ChildFServiceTaxList) == 0 {
			empty := []*dynamodb.AttributeValue{}
			av["child_F_service_tax_list"] = &dynamodb.AttributeValue{L: empty}
		}
		if len(sds.ChildAConsumptionTaxList) == 0 {
			empty := []*dynamodb.AttributeValue{}
			av["child_A_consumption_tax_list"] = &dynamodb.AttributeValue{L: empty}
		}
		if len(sds.ChildBConsumptionTaxList) == 0 {
			empty := []*dynamodb.AttributeValue{}
			av["child_B_consumption_tax_list"] = &dynamodb.AttributeValue{L: empty}
		}
		if len(sds.ChildCConsumptionTaxList) == 0 {
			empty := []*dynamodb.AttributeValue{}
			av["child_C_consumption_tax_list"] = &dynamodb.AttributeValue{L: empty}
		}
		if len(sds.ChildDConsumptionTaxList) == 0 {
			empty := []*dynamodb.AttributeValue{}
			av["child_D_consumption_tax_list"] = &dynamodb.AttributeValue{L: empty}
		}
		if len(sds.ChildEConsumptionTaxList) == 0 {
			empty := []*dynamodb.AttributeValue{}
			av["child_E_consumption_tax_list"] = &dynamodb.AttributeValue{L: empty}
		}
		if len(sds.ChildFConsumptionTaxList) == 0 {
			empty := []*dynamodb.AttributeValue{}
			av["child_F_consumption_tax_list"] = &dynamodb.AttributeValue{L: empty}
		}
		if len(sds.RemindEmailSendHistory) == 0 {
			empty := []*dynamodb.AttributeValue{}
			av["remind_email_send_history"] = &dynamodb.AttributeValue{L: empty}
		}
		if len(sds.WelcomeEmailSendTime) == 0 {
			av["welcome_email_send_time"] = &dynamodb.AttributeValue{S: aws.String("")}
		}
		if len(sds.ThankYouEmailSendTime) == 0 {
			av["thankyou_email_send_time"] = &dynamodb.AttributeValue{S: aws.String("")}
		}
		if len(sds.OptionList) == 0 {
			empty := []*dynamodb.AttributeValue{}
			av["option_list"] = &dynamodb.AttributeValue{L: empty}
		}
		if len(sds.PlanGroupDiscountList) == 0 {
			empty := []*dynamodb.AttributeValue{}
			av["plan_group_discount_list"] = &dynamodb.AttributeValue{L: empty}
		}

		input := &dynamodb.PutItemInput{
			Item:                      av,
			TableName:                 aws.String(tableName),
			ConditionExpression:       expr.Filter(),
			ExpressionAttributeNames:  expr.Names(),
			ExpressionAttributeValues: expr.Values(),
		}

		_, err = db.PutItem(input)
		if err != nil {
			return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
				ErrorMsg: aws.String(errors.New(be.ErrorCouldNotDynamoPutItem).Error()),
			})
		}

		// incress update_count by 1
		inputInc := &dynamodb.UpdateItemInput{
			Key: map[string]*dynamodb.AttributeValue{
				"hotel_id": {
					S: aws.String(hotel_id),
				},
				"id": {
					S: aws.String(id),
				},
			},
			ExpressionAttributeValues: map[string]*dynamodb.AttributeValue{
				":val": {
					N: aws.String("1"),
				},
			},
			TableName:        aws.String(tableName),
			UpdateExpression: aws.String("SET update_count = update_count + :val"),
			ReturnValues:     aws.String("UPDATED_NEW"),
		}

		_, err = db.UpdateItem(inputInc)
		if err != nil {
			return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
				ErrorMsg: aws.String(errors.New(be.ErrorCouldNotDynamoPutItem).Error()),
			})
		}

		return be.ApiResponse(org, http.StatusOK, &sds)
	} else {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorRecordNotFound).Error()),
		})
	}

}

func main() {
	lambda.Start(handler)
}
