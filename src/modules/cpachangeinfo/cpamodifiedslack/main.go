package main

import (
	"errors"
	"fmt"
	"net/http"
	"os"
	"strings"

	be "bitbucket.org/jppsinc/be-modules/pkg"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
)

type Resp struct {
	Message string `json:"message"`
}

func handler(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {

	org := request.Headers["origin"]
	// Table name
	// tableName := "be_cpa"
	tableName := os.Getenv("TABLE_NAME")

	// Change JSON request data to struct : just to check validity of request data
	// var sds be.CPAChangeInfo
	sds := new(be.CPAChangeInfo)

	hotelID := ""

	// query string parameter
	if len(request.QueryStringParameters) == 0 {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorQueryStringParameterNotFound).Error()),
		})
	} else {
		hotelID, _ = request.QueryStringParameters["hotelId"]
		if len(strings.TrimSpace(hotelID)) == 0 {
			return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
				ErrorMsg: aws.String(errors.New("Error QueryStringParameter hotelID").Error()),
			})
		}
	}

	// DynamoDB
	db := be.Dynamodb()

	// input for GetItem
	input := &dynamodb.GetItemInput{
		Key: map[string]*dynamodb.AttributeValue{
			"hotel_id": {
				S: aws.String(hotelID),
			},
		},
		TableName: aws.String(tableName),
	}

	// GetItem from dynamodb table
	result, err := db.GetItem(input)
	if err != nil {
		fmt.Println("err 1: ", err)
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorFailedToFetchRecord).Error()),
		})
	}

	if len(result.Item) > 0 {
		// sds := new(be.CPAChangeInfo)
		err = dynamodbattribute.UnmarshalMap(result.Item, sds)
		if err != nil {
			return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
				ErrorMsg: aws.String(errors.New(be.ErrorCouldNotUnMarshalItem).Error()),
			})
		}

		if sds.CPAChangeRequestCompleted == true {
			r := Resp{"CPA 変更リクエストはすでに完了しています。"}
			return be.ApiResponse(org, http.StatusOK, r)
		}

		var l be.CPAChangeLog

		l.CompleteDate = be.DateString("datetime")
		l.NewCPA = sds.GoogleHotelAdsCPARequested
		l.OldCPA = sds.GoogleHotelAdsCPA
		l.RequestDate = sds.CPAChangeRequestDate

		sds.CPAChangeLog = append(sds.CPAChangeLog, l)
		sds.UpdatedAt = be.DateTimeStamp()
		sds.HotelID = hotelID
		sds.GoogleHotelAdsCPA = sds.GoogleHotelAdsCPARequested
		sds.GoogleHotelAdsCPARequested = 0.0
		sds.CPAChangeRequestCompleted = true

		// change struct data to json data, this data will be stored to database
		av, err := dynamodbattribute.MarshalMap(sds)
		if err != nil {
			return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
				ErrorMsg: aws.String(errors.New(be.ErrorCouldNotMarshalItem).Error()),
			})
		}

		ReturnItem := "SIZE"
		input := &dynamodb.PutItemInput{
			Item:                        av,
			TableName:                   aws.String(tableName),
			ReturnItemCollectionMetrics: &ReturnItem,
		}

		_, err = db.PutItem(input)
		if err != nil {
			return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
				ErrorMsg: aws.String(errors.New(be.ErrorCouldNotDynamoPutItem).Error()),
			})
		}
		r := Resp{"CPA 変更リクエストが完了しました。"}
		return be.ApiResponse(org, http.StatusOK, r)
	} else {
		item := new(be.EmptyStruct)
		return be.ApiResponse(org, http.StatusOK, item)
	}
}

func main() {
	lambda.Start(handler)
}
