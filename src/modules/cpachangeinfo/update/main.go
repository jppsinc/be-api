package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"os"

	be "bitbucket.org/jppsinc/be-modules/pkg"

	valid "github.com/asaskevich/govalidator"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
)

type CPAChangeInfoUser struct {
	HotelID                    string         `json:"hotel_id"`
	GoogleHotelAdsCPA          float64        `json:"google_hotel_ads_cpa"`
	GoogleHotelAdsCPARequested float64        `json:"google_hotel_ads_cpa_requested"`
	CPAChangeRequestDate       string         `json:"cpa_change_request_date"`
	CPAChangeRequestCompleted  bool           `json:"cpa_change_request_completed"`
	CPAChangeLog               []CPAChangeLog `json:"cpa_change_log"`
	CreatedAt                  int64          `json:"created_at"`
	UpdatedAt                  int64          `json:"updated_at"`
}

type CPAChangeLog struct {
	RequestDate  string  `json:"request_date"`
	CompleteDate string  `json:"complete_date"`
	OldCPA       float64 `json:"old_cpa"`
	NewCPA       float64 `json:"new_cpa"`
}

func handler(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {

	org := request.Headers["origin"]
	// Table name
	// tableName := "be_cpa"
	tableName := os.Getenv("TABLE_NAME")

	// Change JSON request data to struct : just to check validity of request data
	var sds be.CPAChangeInfo

	if err := json.Unmarshal([]byte(request.Body), &sds); err != nil {
		fmt.Println(err)
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorCouldNotUnMarshalItem).Error()),
		})
	}

	// Request path parameter
	hotel_id, ok := request.PathParameters["hotel_id"]
	if ok == false || valid.IsUUIDv4(hotel_id) == false {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorInvalidUrlHotelIdNotFound).Error()),
		})
	} else {
		sds.HotelID = hotel_id
		sds.CPAChangeRequestCompleted = false
		sds.CPAChangeRequestDate = be.DateString("datetime")
		sds.UpdatedAt = be.DateTimeStamp()
	}

	// DynamoDB
	db := be.Dynamodb()

	// change struct data to json data, this data will be stored to database
	av, err := dynamodbattribute.MarshalMap(sds)
	if err != nil {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorCouldNotMarshalItem).Error()),
		})
	}

	ReturnItem := "SIZE"
	input := &dynamodb.PutItemInput{
		Item:                        av,
		TableName:                   aws.String(tableName),
		ReturnItemCollectionMetrics: &ReturnItem,
	}

	_, err = db.PutItem(input)
	if err != nil {
		fmt.Println(err)
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorCouldNotDynamoPutItem).Error()),
		})
	}
	return be.ApiResponse(org, http.StatusOK, &sds)
}

func main() {
	lambda.Start(handler)
}
