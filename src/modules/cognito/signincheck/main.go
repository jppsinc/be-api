package main

import (
	"encoding/json"
	"errors"
	"net/http"
	"os"

	be "bitbucket.org/jppsinc/be-modules/pkg"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	cognito "github.com/aws/aws-sdk-go/service/cognitoidentityprovider"
)

type User struct {
	AccessToken string `json:"access_token"`
}
type Resp struct {
	IsValid bool `json:"is_valid"`
}

func handler(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {

	org := request.Headers["origin"]

	var sds User

	if err := json.Unmarshal([]byte(request.Body), &sds); err != nil {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorCouldNotUnMarshalItem).Error()),
		})
	}

	region := os.Getenv("AWS_REGION")
	svc := cognito.New(session.New(), &aws.Config{Region: aws.String(region)})

	user := &cognito.GetUserInput{
		AccessToken: aws.String(sds.AccessToken),
	}

	_, err := svc.GetUser(user)
	var r Resp
	if err != nil {
		r.IsValid = false
		return be.ApiResponse(org, http.StatusBadRequest, r)
	} else {
		r.IsValid = true
		return be.ApiResponse(org, http.StatusOK, r)
	}
}

func main() {
	lambda.Start(handler)
}
