package main

import (
	"errors"
	"net/http"
	"os"

	be "bitbucket.org/jppsinc/be-modules/pkg"
	valid "github.com/asaskevich/govalidator"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	cognito "github.com/aws/aws-sdk-go/service/cognitoidentityprovider"
)

var APP_CLIENT_ID string = "41gm9gmja9idfju0epetk4bt7m"
var USER_POOL_ID string = "ap-northeast-1_GlJHBQ2fg"

func handler(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {

	org := request.Headers["origin"]
	var USER_POOL_ID string = os.Getenv("UserPool")

	// Request path parameter
	sub, ok := request.PathParameters["sub"]
	if ok == false || valid.IsUUIDv4(sub) == false {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorInvalidUrlHotelIdNotFound).Error()),
		})
	}

	region := os.Getenv("AWS_REGION")
	svc := cognito.New(session.New(), &aws.Config{Region: aws.String(region)})

	filter := "sub = \"" + sub + "\""
	user := &cognito.ListUsersInput{
		UserPoolId: aws.String(USER_POOL_ID),
		Filter:     aws.String(filter),
	}

	data, err := svc.ListUsers(user)
	if err != nil {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(err.Error()),
		})
	}

	return be.ApiResponse(org, http.StatusOK, data)
}

func main() {
	lambda.Start(handler)
}
