package main

import (
	"encoding/json"
	"errors"
	"net/http"
	"os"

	be "bitbucket.org/jppsinc/be-modules/pkg"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	cognito "github.com/aws/aws-sdk-go/service/cognitoidentityprovider"
)

type User struct {
	RefreshToken string `json:"refresh_token"`
}

func handler(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {

	org := request.Headers["origin"]
	var APP_CLIENT_ID string = os.Getenv("UserPoolClient")
	var sds User

	if err := json.Unmarshal([]byte(request.Body), &sds); err != nil {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorCouldNotUnMarshalItem).Error()),
		})
	}

	region := os.Getenv("AWS_REGION")
	svc := cognito.New(session.New(), &aws.Config{Region: aws.String(region)})

	user := &cognito.InitiateAuthInput{
		ClientId: aws.String(APP_CLIENT_ID),
		AuthFlow: aws.String("REFRESH_TOKEN_AUTH"),
		AuthParameters: map[string]*string{
			"REFRESH_TOKEN": aws.String(sds.RefreshToken),
		},
	}

	data, err := svc.InitiateAuth(user)
	if err != nil {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(err.Error()),
		})
	}

	return be.ApiResponse(org, http.StatusOK, data)
}

func main() {
	lambda.Start(handler)
}
