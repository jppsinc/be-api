package main

import (
	"errors"
	"fmt"
	"os"
	"time"

	be "bitbucket.org/jppsinc/be-modules/pkg"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/aws/aws-sdk-go/service/dynamodb/expression"
	"github.com/guregu/dynamo"
)

type UserStruct struct {
	HotelID      string `json:"hotel_id"`
	UserSub      string `json:"user_sub"`
	UserName     string `json:"user_name"`
	FacilityCode string `json:"facility_code"`
	Special      string `json:"special"`
}

var (
	portalSiteTable     dynamo.Table
	portalSiteUserTable dynamo.Table
)

func init() {
	be.SetLocalTime()
	sess := session.Must(session.NewSession())
	db := dynamo.New(sess, &aws.Config{Region: aws.String(os.Getenv("AWS_REGION"))})

	portalSite := os.Getenv("PORTAL_SITE")
	portalSiteUser := os.Getenv("PORTAL_SITE_USER")

	portalSiteTable = db.Table(portalSite)
	portalSiteUserTable = db.Table(portalSiteUser)

}

func main() {
	lambda.Start(handler)
}

func handler(event events.CognitoEventUserPoolsPreAuthentication) (events.CognitoEventUserPoolsPreAuthentication, error) {
	// Table name
	// tableName := "be_users"
	tableName := os.Getenv("TABLE_NAME")

	facilityCode := event.Request.UserAttributes["custom:facility_code"]
	InputFacilityCode := event.Request.ValidationData["facility_code"]
	email := event.Request.UserAttributes["email"]
	// special := event.Request.UserAttributes["special"]
	InputEmail := event.Request.ValidationData["email"]
	special := event.Request.ValidationData["special"]

	if InputFacilityCode == "portal" {
		err := login(email, special)
		if err != nil {
			return event, err
		}
		return event, nil
	} else {
		if facilityCode == InputFacilityCode && email == InputEmail {

			// we need hotel id to update users table, scan all the records match
			// the excat one and get hotel id and update last login time
			// DynamoDB
			db := be.Dynamodb()

			filt := expression.Name("facility_code").Equal(expression.Value(facilityCode))
			expr, err := expression.NewBuilder().WithFilter(filt).Build()
			if err != nil {
				return event, err
			}

			/// input for Scan
			input := &dynamodb.ScanInput{
				TableName:                 aws.String(tableName),
				FilterExpression:          expr.Filter(),
				ExpressionAttributeNames:  expr.Names(),
				ExpressionAttributeValues: expr.Values(),
				IndexName:                 aws.String("facility-code-index"),
			}

			fmt.Println("2 input :", input)

			// GetItem from dynamodb table
			result, err := db.Scan(input)
			item := new([]*UserStruct)

			if err == nil {

				err = dynamodbattribute.UnmarshalListOfMaps(result.Items, &item)
				if err != nil {
					return event, err
				} else {
					for _, t := range *item {
						if t.UserName == email && t.FacilityCode == facilityCode {
							if special == "false" {
								fmt.Printf("4. t.UserName : %v, email : %v, t.FacilityCode : %v, facilityCode : %v\n", t.UserName, email, t.FacilityCode, facilityCode)
								// if facilityCode & UserName matches, update the last_login

								ll := be.DateString("datetime")
								update := expression.Set(expression.Name("last_login"), expression.Value(ll))
								expr, err := expression.NewBuilder().WithUpdate(update).Build()
								if err != nil {
									return event, fmt.Errorf("last login update fail")
								}

								// DynamoDB
								db := be.Dynamodb()

								// input for GetItem
								updateInput := &dynamodb.UpdateItemInput{
									Key: map[string]*dynamodb.AttributeValue{"hotel_id": {
										S: aws.String(t.HotelID),
									}},
									TableName:                 aws.String(tableName),
									ExpressionAttributeNames:  expr.Names(),
									ExpressionAttributeValues: expr.Values(),
									UpdateExpression:          expr.Update(),
								}

								fmt.Println("updateInput :", updateInput)

								_, err = db.UpdateItem(updateInput)
								if err != nil {
									fmt.Println("err last : ", err)
									return event, fmt.Errorf("last login update fail")
								}

								return event, nil
							} else {
								fmt.Println("Its a special login, no need to enter last login time")
								return event, nil
							}
						}
					}
					fmt.Println("5 No match ")
					return event, fmt.Errorf("Incorrect facility code")
				}

			} else {
				return event, err
			}

		} else {
			return event, fmt.Errorf("Incorrect facility code")
		}
	}

}

func login(email, special string) error {
	if special == "true" {
		return nil
	}

	var portalSiteUsers []be.PortalSiteUser
	err := portalSiteUserTable.Scan().Filter("user_name = ?", email).All(&portalSiteUsers)
	if err != nil {
		return err
	}

	if len(portalSiteUsers) == 0 {
		return errors.New("登録されていないメールアドレスです。")
	}

	if len(portalSiteUsers) > 1 {
		return errors.New("取得件数が不正です。")
	}

	ok, err := checkContract(portalSiteUsers[0].PortalSiteID)
	if err != nil {
		return err
	}
	if !ok {
		return errors.New("契約期間外のアカウントのため、システムをご利用できません")
	}

	err = portalSiteTable.Update("id", portalSiteUsers[0].PortalSiteID).
		Set("last_login", time.Now().Format(be.DATE_TIME_FORMAT)).
		Run()

	if err != nil {
		return err
	}

	return nil

}

func checkContract(portalSiteID string) (bool, error) {
	var portalSite be.PortalSite
	err := portalSiteTable.Get("id", portalSiteID).One(&portalSite)
	if err != nil {
		return false, err
	}

	today, err := time.Parse(be.DATE_FORMAT, time.Now().Local().Format(be.DATE_FORMAT))
	if err != nil {
		return false, err
	}

	if portalSite.ContractScheduledEndDate != "" {
		contractEndDate, err := time.Parse(be.DATE_FORMAT, portalSite.ContractScheduledEndDate)
		if err != nil {
			return false, err
		}

		if today.After(contractEndDate) {
			return false, nil
		}
	}

	return true, nil
}
