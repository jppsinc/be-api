package main

import (
	"errors"
	"fmt"
	"os"
	"time"

	be "bitbucket.org/jppsinc/be-modules/pkg"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/guregu/dynamo"
)

var (
	userTable           dynamo.Table
	portalSiteUserTable dynamo.Table
	db                  *dynamo.DB
)

func init() {
	be.SetLocalTime()

	user := os.Getenv("USER")
	portalSiteUser := os.Getenv("PORTAL_SITE_USER")

	sess := session.Must(session.NewSession())
	db = dynamo.New(sess, &aws.Config{Region: aws.String(os.Getenv("AWS_REGION"))})

	userTable = db.Table(user)
	portalSiteUserTable = db.Table(portalSiteUser)
}

func main() {
	lambda.Start(handler)
}

func handler(event events.CognitoEventUserPoolsPreSignup) (events.CognitoEventUserPoolsPreSignup, error) {
	facilityCode := event.Request.UserAttributes["custom:facility_code"]
	hotelID := event.Request.UserAttributes["custom:hotel_id"]
	portalSiteID := event.Request.UserAttributes["custom:portal_site_id"]
	email := event.Request.UserAttributes["email"]
	password := event.Request.ClientMetadata["password"]

	if hotelID != "" {
		err := createUser(hotelID, facilityCode, email, event)
		if err != nil {
			fmt.Println(err.Error())
			return event, err
		}
	} else if portalSiteID != "" {
		err := createPortalSiteUser(portalSiteID, email, password, event)
		if err != nil {
			fmt.Println(err.Error())
			return event, err
		}

	}

	event.Response.AutoConfirmUser = true
	event.Response.AutoVerifyEmail = true
	return event, nil
}

func createUser(hotelID, facilityCode, email string, event events.CognitoEventUserPoolsPreSignup) error {
	err := checkDupulicate(email)
	if err != nil {
		return err
	}

	user := be.User{}
	user.HotelID = hotelID
	user.FacilityCode = facilityCode
	user.UserSub = event.UserName
	user.UserName = email

	err = userTable.Put(user).Run()
	if err != nil {
		return err
	}

	return nil
}

func createPortalSiteUser(portalSiteID, email, password string, event events.CognitoEventUserPoolsPreSignup) error {
	err := checkDupulicate(email)
	if err != nil {
		return err
	}

	user := be.PortalSiteUser{}
	user.PortalSiteID = portalSiteID
	user.Password = password
	user.UserSub = event.UserName
	user.UserName = email
	user.CreatedAt = time.Now().Local().Format(be.DATE_TIME_FORMAT)
	user.UpdatedAt = time.Now().Local().Format(be.DATE_TIME_FORMAT)

	err = portalSiteUserTable.Put(user).Run()
	if err != nil {
		return err
	}

	return nil
}

func checkDupulicate(email string) error {

	ok, err := checkResisterdUser(email)
	if err != nil {
		return err
	}

	if !ok {
		return errors.New("ホテルのアカウントで使用されているので、入力されたメールアドレスで登録できませんでした。")
	}

	ok, err = checkResisterdPortalSiteUser(email)
	if err != nil {
		return err
	}

	if !ok {
		return errors.New("ポータルのアカウントで使用されているので、入力されたメールアドレスで登録できませんでした。")
	}

	return nil
}

func checkResisterdUser(email string) (bool, error) {
	var users []be.User
	err := userTable.Scan().Filter("user_name = ?", email).All(&users)
	if err != nil {
		return false, err
	}

	if len(users) > 0 {
		return false, nil
	}

	return true, nil
}

func checkResisterdPortalSiteUser(email string) (bool, error) {
	var portalSiteUsers []be.PortalSiteUser
	err := portalSiteUserTable.Scan().Filter("user_name = ?", email).All(&portalSiteUsers)
	if err != nil {
		return false, err
	}

	if len(portalSiteUsers) > 0 {
		return false, nil
	}

	return true, nil
}
