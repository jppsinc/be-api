package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"os"

	be "bitbucket.org/jppsinc/be-modules/pkg"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	cognito "github.com/aws/aws-sdk-go/service/cognitoidentityprovider"
)

type User struct {
	FacilityCode string `json:"facility_code"`
	UserName     string `json:"user_name"`
}
type Resp struct {
	IsValid bool `json:"is_valid"`
}

func handler(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {

	org := request.Headers["origin"]
	var APP_CLIENT_ID string = os.Getenv("UserPoolClient")
	var sds User

	if err := json.Unmarshal([]byte(request.Body), &sds); err != nil {
		fmt.Println("Unmarshal error : ", err.Error())
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorCouldNotUnMarshalItem).Error()),
		})
	}

	region := os.Getenv("AWS_REGION")
	svc := cognito.New(session.New(), &aws.Config{Region: aws.String(region)})

	user := &cognito.ForgotPasswordInput{
		Username: aws.String(sds.UserName),
		ClientId: aws.String(APP_CLIENT_ID),
	}

	fmt.Println("ForgotPasswordInput : ", user)

	data, err := svc.ForgotPassword(user)
	if err != nil {
		fmt.Println("ForgotPassword error : ", err.Error())
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(err.Error()),
		})
	} else {
		fmt.Println("Finished : Success")
		return be.ApiResponse(org, http.StatusOK, data)
	}
}

func main() {
	lambda.Start(handler)
}
