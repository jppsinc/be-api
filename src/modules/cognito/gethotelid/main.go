package main

import (
	"errors"
	"net/http"
	"os"
	"strings"

	be "bitbucket.org/jppsinc/be-modules/pkg"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
)

type User struct {
	HotelID string `json:"hotel_id"`
}

func handler(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {

	org := request.Headers["origin"]
	// Table name
	// tableName := "be_users"
	tableName := os.Getenv("TABLE_NAME")

	var facilityCode string
	// query string parameter
	if len(request.QueryStringParameters) == 0 {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorQueryStringParameterNotFound).Error()),
		})
	} else {
		userName, ok := request.QueryStringParameters["userName"]
		if ok == false || len(strings.TrimSpace(userName)) == 0 {
			return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
				ErrorMsg: aws.String(errors.New(be.ErrorQueryStringParameterInvalid).Error()),
			})
		}

		facilityCode, ok = request.QueryStringParameters["facilityCode"]
		if ok == false || len(strings.TrimSpace(facilityCode)) == 0 {
			return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
				ErrorMsg: aws.String(errors.New(be.ErrorQueryStringParameterInvalid).Error()),
			})
		}
	}

	// DynamoDB
	db := be.Dynamodb()
	// input for GetItem
	input := &dynamodb.QueryInput{
		ExpressionAttributeValues: map[string]*dynamodb.AttributeValue{
			":facility_code": {
				S: aws.String(facilityCode),
			},
		},
		KeyConditionExpression: aws.String("facility_code = :facility_code"),
		TableName:              aws.String(tableName),
		IndexName:              aws.String("facility-code-index"),
	}

	// GetItem from dynamodb table
	result, err := db.Query(input)
	if err != nil {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorFailedToFetchRecord).Error()),
		})
	}

	if len(result.Items) > 0 {
		item := new([]*User)
		err = dynamodbattribute.UnmarshalListOfMaps(result.Items, &item)
		if err != nil {
			return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
				ErrorMsg: aws.String(errors.New(be.ErrorCouldNotUnMarshalItem).Error()),
			})
		}
		return be.ApiResponse(org, http.StatusOK, *(*item)[0])
	} else {
		item := new(be.EmptyStruct)
		return be.ApiResponse(org, http.StatusOK, item)
	}
}

func main() {
	lambda.Start(handler)
}
