package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"os"

	be "bitbucket.org/jppsinc/be-modules/pkg"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	cognito "github.com/aws/aws-sdk-go/service/cognitoidentityprovider"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/expression"
)

type User struct {
	FacilityCode     string `json:"facility_code"`
	UserName         string `json:"user_name"`
	ConfirmationCode string `json:"confirmation_code"`
	Password         string `json:"password"`
}

func handler(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {

	// Table name
	// tableName := "be_users"
	tableName := os.Getenv("TABLE_NAME")

	org := request.Headers["origin"]
	var APP_CLIENT_ID string = os.Getenv("UserPoolClient")
	var sds User

	if err := json.Unmarshal([]byte(request.Body), &sds); err != nil {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorCouldNotUnMarshalItem).Error()),
		})
	}

	region := os.Getenv("AWS_REGION")
	svc := cognito.New(session.New(), &aws.Config{Region: aws.String(region)})

	user := &cognito.ConfirmForgotPasswordInput{
		Username:         aws.String(sds.UserName),
		ClientId:         aws.String(APP_CLIENT_ID),
		ConfirmationCode: aws.String(sds.ConfirmationCode),
		Password:         aws.String(sds.Password),
	}

	data, err := svc.ConfirmForgotPassword(user)
	if err != nil {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(err.Error()),
		})
	} else {
		// DynamoDB
		db := be.Dynamodb()
		// get hotel id from facility code
		filter := expression.Name("facility_code").Equal(expression.Value(sds.FacilityCode))
		expr, err := expression.NewBuilder().WithFilter(filter).Build()
		if err != nil {
			return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
				ErrorMsg: aws.String(err.Error()),
			})
		}

		// input for GetItem
		input := &dynamodb.ScanInput{
			TableName:                 aws.String(tableName),
			FilterExpression:          expr.Filter(),
			ExpressionAttributeNames:  expr.Names(),
			ExpressionAttributeValues: expr.Values(),
			IndexName:                 aws.String("facility-code-index"),
		}

		fmt.Println("ScanInput :", input)

		// GetItem from dynamodb table

		result, err := db.Scan(input)
		if err != nil {
			return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
				ErrorMsg: aws.String(err.Error()),
			})
		}

		fmt.Println("Scan result :", result)

		hotelID := result.Items[0]["hotel_id"].S

		// if signup successful update user table with password
		cond := expression.Name("hotel_id").Equal(expression.Value(*hotelID))
		update := expression.Set(expression.Name("password"), expression.Value(sds.Password))
		expr1, err := expression.NewBuilder().WithCondition(cond).WithUpdate(update).Build()
		if err != nil {
			return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
				ErrorMsg: aws.String(err.Error()),
			})
		}

		// input for GetItem
		updateInput := &dynamodb.UpdateItemInput{
			Key: map[string]*dynamodb.AttributeValue{"hotel_id": {
				S: aws.String(*hotelID),
			}},
			TableName:                 aws.String(tableName),
			ConditionExpression:       expr1.Condition(),
			ExpressionAttributeNames:  expr1.Names(),
			ExpressionAttributeValues: expr1.Values(),
			UpdateExpression:          expr1.Update(),
		}

		fmt.Println("updateInput :", updateInput)

		_, err = db.UpdateItem(updateInput)
		if err != nil {
			return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
				ErrorMsg: aws.String(err.Error()),
			})
		}
		return be.ApiResponse(org, http.StatusOK, data)
	}
}

func main() {
	lambda.Start(handler)
}
