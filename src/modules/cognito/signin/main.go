package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"os"

	be "bitbucket.org/jppsinc/be-modules/pkg"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	cognito "github.com/aws/aws-sdk-go/service/cognitoidentityprovider"
)

type User struct {
	UserName     string `json:"user_name"`
	Password     string `json:"password"`
	FacilityCode string `json:"facility_code"`
	Special      string `json:"special"`
}

func handler(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {

	org := request.Headers["origin"]
	var APP_CLIENT_ID string = os.Getenv("UserPoolClient")
	var sds User

	if err := json.Unmarshal([]byte(request.Body), &sds); err != nil {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorCouldNotUnMarshalItem).Error()),
		})
	}

	region := os.Getenv("AWS_REGION")
	svc := cognito.New(session.New(), &aws.Config{Region: aws.String(region)})

	user := &cognito.InitiateAuthInput{
		ClientId: aws.String(APP_CLIENT_ID),
		AuthFlow: aws.String("USER_PASSWORD_AUTH"),
		AuthParameters: map[string]*string{
			"USERNAME": aws.String(sds.UserName),
			"PASSWORD": aws.String(sds.Password),
		},
		ClientMetadata: map[string]*string{
			"user_name":     aws.String(sds.UserName),
			"password":      aws.String(sds.Password),
			"facility_code": aws.String(sds.FacilityCode),
			"special":       aws.String(sds.Special),
		},
	}

	fmt.Println("1 facilityCode :", sds.FacilityCode)
	fmt.Println("1 sds.UserName :", sds.UserName)
	fmt.Println("1 sds.Password :", sds.Password)
	fmt.Println("1 sds.Special :", sds.Special)

	data, err := svc.InitiateAuth(user)
	if err != nil {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(err.Error()),
		})
	}

	return be.ApiResponse(org, http.StatusOK, data)
}

func main() {
	lambda.Start(handler)
}
