package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"os"

	be "bitbucket.org/jppsinc/be-modules/pkg"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	cognito "github.com/aws/aws-sdk-go/service/cognitoidentityprovider"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/expression"
)

type User struct {
	UserName     string `json:"user_name"`
	Password     string `json:"password"`
	HotelID      string `json:"hotel_id"`
	FacilityCode string `json:"facility_code"`
}

func handler(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {

	org := request.Headers["origin"]

	// Table name
	// tableName := "be_users"
	tableName := os.Getenv("TABLE_NAME")

	var USER_POOL_ID string = os.Getenv("UserPool")
	var sds User

	if err := json.Unmarshal([]byte(request.Body), &sds); err != nil {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorCouldNotUnMarshalItem).Error()),
		})
	}

	region := os.Getenv("AWS_REGION")
	svc := cognito.New(session.New(), &aws.Config{Region: aws.String(region)})

	user := &cognito.AdminSetUserPasswordInput{
		Username:   aws.String(sds.UserName),
		Password:   aws.String(sds.Password),
		UserPoolId: aws.String(USER_POOL_ID),
		Permanent:  aws.Bool(true),
	}

	data, err := svc.AdminSetUserPassword(user)
	if err != nil {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(err.Error()),
		})
	} else {
		// if signup successful update user table with password
		cond := expression.Name("hotel_id").Equal(expression.Value(sds.HotelID))
		update := expression.Set(expression.Name("password"), expression.Value(sds.Password))
		expr, err := expression.NewBuilder().WithCondition(cond).WithUpdate(update).Build()
		if err != nil {
			return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
				ErrorMsg: aws.String(err.Error()),
			})
		}

		// DynamoDB
		db := be.Dynamodb()

		// input for GetItem
		updateInput := &dynamodb.UpdateItemInput{
			Key: map[string]*dynamodb.AttributeValue{"hotel_id": {
				S: aws.String(sds.HotelID),
			}},
			TableName:                 aws.String(tableName),
			ConditionExpression:       expr.Condition(),
			ExpressionAttributeNames:  expr.Names(),
			ExpressionAttributeValues: expr.Values(),
			UpdateExpression:          expr.Update(),
		}

		fmt.Println("updateInput :", updateInput)

		_, err = db.UpdateItem(updateInput)
		if err != nil {
			return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
				ErrorMsg: aws.String(err.Error()),
			})
		}
	}

	return be.ApiResponse(org, http.StatusOK, data)
}

func main() {
	lambda.Start(handler)
}
