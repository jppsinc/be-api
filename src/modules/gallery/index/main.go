package main

import (
	"errors"
	"fmt"
	"net/http"
	"os"

	be "bitbucket.org/jppsinc/be-modules/pkg"
	valid "github.com/asaskevich/govalidator"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/aws/aws-sdk-go/service/dynamodb/expression"
)

type ImgCount struct {
	ID       string `json:"id"`
	HotelID  string `json:"hotel_id"`
	UseCount int64  `json:"use_count"`
}

type Gallery struct {
	ID                 string   `json:"id"`
	HotelID            string   `json:"hotel_id"`
	Type               string   `json:"type"`
	URL                string   `json:"url"`
	Alt                string   `json:"alt"`
	Desc               string   `json:"desc"`
	IsHide             bool     `json:"is_hide"`
	ShareCount         int64    `json:"share_count"`
	SharedIdList       []string `json:"shared_id_list"`
	SharedWithCategory []string `json:"shared_with_category"`
	UseCount           int64    `json:"use_count"`
	CreatedAt          int64    `json:"created_at"`
	UpdatedAt          int64    `json:"updated_at"`
}

func handler(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {

	org := request.Headers["origin"]

	// Table name
	// tableName := "be_galleries-prod"
	tableName := os.Getenv("TABLE_NAME")

	// Request path parameter
	hotel_id, ok := request.PathParameters["hotel_id"]
	if ok == false || valid.IsUUIDv4(hotel_id) == false {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorInvalidUrlHotelIdNotFound).Error()),
		})
	}

	// DynamoDB
	db := be.Dynamodb()

	// input for GetItem
	input := &dynamodb.QueryInput{
		ExpressionAttributeValues: map[string]*dynamodb.AttributeValue{
			":hotel_id": {
				S: aws.String(hotel_id),
			},
		},
		KeyConditionExpression: aws.String("hotel_id = :hotel_id"),
		TableName:              aws.String(tableName),
	}

	// GetItem from dynamodb table
	result, err := db.Query(input)
	if err != nil {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorFailedToFetchRecord).Error()),
		})
	}

	item := new([]*Gallery)
	err = dynamodbattribute.UnmarshalListOfMaps(result.Items, &item)
	if err != nil {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorCouldNotUnMarshalItem).Error()),
		})
	}
	if len(*item) > 0 {
		// filter blank URL
		img := new([]*Gallery)
		for _, im := range *item {
			if len(im.URL) != 0 {
				*img = append(*img, im)
			}
		}

		// get img count from img count table in form of map
		imgCount, _ := ImageCount(hotel_id, db)

		// re assign img count
		for i := 0; i < len(*item); i++ {
			if val, ok := imgCount[(*item)[i].ID]; ok {
				fmt.Printf("Use count fount id : %v, use count : %v\n", (*item)[i].ID, val)
				(*item)[i].UseCount = val
			}
		}

		return be.ApiResponse(org, http.StatusOK, img)
	} else {
		return be.ApiResponse(org, http.StatusOK, item)
	}
}

func main() {
	lambda.Start(handler)
}

func ImageCount(hotelID string, db *dynamodb.DynamoDB) (map[string]int64, error) {
	// cTableName := "be_galleries_usecount-prod"
	cTableName := os.Getenv("GAL_CNT_TABLE_NAME")
	item := new([]*ImgCount)
	imgMap := make(map[string]int64, 0)

	keyCond := expression.Key("hotel_id").Equal(expression.Value(hotelID))
	expr, err := expression.NewBuilder().WithKeyCondition(keyCond).Build()
	if err != nil {
		return imgMap, err
	}

	input := &dynamodb.QueryInput{
		KeyConditionExpression:    expr.KeyCondition(),
		TableName:                 aws.String(cTableName),
		ExpressionAttributeNames:  expr.Names(),
		ExpressionAttributeValues: expr.Values(),
	}

	// GetItem from dynamodb table
	result, err := db.Query(input)
	if err != nil {
		return imgMap, err
	}

	err = dynamodbattribute.UnmarshalListOfMaps(result.Items, &item)
	if err != nil {
		return imgMap, err
	}

	for _, img := range *item {
		imgMap[img.ID] = img.UseCount
	}

	return imgMap, nil
}
