package main

import (
	"errors"
	"fmt"
	"net/http"
	"os"

	be "bitbucket.org/jppsinc/be-modules/pkg"
	valid "github.com/asaskevich/govalidator"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/aws/aws-sdk-go/service/s3"
)

type Gallery struct {
	ID                 string   `json:"id"`
	HotelID            string   `json:"hotel_id"`
	Type               string   `json:"type"`
	URL                string   `json:"url"`
	Alt                string   `json:"alt"`
	Desc               string   `json:"desc"`
	IsHide             bool     `json:"is_hide"`
	ShareCount         int64    `json:"share_count"`
	SharedIdList       []string `json:"shared_id_list"`
	SharedWithCategory []string `json:"shared_with_category"`
	UseCount           int64    `json:"use_count"`
	CreatedAt          int64    `json:"created_at"`
	UpdatedAt          int64    `json:"updated_at"`
}

func DeleteImageFromS3(S3 *s3.S3, bucketName string, url string) (*s3.DeleteObjectOutput, error) {
	out, err := S3.DeleteObject(&s3.DeleteObjectInput{
		Bucket: aws.String(bucketName),
		Key:    aws.String(url),
	})

	fmt.Println("bucketName :", bucketName)
	fmt.Println("url :", url)

	return out, err
}

func handler(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {

	org := request.Headers["origin"]
	// Table name
	// tableName := "be_galleries-prod"
	tableName := os.Getenv("TABLE_NAME")

	// bucketName := "be-image-gallery-prod"
	bucketName := os.Getenv("GALLERY_NAME")

	// Request path parameter
	hotel_id, ok := request.PathParameters["hotel_id"]
	if ok == false || valid.IsUUIDv4(hotel_id) == false {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorInvalidUrlHotelIdNotFound).Error()),
		})
	}
	id, ok := request.PathParameters["id"]
	if ok == false || valid.IsUUIDv4(id) == false {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorInvalidUrlIdNotFound).Error()),
		})
	}

	// DynamoDB
	db := be.Dynamodb()

	// input for GetItem
	input := &dynamodb.GetItemInput{
		Key: map[string]*dynamodb.AttributeValue{
			"hotel_id": {
				S: aws.String(hotel_id),
			},
			"id": {
				S: aws.String(id),
			},
		},
		TableName: aws.String(tableName),
	}

	// GetItem from dynamodb table
	result, err := db.GetItem(input)
	if err != nil {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorFailedToFetchRecord).Error()),
		})
	}

	if len(result.Item) > 0 {
		item := new(Gallery)
		err = dynamodbattribute.UnmarshalMap(result.Item, item)
		if err != nil {
			return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
				ErrorMsg: aws.String(errors.New(be.ErrorCouldNotUnMarshalItem).Error()),
			})
		}

		fmt.Println("item : ", *item)

		// if this image is not shared by any other type then remove it from s3
		if len(item.SharedWithCategory) == 1 {
			url := item.URL
			S3, _ := be.S3()
			_, err := DeleteImageFromS3(S3, bucketName, url)
			if err != nil {
				return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
					ErrorMsg: aws.String(err.Error()),
				})
			}

			fmt.Println("image deleted form s3: ", url)
		}

		// input for GetItem
		input := &dynamodb.DeleteItemInput{
			Key: map[string]*dynamodb.AttributeValue{
				"hotel_id": {
					S: aws.String(hotel_id),
				},
				"id": {
					S: aws.String(id),
				},
			},
			TableName: aws.String(tableName),
		}

		// GetItem from dynamodb table
		_, err = db.DeleteItem(input)
		if err != nil {
			return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
				ErrorMsg: aws.String(errors.New("Can't delete: Image in use.").Error()),
			})
		}

		fmt.Println("Successfully deleted image id : ", item.ID)
		return be.ApiResponse(org, http.StatusOK, item)

	} else {
		fmt.Println("Requested id not found")
		item := new(be.EmptyStruct)
		return be.ApiResponse(org, http.StatusOK, item)
	}

}

func main() {
	lambda.Start(handler)
}
