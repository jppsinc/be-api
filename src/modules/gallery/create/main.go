package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"os"
	"time"

	be "bitbucket.org/jppsinc/be-modules/pkg"
	valid "github.com/asaskevich/govalidator"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/google/uuid"
)

type Gallery struct {
	ID                 string   `json:"id"`
	HotelID            string   `json:"hotel_id"`
	Type               string   `json:"type"`
	URL                string   `json:"url"`
	Alt                string   `json:"alt"`
	Desc               string   `json:"desc"`
	IsHide             bool     `json:"is_hide"`
	ShareCount         int64    `json:"share_count"`
	SharedIdList       []string `json:"shared_id_list"`
	SharedWithCategory []string `json:"shared_with_category"`
	UseCount           int64    `json:"use_count"`
	CreatedAt          int64    `json:"created_at"`
	UpdatedAt          int64    `json:"updated_at"`
}

func handler(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {

	org := request.Headers["origin"]

	// Table name
	// tableName := "be_galleries"
	tableName := os.Getenv("TABLE_NAME")
	bucketName := os.Getenv("GALLERY_NAME")

	fmt.Println("bucketName : ", bucketName)

	// Change JSON request data to struct : just to check validity of request data
	var sds be.Gallery
	var gal Gallery

	if err := json.Unmarshal([]byte(request.Body), &sds); err != nil {
		fmt.Println("Gal err 1 : ", err.Error())
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(err.Error()),
		})
	}

	// Request path parameter
	hotel_id, ok := request.PathParameters["hotel_id"]
	if !ok || !valid.IsUUIDv4(hotel_id) {
		fmt.Println("Gal err 2 : ", be.ErrorInvalidUrlHotelIdNotFound)
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorInvalidUrlHotelIdNotFound).Error()),
		})
	} else {
		sds.ID = fmt.Sprintf("%s", uuid.New())
		sds.HotelID = hotel_id
		sds.CreatedAt = be.DateTimeStamp()
		sds.UpdatedAt = be.DateTimeStamp()
	}

	fmt.Println("hotel_id : ", hotel_id)

	// filter "" from SharedWithCategory
	temp := make([]string, 0)
	for _, c := range sds.SharedWithCategory {
		if c != "" {
			temp = append(temp, c)
		}
	}
	sds.SharedWithCategory = temp

	if len(sds.SharedWithCategory) > 0 {
		start := time.Now()
		S3, sess := be.S3()
		_, err := be.CreateBucket(S3, bucketName)
		if err != nil {
			if _, ok := err.(awserr.Error); !ok {
				fmt.Println("Gal err 3 : ", err.Error())
				return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
					ErrorMsg: aws.String(err.Error()),
				})
			}
		}

		path := sds.HotelID + "/" + sds.Type + "/" + sds.ID

		resc, err := be.UploadImageToS3(sess, bucketName, sds.ImageName, sds.Image, path)
		if err != nil {
			fmt.Println("Gal err 4 : ", err.Error())
			return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
				ErrorMsg: aws.String(err.Error()),
			})
		}

		duration := time.Since(start)
		fmt.Printf("\nS3 file upload time : %v\n", duration)

		start = time.Now()

		sharedId := fmt.Sprintf("%s", uuid.New())
		if len(sds.SharedWithCategory) != 0 {
			for _, imgType := range sds.SharedWithCategory {
				if imgType != "" {
					sds.URL = resc.Location
					fmt.Println("multy options : ", imgType)
					gal.CreatedAt = sds.CreatedAt
					gal.UpdatedAt = sds.UpdatedAt
					gal.URL = sds.URL
					gal.HotelID = sds.HotelID
					gal.ID = fmt.Sprintf("%s", uuid.New())
					gal.Alt = sds.Alt
					gal.Type = imgType
					gal.IsHide = sds.IsHide
					gal.UseCount = 0
					gal.Desc = sds.Desc
					gal.ShareCount = int64(len(sds.SharedWithCategory) - 1)
					gal.SharedWithCategory = sds.SharedWithCategory
					gal.SharedIdList = append(gal.SharedIdList, sharedId)
					// this shared it will help to easily find out shared images

					// DynamoDB
					db := be.Dynamodb()

					// change struct data to json data, this data will be stored to database
					av, err := dynamodbattribute.MarshalMap(gal)
					if err != nil {
						fmt.Println("Gal err 5 : ", err.Error())
						return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
							ErrorMsg: aws.String(err.Error()),
						})
					}

					ReturnItem := "SIZE"
					input := &dynamodb.PutItemInput{
						Item:                        av,
						TableName:                   aws.String(tableName),
						ReturnItemCollectionMetrics: &ReturnItem,
					}

					_, err = db.PutItem(input)
					if err != nil {
						fmt.Println("Gal err 6 : ", err.Error())
						return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
							ErrorMsg: aws.String(errors.New(be.ErrorCouldNotDynamoPutItem).Error()),
						})
					}
				}
			}
		} else {
			sds.URL = resc.Location
			fmt.Println("single option : ", sds.Type)
			gal.CreatedAt = sds.CreatedAt
			gal.UpdatedAt = sds.UpdatedAt
			gal.URL = sds.URL
			gal.HotelID = sds.HotelID
			gal.ID = sds.ID
			gal.Alt = sds.Alt
			gal.Type = sds.Type
			gal.IsHide = sds.IsHide
			gal.UseCount = 0
			gal.Desc = sds.Desc
			gal.ShareCount = 0
			gal.SharedWithCategory = append(gal.SharedWithCategory, sds.Type)

			// DynamoDB
			db := be.Dynamodb()

			// change struct data to json data, this data will be stored to database
			av, err := dynamodbattribute.MarshalMap(gal)
			if err != nil {
				fmt.Println("Gal err 7 : ", err.Error())
				return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
					ErrorMsg: aws.String(err.Error()),
				})
			}

			ReturnItem := "SIZE"
			input := &dynamodb.PutItemInput{
				Item:                        av,
				TableName:                   aws.String(tableName),
				ReturnItemCollectionMetrics: &ReturnItem,
			}

			_, err = db.PutItem(input)
			if err != nil {
				fmt.Println("Gal err 8 : ", err.Error())
				return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
					ErrorMsg: aws.String(errors.New(be.ErrorCouldNotDynamoPutItem).Error()),
				})
			}
		}

		duration = time.Since(start)
		fmt.Printf("\nDB update time : %v\n", duration)
	}

	fmt.Println("gal : ", gal)
	fmt.Println("Image succesfully uploded")

	return be.ApiResponse(org, http.StatusCreated, &gal)
}

func main() {
	lambda.Start(handler)
}
