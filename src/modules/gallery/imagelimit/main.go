package main

import (
	"errors"
	"fmt"
	"math"
	"net/http"
	"os"
	"strconv"

	be "bitbucket.org/jppsinc/be-modules/pkg"
	valid "github.com/asaskevich/govalidator"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
)

type LimitResponse struct {
	ImageCount int `json:"image_count"`
	Gap        int `json:"gap"`
}

func handler(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {

	org := request.Headers["origin"]

	// Table name
	// tableName := "be_galleries"
	tableName := os.Getenv("TABLE_NAME")
	LIMIT := os.Getenv("IMAGE_COUNT")

	// Request path parameter
	hotel_id, ok := request.PathParameters["hotel_id"]
	if ok == false || valid.IsUUIDv4(hotel_id) == false {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorInvalidUrlHotelIdNotFound).Error()),
		})
	}

	// DynamoDB
	db := be.Dynamodb()

	// input for GetItem
	input := &dynamodb.QueryInput{
		ExpressionAttributeValues: map[string]*dynamodb.AttributeValue{
			":hotel_id": {
				S: aws.String(hotel_id),
			},
		},
		KeyConditionExpression: aws.String("hotel_id = :hotel_id"),
		TableName:              aws.String(tableName),
	}

	// GetItem from dynamodb table
	result, err := db.Query(input)
	if err != nil {
		fmt.Println("1: ", err)
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorFailedToFetchRecord).Error()),
		})
	}

	item := new([]*be.Gallery)
	err = dynamodbattribute.UnmarshalListOfMaps(result.Items, &item)
	if err != nil {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorCouldNotUnMarshalItem).Error()),
		})
	}

	image := make(map[string]string)

	for _, img := range *item {
		if len(img.URL) != 0 {
			image[img.URL] = img.ID
		}
	}

	resp := new(LimitResponse)
	resp.ImageCount = len(image)

	// if image count is less than 90 limit is 100
	// if count > 90 <= 100 : limit 110
	// if count > 100 <= 110 : limit 120
	// if count > 110 <= 120 : limit 130
	// hard limit is 300 if reaches 300, image upload not allowed
	// still want to upload more, call use, we will increase it
	if resp.ImageCount <= 90 {
		resp.Gap = 100 - resp.ImageCount
	} else {
		softLimit := int(math.Ceil(float64(resp.ImageCount)/10))*10 + 10
		hardLimit, _ := strconv.Atoi(LIMIT)
		if softLimit <= hardLimit{
			resp.Gap = softLimit - resp.ImageCount
		} else {
			resp.Gap = 0
		}
		
	}

	return be.ApiResponse(org, http.StatusOK, resp)
}

func main() {
	lambda.Start(handler)
}
