package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"os"

	be "bitbucket.org/jppsinc/be-modules/pkg"

	valid "github.com/asaskevich/govalidator"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/google/uuid"
)

type ShareGallery struct {
	ShareWith string `json:"shareWith"`
}

type Gallery struct {
	ID                 string   `json:"id"`
	HotelID            string   `json:"hotel_id"`
	Type               string   `json:"type"`
	URL                string   `json:"url"`
	Alt                string   `json:"alt"`
	Desc               string   `json:"desc"`
	IsHide             bool     `json:"is_hide"`
	ShareCount         int64    `json:"share_count"`
	SharedIdList       []string `json:"shared_id_list"`
	SharedWithCategory []string `json:"shared_with_category"`
	UseCount           int64    `json:"use_count"`
	CreatedAt          int64    `json:"created_at"`
	UpdatedAt          int64    `json:"updated_at"`
}

func handler(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {

	org := request.Headers["origin"]
	// Table name
	// tableName := "be_galleries"
	tableName := os.Getenv("TABLE_NAME")
	// bucketName := os.Getenv("GALLERY_NAME")

	// Change JSON request data to struct : just to check validity of request data
	var sds ShareGallery

	if err := json.Unmarshal([]byte(request.Body), &sds); err != nil {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorCouldNotUnMarshalItem).Error()),
		})
	}

	// Request path parameter
	id, ok := request.PathParameters["id"]
	if ok == false || valid.IsUUIDv4(id) == false {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorInvalidUrlIdNotFound).Error()),
		})
	}

	hotel_id, ok := request.PathParameters["hotel_id"]
	if ok == false || valid.IsUUIDv4(hotel_id) == false {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorInvalidUrlHotelIdNotFound).Error()),
		})
	}
	// DynamoDB
	db := be.Dynamodb()

	// input for GetItem
	input := &dynamodb.GetItemInput{
		Key: map[string]*dynamodb.AttributeValue{
			"hotel_id": {
				S: aws.String(hotel_id),
			},
			"id": {
				S: aws.String(id),
			},
		},
		TableName: aws.String(tableName),
	}

	// GetItem from dynamodb table
	result, err := db.GetItem(input)
	if err != nil {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorFailedToFetchRecord).Error()),
		})
	}

	item := new(Gallery)
	if len(result.Item) > 0 {
		err = dynamodbattribute.UnmarshalMap(result.Item, item)
		if err != nil {
			return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
				ErrorMsg: aws.String(errors.New(be.ErrorCouldNotUnMarshalItem).Error()),
			})
		}
	} else {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New("Invalid Image ID").Error()),
		})
	}

	// update current image [shared with and sahred id fields]

	sid := fmt.Sprintf("%s", uuid.New())
	item.SharedWithCategory = append(item.SharedWithCategory, sds.ShareWith)
	item.SharedIdList = append(item.SharedIdList, sid)
	item.ShareCount = item.ShareCount + 1

	// change struct data to json data, this data will be stored to database
	av, err := dynamodbattribute.MarshalMap(item)
	if err != nil {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorCouldNotMarshalItem).Error()),
		})
	}

	putInput := &dynamodb.PutItemInput{
		Item:      av,
		TableName: aws.String(tableName),
	}

	_, err = db.PutItem(putInput)
	if err != nil {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorCouldNotDynamoPutItem).Error()),
		})
	}

	fmt.Printf("Updated current image : id : %v", item.ID)

	// insert new image entry
	item.Type = sds.ShareWith
	item.ID = fmt.Sprintf("%s", uuid.New())

	// make them empty and then append
	item.SharedWithCategory = nil
	item.SharedIdList = nil

	// ShareCount is 1
	item.ShareCount = 1

	item.SharedWithCategory = append(item.SharedWithCategory, sds.ShareWith)
	item.SharedWithCategory = append(item.SharedWithCategory, item.Type)
	item.SharedIdList = append(item.SharedIdList, sid)

	item.CreatedAt = be.DateTimeStamp()
	item.UpdatedAt = be.DateTimeStamp()

	av1, err := dynamodbattribute.MarshalMap(item)
	if err != nil {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorCouldNotMarshalItem).Error()),
		})
	}

	putInput1 := &dynamodb.PutItemInput{
		Item:      av1,
		TableName: aws.String(tableName),
	}

	_, err = db.PutItem(putInput1)
	if err != nil {
		return be.ApiResponse(org, http.StatusBadRequest, be.ErrorBody{
			ErrorMsg: aws.String(errors.New(be.ErrorCouldNotDynamoPutItem).Error()),
		})
	}

	fmt.Printf("Created new image : id : %v", item.ID)

	return be.ApiResponse(org, http.StatusOK, item)
}

func main() {
	lambda.Start(handler)
}
